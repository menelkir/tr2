TOMB RAIDER 3

--------------------
// PC & PSX VERSION OF GENERAL GAME STRINGS

GAME_STRINGS:

Inventar
Optionen
Gegenst~ande
Spiel zuende

Spiel laden
Spiel sichern
Neues Spiel
Abschnitt neu beginnen
Zur~uck zum Titelbild
Demo verlassen
Spiel verlassen

Abschnitt w~ahlen
Spielstand sichern

Aufl~osung w~ahlen
Hoch
Mittel
Niedrig

Gehen
Rollen
Laufen
Links
Rechts
Zur~uck
Schritt nach links
?
Schritt nach rechts
?
Umsehen
Springen
Handlung
Waffe ziehen
?
Inventar
Ducken
Spurten

Statistik
Pistolen
Gewehr
Desert Eagle
Uzis
Harpune
MP5
Raketenwerfer
Granatwerfer
Fackel

Pistolen Magazine
Gewehrpatronen
Desert Eagle Magazine
Uzi Magazine
Harpunen
MP5 Magazine
Raketen
Granaten

Kleines Medi Pack
Grosses Medi Pack
Extra
R~atsel
Schl~ussel
Spiel

Lara's Anwesen
Lade...


Ben~otigte Zeit
Gefundene Secrets
Schauplatz
Siege
Verbrauchte Munition
Treffer
Speichervorg~ange
Gereiste Entfernung
Benutzte Medi Packs

Version 1.0

Null
Ende
Bestzeiten
Keine vorgegebenen Zeiten
--
Derzeitige Position
Abschlussstatistik
von
Was bisher geschah...
Infada Stein
Element 115
Das Auge der Isis
Ora Dolch

Speicherkristall
London
Nevada
S~udseeinseln
Antarktis
Peru
Abenteuer
s

END:



PC_STRINGS:

Detailstufen
Demo-Modus
Sound
Steuerung
Helligkeit
Lautst~arke regeln
Spielertasten

Datei konnte nicht gesichert werden!
Erneut versuchen?
Ja
Nein
Spiel gesichert!
Keine Spielst~ande!
Keine g~ultigen
Spiel sichern?
- FREIER SPEICHERPLATZ -



Aus
Ein
Soundkarte einrichten
Vorgegebene Tasten
DOZY

frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei
frei

END:


PSX_STRINGS:

Bildschirmanpassung
Demo Modus
Sound
Steuerung
Helligkeit
Lautst~arke regeln
Tastenbelegung
Datei konnte nicht gesichert werden!
Erneut versuchen?
Ja
Nein
Spiel gesichert!
Keine Spielst~ande!
Kein g~ultiger
Spiel sichern?
- FREIER SPEICHERPLATZ -
Mit beliebiger Taste verlassen
Bewegen Sie das Spielfenster
mit den Richtungstasten
> W~ahlen
; Zur~uck
> Weiter
Pause
Controller entfernt
Spiel sichern auf
Spiel ~uberschreiben auf
Spiel laden von
Memory Card in
Memory Card Steckplatz 1
Sind Sie sicher?
Ja
Nein
ist voll
Es gibt keine Spielst~ande von
Tomb Raider III auf der
Es gibt ein Problem mit der
Sie haben keine Memory Card in
Ist unformatiert
M~ochten Sie
sie formatieren?
Sichere Spiel auf
Lade Spielstand von
Formatiere ...
Spiel ~uberschreiben
Spiel sichern
Spiel laden
Memory Card formatieren
Verlassen
Weiterspielen
Sie k~onnen Ihre Spielst~ande
nur sichern, wenn Sie eine
formatierte Memory Card mit
mindestens zwei freien
Bl~ocken einlegen
Die Memory Card in
Verlassen, ohne zu sichern
Verlassen, ohne zu laden
Spiel beginnen
Unformatierte Memory Card
Legen Sie eine formatierte Memory
Card ein oder dr~ucken Sie >, um
fortzufahren, ohne zu sichern.

// PAL CODES: ENGLISH= SLES-00718, FRENCH= SLES-0719, GERMAN= SLES-0720
bu00::BESLES-01683TOMB3
// NTSC CODES: US= UNKNOWN, JAPAN= UNKNOWN
bu00::BASLUS-00691TOMB3

Erfolgreich geladen
Konnte nicht laden
Erfolgreich gesichert
Konnte nicht sichern
Erfolgreich formatiert
Konnte nicht formatieren
Vibrationen Aus
Vibrationen Ein
Vibrationen ein- oder ausschalten
frei
frei
frei
frei
frei
frei
Abenteuer w~ahlen


