#include "standard.h"
#include "global.h"
#include "drawprimitive.h"
#include "scanlines.h"


/*
	Function : ScanLinesBlack
	Useage	 : Black Polys 
*/

void ScanLinesBlack(GRADIENT* Gradients,EDGE* pLeft,EDGE* pRight,EDGE* StepLeft,EDGE* StepRight)
{
	int Width;
	unsigned short* pDestination;
	long yline;
	short subdivcnt;

	yline = long(Output)+YTable[pLeft->y];
	
	while(StepLeft->height--)
	{
		Width  = pRight->x;
		Width -= pLeft->x;
		Width >>= 16;

		if(Width > 0)
		{
			pDestination = (unsigned short*) yline + (pLeft->x>>16);
			
			subdivcnt = Width>>4;
			Width = Width-(subdivcnt<<4);

			while(subdivcnt--)
			{
				*(pDestination+0) = 0;
				*(pDestination+1) = 0;
				*(pDestination+2) = 0;
				*(pDestination+3) = 0;
				*(pDestination+4) = 0;
				*(pDestination+5) = 0;
				*(pDestination+6) = 0;
				*(pDestination+7) = 0;
				*(pDestination+8) = 0;
				*(pDestination+9) = 0;
				*(pDestination+10) = 0;
				*(pDestination+11) = 0;
				*(pDestination+12) = 0;
				*(pDestination+13) = 0;
				*(pDestination+14) = 0;
				*(pDestination+15) = 0;

				pDestination += 16;
			}

			switch(Width)
			{
				case 15:
				*(pDestination++) = 0;
				case 14:
				*(pDestination++) = 0;
				case 13:
				*(pDestination++) = 0;
				case 12:
				*(pDestination++) = 0;
				case 11:
				*(pDestination++) = 0;
				case 10:
				*(pDestination++) = 0;
				case 9:
				*(pDestination++) = 0;
				case 8:
				*(pDestination++) = 0;
				case 7:
				*(pDestination++) = 0;
				case 6:
				*(pDestination++) = 0;
				case 5:
				*(pDestination++) = 0;
				case 4:
				*(pDestination++) = 0;
				case 3:
				*(pDestination++) = 0;
				case 2:
				*(pDestination++) = 0;
				case 1:
				*(pDestination++) = 0;
				case 0:
				*(pDestination++) = 0;
			}
			
			/*
			
			for(int n=0;n<=Width;n++)
			{
				*(pDestination++) = 0;
			}

			*/
	  	}

		yline += bytepitch;

		StepLeft->x    += StepLeft->xstep;
		StepLeft->y++;

		StepRight->x    += StepRight->xstep;
		StepRight->y++;
	}
}


// EOF : ScanLinesBlack
