#ifndef _LITESRC_H_
#define _LITESRC_H_

#ifdef __cplusplus
extern "C"{
#endif



void S_CalculateLight(sint32 x, sint32 y, sint32 z, sint16 room_number, ITEM_LIGHT *itemlight);
sint16	*calc_vertice_light( sint16 *objptr,sint16* );
void S_CalculateStaticLight  (sint16 adder);

extern sint32 LightPos[12];
extern sint32 LightCol[12];
extern PHD_VECTOR LPos[3];

extern int smcr,smcg,smcb;


#ifdef __cplusplus
}
#endif


#endif