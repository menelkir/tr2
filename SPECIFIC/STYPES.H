/*********************************************************************************************/
/*                                                                                           */
/* Machine Specific Type Definitions                                                         */
/*                                                                                           */
/*********************************************************************************************/

#ifndef STYPES_H
#define STYPES_H

#include "..\3dsystem\3dglodef.h"

#ifdef __cplusplus
extern "C" {
#endif

/********************************* TYPE DEFINITIONS ******************************************/

#define PC_VERSION

/*** log types for game/specific stuff ***/
#define LT_Default			(char*)0
#define LT_Enter			(char*)1
#define LT_Exit				(char*)2
#define LT_Release			(char*)3
#define LT_Info				(char*)4
#define LT_WindowMessage	(char*)5
#define LT_DXTry			(char*)6
#define LT_Error			(char*)7
#define LT_Warning			(char*)8
#define LT_Version			(char*)9
#define LT_Texture			(char*)10	// crappy temporary texture loading messages
#define LT_Sound			(char*)11	// sample info

/* Maximum number of frame compensation */
#define MAX_FRAMES 10

#define MALLOC_SIZE		(1024*1024 * 7/2)		// 3.5 MEGS

#define S_MemSet(A,B,C) 		memset(A,B,C)
#define S_MemCpy(A,B,C) 		memcpy(A,B,C)
#define S_LongMemCpy(A,B,C)		memcpy(A,B,C*4)

typedef struct {
	short sc_width, sc_height;
	short dump_x, dump_y, dump_width, dump_height;
	void *scrn_ptr;
} SETUP;

typedef struct pcx_header {
	char zsoft_flag, version, encoding,bits;
	unsigned short	xmin, ymin, xmax, ymax;
	unsigned short	h_res, v_res;
	char header_pal[48], reserved, planes;
	unsigned short	bytes_per_line, pal_interp, width, height;
	char blank[54];
} PCX_HEADER;

/***************************************************************************************/

typedef unsigned short ushort;
typedef unsigned char uchar;

#define mSqrt phd_sqrt


/***** defines *****/

#define LANGUAGE 0
//#define CD_LOAD

#define SECRET_BONUS
#define LOGFILE

#define GAMEDEBUG
//#define SCREEN_SHOT // allow screen shots to be taken

// want CD_LOAD and !GAMEDEBUG and !NOFMV
//#define DEMO     // alters behaviour for CD demo stuff
//#define NOFMV
//#define PCGAMER
//#define DUDE_DEMO

//#define USE_INVENTORY_TAG		// temporary!!! Shows text on title page
//#define NO_GYM


//#define CHEAT_ENABLE
//#define PLAY_ANY_LEVEL
#define ALWAYS_ENABLE_DOZY_CHEAT

// always required on PC; but defined so that PSX can try doing it too
#define FULL_SAVE
#ifdef GAMEDEBUG
	//#define ALWAYS_ENABLE_DOZY_CHEAT // Un-comment this to allow 'DOZY' cheat
	//#define SAVE_CINI
	//#define PLAY_ANY_LEVEL
	#define CHEAT_ENABLE
	//#define USE_PC_SAVE
	#define PLAY_CUTSCENES
	#define PLAY_FMV
	#define VERSION_EXPIRES
#else
	#define PLAY_FMV
	#define PLAY_CUTSCENES
	#define USE_PC_SAVE
   //#define NO_FLY_CHEAT		// Un-comment this to disable 'C' & 'D' key cheats. Even disables gameflow from enabling it! Over-ridden by defining ALWAYS_ENABLE_DOZY_CHEAT
#endif

// E3 demo; no GAMEDEBUG, disables CheatMode
//#define E3_DEMO
//#define AUTHORISATION_CHECK

//#define USE_CRASH_TELL
#ifdef USE_CRASH_TELL
#define	CRASH_TELL(f,l)	Log( LT_Info, "%s L:%d",f,l )
#else
#define CRASH_TELL(f,l)
#endif


#if defined(GAMEDEBUG)
typedef struct level_info {
	int hit_points;
	int health, health_used;
	int ammo, ammo_used;
} LEVEL_INFO;

extern LEVEL_INFO level;
#endif


//#define BIG_NUM     float ((1 << 26) * ((1 << 26) * 1.5))
//#define FTOL(x)     ((chop_temp = x + BIG_NUM), *(int*)(&chop_temp))
//#define FTOL1616(x) ((chop_temp = x + BIG_NUM/65536.0), *(int*)(&chop_temp))


#ifdef __cplusplus
}
#endif

#endif
