/*
	File	:	texture.h
	Author	:	Richard Flower (c) 1998 Core Design
*/




#ifndef	_TEXTURE_H
#define _TEXTURE_H


#define MAX_D3D_TEXTURES 32
#define MAX_D3D_PALETTES 16
#define TX_FLAG_SLOTUSED 1


struct DXTEXTURE
{	
	IDirectDrawSurface3* pSystemSurface;
	IDirectDrawSurface3* pDeviceSurface;
	IDirectDrawPalette*	 pPalette;
	IDirect3DTexture2*	 pTexture;
	D3DTEXTUREHANDLE	 hTexture;
	int					 nWidth;
	int					 nHeight;
	DWORD				 dwFlags;
	unsigned short*		 pSoftwareSurface;
};

extern  DXTEXTURE DXTextureList[MAX_D3D_TEXTURES];
extern  IDirectDrawPalette* DXPaletteList[MAX_D3D_PALETTES];

bool DXTextureInit(DXTEXTURE[],IDirectDrawPalette*[]);
int		DXTextureFindPaletteSlot();
int		DXTextureNewPalette(unsigned char* pPal);
void	DXTextureFreeAllPalettes();
void	DXTextureFreePalette(int nHandle);
DWORD	DXTextureGetFreeMemory();
IDirect3DTexture2* DXTextureGetInterface(IDirectDrawSurface3* pSurf);
int		DXTextureFindTextureSlot(DXTEXTURE []);
bool	DXTextureMakeSystemSurface(DXTEXTURE& rTexture);
bool	DXTextureMakeDeviceSurface(DXTEXTURE& rTexture);
void	DXTextureReleaseDeviceSurface(DXTEXTURE& rTexture);
int		DXTextureNew(int nWidth,int nHeight,IDirectDrawPalette* pPalette,DXTEXTURE[]);
void	DXTextureCleanup(int nSlot,DXTEXTURE[]);
void	DXTextureFreeAll(DXTEXTURE[]);
bool	DXTextureRestore(int nHandle,bool tForce,DXTEXTURE[]);
bool	DXTextureRestoreAll(bool tForce,DXTEXTURE[]);
DWORD	DXTextureGetHandle(int nHandle,DXTEXTURE[]);
int		DXTextureAddPal(int nWidth,int nHeight,unsigned char* pTexture,int nPaletteHandle,DXTEXTURE[]);
int		DXTextureAdd(int nWidth,int nHeight,unsigned short* pTexture,DXTEXTURE [],int);
void	DXTextureFinish(DXTEXTURE[]);
void	DXTextureFree(int nHandle,DXTEXTURE[]);
void	DXTextureSetGreyScale(bool);

#endif

