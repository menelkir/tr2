
#include "standard.h"
#include "winmain.h"
#include "console.h"
#include "input.h"
#include "hwrender.h"
#include "hwinsert.h"
#include "global.h"
#include "display.h"
#include "init.h"
#include "bgnd.h"
#include "output.h"
#include "drawprimitive.h"

#define GETR(rgb) ((rgb>>16)&0xff)
#define GETG(rgb) ((rgb>>8)&0xff)
#define GETB(rgb) ((rgb)&0xff)


/*
	sort    = sort3dptrbf;\
		info    = info3dptrbf;\
		surfacenumbf++;\
		sort3dptrbf += 2;\
	  info3dptrbf += 5;\


D3DTLVERTEX* pTLV;
	VERTEX_INFO* pVI;
	sint32* sort;
	sint16* info;

	pVI = v_buffer;
	
	if(App.lpZBuffer == 0 || nDrawType == DRAW_TLV_GTA || nDrawType == DRAW_TLV_WGT)
	{
		// Add Verts To Sort List
		
		SetBufferPtrs(sort,info);
	
		sort[0] = (sint32) info;
		sort[1] = (sint32) zdepth;
		info[0] = nDrawType;
		info[1] = nTPage;
		info[2] = nPoints;
		pTLV    = CurrentTLVertex;
	
		*((D3DTLVERTEX**)(info+3)) = pTLV;
		
	
		for (int nPoint=nPoints;nPoint--;)
		{
			pTLV->sx	= pVI->x;
			pTLV->sy	= pVI->y;
			pTLV->sz	= f_a-f_boo*pVI->ooz;
			pTLV->rhw	= pVI->ooz;
			pTLV->color = RGB_MAKE(pVI->vr,pVI->vg,pVI->vb);
		
			newz		= UV_MUL/pVI->ooz;
			
			pTLV->tu	= pVI->u*newz;
			pTLV->tv	= pVI->v*newz;
		
			pVI++;
			pTLV++;
		}
	
		CurrentTLVertex=pTLV;
	
*/

/*
	Function : HWR_SubdivPolyList
	Useage   : Subdivide Triangles
*/

void HWR_SubdivPolyList(int num,sint32* sortptr)
{
	sint32* CurrentSortPtr;
	sint16* CurrentInfoPtr;
	sint16* pInfo;
	D3DTLVERTEX* pV;
	int Routine,Tpage;
	int nV;
	D3DTLVERTEX* pTLV;


	for(;num > 0;num--)
	{
		pInfo    = (sint16*) (*sortptr);
		Routine  = *pInfo++;
		Tpage    = pInfo[0];
		nV		 = pInfo[1];
		pV       = *((D3DTLVERTEX**)(pInfo+2));

		if(Routine != DRAW_TLV_L)
		{
			if(nV == 3)
			{
				float xm,ym,zm;
				float um,vm,rhwm;
				long rm,gm,bm;
				double fconv;
				xm = pV[0].sx + ((pV[2].sx - pV[0].sx)*0.5f);
				ym = pV[0].sy + ((pV[2].sy - pV[0].sy)*0.5f);
				zm = pV[0].sz + ((pV[2].sz - pV[0].sz)*0.5f);
				um = pV[0].tu + ((pV[2].tu - pV[0].tu)*0.5f);
				vm = pV[0].tv + ((pV[2].tv - pV[0].tv)*0.5f);
				rhwm = pV[0].rhw + ((pV[2].rhw - pV[0].rhw)*0.5f);

				rm = GETR(pV[0].color) + ((GETR(pV[2].color)-GETR(pV[0].color)>>1));
				gm = GETG(pV[0].color) + ((GETG(pV[2].color)-GETG(pV[0].color)>>1));
				bm = GETB(pV[0].color) + ((GETB(pV[2].color)-GETB(pV[0].color)>>1));

				// Current Ptrs

				CurrentSortPtr = sort3dptrbf;
				CurrentInfoPtr = info3dptrbf;

				float nZDepth;

				nZDepth = ((pV[1].sz + pV[2].sz + zm)*0.3333f);

				CurrentSortPtr[0] = (sint32) CurrentInfoPtr;
				CurrentSortPtr[1] = (sint32) FTOL(nZDepth);
				CurrentInfoPtr[0] = Routine;
				CurrentInfoPtr[1] = Tpage;
				CurrentInfoPtr[2] = 3;
				pTLV    = CurrentTLVertex;
				*((D3DTLVERTEX**)(CurrentInfoPtr+3)) = pTLV;
				

				pTLV->sx    = pV[1].sx;
				pTLV->sy    = pV[1].sy;
				pTLV->sz    = pV[1].sz;
				pTLV->rhw   = pV[1].rhw;
				pTLV->color = pV[1].color;
				pTLV->tu    = pV[1].tu;
				pTLV->tv    = pV[1].tv;

				pTLV++;

				pTLV->sx    = pV[2].sx;
				pTLV->sy    = pV[2].sy;
				pTLV->sz    = pV[2].sz;
				pTLV->rhw   = pV[2].rhw;
				pTLV->color = pV[2].color;
				pTLV->tu    = pV[2].tu;
				pTLV->tv    = pV[2].tv;

				pTLV++;

				pTLV->sx    = xm;
				pTLV->sy    = ym;
				pTLV->sz    = zm;
				pTLV->rhw   = rhwm;
				pTLV->color = rm<<16|gm<<8|bm;
				pTLV->tu    = um;
				pTLV->tv    = vm;

				pTLV++;


				surfacenumbf++;
				sort3dptrbf += 2;
				info3dptrbf += 5;
				CurrentTLVertex = pTLV;

				// Alter Current List

				nZDepth = ((pV[1].sz + pV[0].sz + zm)*0.3333f);

				sortptr[1] = (sint32) FTOL(nZDepth);

				pV[2].sx = xm;
				pV[2].sy = ym;
				pV[2].sz = zm;
				pV[2].tu = um;
				pV[2].tv = vm;
				pV[2].color = (rm<<16)|(gm<<8)|bm;
				pV[2].rhw = rhwm;

			}

	
			sortptr   += 10;

	
		}
	}
}

// EOF : HWR_SubdivPolyList