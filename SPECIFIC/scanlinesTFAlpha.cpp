
#include "standard.h"
#include "global.h"
#include "drawprimitive.h"
#include "scanlines.h"

#define PIXELOUT(x,r,g,b)	PIXELOUT555(x,r,g,b)

#define PIXELOUT555(x,r,g,b) {unsigned short texel,alpha;\
					    long tr,tg,tb;\
					    long ro,go,bo;\
					    long ar,ag,ab;\
					    texel = *(Texture +((u>>16))+((v>>16)<<8));\
						if(texel != 0)\
						{\
							alpha = *(pDestination+x);\
							ar = (alpha>>10);\
							ag = (alpha>>5)&0x1f;\
							ab = (alpha)&0x1f;\
							tr = (texel>>10)&0x1f;\
							tg = (texel>>5)&0x1f;\
							tb = (texel)&0x1f;\
							ro = *(RGBTable+(tr+r));\
							go = *(RGBTable+(tg+g));\
							bo = *(RGBTable+(tb+b));\
							ro += ar;\
							go += ag;\
							bo += ab;\
							if(ro > 31) ro = 31;\
							if(go > 31) go = 31;\
							if(bo > 31) bo = 31;\
							ro <<= 10;\
							go <<= 5;\
							*(pDestination+x) = ro|go|bo;}\
							v += dv;\
							u += du;}






/*
	Function : ScanLinesTGAlphaAffine
	Useage	 : Textured Goruaud Alpha Affine 
*/

void ScanLinesTFAlphaDecalAffine(GRADIENT* Gradients,EDGE* pLeft,EDGE* pRight,EDGE* StepLeft,EDGE* StepRight)
{
	int Width;
	unsigned short* pDestination;
	long u,v,du,dv;
	long yline;
	float oowidth;
	short subdivcnt;
	short texel;
	short tr,tg,tb;
	short ro,go,bo;
	double fconv;
	short r,g,b;

	r = (Gradients->rf>>3)<<5;
	g = (Gradients->gf>>3)<<5;
	b = (Gradients->bf>>3)<<5;

	yline = long(Output)+YTable[pLeft->y];
	
	while(StepLeft->height--)
	{
		Width  = pRight->x;
		Width -= pLeft->x;

		if(Width > 0)
		{
			pDestination = (unsigned short*) yline + pLeft->x;
			
			u = pLeft->uozf;
			v = pLeft->vozf;
			

			oowidth = WidthTable[Width];

			du = (pRight->uozf - pLeft->uozf);
			dv = (pRight->vozf - pLeft->vozf);
			du = FTOL(float(du)*oowidth);
			dv = FTOL(float(dv)*oowidth);
		
			
			subdivcnt = Width>>4;
			Width = Width-(subdivcnt<<4);
			
			while(subdivcnt--)
			{
				PIXELOUT(0,r,g,b);
				PIXELOUT(1,r,g,b);
				PIXELOUT(2,r,g,b);
				PIXELOUT(3,r,g,b);
				PIXELOUT(4,r,g,b);
				PIXELOUT(5,r,g,b);
				PIXELOUT(6,r,g,b);
				PIXELOUT(7,r,g,b);
				PIXELOUT(8,r,g,b);
				PIXELOUT(9,r,g,b);
				PIXELOUT(10,r,g,b);
				PIXELOUT(11,r,g,b);
				PIXELOUT(12,r,g,b);
				PIXELOUT(13,r,g,b);
				PIXELOUT(14,r,g,b);
				PIXELOUT(15,r,g,b);

				pDestination += 16;
			}

			for(int n=0;n<Width;n++)
			{
				PIXELOUT(0,r,g,b);
				pDestination++;
			}
		}

		yline += bytepitch;

		StepLeft->uozf += StepLeft->uozstepf;
		StepLeft->vozf += StepLeft->vozstepf;
		StepLeft->x    += StepLeft->xstep;
		StepLeft->y++;
		

		StepLeft->ErrorTerm += StepLeft->Numerator;

		if(StepLeft->ErrorTerm >= StepLeft->Denominator)
		{
			StepLeft->x++;
			StepLeft->uozf += StepLeft->uozxtraf;
			StepLeft->vozf += StepLeft->vozxtraf;
			StepLeft->ErrorTerm -= StepLeft->Denominator;
		}

		StepRight->x    += StepRight->xstep;
		StepRight->uozf += StepRight->uozstepf;
		StepRight->vozf += StepRight->vozstepf;
		StepRight->y++;

		StepRight->ErrorTerm += StepRight->Numerator;

		if(StepRight->ErrorTerm >= StepRight->Denominator)
		{
			StepRight->x++;
			StepRight->uozf += StepRight->uozxtraf;
			StepRight->vozf += StepRight->vozxtraf;
			StepRight->ErrorTerm -= StepRight->Denominator;
		}
	}
}


// EOF : ScanLinesTGAlphaAffine
