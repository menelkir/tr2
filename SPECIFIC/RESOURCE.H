//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TombRaid.rc
//
#define IDS_INITIALISE_FAILED           1
#define IDI_APP                         100
#define IDD_PP_VIDEO                    107
#define IDD_PP_AUDIO                    108
#define IDD_PP_CONTROLS                 109
#define IDD_PP_FRONT                    110
#define IDR_LARA_WAV                    127
#define IDD_KEY                         128
#define IDD_DEMO                        129
#define IDI_ICON2                       133
#define IDD_PCGAMER                     143
#define IDD_PP_EXTRA                    144
#define IDI_ICON9                       146
#define IDI_ICON10                      147
#define IDI_ICON11                      148
#define IDI_ICON12                      149
#define IDI_ICON13                      150
#define IDI_ICON14                      151
#define IDI_ICON15                      152
#define IDI_ICON16                      153
#define IDI_ICON17                      154
#define IDI_ICON18                      155
#define IDI_ICON19                      156
#define IDR_WAVE1                       163
#define IDD_INSERT_CD                   165
#define IDC_VIDEO_MODE                  1000
#define IDC_PERSPECTIVE                 1015
#define IDC_ZBUFFER                     1017
#define IDC_BILINEAR                    1018
#define IDC_OUTPUT_DEVICE               1025
#define IDC_TEST_MODE                   1029
#define IDC_FULLSCREEN                  1030
#define IDC_WINDOWED                    1031
#define IDC_WSIZE_SLIDER                1034
#define IDC_WSIZE_TEXTBOX               1035
#define IDC_GROUP_WS                    1037
#define IDC_GROUP_VMFHA                 1038
#define IDC_WSIZE_SMALL                 1039
#define IDC_WSIZE_LARGE                 1040
#define IDC_GROUP_OD                    1041
#define IDC_TEST_RESULT                 1043
#define IDC_GROUP_TEST                  1044
#define IDC_INTERNAL_RENDERER           1045
#define IDC_TEST_RESULT2                1046
#define IDC_DIRECT3D_HARDWARE           1047
#define IDC_TRIPLE_BUFFERING            1048
#define IDC_GROUP_RENDERER              1049
#define IDC_GROUP_DISPLAY_TYPE          1050
#define IDC_GROUP_RENDERING_OPTIONS     1051
#define IDC_SOUND_DEVICE                1052
#define IDC_GROUP_SOUND_OD              1053
#define IDC_JOYSTICK_DEVICE             1055
#define IDC_SOUND_ENABLED               1057
#define IDC_DITHER                      1058
#define IDC_ASPECT_4_TO_3               1060
#define IDC_ASPECT_16_TO_9              1061
#define IDC_ASPECT_ANY                  1062
#define IDC_LARAPIC                     1073
#define IDC_LARABIKEPIC                 1074
#define IDC_LARALOVEPIC                 1075
#define IDC_FR_GFX_DRIVER               1077
#define IDC_FR_GFX_USING                1078
#define IDC_FR_GFX_DISPLAY              1079
#define IDC_FR_GFX_OPTIONS              1080
#define IDC_FR_SND_DRIVER               1081
#define IDC_FR_SND_ENABLED              1082
#define IDC_FR_JOY_DRIVER               1084
#define IDC_FR_SND_MIC_POS              1085
#define IDC_MIC_CAMERA                  1086
#define IDC_MIC_LARA                    1087
#define IDC_SOUND_TEST                  1089
#define IDC_KEY                         1090
#define IDC_APPLY                       1091
#define IDC_BODY                        1092
#define IDC_PASSWORD                    1093
#define IDC_TO                          1094
#define IDC_SUBJECT                     1095
#define IDC_CUSTOM1                     1096
#define IDC_MIC_POS                     1097
#define IDC_SOUND_OUTPUT                1098
#define IDC_JOYSTICK_ENABLE             1099
#define IDC_JOYSTICK_CONFIG             1100
#define IDC_PCGAMER                     1102
#define IDC_DISABLE16                   1103
#define IDC_NOSORT                      1104
#define IDC_TEXEL1                      1107
#define IDC_TEXEL2                      1108
#define IDC_TEXEL3                      1109
#define IDC_DRESS                       1110
#define IDC_DISABLE_FMV                 1111
#define IDC_VIDEO_RESET                 1112

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        169
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1113
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
