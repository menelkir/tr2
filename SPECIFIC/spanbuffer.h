#ifndef _SPANBUFFER_H
#define _SPANBUFFER_H

#define MAX_EDGES		100000			
#define MAX_SURFACES	3000
#undef MAX_POLYGONS
#define MAX_POLYGONS	100000

struct POLYGON
{
//	D3DTLVERTEX v1;
//	D3DTLVERTEX v2;
//	D3DTLVERTEX v3;

	int StackStatus;
	float ooz;
	int rgb;
};

struct SEDGE
{
	int	x;
	POLYGON *Poly;	
	SEDGE *Left;
	SEDGE *Right;
};

struct SURFACE
{
	float x;
	POLYGON* Poly;
	SURFACE* Next;
};


extern int SpanPixels;


bool SBAllocate(int maxx,int maxy);
void SBFree();
//void SBAddEdge(int x,int y,POLYGON* poly,float);
void SBAddEdge(int x,int y);


void SBInsert(SEDGE *ptr,SEDGE* NewEdge);
void SBInit();
void SBTraverse(SEDGE* Edge);
void SBEdgeEvent(SEDGE* Edge);
//void SBAddPolygon(D3DTLVERTEX* v1,D3DTLVERTEX* v2,D3DTLVERTEX* v3);
void SBAddPolygon(float,float,float,float);


void SBDrawSpans();

#endif


