#ifndef _HWINSERT_H_
#define _HWINSERT_H_

#ifdef __cplusplus
extern "C" {
#endif

int RoomZedClipper(int number,POINT_INFO* input,VERTEX_INFO* output);
int RoomXYGUVClipper( int number, VERTEX_INFO *input);

sint16*	HWI_InsertObjectGT3_ZBuffered(sint16* objptr,int number,enum sort_type sort_routine);
sint16*	HWI_InsertObjectGT4_ZBuffered(sint16* objptr,int number,enum sort_type sort_routine);

sint16*	HWI_RoomInsertObjectGT3_ZBuffered(sint16* objptr,int number,enum sort_type sort_routine);
sint16*	HWI_RoomInsertObjectGT4_ZBuffered(sint16* objptr,int number,enum sort_type sort_routine);

sint16*	HWI_InsertObjectG3_ZBuffered(sint16* objptr,int number,enum sort_type sort_routine);
sint16*	HWI_InsertObjectG4_ZBuffered(sint16* objptr,int number,enum sort_type sort_routine);
void	HWI_InsertFlatRect_ZBuffered(sint32 nX1,sint32 nY1,sint32 nX2,sint32 nY2,int nZDepth,int nColour);
void	HWI_InsertLine_ZBuffered(sint32 x1,sint32 y1,sint32 x2,sint32 y2,sint32 z,char color);

sint16*	HWI_InsertObjectGT3_Sorted(sint16* objptr,int number,enum sort_type sort_routine);
sint16*	HWI_InsertObjectGT4_Sorted(sint16* objptr,int number,enum sort_type sort_routine);
sint16*	HWI_InsertObjectG3_Sorted(sint16* objptr,int number,enum sort_type sort_routine);
sint16*	HWI_InsertObjectG4_Sorted(sint16* objptr,int number,enum sort_type sort_routine);
void	HWI_InsertSprite_Sorted(int nZDepth,int nX1,int nY1,int nX2,int nY2,int nSprite,int nShade,int nShade1,int drawtype,int offset);
void	HWI_InsertFlatRect_Sorted(sint32 nX1,sint32 nY1,sint32 nX2,sint32 nY2,int nZDepth,int nColour);
void	HWI_InsertLine_Sorted(sint32 x1,sint32 y1,sint32 x2,sint32 y2,sint32 z,char color);
void	HWI_InsertTrans8_Sorted(PHD_VBUF *vbuf,sint16 shade);
void	HWI_InsertTransQuad_Sorted(sint32 sx,sint32 sy,sint32 w,sint32 h,sint32 z);

void HWI_InsertGT4_Sorted(PHD_VBUF* pV1,PHD_VBUF* pV2,PHD_VBUF* pV3,PHD_VBUF* pV4,PHDTEXTURESTRUCT* pTex,enum sort_type nSortType,unsigned short dbl);
void HWI_InsertGT3_Sorted(PHD_VBUF* pV1,PHD_VBUF* pV2,PHD_VBUF* pV3,PHDTEXTURESTRUCT* pTex,uint16* pUV1,uint16* pUV2,uint16* pUV3,enum sort_type nSortType,unsigned short);

void HWI_InsertAlphaSprite_Sorted(int nX1,int nY1,int nZ1,int nShade1,
								  int nX2,int nY2,int nZ2,int nShade2,
								  int nX3,int nY3,int nZ3,int nShade3,
								  int nX4,int nY4,int nZ4,int nShade4,
								  int nSprite,int type,int dbl);

void InitBuckets();
void DrawBuckets(void *);
void DrawBucketsThread(void*);

#define VERTSPERBUCKET	(1024+32)
#define MAXBUCKETS		20
struct TEXTUREBUCKET
{
	DWORD tpage;
	int cnt;
	D3DTLVERTEX Vertex[VERTSPERBUCKET];
};




extern TEXTUREBUCKET Bucket[];



#ifdef __cplusplus
}
#endif


#endif