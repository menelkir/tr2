#ifndef _DEBUG_H_
#define _DEBUG_H_

#ifdef __cplusplus

enum
	{
	LOG_OUTPUT,
	LOG_RESET,
	LOG_END
	};

#define LOG_DO_STRLEN 0xffffffff

#ifdef DBLOG
extern "C" void __cdecl DB_Log(char* format,...);
void DB_InitLog(char* sLogName);
void DB_SendLogMessage(DWORD dwType,PVOID pData,DWORD dwLen);
void DB_FlushLogStore();
void DB_LogWindowsMessage(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
#define Log DB_Log
#define IfLog
#define InitLog(a) DB_InitLog(a)
#define SendLogMessage(a,b,c) DB_SendLogMessage(a,b,c)
#define FlushLogStore DB_FlushLogStore
#define LogWindowsMessage DB_LogWindowsMessage
#else
// check this!!! totally nukes the Logs
#define Log 0&&
#define IfLog 0&&
#define InitLog(a)
#define SendLogMessage(a,b,c)
#define FlushLogStore()
#define LogWindowsMessage(a,b,c,d)
#endif
#define EndLog() SendLogMessage(LOG_END,0,0)

#endif



#endif