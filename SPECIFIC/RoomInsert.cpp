/*
	File	:	roominsert.cpp
	Author	:	Richard Flower (c) 1998 Core Design
	----------------------------------------------------------------------------------
	Functions
	----------------------------------------------------------------------------------


	  
		
		  
			
			  
	----------------------------------------------------------------------------------
*/



#include "..\specific\include.h"
#include "..\3DSystem\3Dglodef.h"
#include "stdlib.h"
#include "math.h"
#include "..\specific\winmain.h"
#include "roominsert.h"


sint16* RF_Calc_RoomVert( sint16 *objptr, int far_clip );


#define DPQ_S (DPQ_START<<W2V_SHIFT)
#define DPQ_E (DPQ_END<<W2V_SHIFT)


#define MAX_D3DTEXTURES		32
#define MAX_TEXTUREVERTS	5000

D3DTLVERTEX RoomVertex[MAX_D3DTEXTURES][MAX_TEXTUREVERTS];		
int	RoomVertCnt[MAX_D3DTEXTURES];



void InitRoomVertexBuffer()
{
	// Clear All Texture Bucket Cnts;
	memset(&RoomVertCnt,0,sizeof(int)*MAX_D3DTEXTURES);
}


void RF_S_InsertRoom(sint16 *objptr, int outside)
{
	phd_leftfloat = (float)(phd_winxmin + phd_left);
	phd_topfloat = (float)(phd_winymin + phd_top);
	phd_rightfloat = (float)(phd_winxmin + phd_right + 1);
	phd_bottomfloat = (float)(phd_winymin + phd_bottom + 1);
	f_centerx = (float)(phd_winxmin + phd_centerx);
	f_centery = (float)(phd_winymin + phd_centery);
	
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_FOGENABLE,true);

	objptr=RF_Calc_RoomVert(objptr, outside? 0 : 16);
	
	objptr=RoomInsertObjectGT4(objptr+1,*(objptr),FAR_SORT); //rf
	objptr=RoomInsertObjectGT3(objptr+1,*(objptr),FAR_SORT); // rf
	objptr=ins_room_sprite(objptr+1,*(objptr));

	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_FOGENABLE,false);
}


sint16* RF_Calc_RoomVert( sint16 *objptr, int far_clip )
{
	int nVert;
	sint16 *faceptr;

	nVert = (int)*(objptr++);
	faceptr = objptr+(nVert*6);

	for(nVert=(int)*(objptr++);nVert>0;nVert--,objptr+=6)
	{		
	

	



	}
	
		
	
	
	
	PHD_VBUF 	*dest;
	sint32		*mptr;
	float		xv,yv,zv,xs,ys;
	sint32		z;
	int			numvert;
	sint8		clipflag;
	int nShade;
	
	mptr = phd_mxptr;
	dest = vbuf;

	for ( numvert = (int)*(objptr++); numvert>0; numvert--,dest++,objptr+=6 )
	{
		dest->xv = xv = float(mptr[M00] * objptr[0] + mptr[M01] * objptr[1] + mptr[M02] * objptr[2] + mptr[M03]);
		dest->yv = yv = float(mptr[M10] * objptr[0] + mptr[M11] * objptr[1] + mptr[M12] * objptr[2] + mptr[M13]);
		z=mptr[M20] * objptr[0] + mptr[M21] * objptr[1] + mptr[M22] * objptr[2] + mptr[M23];
		dest->zv = zv = float(z);

		nShade=objptr[5];
		
		if(shade_effect)
			nShade+=shade_table[(rand_table[numvert & (WIBBLE_SIZE -1 )] + wibble_offset) & (WIBBLE_SIZE-1)];

		if (zv>=f_znear)
			{
			clipflag = 0;

			zv = f_persp / zv;

			if (z>=DPQ_E) 	// Vertice Is Off FarZ clip plane
			{
				clipflag = far_clip;								
				dest->g = 0x1fff;		//  so set as Darkest Color			       
				dest->zv = f_zfar;		// Dude: deck Z so it doesn't wrap in z buffer
				dest->ooz = f_oneozfar; 
			}
			else
			{
				if (z>DPQ_S)
					dest->g = nShade + (z>>W2V_SHIFT) - DPQ_START;		// In Depth Cue Area ie Z = 12288 to 20480
				else
					dest->g = nShade;
				dest->ooz=zv*f_oneopersp;
			}

			xs = xv * zv + f_centerx;
			ys = yv * zv + f_centery;
			
			/*** WATER EFFECT ***/
			if (water_effect && objptr[4]>=0)
			{
				xs += wibble_table[((int)ys + wibble_offset) & (WIBBLE_SIZE-1)];
				ys += wibble_table[((int)xs + wibble_offset) & (WIBBLE_SIZE-1)];
			}
			if ( xs<phd_leftfloat )
				clipflag++;
			else if ( xs>phd_rightfloat )
				clipflag += 2;
			dest->xs = xs;
			if ( ys<phd_topfloat )
				clipflag += 4;
			else if ( ys>phd_bottomfloat )
				clipflag += 8;
			dest->ys = ys;
			dest->clip = clipflag;          	// insert Clip Flag */
			dest->fog=((uint8)(dest->zv*(12.0f/(256.0f*256.0f*256.0f))))^0xff;
			}
		else
			{
			dest->clip = -128;				//  flag we need Z-clipping
			dest->g = nShade;				// Make Shade Normal....
			dest->fog=255;
			}
		
		/*** SHADE EFFECT ***/
		
		if ( dest->g<0 )
			dest->g = 0;
		else if ( dest->g>0x1fff )
			dest->g = 0x1fff;
		
	
		dest->g = objptr[5];


		}
	return(objptr);       				// return current objptr.
	}

