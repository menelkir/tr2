#include "standard.h"
#include "global.h"
#include "winmain.h"
#include "hwinsert.h"
#include "../game/effect2.h"
#include "../game/objects.h"



extern void mCalcPoint(long dx,long dy,long dz,long *result);
extern void ProjectPCoord(long x,long y,long z,long *result,long cx,long cy,long fov);

unsigned char	SplashLinks[8*4] = {	8<<2,  9<<2,  0<<2, 1<<2,
										9<<2,  10<<2, 1<<2, 2<<2,
										10<<2, 11<<2, 2<<2, 3<<2,
										11<<2, 12<<2, 3<<2, 4<<2,
										12<<2, 13<<2, 4<<2, 5<<2,
										13<<2, 14<<2, 5<<2, 6<<2,
										14<<2, 15<<2, 6<<2, 7<<2,
										15<<2, 8<<2,  7<<2, 0<<2	};	// Table of which points make up which faces.

void S_DrawSplashes()
{
	RIPPLE_STRUCT	*rptr;
	SPLASH_STRUCT	*sptr;
	SPLASH_VERTS	*svptr;
	long			*result;
	long			*scrpoints;	// X,Y,Z.
	unsigned char	*pptr;
	long			lp,vlp,olp;
	long			wx,wy,wz,linkadd;
	long			x1,x2,x3,x4,y1,y2,y3,y4;
	long			z1,z2,z3,z4;
	long			mscrpoints[48*4];
	long			mresult[3];
	int				nSprite;

	int sw = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).w;
	int sh = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).h;

	sptr = &splashes[0];

	for (lp=0;lp<MAX_SPLASHES;lp++)
	{
		if ((sptr->flags & SPL_ON) == 0)
		{
			sptr++;
			continue;
		}

		svptr = &sptr->sv[0];

		scrpoints = (long*) &mscrpoints; 
		result = (long*) &mresult;

		for (vlp=0;vlp<48;vlp++)
		{
			wx = (svptr->wx>>4) + sptr->x;
			wy = svptr->wy + sptr->y;
			wz = (svptr->wz>>4) + sptr->z;

			mCalcPoint(wx,wy,wz,result);
			ProjectPCoord(result[X],result[Y],result[Z],scrpoints,sw>>1,sh>>1,phd_persp);
			scrpoints+=4;
			svptr++;
		}

		scrpoints = (long*) &mscrpoints; 

		for (olp=0;olp<3;olp++)
		{
			if (olp == 2 || (olp == 0 && (sptr->flags & SPL_RIPPLEINNER)) || (olp == 1 && (sptr->flags & SPL_RIPPLEMIDDLE)))
				nSprite = objects[EXPLOSION1].mesh_index+4+((wibble>>4)&3);			
			else
				nSprite = objects[EXPLOSION1].mesh_index+8;

			pptr = &SplashLinks[0];
			linkadd = olp<<6;

			for (vlp=0;vlp<8;vlp++)
			{
				x1 = scrpoints[(*pptr)+linkadd];
				y1 = scrpoints[(*pptr)+1+linkadd];
				z1 = scrpoints[(*pptr++)+2+linkadd];
				x2 = scrpoints[(*pptr)+linkadd];
				y2 = scrpoints[(*pptr)+1+linkadd];
				z2 = scrpoints[(*pptr++)+2+linkadd];
				x3 = scrpoints[(*pptr)+linkadd];
				y3 = scrpoints[(*pptr)+1+linkadd];
				z3 = scrpoints[(*pptr++)+2+linkadd];
				x4 = scrpoints[(*pptr)+linkadd];
				y4 = scrpoints[(*pptr)+1+linkadd];
				z4 = scrpoints[(*pptr++)+2+linkadd];

				if (
					(x1 < 0 && x2 < 0 && x3 < 0 && x4 < 0) ||
					(x1 >= sw && x2 >= sw && x3 >= sw && x4 >= sw) ||
					(y1 < 0 && y2 < 0 && y3 < 0 && y4 < 0) ||
					(y1 >= sh && y2 >= sh && y3 >= sh && y4 >= sh))
				{
					continue;
				}

				z1 <<= W2V_SHIFT;
				if(z1 < phd_znear || z1 > phd_zfar) continue;
				z2 <<= W2V_SHIFT;
				if(z2 < phd_znear || z2 > phd_zfar) continue;
				z3 <<= W2V_SHIFT;
				if(z3 < phd_znear || z3 > phd_zfar) continue;
				z4 <<= W2V_SHIFT;
				if(z4 < phd_znear || z4 > phd_zfar) continue;

				int a,b;
				a = (sptr->life>>2);
				b = (sptr->life>>2)-(sptr->life>>4);

				int nShade1 = a<<10|a<<5|a;
				int nShade2 = b<<10|b<<5|b;
				
	
			if((((x3 - x2) * (y1 - y2))-((x1 - x2) * (y3 - y2))) < 0)
				HWI_InsertAlphaSprite_Sorted(x1,y1,z1,nShade1,
											 x3,y3,z3,nShade2,
											 x4,y4,z4,nShade2,
											 x2,y2,z2,nShade1,
											 nSprite,DRAW_TLV_GTA,1);
			else	
				HWI_InsertAlphaSprite_Sorted(x1,y1,z1,nShade1,
											 x2,y2,z2,nShade1,
											 x4,y4,z4,nShade2,
											 x3,y3,z3,nShade2,
											 nSprite,DRAW_TLV_GTA,0);

			}
		}
		sptr++;
	}


	result = (long *) &mresult;

	rptr = &ripples[0];
	for (lp=0;lp<MAX_RIPPLES;lp++)
	{
		if (rptr->flags & SPL_ON)
		{
			long	size = rptr->size<<2;

			nSprite = objects[EXPLOSION1].mesh_index+9;	
				
			scrpoints = (long*) &mscrpoints;
			mCalcPoint(rptr->x-size,rptr->y,rptr->z-size,result);
			ProjectPCoord(result[X],result[Y],result[Z],scrpoints,sw>>1,sh>>1,phd_persp);
			scrpoints+=3;
			mCalcPoint(rptr->x+size,rptr->y,rptr->z-size,result);
			ProjectPCoord(result[X],result[Y],result[Z],scrpoints,sw>>1,sh>>1,phd_persp);
			scrpoints+=3;
			mCalcPoint(rptr->x-size,rptr->y,rptr->z+size,result);
			ProjectPCoord(result[X],result[Y],result[Z],scrpoints,sw>>1,sh>>1,phd_persp);
			scrpoints+=3;
			mCalcPoint(rptr->x+size,rptr->y,rptr->z+size,result);
			ProjectPCoord(result[X],result[Y],result[Z],scrpoints,sw>>1,sh>>1,phd_persp);
			scrpoints-=9;

			x1 =   *scrpoints++;
			y1 =   *scrpoints++;
			z1 =   *scrpoints++;
			x2 =   *scrpoints++;
			y2 =   *scrpoints++;
			z2 =   *scrpoints++;
			x3 =   *scrpoints++;
			y3 =   *scrpoints++;
			z3 =   *scrpoints++;
			x4 =   *scrpoints++;
			y4 =   *scrpoints++;
			z4 =   *scrpoints;

			if ((x1 < 0 && x2 < 0 && x3 < 0 && x4 < 0) ||
				(x1 >= sw && x2 >= sw && x3 >= sw && x4 >= sw) ||
				(y1 < 0 && y2 < 0 && y3 < 0 && y4 < 0) ||
				(y1 >= sh && y2 >= sh && y3 >= sh && y4 >= sh))
			{
				rptr++;
				continue;
			}

			z1 <<= W2V_SHIFT;
			if(z1 < phd_znear || z1 > phd_zfar) {rptr++; continue;}
			z2 <<= W2V_SHIFT;
			if(z2 < phd_znear || z2 > phd_zfar) {rptr++; continue;}
			z3 <<= W2V_SHIFT;
			if(z3 < phd_znear || z3 > phd_zfar) {rptr++; continue;}
			z4 <<= W2V_SHIFT;
			if(z4 < phd_znear || z4 > phd_zfar) {rptr++; continue;}

			int nShade = 0;
			
			if (rptr->flags & SPL_MORETRANS)
			{
				
				if (rptr->flags & SPL_BLOOD)
				{
					nSprite = objects[EXPLOSION1].mesh_index;	
						
					if (rptr->init)
						nShade = (rptr->init>>1)<<10;//|rptr->init>>5;
					else
						nShade = (rptr->life>>1)<<10;//|rptr->life>>5;
				}
				else			
				{				
					if (rptr->init)
						nShade = (rptr->init>>2)<<10|(rptr->init>>2)<<5|rptr->init>>2;
					else
						nShade = (rptr->life>>2)<<10|(rptr->life>>2)<<5|rptr->life>>2;
				}	
			}
			else
			{
				if (rptr->init)
					nShade = (rptr->init>>1)<<10|(rptr->init>>1)<<5|rptr->init>>1;
					
				else
					nShade = (rptr->life>>1)<<10|(rptr->life>>1)<<5|rptr->life>>1;
			}

		
			if((((x3 - x2) * (y1 - y2))-((x1 - x2) * (y3 - y2))) < 0)
				HWI_InsertAlphaSprite_Sorted(x1,y1,z1,nShade,
											 x3,y3,z3,nShade,
											 x4,y4,z4,nShade,
											 x2,y2,z2,nShade,
											 nSprite,DRAW_TLV_GTA,1);
			else	
				HWI_InsertAlphaSprite_Sorted(x1,y1,z1,nShade,
											 x2,y2,z2,nShade,
											 x4,y4,z4,nShade,
											 x3,y3,z3,nShade,
											 nSprite,DRAW_TLV_GTA,0);
		}
		rptr++;
	}
}



