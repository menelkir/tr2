#include "standard.h"
#include "global.h"
#include "winmain.h"
#include "hwinsert.h"
#include "../game/effect2.h"
#include "../game/effects.h"



extern void mCalcPoint(long dx,long dy,long dz,long *result);
extern void ProjectPCoord(long x,long y,long z,long *result,long cx,long cy,long fov);
extern "C" void S_DrawSparks();


void S_DrawSparks()
{
	long		lp;
	long		result[XYZ];
	long		scr[3][XYZ];
	long		w,h,x1,x2,x3,x4,y1,y2,y3,y4;
	long		wx,wy,wz;
	int z;

	int sw = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).w;
	int sh = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).h;
	SPARKS	*sptr;
	FX_INFO 	*fx;
	ITEM_INFO	*item;

	sptr = &spark[0];

	for (lp=0;lp<MAX_SPARKS;lp++)
	{
		if (sptr->On == 0)
		{
			sptr++;
			continue;
		}
	

		if (sptr->Flags & SP_FX)
		{
			fx = &effects[sptr->FxObj];
			wx = fx->pos.x_pos+sptr->x;
			wy = fx->pos.y_pos+sptr->y;
			wz = fx->pos.z_pos+sptr->z;
		}
		else if(sptr->Flags & SP_ITEM)
		{
			item = &items[sptr->FxObj];
			wx = item->pos.x_pos+sptr->x;
			wy = item->pos.y_pos+sptr->y;
			wz = item->pos.z_pos+sptr->z;
		}
		else
		{
			wx = sptr->x;
			wy = sptr->y;
			wz = sptr->z;
		}

		mCalcPoint(wx,wy,wz,&result[0]);
		ProjectPCoord(result[X],result[Y],result[Z],&scr[0][0],sw>>1,sh>>1,phd_persp);
		
		if (sptr->Flags & SP_DEF)
		{
			if (sptr->Flags & SP_SCALE)
			{
				if(scr[0][Z]==0) scr[0][Z]++;

					w = ((sptr->Width*phd_persp)<<sptr->Scalar)/(scr[0][Z]);
					h = ((sptr->Height*phd_persp)<<sptr->Scalar)/(scr[0][Z]);
					
					if (w > sptr->Width<<sptr->Scalar)
						w = sptr->Width<<sptr->Scalar;
					else if (w < 4)
						w = 4;
					if (h > sptr->Height<<sptr->Scalar)
						h = sptr->Height<<sptr->Scalar;
					else if (h < 4)
						h = 4;
					
				}
				else
				{
					w = sptr->Width;
					h = sptr->Height;
				}

				z = scr[0][Z]<<W2V_SHIFT;
				
				if ((z < phd_znear || z > phd_zfar) ||
					(scr[0][X]+(w>>1) < 0) ||
					(scr[0][X]-(w>>1) > sw) ||
					(scr[0][Y]+(h>>1) < 0) ||
					(scr[0][Y]-(h>>1) > sh))
				{
					sptr++;
					continue;
				}

				if (sptr->Flags & SP_ROTATE)
				{
				
					long	sin,cos;
					long sinx1,sinx2,siny1,siny2;
					long	cosx1,cosx2,cosy1,cosy2;

					sin = ((short)rcossin_tbl[sptr->RotAng<<1]);
					cos = ((short)rcossin_tbl[(sptr->RotAng<<1)+1]);
					sinx1 = ((-w>>1)*sin)>>12;
					sinx2 = ((w>>1)*sin)>>12;
					siny1 = ((-h>>1)*sin)>>12;
					siny2 = ((h>>1)*sin)>>12;
					cosx1 = ((-w>>1)*cos)>>12;
					cosx2 = ((w>>1)*cos)>>12;
					cosy1 = ((-h>>1)*cos)>>12;
					cosy2 = ((h>>1)*cos)>>12;

					x1 = sinx1-cosy1+scr[0][X];
					x2 = sinx2-cosy1+scr[0][X];
					x3 = sinx2-cosy2+scr[0][X];
					x4 = sinx1-cosy2+scr[0][X];
					y1 = cosx1+siny1+scr[0][Y];
					y2 = cosx2+siny1+scr[0][Y];
					y3 = cosx2+siny2+scr[0][Y];
					y4 = cosx1+siny2+scr[0][Y];

					int nShade;
					nShade = ((sptr->R>>3)<<10)|((sptr->G>>3)<<5)|((sptr->B>>3));

					if (z>DPQ_S)
					{			
						int r,g,b;
						int v;
						r = sptr->R;
						g = sptr->G;
						b = sptr->B;
						v = 2048-((z - DPQ_S)>>16);	
						r *= v;
						g *= v;
						b *= v;
						r >>= 14;
						g >>= 14;
						b >>= 14;
						if(r<0) r= 0;	
						if(g<0) g= 0;	
						if(b<0) b= 0;	
						nShade = r<<10|g<<5|b;
					}						

					if (sptr->TransType == COLADD || sptr->TransType == COLSUB)
						HWI_InsertAlphaSprite_Sorted(x1,y1,z,nShade,
													 x2,y2,z,nShade,
													 x3,y3,z,nShade,
													 x4,y4,z,nShade,
													 sptr->Def,DRAW_TLV_GTA,0);
					else
						HWI_InsertAlphaSprite_Sorted(x1,y1,z,nShade,
													 x2,y2,z,nShade,
													 x3,y3,z,nShade,
													 x4,y4,z,nShade,
													 sptr->Def,DRAW_TLV_WGT,0);
					sptr->RotAng += sptr->RotAdd;
					sptr->RotAng &= 4095;

				}
				else
				{
					x1 = scr[0][X]-(w>>1);
					x2 = scr[0][X]+(w>>1);
					y1 = scr[0][Y]-(h>>1);
					y2 = scr[0][Y]+(h>>1);
					
					int nShade;
					nShade = ((sptr->R>>3)<<10)|((sptr->G>>3)<<5)|((sptr->B>>3));

					
					if (z>DPQ_S)
					{			
						int r,g,b;
						int v;
						r = sptr->R;
						g = sptr->G;
						b = sptr->B;
						v = 2048-((z - DPQ_S)>>16);	
						r *= v;
						g *= v;
						b *= v;
						r >>= 14;
						g >>= 14;
						b >>= 14;
						if(r<0) r= 0;	
						if(g<0) g= 0;	
						if(b<0) b= 0;	
						nShade = r<<10|g<<5|b;
					}					

					if (sptr->TransType == COLADD || sptr->TransType == COLSUB)
						HWI_InsertAlphaSprite_Sorted(x1,y1,z,nShade,
													 x2,y1,z,nShade,
													 x2,y2,z,nShade,
													 x1,y2,z,nShade,
													 sptr->Def,DRAW_TLV_GTA,0);
					else
						HWI_InsertAlphaSprite_Sorted(x1,y1,z,nShade,
													 x2,y1,z,nShade,
													 x2,y2,z,nShade,
													 x1,y2,z,nShade,
													 sptr->Def,DRAW_TLV_WGT,0);
				}
			}
			else
			{

				if (sptr->Flags & SP_SCALE)
				{
					if(scr[0][Z] == 0) scr[0][Z] = 1;
					
					w = ((sptr->Width*phd_persp)<<sptr->Scalar)/(scr[0][Z]);
					h = ((sptr->Height*phd_persp)<<sptr->Scalar)/(scr[0][Z]);
					if (w > sptr->Width<<2)
						w = sptr->Width<<2;
					else if (w < 1)
						w = 1;
					if (h > sptr->Height<<2)
						h = sptr->Height<<2;
					else if (h < 1)
						h = 1;
				}
				else
				{
					w = sptr->Width;
					h = sptr->Height;
				}


				z = scr[0][Z]<<W2V_SHIFT;

				if ((z < phd_znear || z > phd_zfar) ||
					(scr[0][X]+(w>>1) < 0) ||
					(scr[0][X]-(w>>1) > sw) ||
					(scr[0][Y]+(h>>1) < 0) ||
					(scr[0][Y]-(h>>1) > sh))
				{
					sptr++;
					continue;
				}

				x1 = scr[0][X]-(w>>1);		
				y1 = scr[0][Y]-(h>>1);		
				x2 = x1+w;
				y2 = y1+h;
								

				int nShade;
				nShade = ((sptr->R>>3)<<10)|((sptr->G>>3)<<5)|((sptr->B>>3));

	
				if (z>DPQ_S)
				{			
					int r,g,b;
					int v;
					r = sptr->R;
					g = sptr->G;
					b = sptr->B;
					v = 2048-((z - DPQ_S)>>16);	
					r *= v;
					g *= v;
					b *= v;
					r >>= 14;
					g >>= 14;
					b >>= 14;
					if(r<0) r= 0;	
					if(g<0) g= 0;	
					if(b<0) b= 0;	
					nShade = r<<10|g<<5|b;
				}					


				if (sptr->TransType == COLADD || sptr->TransType == COLSUB)
					HWI_InsertAlphaSprite_Sorted(x1,y1,z,nShade,
												 x2,y1,z,nShade,
												 x2,y2,z,nShade,
												 x1,y2,z,nShade,
												 -1,DRAW_TLV_GA,0);
				else
					HWI_InsertAlphaSprite_Sorted(x1,y1,z,nShade,
												 x2,y1,z,nShade,
												 x2,y2,z,nShade,
												 x1,y2,z,nShade,
												 -1,DRAW_TLV_G,0);

			
			
			
			
		}

		sptr++;

	}

}
