#ifndef _DS_H_
#define _DS_H_

#include "template.h"

struct SoundAdapter : GenericAdapter{};
global List<SoundAdapter> G_SoundAdapterList;
global SoundAdapter G_SoundAdapter;
global Position G_PrimarySoundAdapter;

bool DS_Init();
void DS_Cleanup();
void DS_Start(HWND hWnd=0);
void DS_Finish();

Position DS_FindPreferredAdapter(GUID* pGUID);
bool DS_IsSoundEnabled();

bool DS_MakeSample(int nSample,WAVEFORMATEX* pWF,void* pWaveData,DWORD dwWaveLength);
void DS_FreeAllSamples();
int DS_StartSample(int nSample,int nVolume,int nPitch,int nPan,DWORD dwFlags);
void DS_AdjustVolumeAndPan(int nHandle,int nVolume,int nPan);
void DS_AdjustPitch(int nHandle,int nPitch);
void DS_StopSample(int nHandle);
void DS_StopAllSamples();
bool DS_IsChannelPlaying(int nSlot);
int DS_HowManyChannelsPlaying();

#define	DS_ERR_SOUND_FAILED			(-2)
#define	DS_ERR_SOUND_NO_FREE_SLOTS	(-1)








#endif