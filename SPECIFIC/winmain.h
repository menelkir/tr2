/*
	File	:	winmain.h
	Author	:	Richard Flower (c) 1998 Core Design
	Notes	:

*/


#ifndef _WINMAIN_H
#define _WINMAIN_H

#include "directx.h"
//#include "crtdbg.h"

#define	WINDOWCLASS	"Window Class"
#define WINDOWNAME	"Tomb Raider III"


int PASCAL WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow);
BOOL WinRegisterWindow(HINSTANCE hInstance);
HWND WinCreateWindow(HINSTANCE hInstance,int nCmdShow);
long FAR PASCAL WinAppProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
void WinAppExit();
void WinUpdate();
void WinRender();
bool WinDXInit(DEVICEINFO*,DXCONFIG*);
void WinFreeDX();
float WinFrameRate(void);
void  WinDisplayString (int x,int y,char *String,...);
int WinMessage();


struct WINAPP
{
	WNDCLASS				WindowClass;	
	HWND					WindowHandle;
	HINSTANCE				hInstance;
		
	DEVICEINFO				DeviceInfo;			// Device Information
	DXCONFIG				DXConfig;
	DEVICEINFO*				DeviceInfoPtr;
	DXCONFIG*				DXConfigPtr;

	LPDIRECTDRAW2			lpDD;				// DirectDraw2 Interface
	LPDIRECT3D2				lpD3D;				// Direct3D2 Interface
	LPDIRECT3DDEVICE2		lpD3DDevice;		// Direct 3D2 Device

	LPDIRECTDRAWSURFACE3	lpFrontBuffer;		// Primary Surface
	LPDIRECTDRAWSURFACE3	lpBackBuffer;		// Back Buffer
	LPDIRECTDRAWSURFACE3	lpZBuffer;			// Z Buffer
	LPDIRECTDRAWSURFACE3	lpPictureBuffer;	// Picture Buffer
	LPDIRECTDRAWSURFACE3	lpConversionBuffer;
	
	LPDIRECT3DVIEWPORT2		lpViewPort;
	LPDIRECT3DMATERIAL2		lpViewPortMaterial;

	LPDIRECTDRAWPALETTE		Palette;				// Palette For SW Renderer
	PALETTEENTRY			PaletteEntries[256];	// Palette Entries

	bool					bFocus;				// Application Window Has Fous
	bool					bRender;			// Ok To Render

	int						nRenderMode;		// Hardware/Internal Renderer
	int						nUVAdd;
	
	bool					tJoystickEnabled;
	int						Joystick;

	float					fps;
};


struct	HWCONFIG
{
	bool	Perspective;
	bool	Dither;
	int		Filter;
	int		nShadeMode;
	int		nFillMode;
};


extern HWCONFIG HWConfig;
extern WINAPP App;

typedef struct
{
	signed char	  shimmer;
	signed char   choppy;
	unsigned char random;
	unsigned char abs;
} WATERTAB;

extern WATERTAB	WaterTable[22][64];
extern "C" short rcossin_tbl[];

#endif