#ifndef _FRONTEND_H_
#define _FRONTEND_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "../game/text.h"

void		S_DrawScreenLine		(sint32 sx,sint32 sy,sint32 z,sint32 w,sint32 h,sint32 col,uint16 *grdptr,uint16 flags);
void		S_DrawLine( sint32 x1, sint32 y1, sint32 z, sint32 x2, sint32 y2, sint32 col, uint16 *grdptr, uint16 flags);
void		S_DrawScreenBox			(sint32 sx,sint32 sy,sint32 z,sint32 w,sint32 h,sint32 col,uint16 *grdptr,uint16 flags);
void		S_DrawScreenFBox		(sint32 sx,sint32 sy,sint32 z,sint32 w,sint32 h,sint32 col,uint16 *grdptr,uint16 flags);
void		S_SetBackgroundGamma	(sint16 gamma);
void		S_SetGamma				(sint16 gamma);
void		S_FinishInventory		(void);
void		S_InitLoadDemo			(int levelnum);
void		S_InitSaveDemo			(void);
void		S_SaveDemoData			(int levelnum);
void		S_FadeToBlack			(void);
void		S_NoFade				(void);
void		S_LoadingScreen			(int level_number);
void		S_Wait					(int nframes, int anykey);
int		S_PlayFMV				(char *fname, int sequence);
int		S_IntroFMV				(char *movie1, char *movie2);
SG_COL 		S_COLOUR				(int R,int G,int B);


#ifdef __cplusplus
}
#endif



#endif