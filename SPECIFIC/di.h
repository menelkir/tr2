#ifndef _DI_H
#define _DI_H


struct Joystick : GenericAdapter
	{
	};
global List<Joystick> G_JoystickList;
global Joystick G_Joystick;


bool DI_Create();
void DI_Release();
bool DI_Init();
void DI_Cleanup();
void DI_Start();
void DI_Finish();
void DI_SetupJoystick(HWND hWnd);

Position DI_FindPreferredJoystick(GUID* pGUID);
void DI_ReadKeyboard(unsigned char* keymap);
int DI_ReadJoystick(int& joy_x,int& joy_y);



#endif