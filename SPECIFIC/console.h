/*
	File	:	console.h
	Author	:	Richard Flower (c) 1998 Core Design
*/

#ifndef _CONSOLE_H_
#define _CONSOLE_H_


bool CSLaunch(HWND hWnd,LPDIRECTDRAW2 lpDD,char* Bitmap,LPDIRECTDRAWSURFACE3 lpDestSurface);
void CSDisplay();
void CSShutDown();
void ConsoleCursor();

void KeyToConsole(int nKey);
void SubmitCommand();
void ScrollConsole();
void ScrollConsole1();
void ConsoleOutput(int col,char* string);
void ClearCommandLine();
void ToggleConsole();
void ClearConsole();
void ClearConsoleCursor();
void ConsoleCursor();
void SetConsoleUser();
void ConsoleOutput1(int col,char* string);
extern "C" void __cdecl ConsolePrintf(char *fmt, ...);


#define DEF_COMMANDLEN		256

struct CONSOLE
{
	bool					bActivated;
	bool					bRunning;

	LPDIRECTDRAW2			lpDD;
	LPDIRECTDRAWSURFACE3	lpBackBuffer;
	LPDIRECTDRAWSURFACE3	lpConsoleSurface;
	LPDIRECTDRAWSURFACE3	lpBitmapSurface;

	int						w;
	int						h;
	int						rows;

	TEXTMETRIC				tm;
	HDC						hdc;
	
	char					CommandLine[DEF_COMMANDLEN];
	int						CommandCol;
	unsigned int			CommandLen;
	
};



extern CONSOLE Console;

#endif