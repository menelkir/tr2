/*
	File	:	directx.h
	Author	:	Richard Flower (c) 1998 Core Design
*/

#ifndef _DIRECTX_H
#define _DIRECTX_H

struct DXCONFIG
{
	int nDD;
	int nD3D;
	int nVMode;
	int nTexture;
	int bZBuffer;
};


struct DISPLAYMODE					// Display Mode Info
{
	int				w;				// Width
	int				h;				// Height
	unsigned long	bpp;			// Bits Per Pixel
	bool			bPalette;		// Is It Palettised
	unsigned char	rbpp;			// Red Bits Per Pixel
	unsigned char	gbpp;			// Green Bits Per Pixel
	unsigned char	bbpp;			// Blue Bits Per Pixel
	unsigned char	abpp;			// Alpha Bits Per Pixel
	DDSURFACEDESC	ddsd;			// Surface Description
	unsigned char	rshift;			// Shifts For Red 
	unsigned char	gshift;			// Shifts For Green
	unsigned char	bshift;			// Shifts For Blue
	unsigned char	ashift;			// Shifts For Alpha
};

struct D3DTEXTUREINFO				// Texture Format Info
{
	unsigned long	bpp;			// Bits Per Pixel (if Palettised)
	bool			bPalette;		// Is it Palettised
	bool			bAlpha;			// Does It Support Alpha
	unsigned char	rbpp;			// Red Bits Per Pixel 
	unsigned char	gbpp;			// Green Bits Per Pixel
	unsigned char	bbpp;			// Blue Bits Per Pixel
	unsigned char	abpp;			// Alpha Bits Per Pixel
	DDSURFACEDESC	ddsd;			// Texture Format Description
	DDPIXELFORMAT	ddpf;			// Texture Pixel Format
	unsigned char	rshift;			// Shifts For Red
	unsigned char	gshift;			// Shifts For Green
	unsigned char	bshift;			// Shifts For Blue
	unsigned char	ashift;			// Shifts For Alpha
};


struct DIRECT3DINFO
{
	char			Name[30];		// Name String
	char			About[80];		// Description String
	LPGUID			lpGuid;			// GUID
	GUID			Guid;		
	D3DDEVICEDESC	DeviceDesc;		// Device Description
	bool			bAlpha;			// Does It Support Alpha
	bool			bHardware;		// Is It Hardware
	int				nDisplayMode;	// Number Of Display Modes
	DISPLAYMODE*	DisplayMode;	// Ptr To Compatible Display Modes
	int				nTexture;		// Number Of Texture Formats
	D3DTEXTUREINFO*	Texture;		// Texture Info
};

struct DIRECTDRAWINFO				// DirectDraw Info
{
	char			Name[30];		// Name String   
    char			About[80];		// Description String
	LPGUID			lpGuid;			// GUID
	GUID			Guid;
	DDCAPS			DDCaps;			// Device Capabilites
	int				nDisplayMode;	// Number Of Display Modes	
	DISPLAYMODE*	DisplayMode;	// Ptr To Display Mode Structure
	int				nD3DInfo;		// Number Of D3D Drivers
	DIRECT3DINFO*	D3DInfo;		// Ptr To D3D Info Structure
};

struct DEVICEINFO					// DirectX Device Information
{	
	int				nDDInfo;		// Entries
	DIRECTDRAWINFO*	DDInfo;			// Ptr To Info Structure
};


// Function Declarations

void	DXGetDeviceInfo(DEVICEINFO* dd,HWND);
void	DXFreeDeviceInfo(DEVICEINFO* dd);
BOOL	CALLBACK DXEnumDirectDraw(GUID FAR* lpGuid,LPSTR lpDeviceDesc,LPSTR lpDeviceName,LPVOID lpContext);
HRESULT	CALLBACK DXEnumDisplayModes(LPDDSURFACEDESC lpddsd, LPVOID lpContext);
HRESULT	CALLBACK DXEnumDirect3D(LPGUID lpGuid,LPSTR lpDeviceDesc,LPSTR lpDeviceName,LPD3DDEVICEDESC lpHWDesc,LPD3DDEVICEDESC lpHELDesc,LPVOID lpContext);
HRESULT CALLBACK DXEnumTextureFormats(LPDDSURFACEDESC lpDDSD,LPVOID lpContext);
void	DXBitMask2ShiftCnt(unsigned long mask,unsigned char* shift,unsigned char* cnt);
DWORD	BPPToDDBD(int bpp);

bool	DXFindDevice(DEVICEINFO* dd,int w,int h,int bpp,bool hw,DXCONFIG* DXConfig);
bool	DXCreateDirectDraw(DEVICEINFO* dd,DXCONFIG* DXConfig,LPDIRECTDRAW2* lpDD2);
bool	DXCreateDirect3D(LPDIRECTDRAW2 lpDD2,LPDIRECT3D2* lpD3D2);
bool	DXSetCooperativeLevel(LPDIRECTDRAW2 lpDD2,HWND WindowHandle,int Flags);
bool	DXSetVideoMode(LPDIRECTDRAW2 lpDD2,int w,int h,int bpp);
bool	DXCreateSurface(LPDIRECTDRAW2 lpDD2,DDSURFACEDESC* ddsd,LPDIRECTDRAWSURFACE3* lpSurface);
bool	DXGetAttachedSurface(LPDIRECTDRAWSURFACE3 lpPrimary,DDSCAPS* ddscaps,LPDIRECTDRAWSURFACE3* lpAttached);
bool	DXAddAttachedSurface(LPDIRECTDRAWSURFACE3 lpSurface,LPDIRECTDRAWSURFACE3 lpAddSurface);
bool	DXCreateDirect3DDevice(LPDIRECT3D2 lpD3D2,GUID Guid,LPDIRECTDRAWSURFACE3 lpSurface,LPDIRECT3DDEVICE2* lpD3DDevice2);
bool	DXCreateViewPort(LPDIRECT3D2 lpD3D,LPDIRECT3DDEVICE2 lpD3DDevice,int w,int h,IDirect3DViewport2** lpViewport);
void	DXGetSurfaceDescription(LPDIRECTDRAWSURFACE3,DDSURFACEDESC*);

bool	DXUserDialog(DEVICEINFO*,DXCONFIG*,HINSTANCE);
BOOL	CALLBACK DXSetupDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
void	DXInitDialogBox(HWND);
void	DXInitD3DDrivers(HWND,int);
void	DXInitVideoModes(HWND hWnd, int nDD, int nD3D);
void	DXInitTextures(HWND hWnd, int nDD, int nD3D);

// Macro Declarations

#define DXRelease(ptr)			{if(ptr!=NULL) {ptr->Release(); ptr = NULL;}}
#define DXInit(dstruct)			{memset(&dstruct,0,sizeof(dstruct));dstruct.dwSize = sizeof(dstruct);}
#define DXDisplayMode(a,b)		a->DDInfo[b->nDD].D3DInfo[b->nD3D].DisplayMode[b->nVMode]  
#define DXD3DGuid(a,b)			a->DDInfo[b->nDD].D3DInfo[b->nD3D].Guid
#define DXD3D(a,b)				a->DDInfo[b->nDD].D3DInfo[b->nD3D]
#define DXD3DTexture(a,b)		a->DDInfo[b->nDD].D3DInfo[b->nD3D].Texture[b->nTexture]
#define DXDD(a,b)				a->DDInfo[b->nDD]

#define DXRGB(i,j,r,g,b)		r>>(8-DXDisplayMode(i,j).rbpp)<<(DXDisplayMode(i,j).rshift)|\
								g>>(8-DXDisplayMode(i,j).gbpp)<<(DXDisplayMode(i,j).gshift)|\
								b>>(8-DXDisplayMode(i,j).bbpp)<<(DXDisplayMode(i,j).bshift)		

#define DXRGBA(i,j,r,g,b,a)		r>>(8-DXDisplayMode(i,j).rbpp)<<(DXDisplayMode(i,j).rshift)|\
								g>>(8-DXDisplayMode(i,j).gbpp)<<(DXDisplayMode(i,j).gshift)|\
								b>>(8-DXDisplayMode(i,j).bbpp)<<(DXDisplayMode(i,j).bshift)|\
								a>>(8-DXDisplayMode(i,j).abpp)<<(DXDisplayMode(i,j).ashift)		

#define DXTEXTURERGB(i,j,r,g,b)	r>>(8-DXD3DTexture(i,j).rbpp)<<(DXD3DTexture(i,j).rshift)|\
								g>>(8-DXD3DTexture(i,j).gbpp)<<(DXD3DTexture(i,j).gshift)|\
								b>>(8-DXD3DTexture(i,j).bbpp)<<(DXD3DTexture(i,j).bshift)	

#define DXTEXTURERGBA(i,j,r,g,b,a)	r>>(8-DXD3DTexture(i,j).rbpp)<<(DXD3DTexture(i,j).rshift)|\
									g>>(8-DXD3DTexture(i,j).gbpp)<<(DXD3DTexture(i,j).gshift)|\
									b>>(8-DXD3DTexture(i,j).bbpp)<<(DXD3DTexture(i,j).bshift)|\
									a>>(8-DXD3DTexture(i,j).abpp)<<(DXD3DTexture(i,j).ashift)	









#endif

