#ifndef _OUTPUT_H
#define _OUTPUT_H


#include "..\game\room.h"


#ifdef __cplusplus
extern "C" {
#endif

#define SIPL_DONT_CLEAR_SCREEN 0
#define SIPL_CLEAR_SCREEN 1

extern int	hires_flag;

void S_CopyScreenToBuffer(void);
void S_InitialisePolyList(int tClearScreen);
void S_CopyBufferToScreen(void);
void S_OutputPolyList(void);
sint32 S_DumpScreen(void);
void S_DontDisplayPicture(void);
void ScreenPartialDump();
void FadeWait(void);
void ScreenDump(void);
void S_ClearScreen();
void		S_AnimateTextures		(int nframes);
void S_DrawSparks();

extern int	wibble_light[WIBBLE_SIZE][32];
extern void S_SetupBelowWater(int underwater);
extern void S_SetupAboveWater(int underwater);
void		S_PrintShadow			(sint16 size,sint16* bptr,ITEM_INFO* iptr,int unknown);



void		S_LightRoom				(ROOM_INFO* r);

void		S_CalculateStaticMeshLight(int x, int y, int z, int shade, int shadeB, ROOM_INFO *r);
void		S_InitialiseScreen		(int type);
void		S_OutputPolyList		(void);
int			S_GetObjectBounds		(sint16* bptr);
//void		S_CalculateStaticLight  (sint16 adder);
void		S_PrintObjectOutline	(sint16* bptr,sint16 color);
void		S_DrawHealthBar			(int percent);
void		S_DrawAirBar			(int percent);
void		S_InsertBackPolygon		(sint32 min_x,sint32 min_y,sint32 max_x,sint32 max_y,int colour);
void		S_AnimateTextures		(int nframes);
void		S_DrawLighteningSegment	(sint32 x1,sint32 y1,sint32 z1,sint32 x2,sint32 y2,sint32 z2,sint32 width);
void		S_SetupBelowWater		(int underwater);
void		S_SetupAboveWater		(int underwater);
void		S_DisplayPicture		(char* filename,int tRemapToGamePalette);
void		S_DontDisplayPicture	(void);
void		S_ConvertPicture		(void);
void		S_DrawSparks(void);
void		S_CalculateLightLara(sint32 x, sint32 y, sint32 z, sint16 room_number);
void		TriggerDynamic(long x, long y, long z,long falloff, long r, long g, long b);
void		ClearDynamics();
void		S_DrawFootPrints();


void DoSnow();


















#ifdef __cplusplus
	void ScreenClear(bool tWindow=false);
#endif

#ifdef __cplusplus
}
#endif


#endif