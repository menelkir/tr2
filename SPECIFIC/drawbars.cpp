#include "standard.h"
#include "global.h"
#include "hwrender.h"
#include "../game/inventry.h"
#include "hwinsert.h"


#define HealthBarX		8
#define HealthBarY		6
#define HealthBarW		100

#define DASHBAR_X		(game_setup.dump_width-110)
#define DASHBAR_Y		6

extern "C" void S_DrawDashBar(int);
extern "C" void S_DrawHealthBar(int);
extern "C" void S_DrawAirBar(int);



void S_DrawDashBar(int percent)
{
	int		x,y;
	int		w,z;
	char colour;

	HWR_EnableZBuffer(true,true);

	bBlueEffect=false;
	
	x = DASHBAR_X;
	y = DASHBAR_Y;
	w = (percent*HealthBarW)/100;

	z = phd_znear + 50;
	colour = (char)inv_colours[C_BLACK];
	InsertLine( x-2,y+1,x+HealthBarW+1,y+1,z,colour );
	InsertLine( x-2,y+2,x+HealthBarW+1,y+2,z,colour );
	InsertLine( x-2,y+3,x+HealthBarW+1,y+3,z,colour );
	InsertLine( x-2,y+4,x+HealthBarW+1,y+4,z,colour );
	InsertLine( x-2,y+5,x+HealthBarW+1,y+5,z,colour );
	InsertLine( x-2,y+6,x+HealthBarW+1,y+6,z,colour );
	InsertLine( x-2,y+7,x+HealthBarW+1,y+7,z,colour );

	z = phd_znear + 40;
	colour = (char)inv_colours[C_GREY];
	InsertLine( x-2,y+8,x+HealthBarW+2,y+8,z,colour );                // Bottom Line
	InsertLine( x+HealthBarW+2,y, x+HealthBarW+2,y+8,z,colour );     // Right Line

	z = phd_znear + 30;
	colour = (char)inv_colours[C_WHITE];
	InsertLine( x-2,y,x+HealthBarW+2,y,z,colour );					 // Top Line
	InsertLine( x-2,y+8,x-2,y,z,colour );                             // Left Line

	if ( w )
	{
		z = phd_znear + 20;
		colour = (char)inv_colours[C_WHITE];
		InsertLine( x,y+3, x+w,y+3,z,colour );

		colour = (char)inv_colours[C_DARKGREEN];
		InsertLine( x,y+2, x+w,y+2,z,colour );
		InsertLine( x,y+4, x+w,y+4,z,colour );
		InsertLine( x,y+5, x+w,y+5,z,colour );
		InsertLine( x,y+6, x+w,y+6,z,colour );
	}
}








	/*******************************************************************************
	*				Draw the Health Bar for PC
******************************************************************************/

void	S_DrawHealthBar( int percent )
	{
	int		x,y;
	int		w,z;
	char colour;

	HWR_EnableZBuffer(true,true);

	bBlueEffect=false;
	
	x = HealthBarX;
	y = HealthBarY;
	w = (percent*HealthBarW)/100;

	z = phd_znear + 50;
	colour = (char)inv_colours[C_BLACK];
	InsertLine( x-2,y+1,x+HealthBarW+1,y+1,z,colour );
	InsertLine( x-2,y+2,x+HealthBarW+1,y+2,z,colour );
	InsertLine( x-2,y+3,x+HealthBarW+1,y+3,z,colour );
	InsertLine( x-2,y+4,x+HealthBarW+1,y+4,z,colour );
	InsertLine( x-2,y+5,x+HealthBarW+1,y+5,z,colour );
	InsertLine( x-2,y+6,x+HealthBarW+1,y+6,z,colour );
	InsertLine( x-2,y+7,x+HealthBarW+1,y+7,z,colour );

	z = phd_znear + 40;
	colour = (char)inv_colours[C_GREY];
	InsertLine( x-2,y+8,x+HealthBarW+2,y+8,z,colour );                // Bottom Line
	InsertLine( x+HealthBarW+2,y, x+HealthBarW+2,y+8,z,colour );     // Right Line

	z = phd_znear + 30;
	colour = (char)inv_colours[C_WHITE];
	InsertLine( x-2,y,x+HealthBarW+2,y,z,colour );					 // Top Line
	InsertLine( x-2,y+8,x-2,y,z,colour );                             // Left Line

	if ( w )
		{
		z = phd_znear + 20;
		colour = (char)inv_colours[C_ORANGE];
		InsertLine( x,y+3, x+w,y+3,z,colour );

		colour = (char)inv_colours[C_RED];
		InsertLine( x,y+2, x+w,y+2,z,colour );
		InsertLine( x,y+4, x+w,y+4,z,colour );
		InsertLine( x,y+5, x+w,y+5,z,colour );
		InsertLine( x,y+6, x+w,y+6,z,colour );
		}
	}

/*******************************************************************************
*				Draw the Air Bar for PC
******************************************************************************/
#define AirBarX		(game_setup.dump_width-110)
#define AirBarY		6
#define AirBarW		100

void	S_DrawAirBar( int percent )
	{
	int		x,y;
	int		w,z;
	char colour;

	HWR_EnableZBuffer(true,true);

	bBlueEffect=false;
	
	x = AirBarX;
	y = AirBarY;
	w = (percent*AirBarW)/100;
	
	z = phd_znear + 50;
	colour = (char)inv_colours[C_BLACK];
	InsertLine( x-2,y+1,x+AirBarW+1,y+1,z,colour );
	InsertLine( x-2,y+2,x+AirBarW+1,y+2,z,colour );
	InsertLine( x-2,y+3,x+AirBarW+1,y+3,z,colour );
	InsertLine( x-2,y+4,x+AirBarW+1,y+4,z,colour );
	InsertLine( x-2,y+5,x+AirBarW+1,y+5,z,colour );
	InsertLine( x-2,y+6,x+AirBarW+1,y+6,z,colour );
	InsertLine( x-2,y+7,x+AirBarW+1,y+7,z,colour );
	
	z = phd_znear + 40;
	colour = (char)inv_colours[C_GREY];
	InsertLine( x-2,y+8,x+AirBarW+2,y+8,z,colour );             // Bottom Line
	InsertLine( x+AirBarW+2,y, x+AirBarW+2,y+8,z,colour );     // Right Line

	z = phd_znear + 30;
	colour = (char)inv_colours[C_WHITE];
	InsertLine( x-2,y,x+AirBarW+2,y,z,colour );			   	   // Top Line
	InsertLine( x-2,y+8,x-2,y,z,colour );                       // Left Line

	if ( percent>0 )
		{
		z = phd_znear + 20;
		colour = (char)inv_colours[C_WHITE];
		InsertLine( x,y+3, x+w,y+3,z,colour );

		colour = (char)inv_colours[C_BLUE];
		InsertLine( x,y+2, x+w,y+2,z,colour );
		InsertLine( x,y+4, x+w,y+4,z,colour );
		InsertLine( x,y+5, x+w,y+5,z,colour );
		InsertLine( x,y+6, x+w,y+6,z,colour );
		}
	}



void S_DrawPercentBar(int x,int y,int bw,int bh,int percent)
{
	int		w,z;
	short   rgb;
	short   rgb1;

	HWR_EnableZBuffer(true,true);

	bBlueEffect=false;

	w = (percent*bw)/100;
	x += phd_winxmin;
	y += phd_winymin;

	z = phd_znear + 50;
	
	rgb = 0x1f<<10;
	rgb1= 0x1f<<5;

	HWI_InsertAlphaSprite_Sorted(x+1,y+1,z,rgb1,
								 x+w,y+1,z,rgb,
								 x+w,y+bh-1,z,rgb,
								 x+1,y+bh-1,z,rgb1,-1,DRAW_TLV_GA,0);

	
	rgb = 0x1f;

	HWI_InsertAlphaSprite_Sorted(x+1,y+1,z,rgb,
								 x+bw-1,y+1,z,rgb,
								 x+bw-1,y+bh-1,z,rgb,
								 x+1,y+bh-1,z,rgb,-1,DRAW_TLV_GA,0);



	rgb = 0x1f<<10|0x1f<<5|0x1f;

	HWI_InsertAlphaSprite_Sorted(x,y,z,rgb,
								 x+bw,y,z,rgb,
								 x+bw,y+bh,z,rgb,
								 x,y+bh,z,rgb,-1,DRAW_TLV_GA,0);
}

