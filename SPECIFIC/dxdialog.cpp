/*
	File	:	dxdialog.cpp
	Author	:	Richard Flower (c) 1998 Core Design
	----------------------------------------------------------------------------------
	Functions
	----------------------------------------------------------------------------------

  




	----------------------------------------------------------------------------------
*/


//#define SOFTWAREDEFAULT
#define SWTEXTUREFORMAT	"555"
#define HWTEXTUREFORMAT	"5551"
#define SOFTWARERES     "320x200x16"

bool bSoftwareDefault = false;

#include "standard.h"
#include "directx.h"
#include "..\resource.h"
#include "winmain.h"


static DEVICEINFO* DeviceInfo;
static DXCONFIG* DXConfig;


/*
	Function	:	DXUserDialog
	Useage		:	Allow User Selection Of DEVICEINFO Structure
*/

bool DXUserDialog(DEVICEINFO* info,DXCONFIG* config,HINSTANCE hInstance)
{
	int RetVal;
	
	DeviceInfo = info;
	DXConfig = config;
	
	ShowCursor(true);
	
	RetVal = DialogBox(hInstance,MAKEINTRESOURCE(IDD_DXSETUP),0,DXSetupDlgProc);

	ShowCursor(false);
	
	if(RetVal == -1)
	{
		MessageBox(NULL,"Unable To Initialise Dialog","",MB_OK);
		return false;
	}

	if(RetVal == 0) return false;

	return true;
}

// EOF : DXUserDialog




BOOL CALLBACK DXSetupDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	switch(msg)
	{
		case WM_INITDIALOG:
			DXInitDialogBox(hWnd);
			return true;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDOK:
					DXConfig->nDD      = ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_DD_DEVICES));
					DXConfig->nD3D     = ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_D3D_DEVICES));
					DXConfig->nVMode   = ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_VIDEOMODES));
					DXConfig->nTexture = ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_TEXTUREFORMAT));
					DXConfig->bZBuffer = Button_GetCheck(GetDlgItem(hWnd,IDC_ZBUFFER));
					EndDialog(hWnd,1);
					return true;

				case IDCANCEL:
					EndDialog(hWnd,0);
					return true;
			
				case IDC_DD_DEVICES:
					switch(HIWORD(wParam))
					{
						case CBN_SELCHANGE:
							DXInitD3DDrivers(hWnd,ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_DD_DEVICES)));
							DXInitVideoModes(hWnd,ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_DD_DEVICES)),
												  ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_D3D_DEVICES)));					
							DXInitTextures(hWnd,ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_DD_DEVICES)),
							  	  			    ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_D3D_DEVICES)));					
							break;
					}
					break;

				case IDC_D3D_DEVICES:
					switch(HIWORD(wParam))
					{
						case CBN_SELCHANGE:
							DXInitVideoModes(hWnd,ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_DD_DEVICES)),
									  			  ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_D3D_DEVICES)));					
							DXInitTextures(hWnd,ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_DD_DEVICES)),
							  	  			    ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_D3D_DEVICES)));					

						break;
					}
					break;

			}
			break;
		
			
	}
	return false;
}

// EOF : DXSetupDlgProc


void DXInitDialogBox(HWND hWnd)
{
	HWND hComboBox;
	int n;
	char Buffer[80];

	hComboBox = GetDlgItem(hWnd,IDC_DD_DEVICES);

	// Fill DirectDraw Device Selection
	
	for(n=0;n<DeviceInfo->nDDInfo;n++)
	{		
		sprintf(Buffer,"%s (%s)",DeviceInfo->DDInfo[n].About,DeviceInfo->DDInfo[n].Name);	
		ComboBox_AddString(hComboBox,Buffer);
	}

	// Set Default Entry
	// Assume Second Card is 3DFX Plug Through, so Preferred..
	
#ifndef SOFTWAREDEFAULT
	int nDD = DeviceInfo->nDDInfo-1;
#else
	int nDD = 0;
#endif
	
	if(bSoftwareDefault) nDD = 0;


	ComboBox_SetCurSel(hComboBox,nDD);

	DXInitD3DDrivers(hWnd,nDD);
	DXInitVideoModes(hWnd,nDD,ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_D3D_DEVICES)));
	DXInitTextures(hWnd,nDD,ComboBox_GetCurSel(GetDlgItem(hWnd,IDC_D3D_DEVICES)));
}



void DXInitD3DDrivers(HWND hWnd,int nDD)
{
	HWND hD3D;
	HWND hZBuffer;
	int  n;
	char Buffer[80];
	char Buffer1[80];
	static int CurSel = -1;
	int	nHW;

	hD3D = GetDlgItem(hWnd,IDC_D3D_DEVICES);
	hZBuffer = GetDlgItem(hWnd,IDC_ZBUFFER);

	// Fill With D3D Drivers

	nHW = -1;

	if(CurSel != -1)
		ComboBox_GetLBText(hD3D,ComboBox_GetCurSel(hD3D),&Buffer1); 


	// Remove Existing Data
	ComboBox_ResetContent(hD3D);


	for(n=0;n<DeviceInfo->DDInfo[nDD].nD3DInfo;n++)
	{
		sprintf(Buffer,"%s",DeviceInfo->DDInfo[nDD].D3DInfo[n].About);
		ComboBox_AddString(hD3D,Buffer);

		// Temp	

#ifndef SOFTWAREDEFAULT		
		if(CurSel == -1 && ! bSoftwareDefault)
			if(DeviceInfo->DDInfo[nDD].D3DInfo[n].bHardware) nHW = n;
#endif
		if(!strcmp(Buffer1,Buffer))
				nHW = n;
	}


	if(CurSel == -1)
	{
		if(nHW != -1)
		{
			ComboBox_SetCurSel(hD3D,nHW);
			CurSel = nHW;
			Button_SetCheck(hZBuffer,true);
		}	
		else
		{
			ComboBox_SetCurSel(hD3D,0);
			CurSel = 0;
			Button_SetCheck(hZBuffer,false);
//			Button_SetCheck(hZBuffer,true);		// Temp
		}
	}
	else
	{
		if(nHW == -1)
		{
			ComboBox_SetCurSel(hD3D,0);
			CurSel = 0;
		}
		else
		{
			ComboBox_SetCurSel(hD3D,nHW);
			CurSel = nHW;
		}
	}	
}




void DXInitVideoModes(HWND hWnd, int nDD, int nD3D)
{
	HWND hVidMode;
	int n;
	char Buffer[20];
	char Buffer1[20];
	
#ifdef SOFTWAREDEFAULT	
	char Default[] = "512x384x8";
#else
	char Default[] = "640x480x16";
#endif	
	static int CurSel = -1;
	int nDef;

	if(bSoftwareDefault) strcpy(Default,SOFTWARERES);


	nDef = -1;
	
	hVidMode = GetDlgItem(hWnd,IDC_VIDEOMODES);

	if(CurSel != -1)
		ComboBox_GetLBText(hVidMode,ComboBox_GetCurSel(hVidMode),&Buffer1); 

	// Remove Existing Data
	ComboBox_ResetContent(hVidMode);
	
	// Fill With Video Modes Of Selected Device

	for(n=0;n<DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].nDisplayMode;n++)
	{
		sprintf(Buffer,"%dx%dx%d",DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].DisplayMode[n].w,
								  DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].DisplayMode[n].h,	
								  DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].DisplayMode[n].bpp);
		
		ComboBox_AddString(hVidMode,Buffer);
			
		if(CurSel == -1)
		{
			if(!strcmp(Default,Buffer)) 
				nDef = n;
		}
		else
		{
			if(!strcmp(Buffer1,Buffer))
				nDef = n;
		}
	}


	if(nDef != -1)
	{
		CurSel = nDef;
		ComboBox_SetCurSel(hVidMode,nDef);
	}
	else
	{
		CurSel = 0;
		ComboBox_SetCurSel(hVidMode,0);
	}

}

// EOF : DXGetVideoModes




void DXInitTextures(HWND hWnd, int nDD, int nD3D)
{
	HWND hTMode;
	int n;
	char Buffer[20];
	char Buffer1[20];

#ifdef SOFTWAREDEFAULT
	char Default[] = SWTEXTUREFORMAT;
#else
	char Default[] = HWTEXTUREFORMAT;
#endif	
	
	static int CurSel = -1;
	int nDef;


	if(bSoftwareDefault) strcpy(Default,SWTEXTUREFORMAT);


	nDef = -1;
	
	hTMode = GetDlgItem(hWnd,IDC_TEXTUREFORMAT);

	if(CurSel != -1)
		ComboBox_GetLBText(hTMode,ComboBox_GetCurSel(hTMode),&Buffer1); 

	// Remove Existing Data
	ComboBox_ResetContent(hTMode);
	
	// Fill With Video Modes Of Selected Device

	for(n=0;n<DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].nTexture;n++)
	{

		if(DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].bPalette == false)
		{
				if(DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].abpp != 0)
				{
					sprintf(Buffer,"%d%d%d%d",DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].rbpp,
											   DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].gbpp,	
											   DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].bbpp,
											   DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].abpp);

				}
				else
				{
					sprintf(Buffer,"%d%d%d",DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].rbpp,
											     DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].gbpp,	
											     DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].bbpp);
				}
		}	
		else
				sprintf(Buffer,"%d Bit",DeviceInfo->DDInfo[nDD].D3DInfo[nD3D].Texture[n].bpp);






		ComboBox_AddString(hTMode,Buffer);
			
		if(CurSel == -1)
		{
			if(!strcmp(Default,Buffer)) 
				nDef = n;
		}
		else
		{
			if(!strcmp(Buffer1,Buffer))
				nDef = n;
		}
	}


	if(nDef != -1)
	{
		CurSel = nDef;
		ComboBox_SetCurSel(hTMode,nDef);
	}
	else
	{
		CurSel = 0;
		ComboBox_SetCurSel(hTMode,0);
	}

}

