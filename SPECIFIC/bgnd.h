#ifndef _BGND_H_
#define _BGND_H_


bool BGND_Init(void);
void BGND_Make640x480(uint8* pImage,uint8* pPalette);
void BGND_Draw(void);
void BGND_Free(void);



#endif