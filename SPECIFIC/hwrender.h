#ifndef _HWRENDER_H_
#define _HWRENDER_H_


extern D3DTLVERTEX* CurrentTLVertex;
extern DWORD GhCurTextureHandle;
extern bool GtColorKeyEnabled;
extern DWORD GahTextureHandle[];

void HWR_EnableZBuffer(bool tWrite,bool tCompare);
void HWR_DrawRoomBucket(void* data);
void HWR_InitVertexList();
void HWR_SetCurrentTexture(DWORD dwHandle);
void HWR_EnableColorKey(bool tEnable);
void HWR_BeginScene();
void HWR_InitState();
void HWR_DrawPolyList(int num,sint32* sortptr);
void HWR_EndScene();
void HWR_GetAllTextureHandles();
bool HWR_Init();
void HWR_ResetCurrentTexture();
void HWR_LoadTexturePages(int nPages,char* pImage,uint8* pPalette);
void HWR_FreeTexturePages();
void HWR_ResetColorKey();
void HWR_DrawPolyListFB(int num,sint32* sortptr);
void HWR_DrawPolyListBF(int num,sint32* sortptr);

#define MAX_TLVERTICES 9216

extern D3DTLVERTEX VertexBuffer[MAX_TLVERTICES];

#endif