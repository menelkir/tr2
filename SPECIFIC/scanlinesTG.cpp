#include "standard.h"
#include "global.h"
#include "drawprimitive.h"
#include "scanlines.h"


#define PIXELOUT(x)	PIXELOUT555LOOKUP(x)


#define PIXELOUT555LOOKUP(x)  {unsigned long t;\
						unsigned short out;\
						texel = *(Texture + ((u>>16))+((v>>16)<<8));\
						t = (redleft>>16+3);\
						tr = ((texel>>5)&0x3e0)+t;\
						out = short(*(RGBTable+short(tr))<<10);\
						t  = (greenleft>>16+3);\
						tg = ((texel)&0x3e0)+t;\
						out|= short(*(RGBTable+tg)<<5);\
						t  = (blueleft>>16+3);\
						tb = ((texel<<5)&0x3e0)+t;\
						out|= short(*(RGBTable+tb));\
						*(pDestination+x) = (unsigned short) out;\
						u+=du;\
						v+=dv;\
						redleft+=dr;\
						greenleft+=dg;\
						blueleft+=db;}



#define PIXELOUT555MUL(x) {texel = *(Texture + ((u>>16))+((v>>16)<<8));\
					   tr = ((texel>>7))&0xf8;\
					   tg = ((texel>>2))&0xf8;\
					   tb = ((texel)<<3)&0xf8;\
					   ro = (short)(tr*(redleft>>16))>>1;\
					   go = (short)(tg*(greenleft>>16))>>6;\
					   bo = (short)(tb*(blueleft>>16))>>11;\
					   ro &= 0x7c00;\
					   go &= 0x03e0;\
					   bo &= 0x1f;\
					   v += dv;\
					   u += du;\
	 				   redleft += dr;\
					   greenleft += dg;\
					   blueleft += db;\
					   *(pDestination+x) = (unsigned short)(ro|go|bo);}


/*
	Function : ScanLinesTGAffine
	Useage	 : Textured Goruaud Affine 
*/

long subdivcnt;
long  u,v,du,dv;
short cnt;
long redleft,greenleft,blueleft;
long dr,dg,db;
unsigned short* pDestination;

#define PIXELOUTASM(x) _asm\
{\
	_asm mov eax,dword ptr [u]				/* eax : u					*/	\
	_asm sar eax,16							/* eax : u>>16				*/	\
	_asm mov ebx,dword ptr [v]				/* ebx : v					*/	\
	_asm sar ebx,8							/* ebx : v>>8				*/	\
	_asm mov edx,[redleft]					/* edx : redleft			*/	\
	_asm mov esi,[Texture]					/* esi : Texture			*/	\
	_asm and ebx,0xffffff00					/* ebx : (v>>8)&0xffffff00	*/	\
	_asm mov edi,[pDestination]				/* edi : pDestination       */	\
    _asm add ebx,eax						/* ebx : Texture Offset		*/	\
	_asm sar edx,19							/* edx : redleft>>19		*/	\
	_asm mov eax,[greenleft]				/* eax : greenleft			*/	\
	_asm mov ecx,[esi+(ebx*2)]				/* ecx : texel				*/	\
	_asm mov esi,[blueleft]					/* esi : blueleft			*/	\
	_asm sar eax,19							/* eax : greenleft>>19		*/	\
	_asm mov ebx,ecx						/* ebx : texel				*/	\
	_asm shr ecx,5							/* ecx : texel>>5			*/	\
	_asm mov ebp,[RGBTable]					/* ebp : RGBTable			*/  \
	_asm shl edx,1							/* edx : redleft<<1			*/	\
	_asm and ecx,0x3e0						/* ecx : (texel>>5)&0x3e0	*/	\
	_asm add ebp,edx						/* ecx : +redleft			*/	\
	_asm mov dx,word ptr [ebp+(ecx*2)]		/* edx : gouraud			*/	\
	_asm mov ebp,[RGBTable]					/* ebp : RGBTable			*/  \
    _asm shl edx,10							/* edx : r					*/	\
	_asm mov ecx,ebx						/* ecx : texel				*/	\
	_asm and ecx,0x3e0						/* ecx : texel&0x3e0		*/	\
	_asm shl eax,1							/* eax : greenleft<<1		*/	\
	_asm add ebp,eax						/* ecx : +greenleft			*/	\
	_asm mov cx,word ptr [ebp+(ecx*2)]		/* ecx : gouraud			*/	\
	_asm mov ebp,[RGBTable]					/* ebp : RGBTable			*/	\
	_asm shl ecx,5							/* ecx : g					*/	\
	_asm and ebx,0x1f						/* ebx : texel&0x1f			*/	\
	_asm sar esi,19							/* eax : blueleft>>19		*/	\
	_asm or  edx,ecx						/* edx : r | g				*/	\
	_asm shl ebx,5							/* ebx : texel<<5			*/	\
	_asm mov eax,dword ptr [du]				/* eax : du					*/ 	\
	_asm add ebx,esi						/* ebx : +blueleft			*/	\
	_asm mov ecx,dword ptr [u]				/* ecx : u					*/	\
	_asm mov si,word ptr [ebp+(ebx*2)]		/* esi : b					*/	\
	_asm mov ebx,dword ptr [dv]				/* ebx : dv					*/	\
	_asm or  edx,esi						/* edx : r | g | b			*/	\
  	_asm mov [edi+x],dx						/* Store Pixel				*/	\
    _asm mov edx,dword ptr [v]				/* edx : v					*/	\
  	_asm add ecx,eax						/* ecx : u+du				*/	\
	_asm add edx,ebx						/* edx : v+dv				*/	\
	_asm mov [u],ecx						/* Store ecx 				*/	\
	_asm mov [v],edx						/* Store edx				*/	\
	_asm mov eax,dword ptr [dr]				/* eax : dr					*/	\
	_asm mov ecx,dword ptr [redleft]		/* ecx : redleft			*/	\
	_asm mov esi,dword ptr [db]				/* esi : db					*/	\
	_asm mov edx,dword ptr [greenleft]		/* edx : greenleft			*/	\
	_asm mov ebx,dword ptr [dg]				/* ebx : dg					*/	\
	_asm mov edi,dword ptr [blueleft]		/* edi : blueleft			*/	\
	_asm add ecx,eax						/* ecx : redleft+dr			*/	\
	_asm mov [redleft],ecx					/* Store redleft+dr			*/	\
	_asm add edx,ebx						/* edx : greenleft+dg		*/	\
	_asm mov [greenleft],edx				/* Store greenleft+dg		*/	\
	_asm add edi,esi						/* edi : blueleft+db		*/	\
	_asm mov [blueleft],edi					/* Store blueleft+db		*/	\
}







void ScanLinesTGAffine(GRADIENT* Gradients,EDGE* pLeft,EDGE* pRight,EDGE* StepLeft,EDGE* StepRight)
{
	int Width;
//	unsigned short* pDestination;
//	long u,v,du,dv;
	long yline;
	float oowidth;
	long oowidthf;	
//	short subdivcnt;
//	long redleft,greenleft,blueleft;
//	long dr,dg,db;
	short texel;
	long ro,go,bo;
	long tr,tg,tb;
	double fconv;
	float mn = MAGICNUM;

	yline = long(Output)+YTable[pLeft->y];
	
	while(StepLeft->height--)
	{
		Width  = pRight->x;
		Width -= pLeft->x;

		if(Width > 0)
		{
			oowidth = WidthTable[Width];
			
			_asm
			{
				mov	edi,[pLeft]				// edi : pLeft
				mov esi,[pRight]			// esi : pRight
				mov ebx,[edi]pLeft.x		// ebx : pLeft->x
				mov eax,[yline]				// eax : yline
				shl	ebx,1					// ebx : pLeft->x<<1
				mov ecx,[edi]pLeft.uozf		// ecx : pLeft->uozf
				add eax,ebx					// eax : yline + pLeft->x<<1
				mov edx,[edi]pLeft.vozf		// edx : pLeft->vozf
				mov [pDestination],eax		// Store pDestination

				mov [u],ecx					// ecx : u
				mov [v],edx					// edx : v

				mov eax,[esi]pRight.uozf	// eax : pRight->uozf
				mov ebx,[esi]pRight.vozf	// ebx : pRight->vozf
				
				sub eax,ecx					// eax : pRight->uozf - u
				sub ebx,edx					// ebx : pRight->vozf - v

				mov [du],eax				// eax : Store du
				mov [dv],ebx				// ebx : Store dv

				mov eax,[edi]pLeft.rf		// eax : pLeft->rf
				mov ebx,[edi]pLeft.gf		// ebx : pLeft->gf
				mov ecx,[edi]pLeft.bf		// ecx : pLeft->bf
				
				mov edi,[esi]pRight.rf		// edi : pRight->rf
				mov [redleft],eax			// eax : Store redleft
				sub edi,eax					// edi : pRight->rf - redleft
				mov [greenleft],ebx			// ebx : Store greenleft
				mov [dr],edi				// edi : Store dr
				mov edi,[esi]pRight.gf		// edi : pRight->gf
				mov [blueleft],ecx			// ecx : Store blueleft
				sub edi,ebx					// edi : pRight->gf - greenleft
				mov eax,[esi]pRight.bf		// eax : pRight->bf
				mov [dg],edi				// edi : Store dg
				sub eax,ecx					// eax : pRight->bf - blueleft
				mov [db],eax				// eax : Store db
/*
											// 0		1		 2		  3		 4		  5		 6
				fld	 [mn]					// mn
				fld  [oowidth]				// ow		mn
				fild [du]					// du		ow		 mn
				fild [dv]					// dv		du		 ow		  mn
				fild [dr]					// dr		dv		 du		  ow	 mn
				fild [dg]					// dg		dr		 dv		  du	 ow		  mn
				fild [db]					// db		dg		 dr		  dv	 du		  ow	 mn
				fxch st(4)					// du		dg		 dr		  dv	 db		  ow	 mn
				fmul st(0),st(5)			// du*ow	dg		 dr		  dv	 db		  ow	 mn
				fxch st(4)					// db		dg		 dr		  dv	 du*ow	  ow	 mn
				fmul st(0),st(5)			// db*ow	dg		 dr		  dv	 du*ow	  ow	 mn
				fxch st(3)					// dv		dg		 dr		  db*ow	 du*ow	  ow	 mn					
				fmul st(0),st(5)			// dv*ow    dg		 dr		  db*ow	 du*ow	  ow	 mn
				fxch st(2)					// dr		dg		 dv*ow	  db*ow	 du*ow	  ow	 mn
				fmul st(0),st(5)			// dr*ow	dg		 dv*ow	  db*ow	 du*ow	  ow	 mn
				fxch st(1)					// dg		dr*ow	 dv*ow	  db*ow	 du*ow	  ow	 mn
				fmul st(0),st(5)			// dg*ow	dr*ow	 dv*ow	  db*ow	 du*ow	  ow	 mn
				fxch st(4)					// du*ow	dr*ow	 dv*ow	  db*ow	 dg*ow	  ow	 mn
				fadd st(0),st(6)			// du*ow+mn	dr*ow	 dv*ow	  db*ow	 dg*ow	  ow	 mn
				fxch st(1)					// dr*ow	du*ow+mn dv*ow	  db*ow	 dg*ow	  ow	 mn
				fadd st(0),st(6)			// dr*ow+mn	du*ow+mn dv*ow	  db*ow	 dg*ow	  ow	 mn
				fxch st(2)					// dv*ow	du*ow+mn dr*ow+mn db*ow	 dg*ow	  ow	 mn
				fadd st(0),st(6)			// dv*ow+mn	du*ow+mn dr*ow+mn db*ow	 dg*ow	  ow	 mn
*/
			}
/*			
			pDestination = (unsigned short*) yline + pLeft->x;

			u		  = pLeft->uozf;
			v		  = pLeft->vozf;
			redleft   = pLeft->rf;
			greenleft = pLeft->gf;
			blueleft  = pLeft->bf;
			du = (pRight->uozf - u);
			dv = (pRight->vozf - v);
			dr = (pRight->rf - redleft);			
			dg = (pRight->gf - greenleft);			
			db = (pRight->bf - blueleft);			
*/
		
			
			du = FTOL(float(du)*oowidth);
			dv = FTOL(float(dv)*oowidth);
			dr = FTOL(float(dr)*oowidth);
			dg = FTOL(float(dg)*oowidth);
			db = FTOL(float(db)*oowidth);


			subdivcnt = Width>>4;
			Width = Width-(subdivcnt<<4);

			_asm
			{

				push ebp						// ebp : Save Local Stack Ptr
Start:				
				mov	ecx,[subdivcnt]				// ecx : subdivcnt
				test ecx,ecx					// ecx : 0 ?
				je	Exit						// ecx : = 0 Exit
				dec ecx							// ecx : subdivcnt--
				mov [subdivcnt],ecx				// ecx : store subdivcnt
				
				PIXELOUTASM(0)
				PIXELOUTASM(2)
				PIXELOUTASM(4)
				PIXELOUTASM(6)
				PIXELOUTASM(8)
				PIXELOUTASM(10)
				PIXELOUTASM(12)
				PIXELOUTASM(14)
				PIXELOUTASM(16)
				PIXELOUTASM(18)
				PIXELOUTASM(20)
				PIXELOUTASM(22)
				PIXELOUTASM(24)
				PIXELOUTASM(26)
				PIXELOUTASM(28)
				PIXELOUTASM(30)

				mov edi,[pDestination]
				add edi,32
				mov [pDestination],edi
				
				jmp	Start						
Exit:
				pop ebp							// ebp : Restore Local Stack
		
			}
			
/*
			
			while(subdivcnt--)
			{
				
				PIXELOUT(0);
				PIXELOUT(1);
				PIXELOUT(2);
				PIXELOUT(3);
				PIXELOUT(4);
				PIXELOUT(5);
				PIXELOUT(6);
				PIXELOUT(7);
				PIXELOUT(8);
				PIXELOUT(9);
				PIXELOUT(10);
				PIXELOUT(11);
				PIXELOUT(12);
				PIXELOUT(13);
				PIXELOUT(14);
				PIXELOUT(15);

				pDestination += 16;
			}

*/
			_asm
			{
				push ebp			
			}
			
			switch(Width)
			{
				case 15:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 14:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 13:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 12:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 11:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 10:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 9:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 8:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 7:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 6:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 5:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 4:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 3:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 2:
					_asm
					{
						PIXELOUTASM(0)
						mov edx,[pDestination]
						add edx,2
						mov [pDestination],edx
					}
				case 1:
					_asm
					{
						PIXELOUTASM(0)
					}
			}

			_asm
			{
				pop	ebp
			}


		}

		yline += bytepitch;

		StepLeft->uozf += StepLeft->uozstepf;
		StepLeft->vozf += StepLeft->vozstepf;
		StepLeft->x    += StepLeft->xstep;
		StepLeft->y++;
		
		StepLeft->rf   += StepLeft->rstepf;
		StepLeft->gf   += StepLeft->gstepf;
		StepLeft->bf   += StepLeft->bstepf;

		StepLeft->ErrorTerm += StepLeft->Numerator;

		if(StepLeft->ErrorTerm >= StepLeft->Denominator)
		{
			StepLeft->x++;
			StepLeft->uozf += StepLeft->uozxtraf;
			StepLeft->vozf += StepLeft->vozxtraf;
			StepLeft->ErrorTerm -= StepLeft->Denominator;
	
			StepLeft->rf += StepLeft->rxtraf;
			StepLeft->gf += StepLeft->gxtraf;
			StepLeft->bf += StepLeft->bxtraf;
		}

		StepRight->x    += StepRight->xstep;
		StepRight->uozf += StepRight->uozstepf;
		StepRight->vozf += StepRight->vozstepf;
		StepRight->y++;

		StepRight->rf   += StepRight->rstepf;
		StepRight->gf   += StepRight->gstepf;
		StepRight->bf   += StepRight->bstepf;

		StepRight->ErrorTerm += StepRight->Numerator;

		if(StepRight->ErrorTerm >= StepRight->Denominator)
		{
			StepRight->x++;
			StepRight->uozf += StepRight->uozxtraf;
			StepRight->vozf += StepRight->vozxtraf;
			StepRight->ErrorTerm -= StepRight->Denominator;

			StepRight->rf += StepRight->rxtraf;
			StepRight->gf += StepRight->gxtraf;
			StepRight->bf += StepRight->bxtraf;
		}
	}
}


// EOF : ScanLinesTGAffine





/*
	Function : ScanLinesTG
	Useage	 : Textured Gouraud Perspective Correct
*/

void ScanLinesTG(GRADIENT* Gradients,EDGE* pLeft,EDGE* pRight,EDGE* StepLeft,EDGE* StepRight)
{
	int Width;
//	long u,v;
//	long du,dv,dr,dg,db;
//	unsigned short* pDestination;
	int subdivcnt;
	int pixelsleft;
	float oozr,uozr,vozr;
	float ZLeft,ULeft,VLeft;
	float ZRight,URight,VRight;
	float oozdx,uozdx,vozdx;
	float one = 1.0f;
	long yline;
	long rdx,gdx,bdx;
//	long redleft,greenleft,blueleft;
	long redright,greenright,blueright;
	short texel;
	long tr,tg,tb;
	short ro,go,bo;
	double fconv;


	oozdx = Gradients->dozx * float(SUBDIVSPAN);
	uozdx = Gradients->duzx * float(SUBDIVSPAN);
	vozdx = Gradients->dvzx * float(SUBDIVSPAN);

	rdx   = FTOLFIX(Gradients->drx * float(SUBDIVSPAN));
	gdx   = FTOLFIX(Gradients->dgx * float(SUBDIVSPAN));
	bdx   = FTOLFIX(Gradients->dbx * float(SUBDIVSPAN));

	yline = long(Output)+YTable[pLeft->y];

	while(StepLeft->height--)
	{
		ZLeft = pLeft->ooz;

		_asm
		{
			// 1.0f/pLeft->ooz
			fld  [one]
			fdiv [ZLeft]
		}
			
		pDestination = (unsigned short*) yline + pLeft->x;
		Width = (pRight->x - pLeft->x);

		redleft   = pLeft->rf;
		greenleft = pLeft->gf;
		blueleft  = pLeft->bf;

		_asm
		{
			// ZLeft = 1.0f/pLeft->ooz
			fstp [ZLeft]
		}

		oozr = pLeft->ooz+oozdx;
		uozr = pLeft->uoz+uozdx;
		vozr = pLeft->voz+vozdx;
		
		ULeft = pLeft->uoz * ZLeft;
		VLeft = pLeft->voz * ZLeft;

		if(Width > 0)
		{
			
			_asm
			{
				fld	 one
				fdiv [oozr]
			}

			subdivcnt  = Width>>SUBDIVSHIFT;				
			pixelsleft = Width-(subdivcnt<<SUBDIVSHIFT);

			redright   = redleft+rdx;
			greenright = greenleft+gdx;
			blueright  = blueleft+bdx;

			_asm
			{
				fstp [ZRight]
			}
			
			while(subdivcnt-- > 0)
			{

				URight = uozr * ZRight;
				VRight = vozr * ZRight;
								
			
				u  = FTOLFIX(ULeft);
				v  = FTOLFIX(VLeft);
				
				du = FTOLFIX(URight - ULeft)>>SUBDIVSHIFT;
				dv = FTOLFIX(VRight - VLeft)>>SUBDIVSHIFT;
			
				dr = (redright - redleft)>>SUBDIVSHIFT; 
				dg = (greenright - greenleft)>>SUBDIVSHIFT; 
				db = (blueright - blueleft)>>SUBDIVSHIFT; 

				oozr += oozdx;
			
				_asm
				{
					// one/oozr;
					fld	 [one]
					fdiv [oozr]

					push ebp						

					PIXELOUTASM(0)
					PIXELOUTASM(2)
					PIXELOUTASM(4)
					PIXELOUTASM(6)
					PIXELOUTASM(8)
					PIXELOUTASM(10)
					PIXELOUTASM(12)
					PIXELOUTASM(14)
					PIXELOUTASM(16)
					PIXELOUTASM(18)
					PIXELOUTASM(20)
					PIXELOUTASM(22)
					PIXELOUTASM(24)
					PIXELOUTASM(26)
					PIXELOUTASM(28)
					PIXELOUTASM(30)

					mov edi,[pDestination]
					add edi,32
					mov [pDestination],edi

					pop	ebp

					// ZRight = one/oozr
					fstp	[ZRight]		
				}

				ULeft = URight;
				VLeft = VRight;
				
				redleft   = redright;
				greenleft = greenright;
				blueleft  = blueright;

				vozr += vozdx;
				uozr += uozdx;

				redright   += rdx;
				greenright += gdx;
				blueright  += bdx;

			}
			
			if(pixelsleft)
			{
				u  = FTOLFIX(ULeft);
				v  = FTOLFIX(VLeft);
			
				if(--pixelsleft)
				{
					ZRight = 1.0f/(pRight->ooz - Gradients->dozx);
	
					URight = ZRight * (pRight->uoz - Gradients->duzx);
					VRight = ZRight * (pRight->voz - Gradients->dvzx);
					
					float opixf;
					opixf = WidthTable[pixelsleft];

					du = FTOLFIX(URight - ULeft);
					dv = FTOLFIX(VRight - VLeft);

					dr = (pRight->rf - StepLeft->rxtraf) - redleft;
					dg = (pRight->gf - StepLeft->gxtraf) - greenleft;
					db = (pRight->bf - StepLeft->bxtraf) - blueleft;

					du = FTOL(float(du)*opixf);
					dv = FTOL(float(dv)*opixf);
	
					dr = FTOL(float(dr)*opixf);
					dg = FTOL(float(dg)*opixf);
					db = FTOL(float(db)*opixf);
				
				}

				for(int n=0;n<=(pixelsleft);n++)
				{	
					PIXELOUT(0);
					pDestination++;
				}
  
			}
		
		}

		yline += bytepitch;

		StepLeft->x += StepLeft->xstep;
		StepLeft->y++;
		StepLeft->uoz += StepLeft->uozstep;
		StepLeft->voz += StepLeft->vozstep;
		StepLeft->ooz += StepLeft->oozstep;

		StepLeft->rf   += StepLeft->rstepf;
		StepLeft->gf   += StepLeft->gstepf;
		StepLeft->bf   += StepLeft->bstepf;

		StepLeft->ErrorTerm += StepLeft->Numerator;
	
		if(StepLeft->ErrorTerm >= StepLeft->Denominator)
		{
			StepLeft->x++;
			StepLeft->ooz += StepLeft->oozxtra;
			StepLeft->uoz += StepLeft->uozxtra;
			StepLeft->voz += StepLeft->vozxtra;
			StepLeft->ErrorTerm -= StepLeft->Denominator;

			StepLeft->rf += StepLeft->rxtraf;
			StepLeft->gf += StepLeft->gxtraf;
			StepLeft->bf += StepLeft->bxtraf;
		}


		StepRight->x += StepRight->xstep;
		StepRight->y++;
		StepRight->uoz += StepRight->uozstep;
		StepRight->voz += StepRight->vozstep;
		StepRight->ooz += StepRight->oozstep;

		StepRight->rf += StepRight->rstepf;
		StepRight->gf += StepRight->gstepf;
		StepRight->bf += StepRight->bstepf;

		StepRight->ErrorTerm += StepRight->Numerator;
	
		if(StepRight->ErrorTerm >= StepRight->Denominator)
		{
			StepRight->x++;
			StepRight->ooz += StepRight->oozxtra;
			StepRight->uoz += StepRight->uozxtra;
			StepRight->voz += StepRight->vozxtra;
			StepRight->ErrorTerm -= StepRight->Denominator;
			
			StepRight->rf += StepRight->rxtraf;
			StepRight->gf += StepRight->gxtraf;
			StepRight->bf += StepRight->bxtraf;
		}
	}
}

// EOF : ScanLinesTG
