#ifndef _TIME_H_
#define _TIME_H_

void TIME_Reset();
bool TIME_Init();
int Sync(void);

#endif