#ifndef _UTILS_H
#define _UTILS_H

void UT_MemBlt(uint8* pDest,int nDX,int nDY,int nDW,int nDH,int nDStride,uint8* pSrc,int nSX,int nSY,int nSStride);
	
bool UT_ExecAndWait(char* sProgramName,DWORD dwTimeout);
void* UT_GetResource(char* szResource,char* szType);
double UT_RDTSC();
#define DOUBLE_TO_DWORD_FRIG_FACTOR ((double)(4294967296+1048576*4294967296))
inline DWORD UT_DoubleToDWORD(double d)
	{
	double dT=d+DOUBLE_TO_DWORD_FRIG_FACTOR;
	return *((DWORD*)&dT);
	}
void UT_InitAccurateTimer();
double UT_GetAccurateTimer();
time_t UT_ConvertDateToTimeT(char* szTime,char* szDate);
void UT_CenterWindow(HWND hWnd);
char* UT_FindArg(char* arg);					// beware!! this is currently shit, it only compares arg with argv[1]...
bool UT_GetArg(char* szArg,int& rnValue);
void UT_ErrorBox(char* szString,HWND hWndOwner=0);
void UT_ErrorBox(int nID,HWND hWndOwner=0);
bool UT_OKCancelBox(char* szDlgResource,HWND hWndOwner);
char* UT_StringFromGUID(GUID& pGUID);
bool UT_GUIDFromString(char* pString,GUID& rGUID);
#define GUID_STRING_LENGTH (16*2+3+2)	// was 16*2+10+2
bool UT_SendMail(char* szTo,char* szSubject=0,char* szBody=0,bool tAllowLogonDialog=true);



#endif