
#ifndef _CONFUNCS_H
#define _CONFUNCS_H

void console_help(char*);
void console_vmode(char*);
void console_fov(char*);
void console_perspective(char*);
void console_dither(char*);
void console_filter(char*);
void console_shade(char*);
void console_fillmode(char*);
void console_fogcolor(char*);
void console_alpha(char*);
void console_showfcnt(char* param);

typedef struct
{	char *command_text;
	void (*command_ptr)(char*);
}CONSOLE_COMMAND;


CONSOLE_COMMAND Console_Functions[] = 
{
	"help",&console_help,
	"vmode",&console_vmode,
	"fov",&console_fov,
	"perspective",&console_perspective,
	"dither",&console_dither,
	"filter",&console_filter,
	"shade",&console_shade,
	"fillmode",&console_fillmode,
	"fogcolor",&console_fogcolor,
	/*"alpha",&console_alpha,*/
	"fps",&console_showfcnt,
};

#endif








