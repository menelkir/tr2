#ifndef _FILE_H_
#define _FILE_H_

#ifdef __cplusplus
extern "C" {
#endif


int			S_LoadTitle				(void);
int 		S_LoadLevelFile			( char *fname, int level_number, int type);
void 		S_UnloadLevelFile		(void);
void		S_AdjustTexelCoordinates(void);
int			S_ReloadLevelGraphics	(int tPalDPQ,int tTPages);
int			S_LoadCinematic			(int level_number);
int			S_FrontEndCheck			(void* data,int size);
int			S_LoadGame				(void* data,int size,int slot);
int			S_SaveGame				(void* data,int size,int slot);
int			S_LoadGameFlow			( char *filename );
void		build_ext				(char* fname,char* ext);
void		log_seq(sint16 *seq);
void	S_SaveDemoData( int level_number );
void	S_InitSaveDemo( void );





#ifdef __cplusplus
}
#endif



#endif