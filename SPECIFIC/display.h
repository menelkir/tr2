#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#ifdef __cplusplus
extern "C" {
#endif


void TempVideoRemove(void);
void		IncreaseScreenSize		();
void		DecreaseScreenSize		();
void		S_FadeOutInventory		(int fade);
void		S_FadeInInventory		(int fade);

#ifdef __cplusplus
}
#endif


#endif