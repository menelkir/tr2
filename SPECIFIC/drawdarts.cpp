#include "standard.h"
#include "global.h"
#include "winmain.h"
#include "hwrender.h"
#include "drawprimitive.h"



extern void mCalcPoint(long dx,long dy,long dz,long *result);
extern void ProjectPCoord(long x,long y,long z,long *result,long cx,long cy,long fov);
extern "C" void S_DrawDarts(ITEM_INFO*);


void S_DrawDarts(ITEM_INFO *item )
{
	long result[XYZ];
	long result2[XYZ];
	long x,y,z;
	long screencoords[XYZ];
	long screencoords2[XYZ];

	int sw = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).w;
	int sh = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).h;

	x = item->pos.x_pos;
	y = item->pos.y_pos;
	z = item->pos.z_pos;

	mCalcPoint(x, y, z, &result[0]);
	ProjectPCoord(result[X],result[Y],result[Z],screencoords,sw>>1,sh>>1,phd_persp);

	result[Z] >>= 3;

	switch (item->pos.y_rot )      		// Setup Position of Dart
	{
		case 0:
			z -= 64;
		break;

		case 16384:
			x -= 64;
		break;

		case -32768:
			z += 64;
		break;

		case -16384:
			x += 64;
		break;
	}

	mCalcPoint(x, y, z, &result2[0]);
	ProjectPCoord(result2[X],result2[Y],result2[Z],screencoords2,sw>>1,sh>>1,phd_persp);

	if (result[Z] > 2048 || result[Z] < 32 || (result2[Z]>>3) < 32 ||
		screencoords[X] < 0|| screencoords2[X] < 0 ||
		screencoords[X] > sw || screencoords2[X] > sw ||
		screencoords[Y] < 0 || screencoords2[Y] < 0		||
		screencoords[Y] > sh || screencoords2[Y] > sh)
	{
		return;
	}


	D3DTLVERTEX v[2];

	v[0].sx = float(screencoords[X]);	
	v[0].sy = float(screencoords[Y]);	
	v[0].sz = f_a-f_boo*(one/(float)(result2[Z]<<W2V_SHIFT));


	v[1].sx = float(screencoords2[X]);	
	v[1].sy = float(screencoords2[Y]);	
	v[1].sz = f_a-f_boo*(one/(float)(result2[Z]<<W2V_SHIFT));

	

	v[0].color = RGB_MAKE(0,0,0);
	v[1].color = RGB_MAKE(255,255,255);
	
	v[0].specular = 0;
	v[1].specular = 0;
	
	v[0].rhw = v[0].sz;
	v[1].rhw = v[1].sz;

	HWR_SetCurrentTexture(0);
	HWR_EnableZBuffer(true,true);
	DrawPrimitive(D3DPT_LINESTRIP,D3DVT_TLVERTEX,&v,2,D3DDP_DONOTCLIP);
	HWR_EnableZBuffer(false,false);
}
