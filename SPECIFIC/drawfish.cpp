#include "standard.h"
#include "global.h"
#include "winmain.h"
#include "hwinsert.h"
#include "../game/fish.h"
#include "../game/objects.h"



extern void mCalcPoint(long dx,long dy,long dz,long *result);
extern void ProjectPCoord(long x,long y,long z,long *result,long cx,long cy,long fov);
extern "C" void S_DrawFish(ITEM_INFO*);


void S_DrawFish(ITEM_INFO *item)
{
	long		lp,leader;
	long		xoff,yoff,zoff;
	unsigned short u1,v1,u2,v2;
	long		angle;
	long		result[XYZ];
	long		scr[3][XYZ];
	long		x,y,z,size;
	long		x1,x2,x3,y1,y2,y3,z1,z2,z3;
	FISH_INFO	*fptr;
	PHDSPRITESTRUCT *pSpriteInfo;

	int sw = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).w;
	int sh = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).h;
	int clipx,clipy;

	clipx = phd_winxmin+phd_winxmax;
	clipy = phd_winymin+phd_winymax;

	if (item->active == 0)
		return;

	leader = item->hit_points;
	if (leader == -1)
		return;
	if (!lead_info[leader].on)
		return;

	xoff = item->pos.x_pos;
	yoff = item->pos.y_pos;
	zoff = item->pos.z_pos;

	if (item->object_number == PIRAHNAS)
		pSpriteInfo=phdsprinfo+objects[EXPLOSION1].mesh_index+10;
	else
		pSpriteInfo=phdsprinfo+objects[EXPLOSION1].mesh_index+11;

	fptr = (FISH_INFO *) &fish[MAX_FISH+(leader*24)];

	for (lp=0;lp<24;lp++)
	{
		angle = (fptr->angle + 2048 + ((rcossin_tbl[fptr->swim<<7])>>5) ) & 4095;

		x = fptr->x + xoff;
		y = fptr->y + yoff;
		z = fptr->z + zoff;
		mCalcPoint(x,y,z,&result[0]);
		ProjectPCoord(result[X],result[Y],result[Z],&scr[0][0],sw>>1,sh>>1,phd_persp);

		size = 192;
		z += (((rcossin_tbl[(angle<<1)+1])*(size))>>12);
		x += -(((rcossin_tbl[(angle<<1)])*(size))>>12);
		y -= size;
	
		mCalcPoint(x,y,z,&result[0]);
		ProjectPCoord(result[X],result[Y],result[Z],&scr[1][0],sw>>1,sh>>1,phd_persp);

		y += size<<1;
		mCalcPoint(x,y,z,&result[0]);
		ProjectPCoord(result[X],result[Y],result[Z],&scr[2][0],sw>>1,sh>>1,phd_persp);

		x1 = scr[0][X];
		x2 = scr[1][X];
		x3 = scr[2][X];
		y1 = scr[0][Y];
		y2 = scr[1][Y];
		y3 = scr[2][Y];
		z1 = scr[0][Z];
		z2 = scr[1][Z];
		z3 = scr[2][Z];


		if ( (z1 > 0x5000) ||
			(z1 < 32 || z2 < 32 || z3 < 32 ) ||
			(x1 < phd_winxmin && x2 < phd_winxmin && x3 < phd_winxmin ) ||
			(x1 >= clipx && x2 >= clipx && x3 >= clipx ) ||
			(y1 < phd_winymin && y2 < phd_winymin && y3 < phd_winymin ) ||
			(y1 >= clipy && y2 >= clipy && y3 >= clipy ))
		{
			fptr++;
			continue;
		}

		if (angle<1024)
			angle -= 512;
		else if (angle<2048)
			angle -= 1536;
		else if (angle<3072)
			angle -= 2560;
		else
			angle -= 3584;

		if (angle > 512 || angle < 0)
			angle = 0;
		else	if (angle < 256)
			angle >>= 2;
		else
			angle = (512-angle) >> 2;

		angle += lp;
		if (angle > 128)
			angle = 128;

		z1 <<=W2V_SHIFT;
		z2 <<=W2V_SHIFT;
		z3 <<=W2V_SHIFT;

		PHD_VBUF v[3];
		PHDTEXTURESTRUCT Tex;

		int clipflag;
		int rgb;

		rgb = 80+angle;
		rgb = (rgb>>3)<<10|(rgb>>3)<<5|(rgb>>3);

		clipflag = 0;

		if(x1<phd_winxmin)
			clipflag++;
		else if(x1>clipx)
			clipflag += 2;
			
		if(y1<phd_winymin)
			clipflag += 4;
		else if(y1>clipy)
			clipflag += 8;

		v[0].xs  = float(x1);
		v[0].ys  = float(y1);
		v[0].ooz = (f_persp/(float)z1)*f_oneopersp;
		v[0].clip = clipflag;
		v[0].g = rgb;
				
		clipflag = 0;

		if(x2<phd_winxmin)
			clipflag++;
		else if(x2>clipx)
			clipflag += 2;
			
		if(y2<phd_winymin)
			clipflag += 4;
		else if(y2>clipy)
			clipflag += 8;

		v[1].xs = float(x2);
		v[1].ys = float(y2);
		v[1].ooz = (f_persp/(float)z2)*f_oneopersp;
		v[1].clip = clipflag;
		v[1].g = rgb;

		clipflag = 0;

		if(x3<phd_winxmin)
			clipflag++;
		else if(x3>clipx)
			clipflag += 2;
			
		if(y3<phd_winymin)
			clipflag += 4;
		else if(y3>clipy)
			clipflag += 8;

		v[2].xs = float(x3);
		v[2].ys = float(y3);
		v[2].ooz = (f_persp/(float)z2)*f_oneopersp;
		v[2].clip = clipflag;					
		v[2].g = rgb;


		v[0].zv = float(z1);
		v[1].zv = float(z2);
		v[2].zv = float(z3);

		u1 =((pSpriteInfo->offset<<8)&0xff00);
		v1 =(pSpriteInfo->offset&0xff00);
		u2 =u1+pSpriteInfo->width;
		v2 =v1+pSpriteInfo->height;
	
		u1+=App.nUVAdd;
		u2-=App.nUVAdd;
		v1+=App.nUVAdd;
		v2-=App.nUVAdd;

		Tex.drawtype = 1;
		Tex.tpage = pSpriteInfo->tpage;


		if (item->object_number == PIRAHNAS)
		{
			if (lp&1)
			{
				v[0].u = u1;
				v[0].v = v1;
				v[1].u = u2;
				v[1].v = v1;
				v[2].u = u1;
				v[2].v = v2;
			}
			else
			{
				v[0].u = u2;
				v[0].v = v2;
				v[1].u = u1;
				v[1].v = v2;
				v[2].u = u2;
				v[2].v = v1;
			}
		}
		else
		{
			if (leader&1)
			{
				v[0].u = u1;
				v[0].v = v1;
				v[1].u = u2;
				v[1].v = v1;
				v[2].u = u1;
				v[2].v = v2;
			}
			else
			{	
				v[0].u = u2;
				v[0].v = v2;
				v[1].u = u1;
				v[1].v = v2;
				v[2].u = u2;
				v[2].v = v1;
			}
		}

		HWI_InsertGT3_Sorted(&v[0],&v[1],&v[2],&Tex,&v[0].u,&v[1].u,&v[2].u,MID_SORT,1);

		fptr++;
	}
}