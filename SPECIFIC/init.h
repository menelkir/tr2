#ifndef _INIT_H_
#define _INIT_H_

#ifdef __cplusplus
extern "C" {
#endif

void *game_malloc(int size,int type);
void game_free(int size,int type);
int	 S_InitialiseSystem(void);
void ShutdownGame();
void init_game_malloc();
void CalculateWibbleTable();
void show_game_malloc_totals(void);


#ifdef __cplusplus
}
#endif


void InitZTable();
void InitUVTable();

#endif