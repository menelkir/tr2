/*





*/

#include "standard.h"
#include "global.h"
#include "directx.h"
#include "winmain.h"
#include "console.h"
#include "..\3dsystem\3Dglodef.h"


void console_vmode(char* param)
{
	int n;
	int mode;

	if(strlen(param) != 0)
	{
		sscanf(param,"%d",&mode);
		if(mode >= 0 && mode < DXD3D(App.DeviceInfoPtr,App.DXConfigPtr).nDisplayMode)		
		{
			App.bRender = false;
			App.DXConfig.nVMode = mode;
			CSShutDown();
		//	TX_FreeAllTextures();
			WinFreeDX();
			//WinDXInit(true);
			S_ReloadLevelGraphics(true,true);
			HWR_InitState();
			setup_screen_size();
			CSLaunch(App.WindowHandle,App.lpDD,"console.bmp",App.lpBackBuffer);
			App.bRender = true;
		}
	}

	for(n=0;n<DXD3D(App.DeviceInfoPtr,App.DXConfigPtr).nDisplayMode;n++)
	{
	
		if(App.DXConfig.nVMode == n)
			ConsolePrintf("%d - %dx%d <<",n,DXD3D(App.DeviceInfoPtr,App.DXConfigPtr).DisplayMode[n].w,
									        DXD3D(App.DeviceInfoPtr,App.DXConfigPtr).DisplayMode[n].h);
		else
			ConsolePrintf("%d - %dx%d",n,DXD3D(App.DeviceInfoPtr,App.DXConfigPtr).DisplayMode[n].w,
									     DXD3D(App.DeviceInfoPtr,App.DXConfigPtr).DisplayMode[n].h);
	
	}
}




void console_fov(char* param)
{
	int fov;
	if(strlen(param)!=0)
	{
		sscanf(param,"%d",&fov);
		AlterFOV(fov*ONE_DEGREE);
		ConsolePrintf("fov is %d",fov);
	}
	else
		ConsolePrintf("fov <n>");
}


void console_perspective(char* param)
{
	bool val;
	if(strlen(param)!=0)
	{
		sscanf(param,"%d",&val);
		if(val == true || val == false) 
		   HWConfig.Perspective = val;
	}
	ConsolePrintf("perspective is %d",HWConfig.Perspective);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_TEXTUREPERSPECTIVE,HWConfig.Perspective);
}

void console_dither(char* param)
{
	bool val;
	if(strlen(param)!=0)
	{
		sscanf(param,"%d",&val);
		if(val == true || val == false) 
		   HWConfig.Dither = val;
	}
	ConsolePrintf("dither is %d",HWConfig.Dither);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_DITHERENABLE,HWConfig.Dither);
}


void console_shade(char* param)
{
	int val;
	if(strlen(param)!=0)
	{
		sscanf(param,"%d",&val);
		if(val > 0 && val < 3) 
		   HWConfig.nShadeMode = val;
	}
	ConsolePrintf("Shade Mode is %d",HWConfig.nShadeMode);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_SHADEMODE,HWConfig.nShadeMode);
}


void console_fillmode(char* param)
{
	int val;
	if(strlen(param)!=0)
	{
		sscanf(param,"%d",&val);
		if(val > 0 && val < 4) 
		   HWConfig.nFillMode = val;
	}
	ConsolePrintf("Fill Mode is %d",HWConfig.nFillMode);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_FILLMODE,HWConfig.nFillMode);
}



void console_filter(char* param)
{
	int val;
	if(strlen(param)!=0)
	{
		sscanf(param,"%d",&val);
		if(val > 0 && val < 3) 
		   HWConfig.Filter = val;
	}
	ConsolePrintf("Filter is %d",HWConfig.Filter);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_TEXTUREMAG,HWConfig.Filter);
}


void console_fogcolor(char* param)
{

	int val;
	if(strlen(param)!=0)
	{
		sscanf(param,"%x",&val);
		HWConfig.FogColor = val;
	}
	ConsolePrintf("FogColor is %x",HWConfig.FogColor);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_FOGCOLOR,HWConfig.FogColor);
}



int sdest=2;
int ddest=2;
int decal=6;
void console_alpha(char* param)
{
	int a,b;
	if(strlen(param)!=0)
	{
		sscanf(param,"%d %d %d",&sdest,&ddest,&decal);
	}
	ConsolePrintf("alpha is s %d d %d m %d",sdest,ddest,decal);
}


bool bShowfcnt;

void console_showfcnt(char* param)
{
	bool val;
	if(strlen(param)!=0)
	{
		sscanf(param,"%d",&val);
		if(val == true || val == false) 
		   bShowfcnt = val;
	}
}

