#include "standard.h"
#include "global.h"
#include "winmain.h"
#include "hwrender.h"
#include "drawprimitive.h"
#include "../game/laraelec.h"
#include "../game/effects.h"
#include "../game/lara.h"
#include "../game/sphere.h"

extern "C" short 	electricity_points[][6];
extern "C"	uchar	lara_meshes[];
extern	"C" uchar	lara_last_points[];
extern	"C" uchar	lara_line_counts[];

bool ClipLine(long &x1,long &y1,long &x2,long &y2,int sw,int sh);


void LaraElectricDeath(long copy)
{
	PHD_VECTOR	pos1,pos2;
//	LINE_G2 	*lineg2;
	BITE_INFO	bite1,bite2;
//	short	*scrxy;	// X,Y
//	long	*scrz;	// Z
	short	distances[200];	// distances for rgbs.
	int dcnt;
	short	TempMesh[3];
	short	*leccy;
	long	dx,dy,dz;
	long	wx,wy,wz,tx,ty,tz,xv,yv,zv;
	long	lp,lp1;
	long screencoords[3*200];

//	mPushMatrix();
//	SetRotMatrix((MATRIX*) Matrix);
//	SetTransMatrix((MATRIX*) Matrix);
//	mTranslateXYZ(lara_item->pos.x_pos,lara_item->pos.y_pos,lara_item->pos.z_pos);

	int sw = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).w-1;
	int sh = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).h-1;
	
	phd_PushMatrix();
	phd_TranslateAbs(lara_item->pos.x_pos,lara_item->pos.y_pos,lara_item->pos.z_pos);

	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE,true);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_SRCBLEND,D3DBLEND_ONE);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_DESTBLEND,D3DBLEND_ONE);


//	scrxy = (short *) 0x1f800000;	// Set pointer to scratch pad.
//	scrz = (long *) 0x1f800180;	// Set pointer to scratch pad.
//	distances = (short *) 0x1f8002c0;	// Set pointer to scratch pad.
//	TempMesh = (short *) 0x1f8003f0;	// Set pointer to scratch pad.
	dcnt = 0;

	for (lp=0;lp<14;lp++)
	{
		leccy = &electricity_points[(lp*5)&15][0];

		bite1.mesh_num = lara_meshes[(lp<<1)];
		bite2.mesh_num = lara_meshes[(lp<<1)+1];

		pos1.y = 0;
		if (copy)
		{
			pos1.z = -48;
			pos1.x = -48;
		}
		else
		{
			pos1.x = 48;
			pos1.z = 48;
		}

		GetJointAbsPosition(lara_item, &pos1, bite1.mesh_num );

		pos2.y = 0;
		if (!lara_last_points[lp] || lp==13)
		{
			if (copy)
			{
				pos2.z = -48;
				pos2.x = -48;
			}
			else
			{
				pos2.z = 48;
				pos2.x = 48;
			}
			if (lp==13)
				pos2.y = -64;
		}
		else
			pos2.x = pos2.z = 0;

		GetJointAbsPosition(lara_item, &pos2, bite2.mesh_num );

		pos1.x -= lara_item->pos.x_pos;
		pos1.y -= lara_item->pos.y_pos;
		pos1.z -= lara_item->pos.z_pos;
		pos2.x -= lara_item->pos.x_pos;
		pos2.y -= lara_item->pos.y_pos;
		pos2.z -= lara_item->pos.z_pos;

		wx = pos1.x;
		wy = pos1.y;
		wz = pos1.z;
		dx = (pos2.x - pos1.x) >> 2;
		dy = (pos2.y - pos1.y) >> 2;
		dz = (pos2.z - pos1.z) >> 2;

		for (lp1=0;lp1<5;lp1++)
		{
			if (lp1 == 4 && !lara_last_points[lp])
				break;
			else if (lp1 == 4 && lara_last_points[lp])	// Last point?
			{
				TempMesh[0] = pos2.x;
				TempMesh[1] = pos2.y;
				TempMesh[2] = pos2.z;
			}
			else
			{
				TempMesh[0] = wx;
				TempMesh[1] = wy;
				TempMesh[2] = wz;
			}

			if (lp1 != 0 && lp1 != 4)
			{
				tx = *leccy++;
				ty = *leccy++;
				tz = *leccy++;

				if (copy)
				{
					TempMesh[0] -= tx >> 3;
					TempMesh[1] -= ty >> 3;
					TempMesh[2] -= tz >> 3;
				}
				else
				{
					TempMesh[0] += tx >> 3;
					TempMesh[1] += ty >> 3;
					TempMesh[2] += tz >> 3;
				}
				leccy+=3;

				xv = abs(tx);
				yv = abs(ty);
				zv = abs(tz);
				if (yv > xv)
					xv = yv;
				if (zv > xv)
					xv = zv;

				distances[dcnt] = xv;
			
			}
			else
			{
				distances[dcnt] = 0;
			}
						
//			gte_ldv0(TempMesh);
//			gte_rtps();
						
			sint32		*mptr;
			mptr = phd_mxptr; 
			long x1,y1,z1;
			

			x1 = (mptr[M00] * TempMesh[0] + mptr[M01] * TempMesh[1] + mptr[M02] * TempMesh[2] + mptr[M03]);
			y1 = (mptr[M10] * TempMesh[0] + mptr[M11] * TempMesh[1] + mptr[M12] * TempMesh[2] + mptr[M13]);
			z1 = (mptr[M20] * TempMesh[0] + mptr[M21] * TempMesh[1] + mptr[M22] * TempMesh[2] + mptr[M23]);
		
			float zv = f_persp/(float)z1;

			screencoords[(dcnt*3)+0] = short(float(x1 * zv + f_centerx));
			screencoords[(dcnt*3)+1] = short(float(y1 * zv + f_centery));
			screencoords[(dcnt*3)+2] = z1>>W2V_SHIFT;
		
			dcnt++;
		
			wx += dx;
			wy += dy;
			wz += dz;

//			gte_stsxy(scrxy);
//			gte_stsz(scrz);
//			scrxy+=2;
//			scrz++;
		}
	}

//	scrxy = (short *) 0x1f800000;	// Set pointer to scratch pad.
//	scrz = (long *) 0x1f800180;	// Set pointer to scratch pad.
//	distances = (short *) 0x1f8002c0;	// Set pointer to scratch pad.

	dcnt = 0;
	
	for (lp=0;lp<6;lp++)
	{
		for (lp1=0;lp1<lara_line_counts[lp];lp1++)        	// Electricity on lara.
		{
			long	x1,x2,y1,y2,z1,rgb,rgb1;

//			x1 = *scrxy++;
//			y1 = *scrxy++;
//			x2 = *scrxy;
//			y2 = *(scrxy+1);
//			z1 = (*scrz++)>>3;
//			rgb = *distances++;
//			rgb1 = *distances;

			x1 = screencoords[(dcnt*3)+0];
			y1 = screencoords[(dcnt*3)+1];
			
			x2 = screencoords[(dcnt*3)+3];
			y2 = screencoords[(dcnt*3)+4];

			z1 = screencoords[(dcnt*3)+2];

			rgb  = distances[(dcnt)+0];
			rgb1 = distances[(dcnt)+1];

			dcnt++;

			if (z1 < 32)
				continue;

/*
			
			lineg2 = (LINE_G2 *)db.polyptr;
			setlen(lineg2, 4);
			setcode(lineg2, 0x52);
*/
			if (rgb > 255)
			{
				rgb = 511-rgb;
				if (rgb < 0)
					rgb = 0;
			}

			if (rgb1 > 255)
			{
				rgb1 = 511-rgb1;
				if (rgb1 < 0)
					rgb1 = 0;
			}

			if (copy)
			{
				rgb >>= 1;
				rgb1 >>= 1;
			}

			if(ClipLine(x1,y1,x2,y2,sw,sh))
			{
				if(x1 >= 0 && x1 <= sw && y1 >= 0 && y1 <= sh && x2 >= 0 && x2 <= sw && y2 >= 0 && y2 <= sh)
				{
	

			D3DTLVERTEX v[2];

			v[0].sx = float(x1);	
			v[0].sy = float(y1);	
			v[0].sz = f_a-f_boo*(one/(float)(z1<<W2V_SHIFT));

		
			v[1].sx = float(x2);	
			v[1].sy = float(y2);	
			v[1].sz = f_a-f_boo*(one/(float)(z1<<W2V_SHIFT));


			v[0].color = RGB_MAKE(0,rgb,rgb);
			v[1].color = RGB_MAKE(0,rgb1,rgb1);
	
			v[0].specular = 0;
			v[1].specular = 0;
	
			v[0].rhw = v[0].sz;
			v[1].rhw = v[1].sz;

			HWR_SetCurrentTexture(0);
			HWR_EnableZBuffer(true,true);
		
		
			DrawPrimitive(D3DPT_LINESTRIP,D3DVT_TLVERTEX,&v,2,D3DDP_DONOTCLIP);
			HWR_EnableZBuffer(false,false);
		

				}
			}

		}

//		distances++;
///		scrxy+=2;
//		scrz++;
		dcnt++;
	
	}

//	mPopMatrix();

	phd_PopMatrix();

	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_SRCBLEND,D3DBLEND_SRCALPHA);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_DESTBLEND,D3DBLEND_INVSRCALPHA);
	App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE,false);
}




