#include "standard.h"
#include "global.h"
#include "winmain.h"
#include "hwinsert.h"
#include "hwrender.h"
#include "drawprimitive.h"



extern void mCalcPoint(long dx,long dy,long dz,long *result);
extern void ProjectPCoord(long x,long y,long z,long *result,long cx,long cy,long fov);
extern "C" void DoRain();
extern "C" LONG IsRoomOutside(LONG x, LONG y, LONG z);
extern "C" PHD_VECTOR CamRot;
extern "C" PHD_VECTOR CamPos;

LONG RainYAdd=1024;
LONG RainAngDiv=1023;
LONG RainAngAdd=512;
LONG scale;


void DoRain()
{
	long		lp;
	long		dir,angle;
	long		result[XYZ];
	long		screencoords[XYZ];
	long		result2[XYZ];
	long		screencoords2[XYZ];
	long		x,y,z,radius;

	D3DTLVERTEX	Lines[128*2];
	int			LineCnt;		
	long distance = 3072;
	
	int sw = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).w;
	int sh = DXDisplayMode(App.DeviceInfoPtr,App.DXConfigPtr).h;

	dir = CamRot.y;

	if (CamRot.x < 2048)
	{
		scale = CamRot.x;

		RainYAdd = ((2048 * scale) >> 10) + 1024;
		RainAngDiv = ((3072 * scale) >> 10) + 1024;
		RainAngAdd = ((1536 * scale) >> 10) + 512;

		distance -= CamRot.x << 2;
	}
	else
	{
		scale = -(3072-CamRot.x);

		RainYAdd = ((2048 * scale) >> 10) - 1024;
		RainAngDiv = 2048 - ((1024 * scale) >> 10);
		RainAngAdd = 1024 - ((512 * scale) >> 10);

		distance -= (4096 - CamRot.x) << 2;
	}


	LineCnt = 0;

	for ( lp=0 ; lp<128 ; lp++ )
	{
		y = -(rand() & 2047)+RainYAdd;

		radius = (rand()&4095) + (distance) - 2048;
		angle = ((dir + (rand()%RainAngDiv) - RainAngAdd) & 4095);

		z = (((rcossin_tbl[(angle<<1)+1])*radius)>>12);
		x = -(((rcossin_tbl[(angle<<1)])*radius)>>12);

		if(IsRoomOutside(CamPos.x+x, CamPos.y+y, CamPos.z+z))
		{
			mCalcPoint(x+CamPos.x, y+CamPos.y, z+CamPos.z, &result[0]);
			ProjectPCoord(result[X],result[Y],result[Z],screencoords,sw>>1,sh>>1,320);

			result[Z] >>= 3;

  			y += -((rand()&31)+32);

			mCalcPoint(x+CamPos.x, y+CamPos.y, z+CamPos.z, &result2[0]);
			ProjectPCoord(result2[X],result2[Y],result2[Z],screencoords2,sw>>1,sh>>1,320);

			if (result[Z] > 0x5000 || result[Z] < 32 || (result2[Z]>>3) < 32 ||
				screencoords[X] < 0 || screencoords2[X] < 0 ||
				screencoords[X] > sw || screencoords2[X] > sw ||
				screencoords[Y] < 0 || screencoords2[Y] < 0		||
				screencoords[Y] > sh || screencoords2[Y] > sh)
			{
				continue;
			}


			int c = (rand()&24);

			Lines[0+(LineCnt)].sx = float(screencoords[X]);	
			Lines[0+(LineCnt)].sy = float(screencoords[Y]);	
			Lines[0+(LineCnt)].sz = f_a-f_boo*(one/(float)(result2[Z]<<W2V_SHIFT));
			
			Lines[1+(LineCnt)].sx = float(screencoords2[X]);	
			Lines[1+(LineCnt)].sy = float(screencoords2[Y]);	
			Lines[1+(LineCnt)].sz = f_a-f_boo*(one/(float)(result2[Z]<<W2V_SHIFT));
						
			Lines[1+(LineCnt)].color = RGBA_MAKE(32+c,48+c,48+c,0xff);
			Lines[0+(LineCnt)].color = RGBA_MAKE(0,0,0,0);
			Lines[0+(LineCnt)].specular = 0;
			Lines[1+(LineCnt)].specular = 0;
			Lines[0+(LineCnt)].rhw = 0;
			Lines[1+(LineCnt)].rhw = 0;

			LineCnt+=2;

		}

	}
	
	if(LineCnt > 0)
	{
	
		HWR_SetCurrentTexture(0);
			
		App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE,true);
		App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_SRCBLEND,D3DBLEND_SRCALPHA);
		App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_DESTBLEND,D3DBLEND_INVSRCALPHA);
		DrawPrimitive(D3DPT_LINELIST,D3DVT_TLVERTEX,&Lines,LineCnt,D3DDP_DONOTCLIP);
		App.lpD3DDevice->SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE,false);
	}
}
