#ifndef _TYPEDEFS_H_INCLUDED
#define _TYPEDEFS_H_INCLUDED

typedef unsigned char	uint8;
typedef signed char		sint8;
typedef unsigned short	uint16;
typedef signed short	sint16;
typedef unsigned long	uint32;
typedef signed long		sint32;

#ifndef NULL
#define NULL ((void*)0)
#endif

#endif

