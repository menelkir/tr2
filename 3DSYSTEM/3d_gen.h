#ifndef _3D_GEN_H_
#define _3D_GEN_H_


#ifdef __cplusplus
extern "C" {
#endif


#include "..\game\items.h"

void S_InsertBackground(sint16* objptr);
void S_InsertRoom(sint16* objptr, int outside);
void S_InsertInvBgnd(sint16 *objptr);




extern sint32 outside;


#ifdef GAMEDEBUG
extern int RoomVerts;
extern int ObjVerts;
#endif


#ifdef __cplusplus
}
#endif



#endif