//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TombRaid.rc
//
#define IDD_DXSETUP                     101
#define IDD_INSERT_CD                   102
#define IDB_BITMAP1                     109
#define IDD_DEMODIALOG                  110
#define IDB_BITMAP2                     114
#define IDI_ICON                        115
#define IDC_DD_DEVICES                  1000
#define IDC_D3D_DEVICES                 1001
#define IDC_VIDEOMODES                  1002
#define IDC_TEXTUREFORMAT               1003
#define IDC_ZBUFFER                     1004
#define IDC_DITHER                      1005
#define IDC_FILTER                      1006
#define IDC_HWRACCEL                    1007
#define IDC_HWREMUL                     1008
#define IDC_8BIT                        1009
#define IDC_DS_DEVICES                  1010
#define IDC_DI_DEVICES                  1011
#define IDC_AGP                         1012
#define IDC_JDISABLE                    1014
#define IDC_SDISABLE                    1015
#define IDC_PROCESSOR                   1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        116
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
