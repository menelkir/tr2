/****************************************************************************
*
* BGENV.H
*
* PROGRAMMER : Chibsy
*    VERSION : 00.00
*    CREATED : 19/12/95
*   MODIFIED : 19/12/95
*       TABS : 4 8
*
* DESCRIPTION
*	Drawing environment structure definition
*
* HISTORY
*
*****************************************************************************/

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#include <sys\types.h>
#include <libgte.h>
#include <libgpu.h>
#include "types.h"

/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/

#define PAL

#define SCN_W	320	//512
#ifdef PAL
#define SCN_H	256
#else
#define SCN_H	240
#endif

#ifdef PAL
#define FixAspectRatio(n)   (n = n-(n>>2))
#else
#define FixAspectRatio(n)   (n = n-(n>>2))
#endif

#define OTSIZE		2560
#define NUM_POLYS	2048

/*---------------------------------------------------------------------------
 *	Type Definitions
\*--------------------------------------------------------------------------*/

typedef struct {
	POLY_GT4	*primptr;				/* ptr to next free primitive */
	ULONG    	ot[OTSIZE];			/* ordering table */
	DRAWENV	draw;				/* drawing environment */
	DISPENV	disp;				/* display environment */
	POLY_GT4	polyGT4[NUM_POLYS];		/* primitive list */
}BGENV;

extern	BGENV *Env;			// current drawing environment
extern	BGENV db[2];			// double-buffered drawing environments

extern	POLY_GT3 *polygt3p;
extern	POLY_FT3 *polyft3p;
extern	POLY_GT4 *polygt4p;
extern	POLY_FT4 *polyft4p;


