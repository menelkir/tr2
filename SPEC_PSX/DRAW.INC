;****************************************************************************
;
; DRAW.INC
;
; PROGRAMMER : Chris
;    VERSION : 00.00
;    CREATED : 20/02/98
;   MODIFIED : 20/02/98
;       TABS : 8 9 8
;
; DESCRIPTION
;	Draw equates & definitions
;
;****************************************************************************

;----------------------------------------------------------------------------
; Constants
;----------------------------------------------------------------------------

NTSC		equ	1

		if	NTSC
SCREEN_WIDTH	equ	512
SCREEN_HEIGHT	equ	240
		else
SCREEN_WIDTH	equ	512
SCREEN_HEIGHT	equ	256
		endif

CLIPX		equ	SCREEN_WIDTH-1
CLIPY		equ	SCREEN_HEIGHT-1
CLIPZ		equ	$5000

OTSIZE		equ	2562
DPQ_START	equ	(12*1024)

GT3_LEN		equ	9
GT4_LEN		equ	12
FT3_LEN		equ	7
FT4_LEN		equ	9
F3_LEN		equ	4
F4_LEN		equ	5

;----------------------------------------------------------------------------
; Type Definitions
;----------------------------------------------------------------------------

; -------- POLY_GT3/4 structure

		rsreset
gtTag		rw	1
gtRGB0		rw	1
gtX0		rh	1
gtY0		rh	1
gtU0		rb	1
gtV0		rb	1
gtClut		rh	1
gtRGB1		rw	1
gtX1		rh	1
gtY1		rh	1
gtU1		rb	1
gtV1		rb	1
gtTpage		rh	1
gtRGB2		rw	1
gtX2		rh	1
gtY2		rh	1
gtU2		rb	1
gtV2		rb	1
gtPad2		rh	1
SIZEOF_GT3	rb	0
gtRGB3		rw	1
gtX3		rh	1
gtY3		rh	1
gtU3		rb	1
gtV3		rb	1
gtPad3		rh	1
SIZEOF_GT4	rb	0

; -------- POLY_FT3/4 structure

		rsreset
ftTag		rw	1
ftRGB0		rw	1
ftX0		rh	1
ftY0		rh	1
ftU0		rb	1
ftV0		rb	1
ftClut		rh	1
ftX1		rh	1
ftY1		rh	1
ftU1		rb	1
ftV1		rb	1
ftTpage		rh	1
ftX2		rh	1
ftY2		rh	1
ftU2		rb	1
ftV2		rb	1
ftPad1		rh	1
SIZEOF_FT3	rb	0
ftX3		rh	1
ftY3		rh	1
ftU3		rb	1
ftV3		rb	1
ftPad2		rh	1
SIZEOF_FT4	rb	0

; -------- POLY_F3/4 structure

		rsreset
fTag		rw	1
fRGB0		rw	1
fX0		rh	1
fY0		rh	1
fX1		rh	1
fY1		rh	1
fX2		rh	1
fY2		rh	1
SIZEOF_F3	rb	0
fX3		rh	1
fY3		rh	1
SIZEOF_F4	rb	0

		rsreset
dbCurrentBuffer	rw	1	; active buffer
dbOT		rw	1	; OT ptr
dbPrimPtr	rw	1	; primitive ptr
dbPrimBase	rw	1	; active primitive list start
dbPrimEnd	rw	1	; active primitive list end
dbOTSize	rw	1	; # OT entries
dbPrimSize	rw	1	; # primitives
dbOTS		rw	2	; OT addresses
dbPoly		rw	2	; poly buffer addresses
dbDraw		rb	0	; DRAWENV
dbDisp		rb	0	; DISPENV
DB_STRUCT	rb	0

