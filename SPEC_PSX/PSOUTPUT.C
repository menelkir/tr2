/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#include <sys\types.h>
#include <libsn.h>
#include "typedefs.h"
#include "cd.h"
#include "gpu.h"
#include "file.h"
#include "3d_obj.h"
#include "..\game\savegame.h"
#include "..\game\lot.h"

#if 0
/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/

#define ANIMATED_TEXTURE 5

/*---------------------------------------------------------------------------
 *	Globals
\*--------------------------------------------------------------------------*/

sint16* aranges;
sint32 water_effect=0;	// ## these can go
sint32 shade_effect=0;

/*---------------------------------------------------------------------------
 *	Public Functions
\*--------------------------------------------------------------------------*/

void	S_SetupBelowWater(int underwater)
{
	water_effect = 1;
	shade_effect = 1;	// -(wibble_offset+1);	// 1..64 but masked so it's ok
}

void	S_SetupAboveWater(int underwater)
{
	water_effect = underwater;
	shade_effect = 0;
}

void	S_AnimateTextures(int nframes)
{
	static int frame_comp=0;

	frame_comp += nframes;

	while (frame_comp > ANIMATED_TEXTURE)
	{
		int i, j;
		WORD *ptr;
		MMTEXTURE tmp1;

		ptr = aranges;

		for ( i=(int)*(ptr++) ; i>0 ; i--,ptr++ )	// Get number of Ranges
		{
			j = (int)*(ptr++);					// Get number of frames-1

			tmp1 = RoomTextInfo[*(ptr)];

			for ( ; j>0 ; j--,ptr++ )
				RoomTextInfo[*(ptr)] = RoomTextInfo[*(ptr+1)];
			RoomTextInfo[*(ptr)] = tmp1;
		}
		frame_comp -= ANIMATED_TEXTURE;
	}
}

void S_InsertInvBgnd(sint16 *objptr)
{
	phd_left = phd_top = 0;
	phd_right = phd_winxmax;
	phd_bottom = phd_winymax;

	phd_PutPolygons_AtBack(objptr, -1, 1);
}

static int OnIdle(PFNIDLECHECKROUTINE pfnIdleCheckRoutine)
	{
//	OptimiseOTagR(db.ot,db.nOTSize);
	return OnIdleLOTUpdate(pfnIdleCheckRoutine);
	//return (*pfnIdleCheckRoutine)();
	}

sint32 S_DumpScreen(void)
	{
	int nFrames;

	nFrames=GPU_FlipNoIdle();
//	nFrames=GPU_Flip(OnIdle);

#ifdef GAMEDEBUG
//	FntFlush(-1);
#endif
#ifdef CREATE_MOVIE
	DoScreenShotter();
#endif

#ifndef CD_LOAD
#ifndef EZORAY
	pollhost();
#endif
#endif
	//SPU_UpdateStatus();

	return nFrames;
	}

sint32 S_DumpScreenCine(void)
	{
	int nFrames;

	nFrames=GPU_FlipNoIdle();

#ifdef GAMEDEBUG
//	FntFlush(-1);
#endif
#ifdef CREATE_MOVIE
	DoScreenShotter();
#endif
#ifndef CD_LOAD
#ifndef EZORAY
	pollhost();
#endif
#endif
	//SPU_UpdateStatus();

	return nFrames;
	}

#if 0
void S_InitialiseScreen(int type)
	{
	phd_InitWindow(GAME_FOV);
	}
#endif

void S_InitialisePolyList(void)
{
//	int nY;

	GPU_BeginScene();

//	nY=GPU_GetDrawRect(BUF_NEXT)->y;		// draw rect of frame during which this stuff will be drawn...

//	dude_envmap_tpage=getTPage(2,0,128,nY)<<16;

//	dude_envmap_tpage=getTPage(2,0,384,0)<<16;
}

#if 0
void S_OutputPolyList(void)
{
	GPU_EndScene();
}
#endif

#endif

typedef struct {
	sint16 Rate;
	sint16 Len;
	sint16 Lev;
	sint16 Acc;
	sint16 Dec;
	sint16 Sus;
	sint16 Flag;
	sint16 Vib;
}VIBRATION;

VIBRATION vib[2];
extern UBYTE Motors[2];
extern UBYTE DualShock;

void SetupPadVibration(sint16 num, sint16 acc, sint16 lev, sint16 sus, sint16 dec, sint16 len)
{
	VIBRATION *v = &vib[num];

	v->Acc = acc;
	v->Lev = lev;
	v->Sus = len-sus;
	v->Dec = dec;
	v->Len = len;
	v->Flag = 0;
	v->Rate = 0;
	v->Vib = 0;
}

void VibratePad()
{
	int i;
	VIBRATION *v = &vib[0];

	if (!DualShock || !VibrateOn)
		Motors[0] = Motors[1] = 0;
	else
	{
		for ( i=0 ; i<2 ; i++,v++ )
		{
			if (v->Len)
			{
				if (v->Flag == 0)
				{
					if ((v->Rate += v->Acc) >= v->Lev)
					{
						v->Rate = v->Lev;
						v->Flag = 1;
					}
				}

				else if (v->Flag == 1)
				{
					if (v->Len <= v->Sus)
						v->Flag = 2;
				}

				else if (v->Flag == 2)
				{
					if ((v->Rate -= v->Dec) < 0)
					{
						v->Rate = 0;
						v->Flag = 3;
					}
				}

				v->Vib += v->Rate;

				if (i)
				{
					if (v->Vib >> 8)
					{
						Motors[1] = v->Vib >> 8;
						v->Vib &= 0xff;
					}
					else
						Motors[1] = 0;
				}
				else
				{
					if (v->Vib >> 12)
					{
						Motors[0] = 1;
						v->Vib -= 4096;
					}
					else
						Motors[0] = 0;
				}

				v->Len--;
			}
			else
				Motors[i] = 0;
		}
	}
}


