/****************************************************************************
*
* NEWSHIT.C
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 13/08/97
*   MODIFIED : 13/08/97
*	   TABS : 3
*
* DESCRIPTION
*
*****************************************************************************/

/*---------------------------------------------------------------------------
 * Import Headers
\*--------------------------------------------------------------------------*/

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <libetc.h>
#include <libgte.h>
#include "types.h"

/*---------------------------------------------------------------------------
 * Constants
\*--------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 * Externals
\*--------------------------------------------------------------------------*/

extern WORD rcossin_tbl[];

/*---------------------------------------------------------------------------
 * Globals
\*--------------------------------------------------------------------------*/

typedef struct
{
	BYTE	 shimmer;
	BYTE	 choppy;
	UBYTE random;
	UBYTE abs;
} WATERTAB;

UBYTE char_anim;
UBYTE OurSqrt[1024];

WATERTAB	WaterTable[22][64];

/*---------------------------------------------------------------------------
 * Locals
\*--------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 * Macros
\*--------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 * Local Functions
\*--------------------------------------------------------------------------*/

static UWORD GetRandom(WATERTAB *w, int n)
{
	int i;
	UWORD random;

	do {
		random  = rand() & 0xfc;

		for ( i=0 ; i<n ; i++ )
		{
			if (w[i].random == random)
				break;
		}

		if (i == n)
			return random;

	} while(1);

	return 0;		// to stop warning
}

/*---------------------------------------------------------------------------
 * Public Functions
\*--------------------------------------------------------------------------*/

void	init_water_table()
{
	long	lp;

	srand(121197);

	for ( lp=0 ; lp<64 ; lp++ )
	{
		int i, j, k;
		WORD sin = rcossin_tbl[(lp<<7)];

	/* -------- underwater/swamp table */

		WaterTable[0][lp].shimmer = (sin*63) >> (12+3);
		WaterTable[0][lp].choppy  = (sin*16) >> 12;
		WaterTable[0][lp].random  = GetRandom(&WaterTable[0][0], lp);
		WaterTable[0][lp].abs	 = 0;

	/* -------- mist tables */

		WaterTable[1][lp].shimmer = (sin*32) >> (12+3);
		WaterTable[1][lp].choppy  = 0;
		WaterTable[1][lp].random  = GetRandom(&WaterTable[1][0], lp);
		WaterTable[1][lp].abs	 = -3;

		WaterTable[2][lp].shimmer = (sin*64) >> (12+3);
		WaterTable[2][lp].choppy  = 0;
		WaterTable[2][lp].random  = GetRandom(&WaterTable[2][0], lp);
		WaterTable[2][lp].abs	 = 0;

		WaterTable[3][lp].shimmer = (sin*96) >> (12+3);
		WaterTable[3][lp].choppy  = 0;
		WaterTable[3][lp].random  = GetRandom(&WaterTable[3][0], lp);
		WaterTable[3][lp].abs	 = 4;

		WaterTable[4][lp].shimmer = (sin*127) >> (12+3);
		WaterTable[4][lp].choppy  = 0;
		WaterTable[4][lp].random  = GetRandom(&WaterTable[4][0], lp);
		WaterTable[4][lp].abs	 = 8;

	/* -------- shimmer/ripple tables */

		for ( i=0,j=5 ; i<4 ; i++,j+=4 )
		{
			for ( k=0 ; k<4 ; k++ )
			{
				static UBYTE off[4] = { 4, 8, 12, 16 };
				static WORD shim[4] = { 31, 63, 95, 127 };
				static WORD chop[4] = { 16, 53, 90, 127 };

				WaterTable[j+k][lp].shimmer = -((sin*shim[k]) >> 15);
				WaterTable[j+k][lp].choppy  = (sin*chop[i]) >> 12;
				WaterTable[j+k][lp].random  = GetRandom(&WaterTable[j+k][0], lp);
				WaterTable[j+k][lp].abs	   = off[k];
			}
		}
	}
}

void init_square_root()
{
	int i;

	for ( i=0 ; i<1024 ; i++ )
		OurSqrt[i] = mSqrt(i);
}

