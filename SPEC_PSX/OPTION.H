/****************************************************************************
*
* OPTION.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 05/06/98
*   MODIFIED : 05/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for OPTION.C
*
*****************************************************************************/

#ifndef _OPTION_H
#define _OPTION_H

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#include "typedefs.h"
#include "..\game\invdata.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

extern void DisplayDeselectText();
extern void RemoveDeselectText();
extern void DisplayContinueText();
extern void RemoveContinueText();
extern void DisplayArrowText(RING_INFO *ring);
extern void RemoveArrowText();
extern void DisplaySelectText();
extern void RemovePromptText();

#endif
