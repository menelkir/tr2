#include "stypes.h"
#include "..\game\items.h"
#include "..\game\room.h"
#include "..\game\camera.h"
#include "..\game\control.h"
#include "..\game\input.h"
#include "..\game\text.h"
#include "include.h"

/*** defines ***/
//#define LOG

#define PIC_WIDTH			512
#define PIC_HEIGHT  		256
#define MAX_PACKED_SIZE		170000
#define PIC_SIZE	(MAX_PACKED_SIZE + ((PIC_WIDTH*PIC_HEIGHT)*2))

#define PIC_VRAMX	512
#define PIC_VRAMY	256


int	game_paused=0;

/*** locals ***/
local int LnFadeValue=FADE_MAX;
local int LnFadeTarget=FADE_MAX;
local int LnFadeDelta=32768;
local RECT LrcPic={PIC_VRAMX,PIC_VRAMY,PIC_WIDTH,PIC_HEIGHT};

/*** macros ***/
#ifdef LOG
#define Log_(format,args...) DebugOut(format "\n" , ## args)
#else
#define Log_(args...)
#endif

/*** prototypes ***/

/*** code ***/
sint32	S_DisplayPicture(char *szFileName)
{
	char* pPacked; 			// packed picture...
	char* pUnpacked;		// unpacked picture...

	Log_("S_DisplayPicture(\"%s\")",szFileName);

	GPU_SetHorizontalResolution(512);		// all pics are 512 now ... need to sort it out...

	pPacked=game_malloc(PIC_SIZE,LOAD_PICCY_BUFFER);
	pUnpacked=pPacked+MAX_PACKED_SIZE;

	if (szFileName[0]=='!')		// what is this all about?
		return(0);

	FILE_Load(szFileName,pPacked);
	unpack(pPacked,pUnpacked);

#ifdef NTSC
	LrcPic.h = 240;
	LoadImage(&LrcPic,(u_long *)(pUnpacked+(PIC_WIDTH*8*2)));
#else
	LoadImage(&LrcPic,(u_long *)pUnpacked);
#endif
	DrawSync(0);

	game_free(PIC_SIZE,LOAD_PICCY_BUFFER);

	return 1;
}


sint32	S_DisplayPictureMem(char *pPacked)
	{
//	char* pPacked; 			// packed picture...
	char* pUnpacked;		// unpacked picture...

	Log_("S_DisplayPictureMem(%x)",(unsigned)pPacked);

	GPU_SetHorizontalResolution(512);		// all pics are 512 ... need to sort it out...

	pUnpacked=game_malloc(((PIC_WIDTH*PIC_HEIGHT)*2),LOAD_PICCY_BUFFER);
	unpack(pPacked,pUnpacked);

#ifdef NTSC
	LoadImage(&LrcPic,(u_long *)(pUnpacked+(PIC_WIDTH*8*2)));
#else
	LoadImage(&LrcPic,(u_long *)pUnpacked);
#endif
	DrawSync(0);

	game_free(((PIC_WIDTH*PIC_HEIGHT)*2),LOAD_PICCY_BUFFER);

	return 1;
	}

void S_DrawPic(void)
	{
	DR_TPAGE* p1;
	SPRT* p;
	u_char fademe;
	uint32* pOT;

	if (!fades_enabled)
		return;

	if (LnFadeValue!=LnFadeTarget)
		LnFadeValue+=LnFadeDelta;

	fademe=(128-(LnFadeValue>>12));

	pOT=db.ot+(db.nOTSize-1);

	p=(SPRT*)db.polyptr;
	setSprt(p);
	setShadeTex(p,0);
	p->r0=p->g0=p->b0=fademe;
	p->x0=p->y0=p->u0=p->v0=0;
	setWH(p,256,MY_PSX_H);
	addPrim(pOT,p);

	p1=(DR_TPAGE*)(p+1);
	setDrawTPage(p1,1,1,(getTPage(TP_16BIT,ABR_DONTCARE,PIC_VRAMX,PIC_VRAMY)));
	addPrim(pOT,p1);

	p=(SPRT*)(p1+1);
	setSprt(p);
	setShadeTex(p,0);
	p->r0=p->g0=p->b0=fademe;
	p->x0=256;
	p->y0=p->u0=p->v0=0;
//##	setWH(p,(GtHiresFrig?256:128),MY_PSX_H);
	setWH(p, 256, MY_PSX_H);

	addPrim(pOT,p);

	p1=(DR_TPAGE*)(p+1);
	setDrawTPage(p1,1,1,(getTPage(TP_16BIT,ABR_DONTCARE,PIC_VRAMX+256,PIC_VRAMY)));
	addPrim(pOT,p1);

	db.polyptr=(char*)(p1+1);
	}

void S_DisplayPictureBuffer(void)
	{
	S_InitialisePolyList();
	S_DrawPic();
	S_OutputPolyList();
	S_DumpScreen();
	}

void S_WaitFade(void)
	{
	if (!fades_enabled)
		return;

	Log_(__FUNCTION__);

	do
		S_DisplayPictureBuffer();
	while (LnFadeValue!=LnFadeTarget);
	S_DisplayPictureBuffer();				// make both buffers identical
	}

void S_InitFade(int nSource,int nDest,int tCopyBuf)
	{
	if (!fades_enabled)
		return;

	Log_(__FUNCTION__);

	SetGeomScreen(phd_persp);			// DUDE says: what the fuck is this doing in here?

	LnFadeValue=nSource;
	LnFadeTarget=nDest;
	if (LnFadeTarget>LnFadeValue)
		LnFadeDelta=32768;
	else
		LnFadeDelta=-32768;

//	if ( LnFadeValue==FADE_MIN ) printf("FADE from MIN"); else
//	if ( LnFadeValue==FADE_MID ) printf("FADE from MID"); else
//	if ( LnFadeValue==FADE_MAX ) printf("FADE from MAX");
//	if ( LnFadeTarget==FADE_MIN ) printf(" to MIN\n"); else
//	if ( LnFadeTarget==FADE_MID ) printf(" to MID\n"); else
//	if ( LnFadeTarget==FADE_MAX ) printf(" to MAX\n");

	if (tCopyBuf)
		{
		//db.current_buffer^=1; 			// frig
		S_CopyScreenToBuffer();			// copy current screen to buffer....
		}
	}


void S_FadeToBlack(void)
	{
	Log_(__FUNCTION__);
	if (!fades_enabled)
		return;

	S_InitFade(FADE_MAX,FADE_MIN,1);
	S_WaitFade();
	}


void S_FadeUp(void)
	{
	Log_(__FUNCTION__);
	if (!fades_enabled)
		return;

	S_InitFade(FADE_MIN,FADE_MAX,0);
	S_WaitFade();
	}


void S_FinishInventory(void)
	{
//	S_WaitFade();
	}


void S_ClearScreen(void)
	{
	}


void S_CopyScreenToBuffer(void)
	{
	RECT* pRect;

	if (!fades_enabled)
		return;

	Log_(__FUNCTION__);

	pRect=GPU_GetDrawRect(BUF_CURRENT);		// rect of draw currently in progress...
	DrawSync(0);
	MoveImage(pRect,PIC_VRAMX,PIC_VRAMY);
	DrawSync(0);
	}


void S_SetGamma(sint16 wank)
	{
	}


void S_SetBackgroundGamma(sint16 wank)
	{
	}


void S_CopyBufferToScreen( void )
	{
	}



void S_LoadingScreen(int nLevel)
	{
	}


void S_Wait(int nFrames)
	{
	int nTargetFrame=GnFrameCounter+nFrames;
	while (GnFrameCounter<nTargetFrame)
		;
	}


int S_PlayFMV(int sequence,int flag)
	{
	return(S_PlayFMVfile(GF_fmvfilenames[sequence],sequence,flag));
	}


int S_PlayFMVfile(char *fname,int sequence,int flag)
	{
#if !defined(CD_LOAD) || !defined(PLAY_FMV) || defined(PSX_SONY_DEMO_SHELL) || defined(TEST_SONY_DEMO_SHELL)
	return 0;
#else
	char *filename;
	int start_frme;
	int end_frme;

	if ( sequence >= MAX_FMV_FILES )
	{
		#ifdef DELS_DEBUG_STUFF
		printf("unlucky... trying to play fmv sequence #%d (not implemented yet)\n",sequence);
		#endif
		return(-1);
	}

	filename = GF_fmvfilenames[sequence];
	start_frme = GF_fmv_data[sequence].startframe;
	end_frme = GF_fmv_data[sequence].endframe;

	#ifdef DELS_DEBUG_STUFF
	printf("trying to play fmv file '%s' (sequence %d)\n",filename,sequence);
	#endif

	S_CDStop();
	return( FMV_Play(filename,start_frme,end_frme) );
#endif
	}



/*-----------------08-29-96 03:06pm-----------------
--------------------------------------------------*/
extern sint32 inputDB;

void	S_DEL_Pause( void )
{
int	cnt;
int quitme;
int nojoy;
int nojoytotal;
uint32		padd;
TEXTSTRING *pausemessage=NULL, *nojoymessage=NULL;
#if defined(NTSC) && !defined(JAPAN)
int	reset_option=0,choice;
#endif

	game_paused = 1;
//	fades_enabled = 1;

	nojoytotal=0;
	SOUND_Stop();
	S_CDPause();									// also pause CD tracks

//	S_InitFade(FADE_MAX,FADE_MID,1);
//	S_WaitFade();

	camera.number_frames=2;

	do
	{
		nojoy = 0;
		S_UpdateInput();

		if (!GoodData(&GPad1) || GetType(&GPad1)!=CTL_PAD)		// read the pad...
		{
   			padd = 0;											// error reading pad...
			nojoy = 1;
			if (nojoymessage==NULL)
			{
  				nojoymessage = T_Print(0,-40,0, GF_PSXStrings[PSSTR_NOJOY_TEXT] );
   				T_CentreH(nojoymessage, 1);
   				T_BottomAlign(nojoymessage, 1);
			}
			nojoytotal = 1;
		}
  		else
		{
   			padd = (~GPad1.data.pad)&0xffff;					// grab the pad data...
			if ( nojoytotal==1 )
				nojoytotal=2;
		}

		S_InitialisePolyList();
		DrawGameInfo(0);
		DrawRooms(camera.pos.room_number);
		S_OutputPolyList();
		camera.number_frames = S_DumpScreen();

#if defined(NTSC) && !defined(JAPAN)
		if ( input&IN_OPTION && !reset_option)
			reset_option = 1;

		if (reset_option==1)
		{
			inputDB=GetDebouncedInput(input);
			if ((choice=PauseResetRequester())>0)
			{
				if (choice==2)
					reset_option = 2;
				else
				{
					reset_option = 0;
					input|=IN_PAUSE;
				}
			}
		}
		if (reset_option==2)
		{
			inputDB=GetDebouncedInput(input);
			if ((choice=SureRequester())>0)
			{
				if (choice==1)
					reset_flag = 1;
				else
					input|=IN_PAUSE;
				reset_option = 0;
			}
		}
#endif


//		S_InitialisePolyList();
//		S_CopyBufferToScreen();
//		S_DrawPic(); 											// DELS! bang tinted piccy onscreen...
//		T_DrawText();
//		S_OutputPolyList();
//		S_DumpScreen();

		if ( pausemessage==NULL )
		{
			pausemessage = T_Print(0,-24,0, GF_PSXStrings[PSSTR_PAUSED_TEXT] );
   			T_CentreH(pausemessage, 1);
   			T_BottomAlign(pausemessage, 1);
		}

		if (nojoytotal==2)
		{
				T_RemovePrint(nojoymessage);
				nojoymessage=NULL;
		}
		if ( input&IN_PAUSE  || reset_flag )
		{
			T_RemovePrint(pausemessage);
			pausemessage=NULL;
			T_RemovePrint(nojoymessage);
			nojoymessage=NULL;
			break;
		}

	} while ( 1 );

//	S_InitFade(FADE_MID,FADE_MAX,0);
//	S_WaitFade();

	S_CDRestart();
	if ( pausemessage )
	{
		T_RemovePrint(pausemessage);
		pausemessage=NULL;
	}
	game_paused = 0;
}

/****************************************************************************
 *				Control The Screen Position
 ***************************************************************************/
void	S_control_screen_position(void)
{
//	printf( "X:%d Y:%d\n",savegame.screen_x,savegame.screen_y );
	if ( input&IN_FORWARD )
	{
   		savegame.screen_y--;
		if ( savegame.screen_y<-6 ) savegame.screen_y = -6;
	}
	if ( input&IN_BACK )
	{
  		savegame.screen_y++;
		if ( savegame.screen_y>40 ) savegame.screen_y = 40;
	}
	if ( input&IN_LEFT )
	{
  		savegame.screen_x--;
		if ( savegame.screen_x<-10 ) savegame.screen_x = -10;
	}
	if ( input&IN_RIGHT )
	{
   		savegame.screen_x++;
		if ( savegame.screen_x>32 ) savegame.screen_x = 32;
	}
	GPU_SetScreenPosition(savegame.screen_x,savegame.screen_y);
}


