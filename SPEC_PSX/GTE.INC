;****************************************************************************
;
;
;
;
;****************************************************************************


C2_VXY0	equr	r0
C2_VZ0 	equr	r1
C2_VXY1	equr	r2
C2_VZ1 	equr	r3
C2_VXY2	equr	r4
C2_VZ2 	equr	r5
C2_RGB 	equr	r6
C2_OTZ 	equr	r7

C2_IR0 	equr	r8
C2_IR1 	equr	r9
C2_IR2 	equr	r10
C2_IR3 	equr	r11
C2_SXY0	equr	r12
C2_SXY1	equr	r13
C2_SXY2	equr	r14
C2_SXYP	equr	r15

C2_SZ0 	equr	r16
C2_SZ1 	equr	r17
C2_SZ2 	equr	r18
C2_SZ3	equr	r19
C2_RGB0	equr	r20
C2_RGB1	equr	r21
C2_RGB2	equr	r22

C2_MAC0	equr	r24
C2_MAC1	equr	r25
C2_MAC2	equr	r26
C2_MAC3	equr	r27
C2_IRGB	equr	r28
C2_ORGB	equr	r29
C2_LZCS	equr	r30
C2_LZCR	equr	r31

;
; GTE control registers
;
C2_R11R12	equs	"r0"
C2_R13R21	equs	"r1"
C2_R22R23	equs	"r2"
C2_R31R32	equs	"r3"
C2_R33		equs	"r4"
C2_TRX		equs	"r5"
C2_TRY		equs	"r6"
C2_TRZ		equs	"r7"

C2_L11L12	equs	"r8"
C2_L13L21	equs	"r9"
C2_L22L23	equs	"r10"
C2_L31L32	equs	"r11"
C2_L33		equs	"r12"
C2_RBK		equs	"r13"
C2_GBK		equs	"r14"
C2_BBK		equs	"r15"

C2_LR1LR2	equs	"r16"
C2_LR3LG1	equs	"r17"
C2_LG2LG3	equs	"r18"
C2_LB1LB2	equs	"r19"
C2_LB3		equs	"r20"
C2_RFC		equs	"r21"
C2_GFC		equs	"r22"
C2_BFC		equs	"r23"

C2_OFX		equs	"r24"
C2_OFY		equs	"r25"
C2_H		equs	"r26"
C2_DQA		equs	"r27"
C2_DQB		equs	"r28"
C2_ZSF3		equs	"r29"
C2_ZSF4		equs	"r30"
C2_FLAG		equs	"r31"


MVMVA	MACRO	sf,mx,v,cv,lm
	dw	$4a400012|((sf&3)<<19)|((mx&3)<<17)|((v&3)<<15)|((cv&3)<<13)|((lm&3)<<11)
	ENDM

RTPT	MACRO
	dw	$4a280030
	ENDM

RTPS	MACRO
	dw	$4a180001
	ENDM

SQR	MACRO	sf
	dw	$4aa00428|((sf&1)<<19)
	ENDM

AVSZ3	MACRO
	dw	$4b58002d
	ENDM

AVSZ4	MACRO
	dw	$4b68002e
	ENDM

NCS	MACRO
	dw	$4ac8041e
	ENDM

NCDS	MACRO
	dw	$4ae80413
	ENDM

NCCS	MACRO
	dw	$4b08041b
	ENDM

DCPL	MACRO
	dw	$4a680029
	ENDM

; -------- 2 cycle waits

GPF	MACRO	sf
	dw	$4b90003d|((sf&1)<<19)
	ENDM

NCLIP	MACRO
	dw	$4b400006
	ENDM
