#include "stypes.h"
#include "..\game\items.h"
#include "..\game\room.h"
#include "include.h"

//#define LOG

#define MAX_VSYNC_ROUTINES 4

/*** locals ***/
local PFNVSYNCROUTINE LapfnVSyncRoutine[MAX_VSYNC_ROUTINES];
local PFNVSYNCROUTINE LpfnOldVSyncCallback;
local int LnVSyncRoutines;

/*** macros ***/
#ifdef LOG
#define Log_(format,args...) DebugOut(format "\n" , ## args)
#else
#define Log_(args...)
#endif

/*** code ***/

/*** GTE stuff ***/
void GTE_Init(void)
	{
	InitGeom();
	}

/*** VSync stuff ***/
local void VSYNC_Handler(void)
	{
	int nRoutine;

	for (nRoutine=LnVSyncRoutines-1;nRoutine>=0;--nRoutine)
		LapfnVSyncRoutine[nRoutine]();
	}

void VSYNC_Init(void)
	{
	Log_(__FUNCTION__);
	LnVSyncRoutines=0;
	LpfnOldVSyncCallback=(PFNVSYNCROUTINE)VSyncCallback(VSYNC_Handler);
	}

void VSYNC_Cleanup(void)
	{
	Log_(__FUNCTION__);
	VSyncCallback(LpfnOldVSyncCallback);
	}

void VSYNC_Add(PFNVSYNCROUTINE pfnRoutine)
	{
	Log_(__FUNCTION__);
	if (LnVSyncRoutines>=MAX_VSYNC_ROUTINES)
		{
		Log_("Too many VSync routines");
		return;
		}
	EnterCriticalSection();
	LapfnVSyncRoutine[LnVSyncRoutines++]=pfnRoutine;
	ExitCriticalSection();
	}

void VSYNC_Remove(PFNVSYNCROUTINE pfnRoutine)
	{
	int nRoutine;

	Log_(__FUNCTION__);
	EnterCriticalSection();
	for (nRoutine=LnVSyncRoutines-1;nRoutine>=0;--nRoutine)
		if (LapfnVSyncRoutine[nRoutine]==pfnRoutine)
			{
			LapfnVSyncRoutine[nRoutine]=LapfnVSyncRoutine[--LnVSyncRoutines];
			break;
			}
	ExitCriticalSection();
	}
