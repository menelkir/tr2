/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#include <sys\types.h>
#include <libapi.h>
#include <libetc.h>
#include <libpad.h>
#include <libsn.h>
#include <stdio.h>
#include "typedefs.h"
#include "psxinput.h"
#include "gpu.h"
#include "cd.h"
#include "file.h"
#include "memcard.h"
#include "spusound.h"
#include "profile.h"
#include "specific.h"
#include "..\game\savegame.h"


//#define	IPROFILE

#ifdef LOG
#define Log_(format,args...) DebugOut(format "\n" , ## args)
#else
#define Log_(args...)
#endif

/*---------------------------------------------------------------------------
 *	Public Functions
\*--------------------------------------------------------------------------*/

void PSX_Init()
{
#if defined(CD_LOAD) || defined (PLAY_CDTRACKS)
	int nResult;
#endif

	ResetCallback();
	SPU_Init();

#if defined(CD_LOAD) || defined (PLAY_CDTRACKS)
	nResult = CdInit();
	Log_("CDInit: %d",nResult);
	CdSetDebug(0);
#endif

/* -------- initialise controllers & memory card */

#ifdef MEMCARD
	InitCARD(0);
	StartCARD();
	_bu_init();
#endif

	PadInitDirect((char *)&GPad1, (char *)&GPad2);
	PadStartCom();

	VSYNC_Init();
	GPU_Init();

#if defined(CD_LOAD) || defined(PLAY_CDTRACKS)
	CD_Init(0);
#endif

	InitGeom();

#ifdef PROFILE_ME
	ProfileInit(1);		// How many Frames Displayed On Profiler...
#endif

#ifdef IPROFILE
	IPROF_Init();
#endif

#if !defined(PSX_SONY_DEMO_SHELL) && !defined(TEST_SONY_DEMO_SHELL)
//#if defined(SHOW_WARNING) && defined(NTSC) && defined(JAPAN) //&& !defined(GAMEDEBUG)
//	PIC_PirateScreen("pixjap\\victorl.raw");
//	PIC_PirateScreen("pixjap\\attenti.raw");
//#endif

#if defined(SHOW_WARNING) && !defined(NTSC) && !defined(JAPAN) && !defined(GAMEDEBUG)
	PIC_PirateScreen("warning.raw");
#endif
#endif

	PutDispEnv(&db.disp[db.current_buffer]); 				// update display environment //
	VSync(0);
	DrawSync(0);
}

void PSX_Exit(void)
{
	DrawSync(0);							// just in case ???
	VSync(0);

#ifdef IPROFILE
	IPROF_Stop();
#endif

#if defined(CD_LOAD) || defined (PLAY_CDTRACKS)
//##	CD_Cleanup();
#endif
	GPU_Cleanup();
	VSYNC_Cleanup();
	SPU_Cleanup();

	StopCallback();
	PadStopCom();
	ResetGraph(3);
	VSync(0);
}

void PSX_Abort(void)
	{
	while (1)
		{
		SetDispMask(0);
		SetDispMask(1);
#ifndef CD_LOAD
#ifndef EZORAY
		pollhost();
#endif
#endif
		}
	}

void S_InitialiseSystem(void)
{
	savegame.SFX_volume=192;
	savegame.CD_volume=192;
	iconfig=0;

	PSX_Init();
#if defined(CD_LOAD) || defined (PLAY_CDTRACKS)
	CDDA_SetMasterVolume(Option_Music_Volume);
#endif
	GPU_UseOrderingTables(GadwOrderingTables,OTSIZE);
	GPU_UsePolygonBuffers(GadwPolygonBuffers,PBSIZE);
	GPU_GetScreenPosition(&savegame.screen_x,&savegame.screen_y);
//	CalculateWibbleTable();
}

#if defined(PSX_SONY_DEMO_SHELL) || defined(TEST_SONY_DEMO_SHELL)

void S_CleanupSystem(void)
{
	PSX_Exit();
}
#endif

void S_ExitSystem(char* exit_message)
	{
	printf("\n Exit Message: %s\n",exit_message );
	PSX_Abort();
	}
