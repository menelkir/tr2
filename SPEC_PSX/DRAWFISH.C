#include <sys\types.h>
#include <libgte.h>
#include <libgpu.h>
#include <gtemac.h>
#include <inline_c.h>
#include "typedefs.h"
#include "gpu.h"
#include "maths.h"
#include "specific.h"
#include "..\game\objects.h"
#include "..\game\items.h"
#include "..\game\anim.h"
#include "..\game\fish.h"
#include "..\game\lara.h"         	//##

#define	X	0
#define	Y	1
#define	Z	2
#define	XYZ	3

extern short rcossin_tbl[];
extern void LightFish(POLY_FT3 *, sint32, sint16);
extern void S_CalculateStaticMeshLight(int x, int y, int z, int shade, int shadeB, ROOM_INFO *room);

void S_DrawFish(ITEM_INFO *item)
{
	long		lp,leader;
	long		xoff,yoff,zoff;
	long		u1,v1,u2,v2,rgb;
	long		angle,size;
	long		x1,x2,x3,y1,y2,y3,z1,z2,z3;
	FISH_INFO	*fptr;
	POLY_FT3	*ft3;
	PSXSPRITESTRUCT* pSpriteInfo;
	short		*scrxy;	// X,Y
	long			*scrz;	// Z
	short		*TempMesh;

	if (!TriggerActive(item))
		return;

	scrxy = (short *) 0x1f800000;	// Set pointer to scratch pad.
	scrz = (long *) 0x1f800100;	// Set pointer to scratch pad.
	TempMesh = (short *) 0x1f800200;	// Set pointer to scratch pad.

	leader = item->hit_points;
//	if (leader == -1)
//		return;
	if (!lead_info[leader].on)
		return;

	mPushMatrix();
//	SetRotMatrix((MATRIX*) Matrix);
//	SetTransMatrix((MATRIX*) Matrix);
//	mTranslateXYZ(lara_item->pos.x_pos,lara_item->pos.y_pos,lara_item->pos.z_pos);
	mTranslateAbsXYZ(lara_item->pos.x_pos,lara_item->pos.y_pos,lara_item->pos.z_pos);

	xoff = item->pos.x_pos;
	yoff = item->pos.y_pos;
	zoff = item->pos.z_pos;

	if (item->object_number == PIRAHNAS)
		pSpriteInfo=psxspriteinfo + objects[EXPLOSION1].mesh_index + 10;
	else
		pSpriteInfo=psxspriteinfo + objects[EXPLOSION1].mesh_index + 11;

	ft3 = (POLY_FT3 *) db.polyptr;

	fptr = (FISH_INFO *) &fish[MAX_FISH+(leader*24)];

	S_CalculateStaticMeshLight(xoff, yoff, zoff, 0x4210, 0, &room[item->room_number]);

	for (lp=0;lp<24;lp++)
	{
		if ((char *)ft3 >= db.polybuf_limit)
		{
			db.polyptr = (char *)ft3;
			mPopMatrix();
			return;
		}

		angle = (fptr->angle + 2048 + ((rcossin_tbl[fptr->swim<<7])>>5) ) & 4095;

		x1 = TempMesh[X] = fptr->x + xoff - lara_item->pos.x_pos;
		y1 = TempMesh[Y] = fptr->y + yoff - lara_item->pos.y_pos;
		z1 = TempMesh[Z] = fptr->z + zoff - lara_item->pos.z_pos;

		if (	x1 < -0x5000 || x1 > 0x5000 ||
			y1 < -0x5000 || y1 > 0x5000 ||
			z1 < -0x5000 || z1 > 0x5000)
		{
			fptr++;
			continue;
		}

		gte_ldv0(TempMesh);
		gte_rtps();
		size = 128 + ((rcossin_tbl[lp<<7])>>5);
		TempMesh[Y] -= size;
		gte_stsxy(scrxy);
		gte_stsz(scrz);

		TempMesh[X] += -(((rcossin_tbl[(angle<<1)])*(size))>>12);
		TempMesh[Z] += (((rcossin_tbl[(angle<<1)+1])*(size))>>12);

		gte_ldv0(TempMesh);
		gte_rtps();
		scrxy+=2;
		scrz++;
		TempMesh[Y] += size<<1;
		gte_stsxy(scrxy);
		gte_stsz(scrz);

		scrxy+=2;
		scrz++;

		gte_ldv0(TempMesh);
		gte_rtps();
		gte_stsxy(scrxy);
		gte_stsz(scrz);

		scrxy-=4;
		scrz-=2;

		x1 = *scrxy++;
		y1 = *scrxy++;
		x2 = *scrxy++;
		y2 = *scrxy++;
		x3 = *scrxy++;
		y3 = *scrxy;
		z1 = *scrz++;
		z2 = *scrz++;
		z3 = *scrz;

		scrxy-=5;
		scrz-=2;

		if ( (z1 > 0x5000) ||
			(z1 < 32 || z2 < 32 || z3 < 32 ) ||
			(x1 < 0 && x2 < 0 && x3 < 0 ) ||
			(x1 >= MY_PSX_W && x2 >= MY_PSX_W && x3 >= MY_PSX_W ) ||
			(y1 < 0 && y2 < 0 && y3 < 0 ) ||
			(y1 >= MY_PSX_H && y2 >= MY_PSX_H && y3 >= MY_PSX_H ))
		{
			fptr++;
			continue;
		}

		if (angle<1024)
			angle -= 512;
		else if (angle<2048)
			angle -= 1536;
		else if (angle<3072)
			angle -= 2560;
		else
			angle -= 3584;

		if (angle > 512 || angle < 0)
			angle = 0;
		else	if (angle < 256)
			angle >>= 2;
		else
			angle = (512-angle) >> 2;

		angle += lp;
		if (angle > 128)
			angle = 128;

#if 0
		rgb=80+angle;

		if (z1 > 0x3000)
			rgb = (rgb*(0x5000-z1))>>13;	// Depth cue;

		setRGB0(ft3,rgb,rgb,rgb);
#else
		rgb = 15 + (angle>>3);
		LightFish(ft3, z1, (rgb<<10)|(rgb<<5)|rgb);
#endif
		setPolyFT3(ft3);

		setXY3(ft3,x1,y1,x2,y2,x3,y3);
		u1 = pSpriteInfo->u1;
		u2 = pSpriteInfo->u2;
		v1 = pSpriteInfo->v1;
		v2 = pSpriteInfo->v2;

		if (item->object_number == PIRAHNAS)
		{
			if (lp&1)
				setUV3(ft3, u1,v1, u2,v1, u1,v2);
			else
				setUV3(ft3, u2,v2, u1,v2, u2,v1);
		}
		else
		{
			if (leader&1)
				setUV3(ft3, u1,v1, u2,v1, u1,v2);
			else
				setUV3(ft3, u2,v2, u1,v2, u2,v1);
		}

		ft3->clut = pSpriteInfo->clut - swClutSub;
		ft3->tpage = pSpriteInfo->tpage;

		MyaddPrim( db.ot, z1>>3, ft3 );
		ft3++;

		fptr++;
	}

	db.polyptr = (uchar*) ft3;

	mPopMatrix();
}
