/****************************************************************************
*
* CD.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 04/06/98
*   MODIFIED : 04/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for CD.C
*
*****************************************************************************/

#ifndef _CD_H
#define _CD_H

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#include <sys\types.h>
#include <libcd.h>
#include "typedefs.h"
#include "types.h"

/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/

//#define CD_LOAD
//#define PLAY_CDTRACKS

#define ENGLISH
//#define FRENCH
//#define GERMAN
//#define ITALIAN
//#define SPANISH

/*---------------------------------------------------------------------------
 *	Type Definitions
\*--------------------------------------------------------------------------*/

#define CD_PLAY_ONCE 0x100
#define CD_PLAY_THEN_RETURN 0x200

enum	{ CD_MODE_NONE, CD_MODE_DATA, CD_MODE_AUDIO };
enum	{ CD_SYNC_STATUS, CD_SYNC_WAIT_START, CD_SYNC_WAIT_END	};
enum	{ CD_STATUS_NOT_STARTED, CD_STATUS_PLAYING, CD_STATUS_FINISHED };

/*---------------------------------------------------------------------------
 *	Macros
\*--------------------------------------------------------------------------*/

#define CDDA_Stop() CDDA_Pause();

#ifdef PLAY_CDTRACKS

#define XAFade(n)			XAReqVolume = n
#define XAWaitFade()		while (XAVolume) XAReqVolume = 0
#define XASetFadeSpeed(n)     XAFadeRate = n

#else

#define XAFade(n)
#define XAWaitFade()
#define XASetFadeSpeed(n)

#endif

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

extern volatile int XACurPos;
extern volatile int XAEndPos;
extern volatile WORD XAVolume;		// current XA volume
extern volatile WORD XAReqVolume;		// required XA volume
extern WORD XAMasterVolume;
extern WORD XAFadeRate;

extern void CD_Init(int);
extern void CD_Cleanup();
extern void CD_SetMode(int nMode);

extern int CDDA_SetMasterVolume(int nVolume);
extern void CDDA_SetVolume(int nVolume);
extern void CDDA_Play(int nTrack);
extern void CDDA_Pause();
extern void CDDA_Unpause();
extern int CDDA_Sync(int nType);
extern int CDDA_GetCurrentLoc();

extern void CDFS_UseSectorBuffer(uint32* pBuffer,int nSize);
extern void CDFS_GetSectors(int nFirstSector,int nSectors,void* pDest);
extern int CDFS_OpenFile(char* szFileName);
extern int CDFS_LoadFile(char* szFileName, void* pDest);
extern int CDFS_GetFileSize(char* szFileName);
extern int CDFS_ReadBytes(void* pvDest,int nBytes);
extern CdlLOC*	CDFS_GetFileLoc();

#endif
