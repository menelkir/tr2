/****************************************************************************
*
* FMV.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 17/08/98
*   MODIFIED : 17/08/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for FMV.C
*
*****************************************************************************/

#ifndef _FMV_H
#define _FMV_H

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#include "..\spec_psx\typedefs.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(RELOC)

typedef int (FMVFUNC)(char *, int);

#define PlayFMV	((FMVFUNC *)VehiclePtr[0])

#else

extern int PlayFMV(char *name, int end);

#endif

#endif
