#include "game.h"

/* -------- externals */

extern uint16 DashTimer;
extern void S_DrawDashBar(int percent);

/*** globals ***/
int		overlay_flag=1;

int FlashIt(void)
{
	static	int		flash_state=0;
	static	int		flash_count=0;

	if (flash_count)
		flash_count--;
	else
	{
		flash_state ^= 1;
		flash_count = 5;
	}
	return(flash_state);
}

void DrawAssaultTimer(void)
{
	int x, timer;
	char txt[8], *c;

	if (CurrentLevel != LV_GYM || !assault_timer_display)
		return;

	timer = savegame.timer / 30;
	sprintf(txt, "%d:%02d.%d", timer/60, timer%60, (int)((savegame.timer%30)/3));

	x = (phd_winxmax>>1) - 20*5/2;
	for (c = txt; *c; c++)
	{
		if (*c == ':')
		{
			x -= 6;
			S_DrawScreenSprite2d(x,36,0, 0x10000, 0x10000, (sint16)(objects[ASSAULT_NUMBERS].mesh_index + 10), 16<<8, 0);
			x += 14;
		}
		else if (*c == '.')
		{
			x -= 6;
			S_DrawScreenSprite2d(x,36,0, 0x10000, 0x10000, (sint16)(objects[ASSAULT_NUMBERS].mesh_index + 11), 16<<8, 0);
			x += 14;
		}
		else
		{
			S_DrawScreenSprite2d(x,36,0, 0x10000, 0x10000, (sint16)(objects[ASSAULT_NUMBERS].mesh_index + (*c-'0')), 16<<8, 0);
			x += 20;
		}
	}
}

/******************************************************************************
 *				Draw All Required Game Information
 *****************************************************************************/
void	DrawGameInfo(int timed)
{
	int	flash_state;

	DrawAmmoInfo();
	DrawModeInfo();

	if ( overlay_flag>0 )
	{
		flash_state = FlashIt();
		DrawHealthBar(flash_state);
		DrawAirBar(flash_state);
		DrawPickups(timed);
		DrawAssaultTimer();

	/* -------- draw dash power-up/timer bar */

		if (DashTimer < LARA_DASH_TIME)
			S_DrawDashBar((DashTimer*100)/LARA_DASH_TIME);
	}
	T_DrawText();
}

/******************************************************************************
 *			Draw Laras Health Bar on Screen
 *****************************************************************************/
void	DrawHealthBar( int flash_state)
{
	int				hitpoints;
	static	int		old_hitpoints=0;
#ifdef INVINCABLE_LARA
	lara_item->hit_points = LARA_HITPOINTS;
#endif
	hitpoints = (int)lara_item->hit_points;				// Get HitPoints 0-100
	if ( hitpoints<0 )
		hitpoints=0;
	else if ( hitpoints>LARA_HITPOINTS )
		hitpoints=LARA_HITPOINTS;

	if ( old_hitpoints!=hitpoints )
	{
		old_hitpoints = hitpoints;
		health_bar_timer = 40;
	}
	if ( health_bar_timer<0 )
		health_bar_timer = 0;
	if ( hitpoints<=LARA_HITPOINTS>>2 )
	{
		if (flash_state)
			S_DrawHealthBar( hitpoints/10 );
		else
			S_DrawHealthBar( 0 );
	}
	else if ( health_bar_timer>0 || hitpoints<=0 || lara.gun_status==LG_READY )
		S_DrawHealthBar( hitpoints/10 );
}

/*****************************************************************************
 *			Draw Air Bar on Screen when Underwater..
 ****************************************************************************/
void	DrawAirBar( int flash_state )
{
	int		air;

	if ( lara.water_status!=LARA_UNDERWATER && lara.water_status!=LARA_SURFACE &&
		!((room[lara_item->room_number].flags & SWAMP) && lara.water_surface_dist < -775))
		return;

	air = (int)lara.air;
	if ( air<0 )
		air=0;
	else if ( air>LARA_AIR )
		air=LARA_AIR;
	if (air<=LARA_AIR>>2)
	{
		if (flash_state)
			S_DrawAirBar( (air*100)/LARA_AIR );
		else
			S_DrawAirBar( 0 );
	}
	else
		S_DrawAirBar( (air*100)/LARA_AIR );
}

/******************************************************************************
 *					Draw Ammo information
 *****************************************************************************/
TEXTSTRING	*ammotext = NULL;

void MakeAmmoString(char *string)
{
	/* Convert string into ASCII codes */
	char *c;

	for (c=string; *c!=0; c++)
	{
		if (*c == 32)
			continue;
		else if (*c - 'A' >= 0)
			*c += 12-'A';
		else
			*c += 1-'0';
	}
}


void DrawAmmoInfo( void )
{
	char ammostring[80];

	ammostring[0]=0;

	if ( lara.gun_status!=LG_READY || overlay_flag<=0 || savegame.bonus_flag )  // only display ammo count if
	{                                           								// guns are Ready  or we dont want info
		if ( ammotext )
		{
			T_RemovePrint( ammotext );
			ammotext = NULL;
		}
		return;
	}
	switch ( lara.gun_type )
	{
		case LG_PISTOLS:
			return;
		break;

		case LG_MAGNUMS:
			wsprintf(ammostring, "%5d", lara.magnums.ammo);
		break;

		case LG_UZIS:
			wsprintf(ammostring, "%5d", lara.uzis.ammo);
		break;

		case LG_SHOTGUN:
			wsprintf(ammostring, "%5d", lara.shotgun.ammo/SHOTGUN_AMMO_CLIP);
		break;

		case LG_HARPOON:
			wsprintf(ammostring, "%5d", lara.harpoon.ammo);
			break;

		case LG_M16:
			wsprintf(ammostring, "%5d", lara.m16.ammo);
			break;

		case LG_ROCKET:
			wsprintf(ammostring, "%5d", lara.rocket.ammo);
			break;

		case LG_FLARE:
		default:
			return;
		break;
	}

	MakeAmmoString(ammostring);

	if ( ammotext )
		T_ChangeText( ammotext, ammostring );
	else
#ifdef PC_VERSION
	{
		ammotext = T_Print(-10,35,0, ammostring );
		T_RightAlign(ammotext, 1);
	}
#else
	{
//		ammotext = T_Print( 256,64,0, ammostring );
		ammotext = T_Print(-17,64,0, ammostring );
		T_RightAlign(ammotext, 1);
	}
#endif
}


/******************************************************************************
 *				Kick off showing weve picked up an Object
 *****************************************************************************/
#define NUM_PU	12

typedef struct displaypu {
		sint16	duration;
		sint16	sprnum;
} DISPLAYPU;

	DISPLAYPU	pickups[NUM_PU];

/******************************************************************************
 *			Initialise pick up displayer
 *****************************************************************************/
void	InitialisePickUpDisplay( void )
{
	int		i;

	for ( i=0;i<NUM_PU;i++ )
		pickups[i].duration = 0;
}

/******************************************************************************
 *			Draw Pickups onto Screen as Reminder weve got them
 *****************************************************************************/
void	DrawPickups( int timed )
{
	int				i,time;
	static	int		old_game_timer;
	DISPLAYPU		*pu;
	int				wa,w,x,y, drawn;
	int				scaler = 12288;


	time = savegame.timer - old_game_timer;
	old_game_timer = savegame.timer;
	if ( time<=0 || time>=60)
		return;

	w = phd_winwidth/10;
	x = phd_winwidth - w;
	y = phd_winheight - w;
	wa = w;//(w*4)/3;

	pu = pickups;
	drawn = 0;
	for ( i=0;i<NUM_PU;i++,pu++,x-=wa )
	{
		if (timed)
			pu->duration -= time;
		if ( pu->duration>0 )
		{
			if (drawn==4 || drawn==8)
			{
				y-=(wa*2)/3;
				x = phd_winwidth - w;
			}
			S_DrawPickup( x, y, scaler, pu->sprnum, 16*256, 0 );
			drawn++;
		}
		else
			pu->duration=0;
	}
}

/*****************************************************************************
 *			Show that weve picked up Something
 ****************************************************************************/
void	AddDisplayPickup( sint16 objnum )
{
	int		i;

	/* Play Secret Track if Secret Item */
	if ( objnum==SECRET_ITEM1 || objnum==SECRET_ITEM2 || objnum==SECRET_ITEM3)
		S_CDPlay(gameflow.secret_track,0);

	for ( i=0;i<NUM_PU;i++ )
	{
		if ( pickups[i].duration<=0 )
		{
			pickups[i].duration = 75;
			pickups[i].sprnum = objects[objnum].mesh_index;
			return;
		}
	}
}

/*** mode info stuff ***/
static TEXTSTRING* LpModeTS=0;
static int LnModeTSLife=0;

void DisplayModeInfo(char* szString)
	{
	if (!szString)
		{
		T_RemovePrint(LpModeTS);
		LpModeTS=0;
		return;
		}
	if (LpModeTS)
		T_ChangeText(LpModeTS,szString);
	else
		{
		LpModeTS=T_Print(-16,-16,0,szString);
		T_RightAlign(LpModeTS,1);
		T_BottomAlign(LpModeTS,1);
		}
	LnModeTSLife=75;
	}

void DrawModeInfo(void)
	{
/*	static int old_game_timer;
	int time;

	time=savegame.timer-old_game_timer;	// ugh
	old_game_timer=savegame.timer;
	if (time<=0 || time>=60)
		return;*/

	if (LpModeTS)
		{
		if (--LnModeTSLife==0)
			{
			T_RemovePrint(LpModeTS);
			LpModeTS=0;
			}
		}
	}
