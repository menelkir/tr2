/*****************************************************
	This file is private to Inventory & Option files *
******************************************************/
#ifndef INVDATA_H
#define INVDATA_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAIN_RING	0
#define	OPTION_RING 1
#define	KEYS_RING   2


/* MAXIMUM ITEMS IN EACH RING */
#define MAX_INV_OBJ 30
/*  KEYS FOR INVENTORY */
#ifdef PSX_VERSION
#define	DESELECT_ITEM	IN_DESELECT
#define	SELECT_ITEM		IN_SELECT
#define	ROTATE_RIGHT	IN_RIGHT
#define	ROTATE_LEFT		IN_LEFT
#define	EXIT_INVENTORY	(IN_DESELECT|IN_OPTION)
#else
#define	DESELECT_ITEM	IN_DESELECT
#define	SELECT_ITEM		IN_SELECT
#define	ROTATE_RIGHT	IN_RIGHT
#define	ROTATE_LEFT		IN_LEFT
#define	EXIT_INVENTORY	(IN_DESELECT|IN_OPTION)
#endif

#define PASSPORT_2FRONT	IN_LEFT
#define PASSPORT_2BACK	IN_RIGHT

#define	DEC_HBAR		IN_LEFT
#define INC_HBAR		IN_RIGHT
#define	PREV_HBAR		IN_FORWARD
#define	NEXT_HBAR		IN_BACK


typedef enum inv_order {
		MAP_POS,
		GUN_POS,
      SGN_POS,
      MAG_POS,
      UZI_POS,
		M16_POS,
		ROCKET_POS,
		HARPOON_POS,
		FLR_POS,
      EXP_POS,
      BGM_POS,
      MED_POS
} INV_ORDER;

typedef enum inv_order2 {
		LBAR_POS=100,
        KY1_POS,
        KY2_POS,
        KY3_POS,
        KY4_POS,
        PZ4_POS,
        PZ3_POS,
        PZ2_POS,
        PZ1_POS,
        SCN_POS,
        PK2_POS,
        PK1_POS,
        } INV_ORDER2;



typedef enum pass_pages {
	PSPINE = 1,
	PFRONT = 2,
	PINFRONT = 4,
	PPAGE2 = 8,
	PBACK = 16,
	PINBACK = 32,
	PPAGE1 = 64
} PASS_PAGES;

typedef enum inv_text {
	IT_NAME,
	IT_QTY,
	IT_NUMBEROF
} INV_TEXT;

/* Inventory Static Values */
#define	OPEN_FRAMES	32
#define	CLOSE_FRAMES 32
#define OPEN_ROTATION -0x8000
#define CLOSE_ROTATION -0x8000
#define	ROTATE_DURATION	24
#define RINGSWITCH_FRAMES 48
#define	SELECTING_FRAMES 16

#define	CAMERA_STARTHEIGHT	-0x600
#define CAMERA_HEIGHT -0x100
#define	CAMERA_2_RING	0x256
#define RING_RADIUS 0x2b0
#define	CAMERA_YOFFSET -0x60
#define	ITEM_TILT 0x0

/* 'ls_adder' lighting settings      */
#define LOW_LIGHT	0x1400
#define HIGH_LIGHT	0x1000


/* Inventory Status Codes */
typedef enum status_codes{
	RNG_OPENING,
	RNG_OPEN,
	RNG_CLOSING,
	RNG_MAIN2OPTION,
	RNG_MAIN2KEYS,
	RNG_KEYS2MAIN,
	RNG_OPTION2MAIN,
	RNG_SELECTING,
	RNG_SELECTED,
	RNG_DESELECTING,
	RNG_DESELECT,
	RNG_CLOSING_ITEM,
	RNG_EXITING_INVENTORY,
	RNG_DONE
} STATUS_CODES;

#ifdef	PC_VERSION
 #ifdef DEMO
#define OPTION_RING_OBJECTS 1
#define TITLE_RING_OBJECTS  1
 #else
  #ifdef RETAIL
#define OPTION_RING_OBJECTS 3
#define TITLE_RING_OBJECTS  4
  #else
#define OPTION_RING_OBJECTS	3
#define	TITLE_RING_OBJECTS	4
  #endif
 #endif
#endif

#ifdef	PSX_VERSION
#define OPTION_RING_OBJECTS 1
#define	TITLE_RING_OBJECTS  4
#endif


/* Item orientation when selected */
/* MAP ORIENTATIONS */
#define	MAP_PT_XROT		0x1100
#define MAP_ZTRANS		320
#ifdef PSX_VERSION
#define	MAP_YTRANS		-62
#else
#define	MAP_YTRANS		-170
#endif
#define	MAP_X_ROT		-1536
#define MAP_Y_ROT		0
#define MAP_MESH		0xffffffff

/* GUN ORIENTATIONS */
#define	GUNS_PT_XROT	0xc80
#define GUNS_ZTRANS		352
#define	GUNS_YTRANS		38
#define	GUNS_X_ROT		2848
#define GUNS_Y_ROT		-32768
#define GUNS_MESH		0xffffffff

/* SHOTGUN ORIENTATIONS */
#define	SGUN_PT_XROT	0xc80
#define SGUN_ZTRANS		228
#define	SGUN_YTRANS		0
#define	SGUN_X_ROT		5120
#define SGUN_Y_ROT		30720
#define SGUN_MESH		0xffffffff

/* FLARE ORIENTATIONS */
#define	FLAR_PT_XROT	0xc80
#define FLAR_ZTRANS		0x128
#define	FLAR_YTRANS		0
#define	FLAR_X_ROT		0
#define FLAR_Y_ROT		-0x2000
#define FLAR_MESH		0xffffffff

/* AUTOMATIC PISTOLS (was MAGNUM) ORIENTATIONS */
#define	MAGN_PT_XROT	0xc80
#define MAGN_ZTRANS		362
#define	MAGN_YTRANS		0
#define	MAGN_X_ROT		3360
#define MAGN_Y_ROT		-32768
#define MAGN_MESH		0xffffffff

/* UZI ORIENTATIONS */
#define	UZI_PT_XROT		0xc80
#define UZI_ZTRANS		322
#define	UZI_YTRANS		56
#define	UZI_X_ROT		2336
#define UZI_Y_ROT		-32768
#define UZI_MESH		0xffffffff

/* HARPOON ORIENTATIONS */
#define HARPOON_PT_XROT	0xc80
#define HARPOON_ZTRANS  296
#define HARPOON_YTRANS  58
#define HARPOON_X_ROT   -736
#define HARPOON_Y_ROT   -19456
#define HARPOON_MESH    0xffffffff

/* M16 ORIENTATIONS */
#define M16_PT_XROT		0xc80
#define M16_ZTRANS		296
#define M16_YTRANS		84
#define M16_X_ROT		-224
#define M16_Y_ROT		-18432
#define M16_MESH		0xffffffff

/* MINI ROCKET GUN ORIENTATIONS */
#define ROCKET_PT_XROT 0xc80
#define ROCKET_ZTRANS  296
#define ROCKET_YTRANS  56
#define ROCKET_X_ROT   -224
#define ROCKET_Y_ROT   14336
#define ROCKET_MESH    0xffffffff


/* GUN AMMO ORIENTATIONS */
#define	GAMO_PT_XROT	0xc80
#define GAMO_ZTRANS		0x128
#define	GAMO_YTRANS		0
#define	GAMO_X_ROT		-0xee0
#define GAMO_Y_ROT		0
#define GAMO_MESH		0xffffffff

/* SHOTGUN AMMO ORIENTATIONS */
#define	SGAM_PT_XROT	0xc80
#define SGAM_ZTRANS		0x128
#define	SGAM_YTRANS		0
#define	SGAM_X_ROT		-0xee0
#define SGAM_Y_ROT		0
#define SGAM_MESH		0xffffffff

/* MAGNUM AMMO ORIENTATIONS */
#define	MGAM_PT_XROT	0xc80
#define MGAM_ZTRANS		0x128
#define	MGAM_YTRANS		0
#define	MGAM_X_ROT		-0xee0
#define MGAM_Y_ROT		0
#define MGAM_MESH		0xffffffff

/* UZI AMMO ORIENTATIONS */
#define	UZAM_PT_XROT	0xc80
#define UZAM_ZTRANS		0x128
#define	UZAM_YTRANS		0
#define	UZAM_X_ROT		-0xee0
#define UZAM_Y_ROT		0
#define UZAM_MESH		0xffffffff

/* HARPOON AMMO ORIENTATIONS */
#define HAM_PT_XROT 0xc80
#define HAM_ZTRANS  0x128
#define HAM_YTRANS  0
#define HAM_X_ROT   -0xee0
#define HAM_Y_ROT   0
#define HAM_MESH    0xffffffff

/* M16 AMMO ORIENTATIONS */
#define M16AM_PT_XROT	0xc80
#define M16AM_ZTRANS		0x128
#define M16AM_YTRANS		0
#define M16AM_X_ROT		-0xee0
#define M16AM_Y_ROT		0
#define M16AM_MESH		0xffffffff

/* ROCKET AMMO ORIENTATIONS */
#define RAM_PT_XROT	0xc80
#define RAM_ZTRANS	0x128
#define RAM_YTRANS	0
#define RAM_X_ROT		-0xee0
#define RAM_Y_ROT		0
#define RAM_MESH		0xffffffff


/* MEDI ORIENTATIONS */
#define	MEDI_PT_XROT	0xfc0
#define MEDI_ZTRANS		0xd8
#define	MEDI_YTRANS		0
#define	MEDI_X_ROT		-0x1c80
#define MEDI_Y_ROT		-0x1000
#define MEDI_MESH		0xffffffff

/* BIGMEDI ORIENTATIONS */
#define	BMED_PT_XROT	0xe20
#define BMED_ZTRANS		0x160
#define	BMED_YTRANS		0
#define	BMED_X_ROT		-0x1fe0
#define BMED_Y_ROT		-0x1000
#define BMED_MESH		0xffffffff

/* LEAD BAR ORIENTATIONS */
#define	LBAR_PT_XROT	0xe20
#define LBAR_ZTRANS		0x160
#define	LBAR_YTRANS		0
#define	LBAR_X_ROT		-0x1fe0
#define LBAR_Y_ROT		-0x1000
#define LBAR_MESH		0xffffffff

/* PICKUP1 ORIENTATIONS */
#define	PCK1_PT_XROT	0x1c20
#define PCK1_ZTRANS		0x100
#define	PCK1_YTRANS		0
#define	PCK1_X_ROT		-0x1100
#define PCK1_Y_ROT		0
#define PCK1_MESH		0xffffffff

/* PICKUP2 ORIENTATIONS */
#define	PCK2_PT_XROT	0x1c20
#define PCK2_ZTRANS		0x100
#define	PCK2_YTRANS		0
#define	PCK2_X_ROT		-0x1100
#define PCK2_Y_ROT		0
#define PCK2_MESH		0xffffffff

/* SCION ORIENTATIONS */
#define	SCON_PT_XROT	0x1c20
#define SCON_ZTRANS		0x100
#define	SCON_YTRANS		0
#define	SCON_X_ROT		-0x1100
#define SCON_Y_ROT		0
#define SCON_MESH		0xffffffff

/* PUZZLE1 ORIENTATIONS */
#define	PUZ1_PT_XROT	0x1c20
#define PUZ1_ZTRANS		0x100
#define	PUZ1_YTRANS		0
#define	PUZ1_X_ROT		-0x1100
#define PUZ1_Y_ROT		0
#define PUZ1_MESH		0xffffffff

/* PUZZLE2 ORIENTATIONS */
#define	PUZ2_PT_XROT	0x1c20
#define PUZ2_ZTRANS		0x100
#define	PUZ2_YTRANS		0
#define	PUZ2_X_ROT		-0x1100
#define PUZ2_Y_ROT		0
#define PUZ2_MESH		0xffffffff

/* PUZZLE3 ORIENTATIONS */
#define	PUZ3_PT_XROT	0x1c20
#define PUZ3_ZTRANS		0x100
#define	PUZ3_YTRANS		0
#define	PUZ3_X_ROT		-0x1100
#define PUZ3_Y_ROT		0
#define PUZ3_MESH		0xffffffff

/* PUZZLE4 ORIENTATIONS */
#define	PUZ4_PT_XROT	0x1c20
#define PUZ4_ZTRANS		0x100
#define	PUZ4_YTRANS		0
#define	PUZ4_X_ROT		-0x1100
#define PUZ4_Y_ROT		0
#define PUZ4_MESH		0xffffffff

/* KEY1 ORIENTATIONS */
#define	KEY1_PT_XROT	0x1c20
#define KEY1_ZTRANS		0x100
#define	KEY1_YTRANS		0
#define	KEY1_X_ROT		-0x1100
#define KEY1_Y_ROT		0
#define KEY1_MESH		0xffffffff

/* KEY2 ORIENTATIONS */
#define	KEY2_PT_XROT	0x1c20
#define KEY2_ZTRANS		0x100
#define	KEY2_YTRANS		0
#define	KEY2_X_ROT		-0x1100
#define KEY2_Y_ROT		0
#define KEY2_MESH		0xffffffff

/* KEY3 ORIENTATIONS */
#define	KEY3_PT_XROT	0x1c20
#define KEY3_ZTRANS		0x100
#define	KEY3_YTRANS		0
#define	KEY3_X_ROT		-0x1100
#define KEY3_Y_ROT		0
#define KEY3_MESH		0xffffffff

/* KEY4 ORIENTATIONS */
#define	KEY4_PT_XROT	0x1c20
#define KEY4_ZTRANS		0x100
#define	KEY4_YTRANS		0
#define	KEY4_X_ROT		-0x1100
#define KEY4_Y_ROT		0
#define KEY4_MESH		0xffffffff




/* PASSPORT ORIENTATIONS */
#define	PASS_PT_XROT	0x1220
#define PASS_ZTRANS		0x180
#define	PASS_YTRANS		0
#define	PASS_X_ROT		-0x10e0
#define PASS_Y_ROT		0
#define PASS_MESH		(PFRONT|PSPINE|PBACK)

/* PHOTO ORIENTATIONS */
#define	GYM_PT_XROT		0x1220
#define GYM_ZTRANS		0x180
#define	GYM_YTRANS		0
#define	GYM_X_ROT		-0x10e0
#define GYM_Y_ROT		0
#define GYM_MESH		0xffffffff

/* DETAIL ORIENTATIONS */
#define	DETL_PT_XROT	0x1080
#define DETL_ZTRANS		444
#define	DETL_YTRANS		16
#define	DETL_X_ROT		-7232
#define DETL_Y_ROT		0
#define DETL_MESH		0xffffffff

/* SOUND ORIENTATIONS */
#define	SND_PT_XROT		0x12e0
#define SND_ZTRANS		350
#define	SND_YTRANS		-2
#define	SND_X_ROT		-5408
#define SND_Y_ROT		-3072
#define SND_MESH		0xffffffff

/* CONTROL ORIENTATIONS */
#ifdef PC_VERSION
/*
#define	CTRL_PT_XROT	0x1580
#define CTRL_ZTRANS		246
#define	CTRL_YTRANS		-26
#define	CTRL_X_ROT		1536
#define CTRL_Y_ROT		-512
#define CTRL_MESH		0xffffffff
  */
#define	CTRL_PT_XROT	0x1580
#define CTRL_ZTRANS		508
#define	CTRL_YTRANS		46
#define	CTRL_X_ROT		-2560
#define CTRL_Y_ROT		13312
#define CTRL_MESH		0xffffffff

#else
#define	CTRL_PT_XROT	0x1580
#define CTRL_ZTRANS		246
#define	CTRL_YTRANS		-26
#define	CTRL_X_ROT		1536
#define CTRL_Y_ROT		-512
#define CTRL_MESH		0xffffffff
/*
#define	CTRL_PT_XROT	0x1580
#define CTRL_ZTRANS		522
#define	CTRL_YTRANS		22
#define	CTRL_X_ROT		-4096
#define CTRL_Y_ROT		0
#define CTRL_MESH		0xffffffff
*/
#endif


/* GAMMA ORIENTATIONS */
#define	GAMM_PT_XROT	0xf40
#define GAMM_ZTRANS		0x1b8
#define	GAMM_YTRANS		0
#define	GAMM_X_ROT		0
#define GAMM_Y_ROT		0
#define GAMM_MESH		0xffffffff


#define GAMMA_MODIFIER	8
#define	MIN_GAMMA_LEVEL -127
#define MAX_GAMMA_LEVEL 127

#define	SOUND_MODIFIER	8
#define	MIN_SOUND_LEVEL 0
#define	MAX_SOUND_LEVEL 255

#define DETAIL_MODIFIER	8
#define	MIN_DETAIL_LEVEL 0
#define	MAX_DETAIL_LEVEL 255
  



/****************************** Inventory Typedefs ***************************************/
typedef struct	imotion_info {
	sint16	count;						//  Frames to perform motion in
	sint16	status;						//  Current status of motion  ie 'OPENING', 'CLOSING'
	sint16	status_target;				//  Target status of motion ie 'OPEN', 'SELECTED', 'EXITING_INVENTORY'
	sint16	radius_target;				//  	Ring target radius after motion
	sint16	radius_rate;				//		Ring radius adjust rate per frame
	sint16	camera_ytarget;				//  Camera target Y position after motion
	sint16	camera_yrate;				//	Camera adjust rate per frame
	sint16	camera_pitch_target;
	sint16	camera_pitch_rate;
	sint16	rotate_target;				//		Ring rotation target after motion
	sint16	rotate_rate;				//		Ring rotate rate thru motion
	PHD_ANGLE	item_ptxrot_target;		//	Selected item's pre-translation x rot target after motion
	PHD_ANGLE	item_ptxrot_rate;		//	Selected item's Pre-translation x rot rate thru motion
	PHD_ANGLE	item_xrot_target;		//		Selected item's target x rot after motion
	PHD_ANGLE	item_xrot_rate;			//		Selected item's x rot rate thru motion
	sint32	item_ytrans_target;			//	Selected item's target y translation after motion
	sint32	item_ytrans_rate;			//	Selected item's y translate rate thru motion
	sint32	item_ztrans_target;			//		Selected item's target z translation after motion
	sint32	item_ztrans_rate;			//		Selected item's z translation rate thru motion
	sint32	misc;						//  Misc data
} IMOTION_INFO;

typedef	struct inventory_sprite {
	sint16	shape;
	sint16	x;
	sint16	y;
	sint16	z;
	sint32	param1;
	sint32	param2;
	SG_COL	*grdptr;
	sint16	sprnum;
} INVENTORY_SPRITE;

typedef struct inventory_item {
	char	*itemText;
	sint16	object_number;
	sint16	frames_total;
	sint16	current_frame;
	sint16	goal_frame;
	sint16	open_frame;
	sint16	anim_direction;
	sint16	anim_speed;
	sint16	anim_count;
	PHD_ANGLE	pt_xrot_sel;
	PHD_ANGLE	pt_xrot;
	PHD_ANGLE	x_rot_sel;
	PHD_ANGLE	x_rot_nosel;
	PHD_ANGLE	x_rot;
	PHD_ANGLE	y_rot_sel;
	PHD_ANGLE	y_rot;
	sint32	ytrans_sel;
	sint32	ytrans;
	sint32	ztrans_sel;
	sint32	ztrans;
	uint32	which_meshes;
	uint32	drawn_meshes;
    sint16	inv_pos;
	INVENTORY_SPRITE **sprlist;
	sint32	misc_data[4];
} INVENTORY_ITEM;

typedef struct ring_info {
	INVENTORY_ITEM **list;
	sint16	type;
	sint16	radius;
	sint16	camera_pitch;
	sint16	rotating;
	sint16	rot_count;
	sint16	current_object;
	sint16	target_object;
	sint16	number_of_objects;
	sint16	angle_adder;
	sint16	rot_adder;
	sint16	rot_adderL;
	sint16	rot_adderR;
	PHD_3DPOS	ringpos;
	PHD_3DPOS	camera;
	PHD_VECTOR	light;
	IMOTION_INFO *imo;
} RING_INFO;

enum R_flags {
	R_USE		= (1<<0),
	R_LEFTALIGN = (1<<1),
	R_CENTRE	= 0,
	R_RIGHTALIGN= (1<<2)
};
#define	MAX_REQLINES	24
typedef struct request_info {
	uint16	noselector:1;
	uint16	items;
	uint16	selected;
	uint16	vis_lines;
	uint16	line_offset;
	uint16	line_oldoffset;
	uint16	pixwidth;
	uint16	line_height;
	sint16	xpos;
	sint16	ypos;
	sint16	zpos;
	sint16	itemtextlen;
	char	*itemtexts1;
	char	*itemtexts2;
	uint32	*itemtexts1_flags;
	uint32	*itemtexts2_flags;

	uint32	heading1_flags;
	uint32	heading2_flags;
	uint32	background_flags;
	uint32	moreup_flags;
	uint32	moredown_flags;
	uint32	texts1_flags[MAX_REQLINES];
	uint32	texts2_flags[MAX_REQLINES];
	TEXTSTRING *heading1text;
	TEXTSTRING *heading2text;
	TEXTSTRING *backgroundtext;
	TEXTSTRING *moreuptext;
	TEXTSTRING *moredowntext;
	TEXTSTRING *texts1[MAX_REQLINES];
	TEXTSTRING *texts2[MAX_REQLINES];
	char	heading1_str[32];
	char	heading2_str[32];
#ifdef PC_VERSION
	int		original_render_width;	// Dude added these!  So that I can automatically resize requesters if the render size changes.
	int		original_render_height;	// Without this you can get some spectacular crashes!  Especially with the internal renderer...
#endif
} REQUEST_INFO;

extern sint32	inv_nframes;

extern SG_COL	req_bgnd_moreup[]; // More up arrow Gouraud
extern SG_COL	req_bgnd_moredown[]; // More down arrow Gouraud
extern SG_COL	req_bgnd_gour1[]; // Background Gouraud
extern SG_COL	req_bgnd_gour2[]; // Outline Gouraud
extern SG_COL	req_main_gour1[]; // Background Gouraud
extern SG_COL	req_main_gour2[]; // Outline Gouraud
extern SG_COL	req_sel_gour1[];  // Background Gouraud
extern SG_COL	req_sel_gour2[];  // Outline Gouraud
extern SG_COL	req_unsel_gour1[];// Background Gouraud
extern SG_COL	req_unsel_gour2[];// Outline Gouraud


//extern INVENTORY_SPRITE is_gamma_level[];
extern INVENTORY_SPRITE *is_gamma_list[];
extern INVENTORY_SPRITE is_detail_level[];
extern INVENTORY_SPRITE *is_detail_list[];
#ifdef PC_VERSION
extern INVENTORY_SPRITE is_sound_muslevel[];
extern INVENTORY_SPRITE is_sound_sfxlevel[];
extern INVENTORY_SPRITE is_sound_muslevellow[];
extern INVENTORY_SPRITE is_sound_sfxlevellow[];
#endif
extern INVENTORY_SPRITE is_sound_names[];
extern INVENTORY_SPRITE *is_sound_list[];

extern INVENTORY_ITEM	icompass_option;
extern INVENTORY_ITEM	igun_option;
extern INVENTORY_ITEM	iflare_option;
extern INVENTORY_ITEM	ishotgun_option;
extern INVENTORY_ITEM	imagnum_option;
extern INVENTORY_ITEM	iuzi_option;
extern INVENTORY_ITEM	iharpoon_option;
extern INVENTORY_ITEM	im16_option;
extern INVENTORY_ITEM	irocket_option;

extern INVENTORY_ITEM	igunammo_option;
extern INVENTORY_ITEM	isgunammo_option;
extern INVENTORY_ITEM	imagammo_option;
extern INVENTORY_ITEM	iuziammo_option;
extern INVENTORY_ITEM	iharpoonammo_option;
extern INVENTORY_ITEM	im16ammo_option;
extern INVENTORY_ITEM	irocketammo_option;

extern INVENTORY_ITEM	iexplosiv_option;
extern INVENTORY_ITEM	imedi_option;
extern INVENTORY_ITEM	ibigmedi_option;
extern INVENTORY_ITEM	ipickup1_option;
extern INVENTORY_ITEM	ipickup2_option;
extern INVENTORY_ITEM	iscion_option;
extern INVENTORY_ITEM	ipuzzle1_option;
extern INVENTORY_ITEM	ipuzzle2_option;
extern INVENTORY_ITEM	ipuzzle3_option;
extern INVENTORY_ITEM	ipuzzle4_option;
extern INVENTORY_ITEM	ikey1_option;
extern INVENTORY_ITEM	ikey2_option;
extern INVENTORY_ITEM	ikey3_option;
extern INVENTORY_ITEM	ikey4_option;

extern INVENTORY_ITEM	ipassport_option;
extern INVENTORY_ITEM	idetail_option;
extern INVENTORY_ITEM	isound_option; 
extern INVENTORY_ITEM	icontrol_option;
extern INVENTORY_ITEM	igamma_option;
extern INVENTORY_ITEM	iphoto_option;

extern sint16	inv_main_objects;
extern sint16	inv_main_current;
extern INVENTORY_ITEM	*inv_main_list[];
extern sint16	inv_main_qtys[];

extern sint16	inv_keys_objects;
extern sint16	inv_keys_current;
extern INVENTORY_ITEM	*inv_keys_list[];
extern sint16	inv_keys_qtys[];

extern sint16	inv_option_objects;
extern sint16	inv_option_current;
extern INVENTORY_ITEM	*inv_option_list[];

extern TEXTSTRING *Inv_itemText[];
extern TEXTSTRING *Inv_levelText;
extern TEXTSTRING *Inv_tagText;
extern TEXTSTRING *Inv_ringText;
extern TEXTSTRING *Inv_upArrow1;
extern TEXTSTRING *Inv_upArrow2;
extern TEXTSTRING *Inv_downArrow1;
extern TEXTSTRING *Inv_downArrow2;

extern TEXTSTRING *passport_text1;

extern int	OpenInvOnGym;

sint32	GetDebouncedInput(sint32 input);
extern sint32 inputDB;
extern sint32 oldInputDB;

/* Invfunc.c */
void	Inv_InsertItem(INVENTORY_ITEM *inv_item);
void	RingIsOpen(RING_INFO *ring);
void	RingIsNotOpen(RING_INFO *ring);
void	RingNotActive(INVENTORY_ITEM *inv_item);
void	RingActive(void);
void	RemoveInventoryText(void);

void	Inv_RingInit(RING_INFO *ring, sint16 type, INVENTORY_ITEM **list, sint16 qty, sint16 current, IMOTION_INFO *imo);
void	Inv_RingGetView(RING_INFO *ring, PHD_3DPOS *viewer);
void	Inv_RingLight(RING_INFO *ring);
void	Inv_RingCalcAdders(RING_INFO *ring, sint16 rotation_duration);
void	Inv_RingDoMotions(RING_INFO *ring);
void	Inv_RingRotateLeft(RING_INFO *ring);
void	Inv_RingRotateRight(RING_INFO *ring);

void	Inv_RingMotionInit(RING_INFO *ring, sint16 frames, sint16 status, sint16 status_target);
void	Inv_RingMotionSetup(RING_INFO *ring, sint16 status, sint16 status_target, sint16 frames);
void	Inv_RingMotionRadius(RING_INFO *ring, sint16 target);
void	Inv_RingMotionRotation(RING_INFO *ring, sint16 rotation, sint16 target);
void	Inv_RingMotionCameraPos(RING_INFO *ring, sint16 target);
void	Inv_RingMotionCameraPitch(RING_INFO *ring, sint16 target);
void	Inv_RingMotionItemSelect(RING_INFO *ring, INVENTORY_ITEM *inv_item);
void	Inv_RingMotionItemDeselect(RING_INFO *ring, INVENTORY_ITEM *inv_item);

int	AddAssaultTime(uint32 time);
void ShowStatsText( char *time_str, int type );
void ShowGymStatsText( char *time_str, int type );
void ShowEndStatsText( void );
int PauseResetRequester( void );
int SureRequester( void );
void SetPCRequesterSize(REQUEST_INFO *req, int maxlines, int ypos);


/* option.c */
void	do_inventory_options(INVENTORY_ITEM *inv_item);
void	do_compass_option(INVENTORY_ITEM *inv_item);
void	do_passport_option(INVENTORY_ITEM *inv_item);
void	do_gamma_option(INVENTORY_ITEM *inv_item);
void	do_detail_option(INVENTORY_ITEM *inv_item);
void	do_sound_option(INVENTORY_ITEM *inv_item);
int		GetRenderHeight( void );
int		GetRenderWidth( void );

/* game.c */								  
extern	REQUEST_INFO Level_Select_Requester;
extern	REQUEST_INFO Load_Game_Requester;
extern	REQUEST_INFO Stats_Requester;
//extern	REQUEST_INFO Save_Game_Requester;
//extern	REQUEST_INFO Exit_Game_Requester;
//extern	REQUEST_INFO Play_FMV_Requester;

void GetValidLevelsList(REQUEST_INFO *req);
void GetSavedGamesList(REQUEST_INFO *req);


/* R e q u e s t e r   F u n c t i o n s */
extern uint32	RequesterFlags1[];
extern uint32	RequesterFlags2[];
#ifdef PC_VERSION
extern uint32	SaveGameReqFlags1[];
extern uint32	SaveGameReqFlags2[];
#endif


void Init_Requester(REQUEST_INFO *req);
void Remove_Requester(REQUEST_INFO *req);
sint32 Display_Requester(REQUEST_INFO *req, int des, int backgrounds);
void SetRequesterHeading( REQUEST_INFO *req, char *text1, uint32 flags1, char *text2, uint32 flags2 );
void RemoveAllReqItems( REQUEST_INFO *req );
void AddRequesterItem( REQUEST_INFO *req, char *text1, uint32 flags1, char *text2, uint32 flags2 );
void ChangeRequesterItem( REQUEST_INFO *req, int item, char *text1, uint32 flags1, char *text2, uint32 flags2 );


#ifdef __cplusplus
}
#endif

#endif	// INVDATA_H