/**************************************************************************************************************/
/*                                                                                                            */
/* Save/load game packet stuff                                                                 G.Rummery 1996 */
/*                                                                                                            */
/**************************************************************************************************************/

#ifndef SAVEGAME_H
#define SAVEGAME_H

#ifdef __cplusplus
extern "C" {
#endif

/**************************************** TYPE DEFINITIONS ****************************************************/

// Don't go over one memcard sector (7K)
#define MAX_SAVEGAME_BUFFER (6*1024+128)

typedef struct start_info {
	uint16 pistol_ammo, magnum_ammo, uzi_ammo, shotgun_ammo, m16_ammo, rocket_ammo, harpoon_ammo;
	unsigned char num_medis, num_big_medis, num_scions, num_flares;
	char gun_status, gun_type;
	uint16 available:1;
	uint16 got_pistols:1;
	uint16 got_magnums:1;
	uint16 got_uzis:1;
	uint16 got_shotgun:1;
	uint16 got_m16:1;
	uint16 got_rocket:1;
	uint16 got_harpoon:1;
	uint16 secrets; // actually records whether player has secrets at end of level

	uint32 	timer;
	uint32	ammo_used, ammo_hit;
	uint32	distance_travelled;
	uint16 	kills;
	unsigned char	secrets_found; // bit packed
	unsigned char	health_used; // in units of small medi paks	
} START_INFO;

typedef struct savegame_info {
	START_INFO start[MAXIMUM_LEVELS];
	uint32 	timer;
	uint32	ammo_used, ammo_hit;
	uint32	distance_travelled;
	uint16 	kills;
	unsigned char	secrets; // bit packed
	unsigned char	health_used; // in units of small medi paks	
	sint16 	current_level;
#ifdef PSX_VERSION
	sint16	iconfig;
	sint16	SFX_volume;
	sint16	CD_volume;
	sint16	screen_x,screen_y;
#endif
	unsigned char bonus_flag;
	unsigned char num_pickup1, num_pickup2;
	unsigned char num_puzzle1, num_puzzle2, num_puzzle3, num_puzzle4;
	unsigned char num_key1, num_key2, num_key3, num_key4;
	signed char	checksum;
	unsigned char mid_level_save:1;
	char   	buffer[MAX_SAVEGAME_BUFFER];
} SAVEGAME_INFO;

extern SAVEGAME_INFO savegame;
#ifdef PSX_VERSION
extern SAVEGAME_INFO savegame_backup;
#endif
extern int number_cameras;

#ifdef PSX_VERSION
#define iconfig  				savegame.iconfig
#define Option_SoundFX_Volume   savegame.SFX_volume
#define Option_Music_Volume		savegame.CD_volume
#endif

/************************************** FUNCTION PROTOTYPES ***************************************************/

int  LoadSaveGameInfo(void);
void InitialiseStartInfo(void);
void ModifyStartInfo(int level_number);
void CreateStartInfo(int level_number);
void CreateSaveGameInfo(void);
void ExtractSaveGameInfo(void);

void ResetSG();
void WriteSG(void *pointer, int size);
void ReadSG(void *pointer, int size);

int 	ConfirmSave(int fade);
void 	CompleteSave(int fade);
#ifdef PSX_VERSION
void	CreateCheckSum( SAVEGAME_INFO *sg );
#else
void	CreateCheckSum( void );
#endif
int		CheckSumValid( SAVEGAME_INFO *buffer );

/**********************************************************************************************************/

#ifdef __cplusplus
}
#endif

#endif
