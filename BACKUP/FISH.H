#define	MAX_FISH	8

typedef struct
{
	short	x;
	short	y;
	short	z;
	ushort	angle;
	signed char	angadd;
	unsigned char	speed;
	unsigned char	acc;
	unsigned char	swim;
} FISH_INFO;

typedef struct
{
	short	angle;
	unsigned	char	speed;
	unsigned  char	on;
	short	angle_time;
	short	speed_time;
	short	Xrange,Zrange,Yrange;
} LEADER_INFO;

extern	FISH_INFO	fish[];
extern	LEADER_INFO lead_info[];
extern	long	fish_ranges[8][3];
