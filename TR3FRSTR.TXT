// PC & PSX VERSION OF GENERAL GAME STRINGS

GAME_STRINGS:

INVENTAIRE
OPTIONS
OBJETS
PARTIE TERMINEE

Charger une partie
Sauvegarder la partie
Nouvelle Partie
Recommencer Niveau
Ecran d'Accueil
Quitter D)emo
Quitter le Jeu

Choisir un niveau
Sauvegarder Position

R)esolution
Haute
Moyenne
Basse

Marcher
Rouler
Courir
Gauche
Droite
Arri$ere
Pas $a gauche
?
Pas $a droite
?
Regarder
Sauter
Action
D)egainer
?
Inventaire
Se baisser
Sprinter

Statistiques
Pistolets
Fusil $a canon sci)e
Desert Eagle
Uzis
Fusil $a harpon
MP5
Lance-roquettes
Lance-grenades
Torche

Balles de pistolet
Cartouches de fusil
Balles de Desert Eagle
Balles pour Uzi
Harpons
Cartouches de MP5
Roquettes
Grenades

Petite trousse de secours
Grande trousse de secours
Objet
Enigme
Cl)e
Partie

La Demeure de Lara
Chargement en cours


Temps r)ealis)e
Secrets d)ecouverts
Lieu
Victimes
Munitions utilis)ees
Tirs r)eussis
Sauvegardes effectu)ees
Distance parcourue
Trousses de soins utilis)ees

Version 1.0

Aucun
Termin)e
MEILLEURS TEMPS
Pas de dur)ee
N/D
Position
Statistiques de fin
sur
En r)esum)e...
Pierre Infada
Dague d'Ora
L'oeil d'Isis
Objet 115

Cristal de sauvegarde
Londres
Nevada
Pacifique Sud
Antarctique
P)erou
Aventure
s

END:



PC_STRINGS:

R)esolution
Mode D)emo
Son
Contr(oles
Luminosit)e
R)egler Volume
Touches

Impossible de sauvegarder ce fichier
R)eessayer?
OUI
NON
Sauvegarde effectu)ee
Pas de sauvegarde
Non valable
Sauvegarder partie?
- EMPLACEMENT LIBRE -



NON
OUI
Config. carte sonore
Touches par d)efaut
DOZY

spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare

END:


PSX_STRINGS:

Centrer l'image
MODE DEMO
Son
Contr(oles
Luminosit)e
R)egler Volume
Type de contr(oles
Impossible de sauvegarder ce fichier
R)eessayer?
OUI
NON
Sauvegarde termin)ee
Pas de sauvegarde!
Non valable
Sauvegarder?
- EMPLACEMENT LIBRE -
Appuyez sur une touche pour quitter
Touches directionnelles
pour centrer l')ecran
> Choisir
; Revenir
> Continuer
PAUSE
Manette d)ebranch)ee
Sauvegarder sur
Ecraser partie sur
Charger $a partir de
la Carte M)emoire dans la
fente pour carte m)emoire No 1
Vous confirmez?
Oui
Non
est pleine
Pas de
partie de Tomb Raider III sur
Probl$eme de
Pas de Carte M)emoire dans
non format)ee
Souhaitez-vous
la formater?
Enregistrement sur
Charger $a partir de
Formatage en cours
Ecraser la partie
Sauvegarder la partie
Charger la partie
Formater la Carte M)emoire
Quitter
Continuer
Impossible de
sauvegarder une partie
si vous n'ins)erez pas une
carte m)emoire format)ee
avec au moins deux blocs libres
La Carte M)emoire dans
Quitter sans sauvegarder
Quitter sans charger
Jouer
CARTE MEMOIRE NON FORMATEE
Ins)erez une carte format)ee
ou appuyez sur > pour continuer
sans sauvegarder

// PAL CODES: ENGLISH= SLES-00718, FRENCH= SLES-0719, GERMAN= SLES-0720
bu00::BESLES-01682TOMB3
// NTSC CODES: US= UNKNOWN, JAPAN= UNKNOWN
bu00::BASLUS-00691TOMB3

Chargement OK
Echec chargement
Sauvegarde OK
Echec sauvegarde
Formatage OK
Echec formatage
Vibration Non
Vibration Oui
Option Vibration
Libre
Libre
Libre
Libre
Libre
Libre
Choix Aventure

END:
