/****************************************************************************
*
* 51ROCKET.H
*
* PROGRAMMER : Gibby
*    VERSION : 00.00
*    CREATED : 20/08/98
*   MODIFIED : 20/08/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for 51ROCKET.C
*
*****************************************************************************/

#ifndef _51ROCKET_H
#define _51ROCKET_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC)

typedef void (ROCKET51FUNC1)(long, long, long, long);
typedef void (ROCKET51FUNC2)(long, long, long, long, long);

#define ControlArea51Rocket ((VOIDFUNCSINT16*) Effect1Ptr[0])
#define Trigger51RocketSmoke	((ROCKET51FUNC1*) Effect1Ptr[1])
#define Trigger51BlastFire	((ROCKET51FUNC2*) Effect1Ptr[2])
#define ControlArea51Struts ((VOIDFUNCSINT16*) Effect1Ptr[3])
#define InitialiseArea51Struts ((VOIDFUNCSINT16*) Effect1Ptr[4])

#else

void ControlArea51Rocket(sint16 item_number);
void Trigger51RocketSmoke(long x, long y, long z, long yv, long fire);
void Trigger51BlastFire(long x, long y, long z, long smoke, long end);
void ControlArea51Struts(sint16 item_number);
void InitialiseArea51Struts(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
