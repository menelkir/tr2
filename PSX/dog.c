/*********************************************************************************************/
/*                                                                                           */
/* Dog Control                                                                         TOMB2 */
/*                                                                                           */
/*********************************************************************************************/

#include "game.h"

/*********************************** TYPE DEFINITIONS ****************************************/

#define DOG_BITE_DAMAGE 100
#define DOG_LEAP_DAMAGE 200
#define DOG_LUNGE_DAMAGE 100

#define TIGER_BITE_DAMAGE 150

//-------------------------------------------------------------------------------------------------

/* Define all the bits of a Doberman that count as pain if they touch Lara */
#define DOG_TOUCH (0x13f7000)

enum dog_anims {DOG_EMPTY, DOG_WALK, DOG_RUN, DOG_STOP, DOG_BARK, DOG_CROUCH, DOG_STAND, DOG_ATTACK1, DOG_ATTACK2, DOG_ATTACK3, DOG_DEATH};

#define DOG_DIE_ANIM 13

#define DOG_WALK_TURN (3*ONE_DEGREE)
#define DOG_RUN_TURN  (6*ONE_DEGREE)

#define DOG_ATTACK1_RANGE SQUARE(WALL_L/3)
#define DOG_ATTACK2_RANGE SQUARE(WALL_L*3/4)
#define DOG_ATTACK3_RANGE SQUARE(WALL_L*2/3)

#define DOG_BARK_CHANCE		0x300
#define DOG_CROUCH_CHANCE	(DOG_BARK_CHANCE + 0x300)
#define DOG_STAND_CHANCE	(DOG_CROUCH_CHANCE + 0x500)
#define DOG_WALK_CHANCE		(DOG_CROUCH_CHANCE + 0x2000)

#define DOG_UNCROUCH_CHANCE 0x100
#define DOG_UNSTAND_CHANCE  0x200
#define DOG_UNBARK_CHANCE   0x500

BITE_INFO dog_bite = {0,30,141, 20};


#define TIGER_TOUCH (0x7fdc000)

enum tiger_anims {TIGER_EMPTY, TIGER_STOP, TIGER_WALK, TIGER_RUN, TIGER_WAIT, TIGER_ROAR, TIGER_ATTACK1, TIGER_ATTACK2, TIGER_ATTACK3, TIGER_DEATH};

#define TIGER_DIE_ANIM 11

#define TIGER_WALK_TURN (3*ONE_DEGREE)
#define TIGER_RUN_TURN  (6*ONE_DEGREE)

#define TIGER_ATTACK1_RANGE SQUARE(WALL_L/3)
#define TIGER_ATTACK2_RANGE SQUARE(WALL_L*3/2)
#define TIGER_ATTACK3_RANGE SQUARE(WALL_L)

#define TIGER_ROAR_CHANCE 0x60
#define TIGER_WALK_CHANCE (TIGER_ROAR_CHANCE + 0x400)
#define COBRA_UPSET_SPEED 15
#define FEELER_DISTANCE 2000
#define SHORT_FEELER_DISTANCE 1000
#define FEELER_ANGLE (60*ONE_DEGREE)
#define MY_FRONT_ARC (45*ONE_DEGREE)

//added by TS
int turning = -1;
int stuck = 0;
int last_angle = 0;
sint32 oldtargetx = 0;
sint32 oldtargetz = 0;
sint32 myoldx = 0;
sint32 myoldz = 0;
sint32 D2 = 0;
BITE_INFO tiger_bite = {19,-13,3, 26};

/*********************************** FUNCTION CODE *******************************************/

void DogControl(sint16 item_number)
{
	ITEM_INFO *item;
	CREATURE_INFO *dog;
	sint16 angle, head, tilt, random;
	AI_INFO info;

	if (!CreatureActive(item_number))
		return;

	item = &items[item_number];
	dog = (CREATURE_INFO *)item->data;
	head = angle = tilt = 0;

	if (item->hit_points <= 0)
	{
		if (item->current_anim_state != DOG_DEATH)
		{
			item->anim_number = objects[DOG].anim_index + DOG_DIE_ANIM;
			item->frame_number = anims[item->anim_number].frame_base;
			item->current_anim_state = DOG_DEATH;
		}
	}
	else
	{
		CreatureAIInfo(item, &info);

		if (info.ahead)
			head = info.angle;

		CreatureMood(item, &info, TIMID);

		angle = CreatureTurn(item, dog->maximum_turn);

		switch (item->current_anim_state)
		{
		case DOG_STOP:
			dog->maximum_turn = 0;
			dog->flags = 0;

			if (dog->mood == BORED_MOOD)
			{
				if (item->required_anim_state)
					item->goal_anim_state = item->required_anim_state;
				else
				{
					random =(sint16) GetRandomControl();
					if (random < DOG_BARK_CHANCE)
						item->goal_anim_state = DOG_BARK;
					else if (random < DOG_CROUCH_CHANCE)
						item->goal_anim_state = DOG_CROUCH;
					else if (random < DOG_WALK_CHANCE)
						item->goal_anim_state = DOG_WALK;
				}
			}
			else if (dog->mood == ESCAPE_MOOD)
				item->goal_anim_state = DOG_RUN;
			else if (info.distance < DOG_ATTACK1_RANGE && info.ahead)
				item->goal_anim_state = DOG_ATTACK1;
			else
				item->goal_anim_state = DOG_RUN;
			break;

		case DOG_WALK:
			dog->maximum_turn = DOG_WALK_TURN;

			if (dog->mood == BORED_MOOD)
			{
				random =(sint16) GetRandomControl();
				if (random < DOG_BARK_CHANCE)
				{
					item->required_anim_state = DOG_BARK;
					item->goal_anim_state = DOG_STOP;
				}
				else if (random < DOG_CROUCH_CHANCE)
				{
					item->required_anim_state = DOG_CROUCH;
					item->goal_anim_state = DOG_STOP;
				}
				else if (random < DOG_STAND_CHANCE)
					item->goal_anim_state = DOG_STOP;
			}
			else
				item->goal_anim_state = DOG_RUN;
			break;

		case DOG_RUN:
			tilt = angle;
			dog->maximum_turn = DOG_RUN_TURN;

			if (dog->mood == BORED_MOOD)
				item->goal_anim_state = DOG_STOP;
			else if (info.distance < DOG_ATTACK2_RANGE)
				item->goal_anim_state = DOG_ATTACK2;
			break;

		case DOG_ATTACK2:
			// Charging leap attack
			if (dog->flags!=2 && (item->touch_bits & DOG_TOUCH))
			{
				CreatureEffect(item, &dog_bite, DoBloodSplat);
				lara_item->hit_points -= DOG_LEAP_DAMAGE;
				lara_item->hit_status = 1;
				dog->flags = 2;
			}

			// If leap ends near to Lara, bite or lunge to get her
			if (info.distance < DOG_ATTACK1_RANGE)
				item->goal_anim_state = DOG_ATTACK1;
			else if (info.distance < DOG_ATTACK3_RANGE)
				item->goal_anim_state = DOG_ATTACK3;
			break;

		case DOG_ATTACK1:
			// Standing bite attack
			dog->maximum_turn = 0;

			if (dog->flags!=1 && info.ahead && (item->touch_bits & DOG_TOUCH))
			{
				CreatureEffect(item, &dog_bite, DoBloodSplat);
				lara_item->hit_points -= DOG_BITE_DAMAGE;
				lara_item->hit_status = 1;
				dog->flags = 1;
			}

			// If Lara moves a small distance away, lunge at her
			if (info.distance > DOG_ATTACK1_RANGE && info.distance < DOG_ATTACK3_RANGE)
				item->goal_anim_state = DOG_ATTACK3;
			else
				item->goal_anim_state = DOG_STOP;
			break;

		case DOG_ATTACK3:
			// Lunging jump attack
			dog->maximum_turn = DOG_RUN_TURN;

			if (dog->flags!=3 && (item->touch_bits & DOG_TOUCH))
			{
				CreatureEffect(item, &dog_bite, DoBloodSplat);
				lara_item->hit_points -= DOG_LUNGE_DAMAGE;
				lara_item->hit_status = 1;
				dog->flags = 3;
			}

			// If lunge results in arriving next to Lara, bite her
			if (info.distance < DOG_ATTACK1_RANGE)
				item->goal_anim_state = DOG_ATTACK1;
			break;

		case DOG_BARK:
			if (dog->mood != BORED_MOOD || GetRandomControl() < DOG_UNBARK_CHANCE)
				item->goal_anim_state = DOG_STOP;
			break;

		case DOG_CROUCH:
			if (dog->mood != BORED_MOOD || GetRandomControl() < DOG_UNCROUCH_CHANCE)
				item->goal_anim_state = DOG_STOP;
			break;

		case DOG_STAND:
			if (dog->mood != BORED_MOOD || GetRandomControl() < DOG_UNSTAND_CHANCE)
				item->goal_anim_state = DOG_STOP;
			break;
		}
	}

	CreatureTilt(item, tilt);
	CreatureHead(item, head);

	CreatureAnimation(item_number, angle, tilt);
}


void TigerControl(sint16 item_number)
{
	ITEM_INFO *item;
	CREATURE_INFO *tiger;
	sint16 angle, head, tilt, random;
	AI_INFO info;
	//added by TS
	int isLOS;
	GAME_VECTOR start, target;
	int index, done, box_number;
	BOX_INFO *box;

	if (!CreatureActive(item_number))
		return;

	item = &items[item_number];
	tiger = (CREATURE_INFO *)item->data;
	head = angle = tilt = 0;

	if (item->hit_points <= 0)
	{
		if (item->current_anim_state != TIGER_DEATH)
		{
			item->anim_number = objects[TIGER].anim_index + TIGER_DIE_ANIM;
			item->frame_number = anims[item->anim_number].frame_base;
			item->current_anim_state = TIGER_DEATH;
		}
	}
	else
	{
		CreatureAIInfo(item, &info);

		if (info.ahead)
			head = info.angle;

//added by TS

		//added by TS -  LOS stuff
//	if (info->ahead && info->distance < PEOPLE_SHOOT_RANGE)


		start.x = item->pos.x_pos;
		start.y = item->pos.y_pos - STEP_L*3;
		start.z = item->pos.z_pos;
		start.room_number = item->room_number;

		target.x = lara_item->pos.x_pos;
		target.y = lara_item->pos.y_pos - STEP_L*3;
		target.z = lara_item->pos.z_pos;

		isLOS = LOS(&start, &target);
		box = &boxes[lara_item->box_number];

		

//	if (lara.gun_status == LG_ARMLESS)
//		item->goal_anim_state = TIGER_WALK;

//		printf("MyBox: %d, LBox: %d, MyZone: %d, LZone: %d, MyAng: %d, LFace: %d\n",item->box_number, lara_item->box_number, info.zone_number , info.enemy_zone, info.angle, info.enemy_facing);
//	else	
//	{
//		item->goal_anim_state = TIGER_STOP;
//			
//		printf("MyBox: %d, LBox: %d, MyZone: %d, LZone: %d, MyAng: %d, LFace: %d\n",item->box_number, lara_item->box_number, info.zone_number , info.enemy_zone, info.angle, info.enemy_facing);

//		printf("LaraBox: %d, L: %d, R: %d, T: %d, B: %d, H: %d\n",lara_item->box_number, box->left, box->right, box->top, box->bottom, box->height);

//		printf("LaraBox: %d, X: %d, Y: %d, Z: %d, TigerBox: %d, X: %d, Y: %d, Z: %d\n",lara_item->box_number, lara_item->pos.x_pos, lara_item->pos.y_pos, lara_item->pos.z_pos, item->room_number, item->pos.x_pos,item->pos.y_pos,item->pos.z_pos);
		
//		printf("LOS: %d, Ahead: %d, Distance: %d, Hit: %d, Mood: %d, HPs: %d, Flags: %d\n",isLOS, info.ahead, info.distance >> 12, item->hit_status, tiger->mood, item->hit_points, tiger->flags);
//	}


//end of LOS stuff

//		if (isLOS && info.zone_number == info.enemy_zone)
//		{
//			tiger->target.x = lara_item->pos.x_pos;
//			tiger->target.z = lara_item->pos.z_pos;
//		}
//		else
//		{
//			CreatureMood(item, &info, VIOLENT);
//		}

					/* Expand node */

/*
		box = &boxes[lara_item->box_number];
		printf("%d : ",lara_item->box_number);
		index = box->overlap_index & OVERLAP_INDEX;
		done = 0;
		do
		{
			box_number = overlap[index++];
			printf("%d ",box_number);
			if (box_number & END_BIT)
			{
				done = 1;
				box_number &= BOX_NUMBER;
				printf("\n");
			}
*/

//			/* Allow for zone considerations */
//			if (search_zone != zone[box_number])
//				continue;

//			/* Allow for LOT consideration */
//			change = boxes[box_number].height - box->height;
//			if (change > LOT->step || change < LOT->drop)
//				continue;
//		}
//		while (!done);



	//	CreatureMood(item, &info, VIOLENT);
		
		//next lines addded by TS
	//	if (lara_item->speed > 0) 
	//		item->speed = lara_item->speed;
	//	else 
	//		item->speed = 1;
	/*
		if (item->current_anim_state == TIGER_RUN);
		item->goal_anim_state = TIGER_STOP;
				
		if (item->current_anim_state == TIGER_STOP);
			item->goal_anim_state = TIGER_WALK;

	//	item->current_anim_state = TIGER_WALK;
	*/
	

//		angle = CreatureTurn(item, tiger->maximum_turn);
		
		//next lines addded by TS
//		if (info.ahead && info.distance > TIGER_ATTACK2_RANGE)
//		item->goal_anim_state = TIGER_ROAR;
//		else if ((info.ahead && info.distance < TIGER_ATTACK3_RANGE) || (item->current_anim_state == TIGER_STOP && lara_item->speed > COBRA_UPSET_SPEED))
//		item->goal_anim_state = TIGER_ATTACK3;
//		else 
//		item->goal_anim_state = TIGER_STOP;

	



	switch (item->current_anim_state)
		{
		case TIGER_STOP:
			tiger->maximum_turn = 0;
			tiger->flags = 0;


			if (tiger->mood == ESCAPE_MOOD)
				item->goal_anim_state = TIGER_RUN;
			else if (tiger->mood == BORED_MOOD)
			{
				random =(sint16) GetRandomControl();
				if (random < TIGER_ROAR_CHANCE)
					item->goal_anim_state = TIGER_ROAR;
				else if (random < TIGER_WALK_CHANCE);
					item->goal_anim_state = TIGER_WALK;
			}
			else if (info.ahead && info.distance < TIGER_ATTACK1_RANGE)
				item->goal_anim_state = TIGER_ATTACK1;
			else if (info.ahead && info.distance < TIGER_ATTACK3_RANGE)
			{
				tiger->maximum_turn = TIGER_WALK_TURN;
				item->goal_anim_state = TIGER_ATTACK3;
			}
			else if (item->required_anim_state)
				item->goal_anim_state = item->required_anim_state;
			else if (tiger->mood != ATTACK_MOOD && GetRandomControl() < TIGER_ROAR_CHANCE)
				item->goal_anim_state = TIGER_ROAR;
			else
				item->goal_anim_state = TIGER_RUN;

			break;

		case TIGER_WALK:
			tiger->maximum_turn = TIGER_WALK_TURN;

			if (tiger->mood == ESCAPE_MOOD || tiger->mood == ATTACK_MOOD)
				item->goal_anim_state = TIGER_RUN;
			else if (GetRandomControl() < TIGER_ROAR_CHANCE)
			{
				item->goal_anim_state = TIGER_STOP;
				item->required_anim_state = TIGER_ROAR;
			}
			break;

		case TIGER_RUN:
			tiger->maximum_turn = TIGER_RUN_TURN;

			if (tiger->mood == BORED_MOOD)
				item->goal_anim_state = TIGER_STOP;
			else if (tiger->flags && info.ahead)
				/* After TIGER_ATTACK2, if close to Lara, stop and start close attacks */
				item->goal_anim_state = TIGER_STOP;
			else if (info.ahead && info.distance < TIGER_ATTACK2_RANGE)
			{
				if (lara_item->speed < 5) // this maybe should be a small range rather than zero
					item->goal_anim_state = TIGER_STOP;
				else
					item->goal_anim_state = TIGER_ATTACK2;
			}
			else if (tiger->mood != ATTACK_MOOD && GetRandomControl() < TIGER_ROAR_CHANCE)
			{
				item->required_anim_state = TIGER_ROAR;
				item->goal_anim_state = TIGER_STOP;
			}

			tiger->flags = 0;
			break;

		case TIGER_ATTACK1:
		case TIGER_ATTACK2:
		case TIGER_ATTACK3:
			if (!tiger->flags && (item->touch_bits & TIGER_TOUCH))
			{
				lara_item->hit_status = 1;
				lara_item->hit_points -= TIGER_BITE_DAMAGE;
				CreatureEffect(item, &tiger_bite, DoBloodSplat);

				tiger->flags = 1;
			}
			break;
		}
	
	}

	//added by TS

//		tiger->maximum_turn = TIGER_WALK_TURN;
//if (info.zone_number == info.enemy_zone && isLOS)
if (info.zone_number == info.enemy_zone)
{
	if (isLOS || item->hit_status)
		tiger->mood = ATTACK_MOOD;
//		tiger->target.x = lara_item->pos.x_pos;
//		tiger->target.z = lara_item->pos.z_pos;
	else {
		tiger->mood = STALK_MOOD;
		item->goal_anim_state = TIGER_STOP;
	}
//else if (info.zone_number == info.enemy_zone)
//{
//	tiger->mood = STALK_MOOD;
//}
}
else if (isLOS || item->hit_status)
{
	tiger->mood = ESCAPE_MOOD;
//	tiger->target.x = 0;
//	tiger->target.y = 0;
}
else 
{
	tiger->mood = STALK_MOOD;
}

//	printf("Target X: %d, TargetZ: %d, My Speed: %d, Goal: %d, Current: %d\n",tiger->target.x, tiger->target.z, item->speed, item->goal_anim_state, item->current_anim_state);


//Added by TS - height checking stuff
		{
	sint16 room_number;
	sint32 ceiling, midheight, laraheight, laraleft, lararight;
	sint32 leftheight, rightheight, shleft, shright, shmid, height;
	sint32 feel_x, feel_y, feel_z;
	FLOOR_INFO *floor;

//for tiger
	height = item->pos.y_pos;

	feel_z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
	feel_x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
	feel_y = item->pos.y_pos;
	room_number = item->room_number;
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	midheight = GetHeight(floor, feel_x, feel_y, feel_z) - height;

	feel_z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
	feel_x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	leftheight = GetHeight(floor, feel_x, feel_y, feel_z) - height;

	feel_z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
	feel_x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	rightheight = GetHeight(floor, feel_x, feel_y, feel_z) - height;

	feel_z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
	feel_x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	shmid = GetHeight(floor, feel_x, feel_y, feel_z) - height;

	feel_z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
	feel_x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	shleft = GetHeight(floor, feel_x, feel_y, feel_z) - height;

	feel_z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
	feel_x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	shright = GetHeight(floor, feel_x, feel_y, feel_z) - height;

/*
//for Lara
	feel_z = lara_item->pos.z_pos + (FEELER_DISTANCE * phd_cos(lara_item->pos.y_rot) >> W2V_SHIFT);
	feel_x = lara_item->pos.x_pos + (FEELER_DISTANCE * phd_sin(lara_item->pos.y_rot) >> W2V_SHIFT);
	feel_y = lara_item->pos.y_pos;
	room_number = lara_item->room_number;
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	laraheight = GetHeight(floor, feel_x, feel_y, feel_z);
//for Lara's Left
	feel_z = lara_item->pos.z_pos + (FEELER_DISTANCE * (phd_cos(lara_item->pos.y_rot - FEELER_ANGLE)) >> W2V_SHIFT);
	feel_x = lara_item->pos.x_pos + (FEELER_DISTANCE * (phd_sin(lara_item->pos.y_rot - FEELER_ANGLE)) >> W2V_SHIFT);
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	laraleft = GetHeight(floor, feel_x, feel_y, feel_z);
//for Lara's Right
	feel_z = lara_item->pos.z_pos + (FEELER_DISTANCE * (phd_cos(lara_item->pos.y_rot + FEELER_ANGLE)) >> W2V_SHIFT);
	feel_x = lara_item->pos.x_pos + (FEELER_DISTANCE * (phd_sin(lara_item->pos.y_rot + FEELER_ANGLE)) >> W2V_SHIFT);
	floor = GetFloor(feel_x, feel_y, feel_z, &room_number);
	lararight = GetHeight(floor, feel_x, feel_y, feel_z);

	printf("Tiger: %d, Lara... Left: %d, Forward: %d, Right: %d, Y: %d\n", height,  laraleft, laraheight, lararight, lara_item->pos.y_pos);
	
*/
//printf("Short - L: %d, M: %d, R: %d, Long - L: %d, M: %d, R: %d\n",  shleft, shmid, shright, leftheight, midheight, rightheight);
		
//Right, now we use all this height info - added by TS
	
switch (tiger->mood)
{
case STALK_MOOD:
	if (info.ahead)
	item->goal_anim_state = TIGER_STOP;
case ATTACK_MOOD:
// *** should do the blocked tests first (and record them) for the six positions to make this code more efficient
if (!info.ahead) //perhaps we should distinguish *where* behind us - to enable quickest turn
{
	/*
	if (shleft > shright && shleft > -STEP_L && shleft < STEP_L)
	{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
			printf("BA...");
	}
	else if  (shright >= shleft && shright > -STEP_L && shright < STEP_L)
	{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
			printf("BB...");
	}
	else if (shmid > -STEP_L && shmid <STEP_L)
	{
		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
			printf("BC...");
	}
	else 
	{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		printf("BD...");
	}
	*/

//	if (last_angle == 0)
//	{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	}
//	else
//	{
//			tiger->target.x = oldtargetx;
//			tiger->target.z = oldtargetz;
//	}

	//	tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE * turning) >> W2V_SHIFT);
	//	tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE * turning) >> W2V_SHIFT);

//	printf("Behind...");

}
else // Lara *is* in our front arc
{
	if (info.angle < -MY_FRONT_ARC) //Lara's to the left of me
	{
		if (info.distance < SHORT_FEELER_DISTANCE || (info.distance < FEELER_DISTANCE && shleft > -STEP_L && shleft < STEP_L))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("LA...");
		}
		else if (shleft > -STEP_L && shleft < STEP_L && (leftheight - shleft) > -STEP_L && (leftheight -shleft) < STEP_L)
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("LB...");
		}
		//The way left is blocked, so try ahead then right
		else if (shmid > -STEP_L && shmid < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
//	printf("LC...");
		}
		else if  (shright > -STEP_L && shright < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
//	printf("LD...");
		}
		else // ***need another clause here in case way left is blocked (unlikely to get into this position tho')
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
//	printf("LE...");
		}
	}
	else if (info.angle > MY_FRONT_ARC)	//Lara's to the right
	{
		if (info.distance < SHORT_FEELER_DISTANCE || (info.distance < FEELER_DISTANCE && shright > -STEP_L && shright < STEP_L))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("RA...");
		}
		else if (shright > -STEP_L && shright < STEP_L && (rightheight - shright) > -STEP_L && (rightheight -shright) < STEP_L)
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("RB...");
		}
		//The way right is blocked, so try ahead then left
		else if (shmid > -STEP_L && shmid < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
//	printf("RC...");
		}
		else if  (shleft > -STEP_L && shleft < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
//	printf("RD...");
		}
		else // ***need another clause here in case way right is blocked (unlikely to get into this position tho')
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
//	printf("RE...");
		}
	}
	else	//Here I am, stuck in the middle with you :)
	{
		if (info.distance < SHORT_FEELER_DISTANCE || (info.distance < FEELER_DISTANCE && shmid > -STEP_L && shmid < STEP_L))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("A...");
		}
		else if (shmid > -STEP_L && shmid < STEP_L && (midheight - shmid) > -STEP_L && (midheight -shmid) < STEP_L)
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("B...");
		}
		//The way ahead is blocked, so try left then right
		else if (shleft > shright && shleft > -STEP_L && shleft < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
//	printf("C...");
		}
		else if  (shright >= shleft && shright > -STEP_L && shright < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
//	printf("D...");
		}
		else if (shmid > -STEP_L && shmid < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
//	printf("E...");
		}
		/*
		else if (GetRandomControl() & 1) 
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE * 2) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE * 2) >> W2V_SHIFT);
			printf("F...");
		}
		*/
		else 
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + 2 * FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + 2 * FEELER_ANGLE) >> W2V_SHIFT);
//		tiger->target.x = 55000;
//		tiger->target.z = 89000;
			
//printf("G...");
		}

	}
}
//printf("ATTACK\n");
break;
//case STALK_MOOD:
//	if (info.ahead)
//	item->goal_anim_state = TIGER_STOP;
case BORED_MOOD:
case ESCAPE_MOOD:
// *** should do the blocked tests first (and record them) for the six positions to make this code more efficient
if (!info.ahead) //perhaps we should distinguish *where* behind us - to enable quickest turn
{

	if (shleft > shright && shleft > -STEP_L && shleft < STEP_L)
	{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
//	printf("BA...");
	}
	else if  (shright >= shleft && shright > -STEP_L && shright < STEP_L)
	{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
//	printf("BB...");
	}
	else if (shmid > -STEP_L && shmid <STEP_L)
	{
		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
//	printf("BC...");
	}
	else 
	{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + turning * FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + turning * FEELER_ANGLE) >> W2V_SHIFT);
//printf("BD...");
	}
	
//			tiger->target.x = lara_item->pos.x_pos;
//			tiger->target.z = lara_item->pos.z_pos;
//			printf("Behind...");

}
else // Lara *is* in our front arc
{
	if (info.angle > MY_FRONT_ARC) //Lara's to the right of me
	{
		if ((info.zone_number == info.enemy_zone) && (info.distance < SHORT_FEELER_DISTANCE || (info.distance < FEELER_DISTANCE && shleft > -STEP_L && shleft < STEP_L)))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("LA...");
		}
		else if ((info.zone_number == info.enemy_zone) && (shleft > -STEP_L && shleft < STEP_L && (leftheight - shleft) > -STEP_L && (leftheight -shleft) < STEP_L))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("LB...");
		}
		//The way left is blocked, so try ahead then right
		else if (shmid > -STEP_L && shmid < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
//	printf("LC...");
		}
		else if  (shright > -STEP_L && shright < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
//	printf("LD...");
		}
		else // ***need another clause here in case way left is blocked (unlikely to get into this position tho')
		{
//		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(-item->pos.y_rot) >> W2V_SHIFT);
//		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(-item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + turning *  FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + turning *  FEELER_ANGLE) >> W2V_SHIFT);
//	printf("LE...");
		}
	}
	else if (info.angle < -MY_FRONT_ARC)	//Lara's to the left
	{
		if ((info.zone_number == info.enemy_zone) && (info.distance < SHORT_FEELER_DISTANCE || (info.distance < FEELER_DISTANCE && shright > -STEP_L && shright < STEP_L)))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("RA...");
		}
		else if ((info.zone_number == info.enemy_zone) && (shright > -STEP_L && shright < STEP_L && (rightheight - shright) > -STEP_L && (rightheight -shright) < STEP_L))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("RB...");
		}
		//The way right is blocked, so try ahead then left
		else if (shmid > -STEP_L && shmid < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
//	printf("RC...");
		}
		else if  (shleft > -STEP_L && shleft < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
//	printf("RD...");
		}
		else // ***need another clause here in case way right is blocked (unlikely to get into this position tho')
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + turning *  FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + turning *  FEELER_ANGLE) >> W2V_SHIFT);
//	printf("RE...");
		}
	}
	else	//Here I am, stuck in the middle with you :)
	{
		if ((info.zone_number == info.enemy_zone) && (info.distance < SHORT_FEELER_DISTANCE || (info.distance < FEELER_DISTANCE && shmid > -STEP_L && shmid < STEP_L)))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("A...");
		}
		else if ((info.zone_number == info.enemy_zone) && (shmid > -STEP_L && shmid < STEP_L && (midheight - shmid) > -STEP_L && (midheight -shmid) < STEP_L))
		{
			tiger->target.x = lara_item->pos.x_pos;
			tiger->target.z = lara_item->pos.z_pos;
//	printf("B...");
		}
		//The way ahead is blocked, so try left then right
		else if (shleft > shright && shleft > -STEP_L && shleft < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE) >> W2V_SHIFT);
//	printf("C...");
		}
		else if  (shright >= shleft && shright > -STEP_L && shright < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + FEELER_ANGLE) >> W2V_SHIFT);
//	printf("D...");
		}
		else if (shmid > -STEP_L && shmid < STEP_L)
		{
		tiger->target.x = item->pos.x_pos + (FEELER_DISTANCE * phd_sin(item->pos.y_rot) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (FEELER_DISTANCE * phd_cos(item->pos.y_rot) >> W2V_SHIFT);
//	printf("E...");
		}
		/*
		else if (GetRandomControl() & 1) 
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot - FEELER_ANGLE * 2) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot - FEELER_ANGLE * 2) >> W2V_SHIFT);
			printf("F...");
		}
		*/
		else 
		{
		tiger->target.x = item->pos.x_pos + (SHORT_FEELER_DISTANCE * phd_sin(item->pos.y_rot + turning *  FEELER_ANGLE) >> W2V_SHIFT);
		tiger->target.z = item->pos.z_pos + (SHORT_FEELER_DISTANCE * phd_cos(item->pos.y_rot + turning *  FEELER_ANGLE) >> W2V_SHIFT);
//		tiger->target.x = 55000;
//		tiger->target.z = 89000;
			
//printf("G...");
	}

	}
}
//printf("ESCAPE\n");

} //for switch

		
		
//		if (item->current_anim_state == TIGER_RUN) item->goal_anim_state = TIGER_STOP;
//		else item->goal_anim_state = TIGER_WALK;


//end of height checking

//	if (lara.gun_status == LG_ARMLESS)
	
//		printf("Short - L: %d, M: %d, R: %d, Long - L: %d, M: %d, R: %d\n",  shleft, shmid, shright, leftheight, midheight, rightheight);
//	else
//		printf("Mood: %d, MyX: %d, MyZ: %d, TargetX: %d, TargetZ: %d\n", tiger->mood, item->pos.x_pos, item->pos.z_pos, tiger->target.x, tiger->target.z);
	}
//LOS stuff *was * here
		angle = CreatureTurn(item, tiger->maximum_turn);
	D2 = 	((myoldx - item->pos.x_pos)*(myoldx - item->pos.x_pos)+(myoldz - item->pos.z_pos)*(myoldz - item->pos.z_pos));
// printf("Mood: %d, LOS: %d, Turn: %d, Speed: %d, Ang: %d, CAnm: %d, GAnm: %d, D2: %d\n", tiger->mood, isLOS, turning, item->speed, angle, item->current_anim_state, item->goal_anim_state,D2);

		myoldx = item->pos.x_pos;
		myoldz = item->pos.z_pos;
		oldtargetx = tiger->target.x;
		oldtargetz = tiger->target.z;



//Added by TS - testing mood switching
		if (item->goal_anim_state!=TIGER_STOP && D2 < 100)
		{
			stuck++;
			if (stuck > 10)
			{
				stuck = 0;
				turning *= -1;
				item->goal_anim_state = TIGER_STOP;
			}
		}
	if (item->hit_status) 
	{
		turning *= -1;
	}

//end of mood switching stuff
	CreatureTilt(item, tilt);
	CreatureHead(item, head);

	CreatureAnimation(item_number, angle, tilt);
}
