@echo off

z:
pushd
cd \tomb21\graphics\wads
c:

rem
rem Title
rem
tom2fab /r0.7 /g0.9 /b0.6 /i0.6 /j0.8 /k0.5 z:title.tom data\title

rem
rem House
rem
tom2fab /r0.7 /g0.9 /b0.6 /i0.6 /j0.8 /k0.5 /lHOUSE z:house.tom data\house

rem
rem Jungle
rem
tom2fab /r0.7 /g0.9 /b0.6 /i0.6 /j0.8 /k0.5 /lJUNGLE z:jungle.tom data\jungle
rem
rem Temple
rem
tom2fab /r0.7 /g0.9 /b0.6 /i0.6 /j0.8 /k0.5 /lTEMPLE z:temple.tom data\temple
rem
rem Quadbike chase
rem
tom2fab /r0.7 /g0.9 /b0.6 /i0.6 /j0.8 /k0.5 /lQUADCHAS z:quadchas.tom data\quadchas
rem
rem India boss
rem
tom2fab /r0.7 /g0.9 /b0.6 /i0.6 /j0.8 /k0.5 /lTONYBOSS z:tonyboss.tom data\tonyboss

rem
rem Shore
rem
tom2fab /r0.8 /g1.5 /b1.5 /i0.5 /j0.8 /k0.8 /lSHORE z:shore.tom data\shore
rem
rem Rapids
rem
tom2fab /r0.6 /g0.6 /b0.6 /i0.6 /j0.6 /k0.6 /lRAPIDS z:rapids.tom data\rapids
rem
rem Crash
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lCRASH z:crash.tom data\crash
rem
rem Tribe Boss
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lTRIBOSS z:triboss.tom data\triboss

rem
rem Nevada
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lNEVADA z:nevada.tom data\nevada
rem
rem Compound
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lCOMPOUND z:compound.tom data\compound
rem
rem Area 51
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lAREA51 z:area51.tom data\area51

rem
rem Rooftops
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lROOFS z:roofs.tom data\roofs
rem
rem St. Paul
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lSTPAUL z:stpaul.tom data\stpaul
rem
rem Sewer
rem
tom2fab /r1.0 /g1.0 /b1.0 /i0.7 /j0.6 /k0.4 /lSEWER z:sewer.tom data\sewer
rem
rem Tower
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lTOWER z:tower.tom data\tower
rem
rem Office
rem
tom2fab /r1.0 /g1.0 /b1.0 /i1.0 /j1.0 /k1.0 /lOFFICE z:office.tom data\office

rem
rem Antarc
rem
tom2fab /r0.2 /g0.5 /b0.4 /i0.2 /j0.5 /k0.4 /lANTARC z:antarc.tom data\antarc
rem
rem Mines
rem
tom2fab /r0.2 /g0.5 /b0.4 /i0.2 /j0.5 /k0.4 /lMINES z:mines.tom data\mines
rem
rem City
rem
tom2fab /r0.2 /g0.5 /b0.4 /i0.2 /j0.5 /k0.4 /lCITY z:city.tom data\city
rem
rem Chamber
rem
tom2fab /r0.2 /g0.5 /b0.4 /i0.2 /j0.5 /k0.4 /lCHAMBER z:chamber.tom data\chamber

rem
rem Cutscenes
rem
call getcut cut1
call getcut cut2
call getcut cut3
call getcut cut4
call getcut cut5
call getcut cut6
call getcut cut7
call getcut cut8
call getcut cut9
call getcut cut11
call getcut cut12

z:
popd
c:
