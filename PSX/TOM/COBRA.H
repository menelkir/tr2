/****************************************************************************
*
* COBRA.H
*
* PROGRAMMER : Gibby
*    VERSION : 00.00
*    CREATED : 20/08/98
*   MODIFIED : 20/08/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for COBRA.C
*
*****************************************************************************/

#ifndef _COBRA_H
#define _COBRA_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC)

#define InitialiseCobra ((VOIDFUNCSINT16*) Baddie3Ptr[0])
#define CobraControl ((VOIDFUNCSINT16*) Baddie3Ptr[1])

#else

void InitialiseCobra(sint16 item_number);
void CobraControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
