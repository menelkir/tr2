/****************************************************************************
*
* CINEMA.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 02/06/98
*   MODIFIED : 02/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for CINEMA.C
*
*****************************************************************************/

#ifndef _CINEMA_H
#define _CINEMA_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#include "../spec_psx/3d_gen.h"
#else
#include "..\specific\stypes.h"
#endif

/*---------------------------------------------------------------------------
 *	Type Definitions
\*--------------------------------------------------------------------------*/

typedef sint32 (CINEFUNC1)(int);
typedef sint32 (CINEFUNC2)(void);
typedef void (CINEFUNC3)(int);

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC)

#define StartCinematic			((CINEFUNC1 *)VehiclePtr[0])
#define DrawPhaseCinematic		((CINEFUNC2 *)VehiclePtr[1])
#define InitialiseGenPlayer		((VOIDFUNCSINT16 *)VehiclePtr[2])
#define ControlCinematicPlayer	((VOIDFUNCSINT16 *)VehiclePtr[3])
#define InGameCinematicCamera		((VOIDFUNCVOID *)VehiclePtr[4])
#define SetCutscene				((CINEFUNC3 *)VehiclePtr[5])
#define SetCutsceneAngle			((CINEFUNC3 *)VehiclePtr[6])

#else

extern sint32 StartCinematic(int level_number);
extern sint32 DrawPhaseCinematic();
extern void InitialiseGenPlayer(sint16 item_num);
extern void ControlCinematicPlayer(sint16 item_num);
extern void InGameCinematicCamera();

#endif

/* -------- GAMEFLOW.C */

extern int cutscene_track;

/* -------- ROOMLOAD.C */

extern sint16 *cine;
extern int cutscene_play;
extern sint16 cine_frame;
extern sint16 cine_loaded;
extern sint16 num_cine_frames;
extern PHD_3DPOS cinematic_pos;

#ifdef __cplusplus
}
#endif

#endif

