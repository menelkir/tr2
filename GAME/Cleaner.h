/****************************************************************************
*
* CLEANER.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 16/09/98
*   MODIFIED : 16/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for CLEANER.C
*
*****************************************************************************/

#ifndef _CLEANER_H
#define _CLEANER_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC)

#define InitialiseCleaner ((VOIDFUNCSINT16*) Baddie3Ptr[0])
#define CleanerControl ((VOIDFUNCSINT16*) Baddie3Ptr[1])

#else

void InitialiseCleaner(sint16 item_number);
void CleanerControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
