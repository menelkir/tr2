/****************************************************************************
*
* BOOMUTE.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 27/09/98
*   MODIFIED : 27/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for BOOMUTE.C
*
*****************************************************************************/

#ifndef _BOOMUTE_H
#define _BOOMUTE_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_BOOMUTE
#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_BOOMUTE)

#define BoomuteControl	((VOIDFUNCSINT16 *)Baddie4Ptr)

#else

extern void BoomuteControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
