/****************************************************************************
*
* ORCA.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 14/09/98
*   MODIFIED : 14/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for ORCA.C
*
*****************************************************************************/

#ifndef _ORCA_H
#define _ORCA_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_ORCA
#if defined(PSX_VERSION) && defined(RELOC) && !defined (NO_RELOC_ORCA)

#define OrcaControl ((VOIDFUNCSINT16 *)Baddie8Ptr)
#else
extern void OrcaControl(sint16 item_number);
#endif

#ifdef __cplusplus
}
#endif

#endif
