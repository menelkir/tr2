/****************************************************************************
*
* FLYMAKER.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 22/09/98
*   MODIFIED : 22/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for FLYMAKER.C
*
*****************************************************************************/

#ifndef _FLYMAKER_H
#define _FLYMAKER_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_FLYMAKER
#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_FLYMAKER)


#define FlyEmitterControl ((VOIDFUNCSINT16*)Baddie5Ptr)

#else

//void InitialiseFlyEmitter(sint16 item_number);
void FlyEmitterControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
