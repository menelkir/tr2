/****************************************************************************
*
* SPHERE.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 02/06/98
*   MODIFIED : 02/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for SPHERE.C
*
*****************************************************************************/

#ifndef _SPHERE_H
#define _SPHERE_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#include "../spec_psx/3d_gen.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Type Definitions
\*--------------------------------------------------------------------------*/

typedef struct {
	sint32 x;
	sint32 y;
	sint32 z;
	sint32 r;
}SPHERE;

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

extern int TestCollision(ITEM_INFO *item, ITEM_INFO *laraitem);
extern int GetSpheres(ITEM_INFO *item, SPHERE *ptr, int WorldSpace);
extern void GetJointAbsPosition(ITEM_INFO *item, PHD_VECTOR *vec, int joint);


#ifdef __cplusplus
}
#endif


#endif
