/*********************************************************************************************/
/*                                                                                           */
/* BooMutant Trap Control - TS  - 27/8/98                                                     */
/*                                                                                           */
/*********************************************************************************************/

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "../specific/stypes.h"
#endif

#include "objects.h"
#include "items.h"
#include "box.h"
#include "lara.h"
#include "laraanim.h"
#include "control.h"
#include "people.h"
#include "traps.h"
#include "sphere.h"
#include "effect2.h"

static void TriggerSealmuteGas(long x, long y, long z, long xv, long yv, long zv, long fxnum);
static sint16 TriggerSealmuteGasThrower(ITEM_INFO *item, BITE_INFO *bite, sint16 speed);

/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/

#define BOOMUTE_TURN (1*ONE_DEGREE)

/*---------------------------------------------------------------------------
 *	Externals
\*--------------------------------------------------------------------------*/
//#define DEBUG_SEAL

#ifdef DEBUG_SEAL
extern char exit_message[];
#endif

#ifdef DEBUG_SEAL
static char *SealStrings[] = {"STOP", "WALK", "BURP", "DEATH"};
#endif

//Gibby - the place where he shoots from is his head, which is mesh_num 10
//He actually 'fires' on frame 46 of the BURP anim
/*---------------------------------------------------------------------------
 *	Locals
\*--------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 *	Local Functions
\*--------------------------------------------------------------------------*/

static BITE_INFO seal_gas = {0,48,140, 10};
//static BITE_INFO seal_bite_right = {19,-13,3, 14};

/*---------------------------------------------------------------------------
 *	Public Functions
\*--------------------------------------------------------------------------*/

void BoomuteControl(sint16 item_number)
{
	ITEM_INFO *item;
//	sint16 angle;
	item = &items[item_number];

	#ifdef DEBUG_SEAL
	sprintf(exit_message, "Status: %d", item->status);
	PrintDbug(2, 2, exit_message);
	#endif

	if (item->status == DEACTIVATED)
		return;
	else if (item->status == ACTIVE)
	{
		/*
		angle = phd_atan(item->pos.z_pos - lara_item->pos.z_pos, item->pos.x_pos - lara_item->pos.x_pos);

		if (abs(angle) <  BOOMUTE_TURN)
			item->pos.y_rot += angle;
		else if (angle < 0)
			item->pos.y_rot -= BOOMUTE_TURN;
		else
			item->pos.y_rot += BOOMUTE_TURN;
		*/
		//Can't really rotate it cos origin's in a strange place
		if (	item->frame_number >= anims[item->anim_number].frame_base + 1 &&
				item->frame_number <= anims[item->anim_number].frame_end - 8)
		{
			unsigned long	length;

			length = item->frame_number - anims[item->anim_number].frame_base + 1;
			if (length > 24)
			{
				length = anims[item->anim_number].frame_end - item->frame_number - 8;
				if (length <= 0)
					length = 1;
				else if (length > 24)
					length = (GetRandomControl()&15)+8;
			}

			TriggerSealmuteGasThrower(item, &seal_gas, length);
		}
		AnimateItem(item);
	}

}

static void TriggerSealmuteGas(long x, long y, long z, long xv, long yv, long zv, long fxnum)
{
	long		size;
	SPARKS	*sptr;

	sptr = &spark[GetFreeSpark()];

	sptr->On = 1;
	sptr->sR = 128+(GetRandomControl()&63);
	sptr->sG = 128+(GetRandomControl()&63);
	sptr->sB = 32;

	sptr->dR = 32+(GetRandomControl()&15);
	sptr->dG = 32+(GetRandomControl()&15);
	sptr->dB = 0;

	if (xv||yv||zv)
	{
		sptr->ColFadeSpeed = 6;
		sptr->FadeToBlack = 2;
		sptr->sLife = sptr->Life = (GetRandomControl()&1)+16;
	}
	else
	{
		sptr->ColFadeSpeed = 8;
		sptr->FadeToBlack = 16;
		sptr->sLife = sptr->Life = (GetRandomControl()&3)+28;
	}

	sptr->TransType = COLADD;

	sptr->extras = 0;
	sptr->Dynamic = -1;

	sptr->x = x + ((GetRandomControl()&31)-16);
	sptr->y = y;
	sptr->z = z + ((GetRandomControl()&31)-16);

	sptr->Xvel = ((GetRandomControl()&15)-16)+xv;
	sptr->Yvel = yv;
	sptr->Zvel = ((GetRandomControl()&15)-16)+zv;
	sptr->Friction = 0;

	if (GetRandomControl()&1)
	{
		if (fxnum>=0)
			sptr->Flags = SP_SCALE|SP_DEF|SP_ROTATE|SP_EXPDEF|SP_FX;
		else
			sptr->Flags = SP_SCALE|SP_DEF|SP_ROTATE|SP_EXPDEF;
		sptr->RotAng = GetRandomControl()&4095;
		if (GetRandomControl()&1)
			sptr->RotAdd = -(GetRandomControl()&15)-16;
		else
			sptr->RotAdd = (GetRandomControl()&15)+16;
	}
	else
	{
		if (fxnum>=0)
	 		sptr->Flags = SP_SCALE|SP_DEF|SP_EXPDEF|SP_FX;
		else
			sptr->Flags = SP_SCALE|SP_DEF|SP_EXPDEF;
	}

	sptr->FxObj = fxnum;
	sptr->Gravity = sptr->MaxYvel = 0;
	sptr->Def = objects[EXPLOSION1].mesh_index;
	size = (GetRandomControl()&31)+48;
	if (xv||yv||zv)
	{
		sptr->sWidth = sptr->Width = size>>5;
 		sptr->sHeight = sptr->Height = size>>5;
		if (fxnum == -2)
			sptr->Scalar = 2;
		else
			sptr->Scalar = 3;
	}
	else
	{
		sptr->sWidth = sptr->Width = size>>4;
 		sptr->sHeight = sptr->Height = size>>4;
		sptr->Scalar = 4;
	}
	sptr->dWidth = size>>1;
	sptr->dHeight = size>>1;
}



static sint16 TriggerSealmuteGasThrower(ITEM_INFO *item, BITE_INFO *bite, sint16 speed)
{
	PHD_VECTOR	pos1,pos2;
	FX_INFO		*fx;
	sint16 		fx_number;
	PHD_ANGLE		angles[2];
	long			xv,yv,zv,vel,spd,lp;


	fx_number = CreateEffect(item->room_number);


	if (fx_number != NO_ITEM)
	{
		fx = &effects[fx_number];

		pos1.x = bite->x;
		pos1.y = bite->y;
		pos1.z = bite->z;
		GetJointAbsPosition( item, &pos1, bite->mesh_num );
		pos2.x = bite->x;
		pos2.y = bite->y<<1;
		pos2.z = bite->z<<3;
		GetJointAbsPosition( item, &pos2, bite->mesh_num );

		phd_GetVectorAngles( pos2.x-pos1.x,pos2.y-pos1.y,pos2.z-pos1.z, angles );

		fx->pos.x_pos = pos1.x;
		fx->pos.y_pos = pos1.y;
		fx->pos.z_pos = pos1.z;
		fx->room_number = item->room_number;
		fx->pos.x_rot = angles[1];
		fx->pos.z_rot = 0;
		fx->pos.y_rot = angles[0];
		fx->speed = speed<<2;
		fx->object_number = DRAGON_FIRE;
		fx->counter = 20;
		fx->flag1 = 1;	// Set to not fire.

		TriggerSealmuteGas(0,0,0,0,0,0,fx_number);

		for (lp=0;lp<2;lp++)
		{
			spd = (GetRandomControl()%(speed<<2))+32;
			vel = (spd * phd_cos(fx->pos.x_rot)) >> W2V_SHIFT;
			zv = (vel * phd_cos(fx->pos.y_rot)) >> W2V_SHIFT;
			xv = (vel * phd_sin(fx->pos.y_rot)) >> W2V_SHIFT;
			yv = -((spd * phd_sin(fx->pos.x_rot)) >> W2V_SHIFT);
			TriggerSealmuteGas(fx->pos.x_pos,fx->pos.y_pos,fx->pos.z_pos,xv<<5,yv<<5,zv<<5,-1);
		}

		vel = ((speed<<1) * phd_cos(fx->pos.x_rot)) >> W2V_SHIFT;
		zv = (vel * phd_cos(fx->pos.y_rot)) >> W2V_SHIFT;
		xv = (vel * phd_sin(fx->pos.y_rot)) >> W2V_SHIFT;
		yv = -(((speed<<1) * phd_sin(fx->pos.x_rot)) >> W2V_SHIFT);
		TriggerSealmuteGas(fx->pos.x_pos,fx->pos.y_pos,fx->pos.z_pos,xv<<5,yv<<5,zv<<5,-2);
	}

	return (fx_number);
}

