
/*********************************************************************************************/
/*                                                                                           */
/* Lond-Sec Control - TS - 23-7-98                                                            */
/*                                                                                           */
/*********************************************************************************************/

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "../specific/stypes.h"
#endif

#include "objects.h"
#include "lara.h"
#include "control.h"
#include "sound.h"
#include "sphere.h"
#include "people.h"
#include "effect2.h"
#include "box.h"

/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/

//#define DEBUG_LONDSEC

//#define LONDSEC_SHOT_DAMAGE 1
#define LONDSEC_SHOT_DAMAGE 32

enum londsec_anims {LONDSEC_EMPTY, LONDSEC_WAIT, LONDSEC_WALK, LONDSEC_RUN, LONDSEC_AIM1, LONDSEC_SHOOT1, LONDSEC_AIM2,
	LONDSEC_SHOOT2, LONDSEC_SHOOT3A, LONDSEC_SHOOT3B, LONDSEC_SHOOT4A, LONDSEC_AIM3, LONDSEC_AIM4, LONDSEC_DEATH, LONDSEC_SHOOT4B,
	LONDSEC_DUCK, LONDSEC_DUCKED, LONDSEC_DUCKAIM, LONDSEC_DUCKSHOT, LONDSEC_DUCKWALK, LONDSEC_STAND};
// TS - Upped the turn speeds

#define LONDSEC_WALK_TURN (ONE_DEGREE*5)
#define LONDSEC_RUN_TURN (ONE_DEGREE*10)

#define LONDSEC_WALK_RANGE SQUARE(WALL_L*2)
#define LONDSEC_FEELER_DISTANCE WALL_L

#define LONDSEC_DIE_ANIM 14
#define LONDSEC_AIM1_ANIM 12
#define LONDSEC_AIM2_ANIM 13
#define LONDSEC_WT1_SHT1_ANIM 1
#define LONDSEC_WT1_SHT2_ANIM 4
#define LONDSEC_WLK_SHT4A_ANIM 18
#define LONDSEC_WLK_SHT4B_ANIM 19
#define LONDSEC_WALK_WAIT_ANIM 17
#define LONDSEC_RUN_WAIT1_ANIM 27
#define LONDSEC_RUN_WAIT2_ANIM 28



#define LONDSEC_DEATH_SHOT_ANGLE 0x2000
#define LONDSEC_AWARE_DISTANCE SQUARE(WALL_L)

/*---------------------------------------------------------------------------
 *	Externals
\*--------------------------------------------------------------------------*/

#ifdef DEBUG_LONDSEC
extern char exit_message[];
#endif

/*---------------------------------------------------------------------------
 *	Locals
\*--------------------------------------------------------------------------*/

static BITE_INFO londsec_gun = {0,160,40, 13};

#ifdef DEBUG_LONDSEC
static char *LondSecStrings[] = {
"EMPTY", "WAIT", "WALK", "RUN", "AIM1", "SHOOT1", "AIM2",
"SHOOT2", "SHOOT3A", "SHOOT3B", "SHOOT4A", "AIM3", "AIM4", "DEATH", "SHOOT4B",
"DUCK", "DUCKED", "DUCKAIM", "DUCKSHOT", "DUCKWALK", "STAND"
};
#endif

/*---------------------------------------------------------------------------
 *	Public Functions
\*--------------------------------------------------------------------------*/

// TS - LOND_SEC does guarding and allows for sneaking behaviour

void LondSecControl(sint16 item_number)
{
	ITEM_INFO *item, *real_enemy;
	CREATURE_INFO *londsec;
	sint16 angle, torso_y, torso_x, head, tilt, room_number, near_cover, meta_mood;
	sint32 lara_dx, lara_dz, x, z, y, height;
	int random;
	AI_INFO info, lara_info;
	FLOOR_INFO *floor;

	if (!CreatureActive(item_number))
		return;

	item = &items[item_number];
	londsec = (CREATURE_INFO *)item->data;
	torso_y = torso_x = head = angle = tilt = 0;

	if (item->fired_weapon)	// Dynamic light for firing weapon.
	{
		PHD_VECTOR pos;

		phd_PushMatrix();
		pos.x = londsec_gun.x;
		pos.y = londsec_gun.y;
		pos.z = londsec_gun.z;
		GetJointAbsPosition(item, &pos, londsec_gun.mesh_num);
		TriggerDynamic(pos.x, pos.y, pos.z, (item->fired_weapon<<1)+4, 24, 16, 4);
		phd_PopMatrix();
	}

	#ifdef DEBUG_LONDSEC
	sprintf(exit_message, "%s", LondSecStrings[item->current_anim_state]);
	PrintDbug(2, 4, exit_message);
//	sprintf(exit_message, "%s", LondSecStrings[item->goal_anim_state]);
//	PrintDbug(2, 5, exit_message);
//	sprintf(exit_message, "%s", LondSecStrings[item->required_anim_state]);
//	PrintDbug(2, 6, exit_message);
	sprintf(exit_message, "AI Bits:%d", item->ai_bits);
	PrintDbug(2,6, exit_message);

	sprintf(exit_message, "Alert:%d, Goal:%d, Hurt:%d", londsec->alerted, londsec->reached_goal, londsec->hurt_by_lara);
	PrintDbug(2,7, exit_message);
//	sprintf(exit_message, "Head Left:%d, Head Right: %d", londsec->head_left, londsec->head_right);
//	PrintDbug(2,7, exit_message);

	#endif


	if (item->hit_points <= 0)
	{
		item->hit_points = 0;
		if (item->current_anim_state != LONDSEC_DEATH)
		{
			item->anim_number = objects[item->object_number].anim_index + LONDSEC_DIE_ANIM;
			item->frame_number = anims[item->anim_number].frame_base;
			item->current_anim_state = LONDSEC_DEATH;
		}
		else if ((GetRandomControl() & 1) && (item->frame_number == anims[item->anim_number].frame_base + 3 || item->frame_number == anims[item->anim_number].frame_base + 28)) //This is the frame where the gun is fired
		{
			CreatureAIInfo(item, &info);
			if (Targetable(item, &info))
			{
				if (info.angle > -LONDSEC_DEATH_SHOT_ANGLE && info.angle < LONDSEC_DEATH_SHOT_ANGLE)
				{
					torso_y = info.angle;
					head = info.angle;
					ShotLara(item, &info, &londsec_gun, torso_y, LONDSEC_SHOT_DAMAGE*2);
					SoundEffect(305, &item->pos, 3<<13); //Pitch & Volume wibble?
				}
			}

		}
	}
	else
	{
		if (item->ai_bits)
			GetAITarget(londsec);
		else
			londsec->enemy = lara_item;
		/*
		if (!londsec->enemy)
		{
			for (i=0, target_item=&items[0]; i<level_items; i++, target_item++)
			{
				if (target_item->active && target_item->object_number == ANIMATING6)
				{
					londsec->enemy = target_item;
	//				PrintDbug(2, 3, "Targetting the flag");
				}
			}
		}
		*/

		CreatureAIInfo(item, &info);

		if (londsec->enemy == lara_item)
		{
			lara_info.angle = info.angle;
			lara_info.distance = info.distance;
		}
		else
		{
			lara_dz = lara_item->pos.z_pos - item->pos.z_pos;
			lara_dx = lara_item->pos.x_pos - item->pos.x_pos;
			lara_info.angle = phd_atan(lara_dz, lara_dx) - item->pos.y_rot; //only need to fill out the bits of lara_info that will be needed by TargetVisible
			lara_info.distance = lara_dz * lara_dz + lara_dx * lara_dx;
		}

		if (londsec->enemy != lara_item) //Should eventually change this to be any person
			meta_mood = VIOLENT;	//Shooters run up to their target if it's not a person
		else
			meta_mood = TIMID;		//Else they stay their distance

		GetCreatureMood(item, &info, meta_mood);

		CreatureMood(item, &info, meta_mood); // TS - Should be timid?

		angle = CreatureTurn(item, londsec->maximum_turn);

		room_number = item->room_number;
		x = item->pos.x_pos + (LONDSEC_FEELER_DISTANCE * phd_sin(item->pos.y_rot + lara_info.angle) >> W2V_SHIFT);
		y = item->pos.y_pos;
		z = item->pos.z_pos + (LONDSEC_FEELER_DISTANCE * phd_cos(item->pos.y_rot + lara_info.angle) >> W2V_SHIFT);
		floor = GetFloor(x, y, z, &room_number);
		height = GetHeight(floor, x, y, z);
//		TriggerFlareSparks(x,y - STEP_L,z, -1,-1,0,1,1);
		near_cover = (item->pos.y_pos > (height + (STEP_L * 3 / 2)) && item->pos.y_pos < (height + (STEP_L * 9 / 2)) && lara_info.distance > LONDSEC_AWARE_DISTANCE);
//		near_cover = ( (height - (STEP_L * 3 / 2)));

	#ifdef DEBUG_LONDSEC
//	sprintf(exit_message, "head: %d, Target Visible?: %d", londsec->joint_rotation[1], TargetVisible(item, &info));
//	PrintDbug(2, 5, exit_message);
	#endif

	real_enemy = londsec->enemy; //TargetVisible uses enemy, so need to fill this in as lara if we're doing other things
	londsec->enemy = lara_item;
	if ( item->hit_status || ((lara_info.distance < LONDSEC_AWARE_DISTANCE || TargetVisible(item, &lara_info)) && (abs(lara_item->pos.y_pos - item->pos.y_pos) < (WALL_L*2)))) //Maybe move this into LONDSEC_WAIT case?
		{
			if (!londsec->alerted)
				SoundEffect(299, &item->pos, 0);
			AlertAllGuards(item_number);
		}
	londsec->enemy = real_enemy;


#ifdef DEBUG_LONDSEC
		if (londsec->mood == BORED_MOOD)
			PrintDbug(2, 2, "Bored");
		else if (londsec->mood == ESCAPE_MOOD)
			PrintDbug(2, 2, "Escape");
		else if (londsec->mood == ATTACK_MOOD)
			PrintDbug(2, 2, "Attack");
		else if (londsec->mood == STALK_MOOD)
			PrintDbug(2, 2, "Stalk");
#endif

		switch (item->current_anim_state)
		{
		case LONDSEC_WAIT:
		//	if (info.ahead)
		//		head = info.angle;
			head = lara_info.angle;
		//	londsec->flags = 0;

			londsec->maximum_turn = 0;

			if (item->anim_number == objects[item->object_number].anim_index + LONDSEC_WALK_WAIT_ANIM ||
				item->anim_number == objects[item->object_number].anim_index + LONDSEC_RUN_WAIT1_ANIM ||
				item->anim_number == objects[item->object_number].anim_index + LONDSEC_RUN_WAIT2_ANIM)
			{
				if (abs(info.angle) <  LONDSEC_RUN_TURN)
				item->pos.y_rot += info.angle;
				else if (info.angle < 0)
				item->pos.y_rot -= LONDSEC_RUN_TURN;
				else
				item->pos.y_rot += LONDSEC_RUN_TURN;
			}

			if (item->ai_bits & GUARD)
			{
				head = AIGuard(londsec);
				item->goal_anim_state = LONDSEC_WAIT;
				break;
			}

			else if (item->ai_bits & PATROL1)
			{
				item->goal_anim_state = LONDSEC_WALK;
				head= 0;
			}

			else if (near_cover && (lara.target == item || item->hit_status))
				item->goal_anim_state = LONDSEC_DUCK;
			else if (item->required_anim_state == LONDSEC_DUCK)
				item->goal_anim_state = LONDSEC_DUCK;
			else if (londsec->mood == ESCAPE_MOOD)
				item->goal_anim_state = LONDSEC_RUN;
			else if (Targetable(item, &info))
			{
				if (info.distance > LONDSEC_WALK_RANGE)
					item->goal_anim_state = LONDSEC_WALK;
				else
				{
					// 3 different shooting stances to choose from!
					random = GetRandomControl();
					if (random < 0x2000)
						item->goal_anim_state = LONDSEC_SHOOT1;
					else if (random < 0x4000)
						item->goal_anim_state = LONDSEC_SHOOT2;
					else
						item->goal_anim_state = LONDSEC_AIM3;
				}
			}
			else if (londsec->mood == BORED_MOOD || ((item->ai_bits & FOLLOW ) && (londsec->reached_goal || lara_info.distance > SQUARE(WALL_L*2))))
			{
				if (info.ahead)
					item->goal_anim_state = LONDSEC_WAIT;
				else
					item->goal_anim_state = LONDSEC_WALK;
			}
			else
				item->goal_anim_state = LONDSEC_RUN;
			break;

		case LONDSEC_WALK:
	//		if (info.ahead)
	//			head = info.angle;
			head=lara_info.angle;
	//		londsec->flags = 0;

			londsec->maximum_turn = LONDSEC_WALK_TURN;
			if (item->ai_bits & PATROL1)
			{
				item->goal_anim_state = LONDSEC_WALK;
				head = 0;
			}
			else if (near_cover && (lara.target == item || item->hit_status))
			{
				item->required_anim_state = LONDSEC_DUCK;
				item->goal_anim_state = LONDSEC_WAIT;
			}
			else if (londsec->mood == ESCAPE_MOOD)
				item->goal_anim_state = LONDSEC_RUN;
			else if (Targetable(item, &info))
			{
				if (info.distance > LONDSEC_WALK_RANGE && info.zone_number == info.enemy_zone)
					item->goal_anim_state = LONDSEC_AIM4;
				else
					item->goal_anim_state = LONDSEC_WAIT;
			}
			else if (londsec->mood == BORED_MOOD)
			{
				if (info.ahead)
					item->goal_anim_state = LONDSEC_WALK;
				else
					item->goal_anim_state = LONDSEC_WAIT;
			}
			else
				item->goal_anim_state = LONDSEC_RUN;
			break;

		case LONDSEC_RUN:
			if (info.ahead)
				head = info.angle;

			londsec->maximum_turn = LONDSEC_RUN_TURN;
			tilt = angle/2;
			if (item->ai_bits & GUARD)
				item->goal_anim_state = LONDSEC_WAIT;
			else if (near_cover && (lara.target == item || item->hit_status))
			{
				item->required_anim_state = LONDSEC_DUCK;
				item->goal_anim_state = LONDSEC_WAIT;
			}
			else if (londsec->mood == ESCAPE_MOOD)
				break;
			else if (Targetable(item, &info) || ((item->ai_bits & FOLLOW ) && (londsec->reached_goal || lara_info.distance > SQUARE(WALL_L*2))))
				item->goal_anim_state = LONDSEC_WAIT;
			else if (londsec->mood == BORED_MOOD)
				item->goal_anim_state = LONDSEC_WALK;
			break;

		case LONDSEC_AIM1:
			if (info.ahead)
			{
				torso_y = info.angle;
				torso_x = info.x_angle;
			}

			if ((item->anim_number == objects[SECURITY_GUARD].anim_index + LONDSEC_AIM1_ANIM) || (item->anim_number == objects[SECURITY_GUARD].anim_index + LONDSEC_WT1_SHT1_ANIM && item->frame_number == anims[item->anim_number].frame_base + 10)) // Cheers, Phil :)
			{
				if (!ShotLara(item, &info, &londsec_gun, torso_y, LONDSEC_SHOT_DAMAGE))
					item->required_anim_state = LONDSEC_WAIT;
			}
			else if (item->hit_status && !(GetRandomControl() & 0x3) && near_cover)
			{
				item->required_anim_state = LONDSEC_DUCK;
				item->goal_anim_state = LONDSEC_WAIT;
			}

			break;

		case LONDSEC_SHOOT1:
			if (info.ahead)
			{
				torso_y = info.angle;
				torso_x = info.x_angle;
			}
			if (item->required_anim_state == LONDSEC_WAIT)
				item->goal_anim_state = LONDSEC_WAIT;
			break;

		case LONDSEC_SHOOT2:
			if (info.ahead)
			{
				torso_y = info.angle;
				torso_x = info.x_angle;
			}
			if (item->frame_number == anims[item->anim_number].frame_base)
			{
				if (!ShotLara(item, &info, &londsec_gun, torso_y, LONDSEC_SHOT_DAMAGE))
					item->goal_anim_state = LONDSEC_WAIT;
			}
			else if (item->hit_status && !(GetRandomControl() & 0x3) && near_cover)
			{
				item->required_anim_state = LONDSEC_DUCK;
				item->goal_anim_state = LONDSEC_WAIT;
			}

			break;

		case LONDSEC_SHOOT3A:
		case LONDSEC_SHOOT3B:
			if (info.ahead)
			{
				torso_y = info.angle;
				torso_x = info.x_angle;
			}
			if (item->frame_number == anims[item->anim_number].frame_base || (item->frame_number == anims[item->anim_number].frame_base + 11))
			{
				if (!ShotLara(item, &info, &londsec_gun, torso_y, LONDSEC_SHOT_DAMAGE))
					item->goal_anim_state = LONDSEC_WAIT;
			}
			else if (item->hit_status && !(GetRandomControl() & 0x3) && near_cover)
			{
				item->required_anim_state = LONDSEC_DUCK;
				item->goal_anim_state = LONDSEC_WAIT;
			}

			break;

		case LONDSEC_AIM4:
			if (info.ahead)
			{
				torso_y = info.angle;
				torso_x = info.x_angle;
			}
			if ((item->anim_number == objects[SECURITY_GUARD].anim_index + LONDSEC_WLK_SHT4A_ANIM && item->frame_number == anims[item->anim_number].frame_base + 17) || (item->anim_number == objects[SECURITY_GUARD].anim_index + LONDSEC_WLK_SHT4B_ANIM && item->frame_number == anims[item->anim_number].frame_base + 6)) // Cheers, Phil :)
			{
				if (!ShotLara(item, &info, &londsec_gun, torso_y, LONDSEC_SHOT_DAMAGE))
					item->required_anim_state = LONDSEC_WALK;
			}
			else if (item->hit_status && !(GetRandomControl() & 0x3) && near_cover)
			{
				item->required_anim_state = LONDSEC_DUCK;
				item->goal_anim_state = LONDSEC_WAIT;
			}

			// Go back to walk so guard can stop when he gets close enough
			if (info.distance < LONDSEC_WALK_RANGE)
				item->required_anim_state = LONDSEC_WALK;
			break;

		case LONDSEC_SHOOT4A:
		case LONDSEC_SHOOT4B:
			if (info.ahead)
			{
				torso_y = info.angle;
				torso_x = info.x_angle;
			}
			if (item->required_anim_state == LONDSEC_WALK)
			{
				item->goal_anim_state = LONDSEC_WALK;
			}
			if (item->frame_number == anims[item->anim_number].frame_base + 16)
			{
				if (!ShotLara(item, &info, &londsec_gun, torso_y, LONDSEC_SHOT_DAMAGE))
					item->goal_anim_state = LONDSEC_WALK;
			}
			// Go back to walk so guard can stop when he gets close enough
			if (info.distance < LONDSEC_WALK_RANGE)
				item->goal_anim_state = LONDSEC_WALK;
			break;

		case LONDSEC_DUCKED:
			if (info.ahead)
				head = info.angle;

			londsec->maximum_turn = 0;

			if (Targetable(item, &info))
				item->goal_anim_state = LONDSEC_DUCKAIM;
			else if (item->hit_status || !near_cover || (info.ahead && !(GetRandomControl() & 0x1F)))
				item->goal_anim_state = LONDSEC_STAND;
			else 
				item->goal_anim_state = LONDSEC_DUCKWALK;
			break;

		case LONDSEC_DUCKAIM:
			londsec->maximum_turn = ONE_DEGREE;

			if (info.ahead)
				torso_y = info.angle;

			if (Targetable(item, &info))
				item->goal_anim_state = LONDSEC_DUCKSHOT;
			else
				item->goal_anim_state = LONDSEC_DUCKED;
			break;

		case LONDSEC_DUCKSHOT:
			if (info.ahead)
				torso_y = info.angle;

			if (item->frame_number == anims[item->anim_number].frame_base)
			{
				if (!ShotLara(item, &info, &londsec_gun, torso_y, LONDSEC_SHOT_DAMAGE) || !(GetRandomControl() & 0x7))
					item->goal_anim_state = LONDSEC_DUCKED;
			}

			break;

		case LONDSEC_DUCKWALK:
			if (info.ahead)
				head = info.angle;

			londsec->maximum_turn = LONDSEC_WALK_TURN;

			if (Targetable(item, &info) || item->hit_status || !near_cover || (info.ahead && !(GetRandomControl() & 0x1F)))
				item->goal_anim_state = LONDSEC_DUCKED;
			break;

		}
	}

	CreatureTilt(item, tilt);
	CreatureJoint(item, 0, torso_y);
	CreatureJoint(item, 1, torso_x);
	CreatureJoint(item, 2, head);

	// Actually do animation allowing for collisions
	CreatureAnimation(item_number, angle, 0);
}
