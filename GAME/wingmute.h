/****************************************************************************
*
* WINGMUTE.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 09/09/98
*   MODIFIED : 09/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for WINGMUTE.C
*
*****************************************************************************/

#ifndef _WINGMUTE_H
#define _WINGMUTE_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_WINGMUTE
#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_WINGMUTE)


#define InitialiseWingmute ((VOIDFUNCSINT16 *)Baddie4Ptr[0])
#define WingmuteControl ((VOIDFUNCSINT16 *)Baddie4Ptr[1])

#else

void InitialiseWingmute(sint16 item_number);
void WingmuteControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
