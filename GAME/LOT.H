/****************************************************************************
*
* LOT.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 03/06/98
*   MODIFIED : 03/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for LOT.C
*
*****************************************************************************/

#ifndef _LOT_H
#define _LOT_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "box.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

extern int slots_used;
extern CREATURE_INFO *baddie_slots;

extern void InitialiseLOTarray( void );
extern void DisableBaddieAI( sint16 item_num );
extern int EnableBaddieAI( sint16 item_number, int Always );
extern void InitialiseSlot( sint16 item_number, int slot );
extern int InitialiseLOT(LOT_INFO *LOT);
extern void ClearLOT(LOT_INFO *LOT);
extern int	EnableNonLotAI( sint16 item_number, int Always );
extern void	InitialiseNonLotAI( sint16 item_number, int slot);

#ifdef PSX_VERSION
extern int OnIdleLOTUpdate(int (*pfnIdleCheckRoutine)(void));
#endif

#ifdef __cplusplus
}
#endif

#endif
