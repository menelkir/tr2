/****************************************************************************
*
* TIGER.H
*
* PROGRAMMER : Gibby
*    VERSION : 00.00
*    CREATED : 20/08/98
*   MODIFIED : 20/08/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for TIGER.C
*
*****************************************************************************/

#ifndef _TIGER_H
#define _TIGER_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC)

#define TigerControl ((VOIDFUNCSINT16*) Baddie1Ptr[0])

#else

void TigerControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
