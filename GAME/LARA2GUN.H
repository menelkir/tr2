/****************************************************************************
*
* LARA2GUN.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 27/05/98
*   MODIFIED : 27/05/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for LARA2GUN.C
*
*****************************************************************************/

#ifndef _LARA2GUN_H
#define _LARA2GUN_H

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

extern void draw_pistols(int weapon_type);
extern void undraw_pistols(int weapon_type);
extern void ready_pistols(int weapon_type);
extern void draw_pistol_meshes(int weapon_type);
extern void undraw_pistol_mesh_left(int weapon_type);
extern void undraw_pistol_mesh_right(int weapon_type);
extern void LaraSwapMeshExtra();
extern void PistolHandler(int weapon_type);
extern void AnimatePistols(int weapon_type);

#endif
