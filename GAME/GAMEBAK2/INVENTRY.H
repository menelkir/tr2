/********************************* DEFINES ******************************************/
#ifndef INVENTRY_H
#define INVENTRY_H

#ifdef __cplusplus
extern "C" {
#endif

enum	inv_modes {	INV_GAME_MODE=0,
					INV_TITLE_MODE,
					INV_KEYS_MODE,
					INV_SAVE_MODE,
					INV_LOAD_MODE,
					INV_DEATH_MODE,
					INV_FMV_MODE
					};


//		SHAPES & SPRITES
#define	MAX_MAP_SHAPES	32*32
enum	shapes { SHAPE_SPRITE=1,
				 SHAPE_LINE,
				 SHAPE_BOX,
				 SHAPE_FBOX
				 };

// Inventory flat colours
enum inventory_colours 
{
	C_BLACK = 0,
	C_GREY,
	C_WHITE,
	C_RED,
	C_ORANGE,
	C_YELLOW,
	C_GREEN1,
	C_GREEN2,
	C_GREEN3,
	C_GREEN4,
	C_GREEN5,
	C_GREEN6,
	C_DARKGREEN,
	C_GREEN,
	C_CYAN,
	C_BLUE,
	C_MAGENTA,
	C_NUMBER_COLOURS
};

#define C_TRANS	(1<<15)


/****************************** Inventory Typedefs ***************************************/

/****************************** Inventory Externs ***************************************/
extern sint16	Option_Gamma_Level;
extern sint16	Option_SoundFX_Volume;	
extern sint16	Option_Music_Volume;
extern sint16	Option_Detail_Level;

extern sint16	Inventory_Displaying;
extern sint16	Inventory_Chosen;
extern sint32	Inventory_ExtraData[];
extern sint32	Inventory_Mode;
extern sint32	Inventory_DemoMode;


extern sint16	Compass_Status;		// 1 = selected

extern sint32	idelay;
extern sint32	idcount;
extern sint16	item_data;
extern sint32	m_sizer;

extern	sint16	old_iconfig;

#ifdef __cplusplus
extern "C" SG_COL inv_colours[];
#else
extern SG_COL	inv_colours[];
#endif



/* Inventory Prototypes ******************************************/
/* Inventry.c */
sint32	Display_Inventory(sint32 inventory_mode);

/* Invfunc.c */
void	DrawGameInfo(int timed);
void	InitColours(void);
int		Inv_AddItem(int itemNum);
int		Inv_RequestItem(int	itemNum);
void 	Inv_RemoveAllItems(void);
int		Inv_RemoveItem(int itemNum);
int		Inv_GetCount(void);
int		Inv_GetItemOption(int itemNum);

#ifdef __cplusplus
	}
#endif

#endif	// INVENTRY_H_
