#ifndef TEXT_H
#define TEXT_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef PC_VERSION
	#define	MAX_TEXT_STRINGS 64
	#define	MAX_STRING_SIZE  64
#else
	#define	MAX_TEXT_STRINGS 32
	#define	MAX_STRING_SIZE  50
#endif


#define TEXT_HEIGHT	11

#ifdef PSX_VERSION
#define SCR_LEFT	0
#define	SCR_RIGHT	(phd_scrwidth-16)
#define SCR_TOP		0
#define	SCR_BOTTOM	phd_scrheight
#endif
#ifdef PC_VERSION
#define SCR_LEFT	  0
#define	SCR_RIGHT  GetRenderWidth()
#define SCR_TOP	  0
#define	SCR_BOTTOM GetRenderHeight()
#endif

// For screen saver button
extern int save_toggle;

typedef	struct textstringinfo {
	uint32	flags;
	uint16	textflags;
	uint16	bgndflags;
	uint16	outlflags;
	sint16	xpos;
	sint16	ypos;
	sint16	zpos;
	sint16	letterSpacing;
	sint16	wordSpacing;			 
	sint16	flashRate;
	sint16	flashCount;
	sint16	bgndColour;
	SG_COL	*bgndGour;
	sint16	outlColour;
	SG_COL	*outlGour;
	sint16	bgndSizeX;
	sint16	bgndSizeY;
	sint16	bgndOffX;
	sint16	bgndOffY;
	sint16	bgndOffZ;
	sint32	scaleH;
	sint32	scaleV;
	char	*string;
} TEXTSTRING;

/*
enum T_flags {
	T_ACTIVE =   (1<<0),
	T_FLASH = 	 (1<<1),
	T_ROTATE_H = (1<<2),
	T_ROTATE_V = (1<<3),
	T_CENTRE_H = (1<<4),
	T_CENTRE_V = (1<<5),
	T_RIGHT =    (1<<7),
	T_BOTTOM =   (1<<8),
	T_BGND = 	 (1<<9),
	T_OUTLINE =  (1<<10)
};
*/

enum T_flags {
	T_TOPALIGN =	0,
	T_LEFTALIGN =	0,
	T_ACTIVE =		(1<<0),
	T_FLASH = 		(1<<1),
	T_ROTATE_H =	(1<<2),
	T_ROTATE_V =	(1<<3),
	T_CENTRE_H =	(1<<4),
	T_CENTRE_V =	(1<<5),
	T_RIGHTALIGN =  (1<<7),
	T_BOTTOMALIGN =	(1<<8),
	T_ADDBACKGROUND=(1<<9),
	T_ADDOUTLINE =	(1<<10)
};


/* Transparency levels 1-4 (light to heavy)*/
enum D_flags {	
	D_TRANS1 = 	 1,
	D_TRANS2 = 	 2,
	D_TRANS3 = 	 3,
	D_TRANS4 = 	 4,
	D_NEXT	=	 (1<<3)
};

/* TEXT.C */
void	T_InitPrint(void);
TEXTSTRING *T_Print(sint32 xpos, sint32 ypos, sint32 zpos, char *string);
void	T_MovePrint(TEXTSTRING *textString, sint32 xpos, sint32 ypos, sint32 zpos);
void	T_ChangeText(TEXTSTRING *textString, char *string);
void	T_SetFlags(TEXTSTRING *textString, uint16 flags);
void	T_SetBgndFlags(TEXTSTRING *textString, uint16 flags);
void	T_SetOutlFlags(TEXTSTRING *textString, uint16 flags);
void	T_SetScale(TEXTSTRING *textString, sint32 scaleH, sint32 scaleV);
void	T_LetterSpacing(TEXTSTRING *textString, sint16 pixels);
void	T_WordSpacing(TEXTSTRING *textString, sint16 pixels);
void	T_FlashText(TEXTSTRING *textString, sint16 ok, sint16	rate);
sint32	T_GetTextWidth(TEXTSTRING *textString);

void	T_AddBackground(TEXTSTRING *textString, sint16 xsize, sint16 ysize, sint16 xoff, sint16 yoff, sint16 zoff, sint16 colour, SG_COL *gourptr, uint16 flags);
void	T_RemoveBackground(TEXTSTRING *textString);
void	T_AddOutline(TEXTSTRING *textString, sint16 ok, sint16 colour, SG_COL *gourptr, uint16 flags);
void	T_RemoveOutline(TEXTSTRING *textString);

void	T_CentreH(TEXTSTRING *textString, sint16 ok);
void	T_CentreV(TEXTSTRING *textString, sint16 ok);
void	T_RightAlign(TEXTSTRING *textString, sint16 ok);
void	T_BottomAlign(TEXTSTRING *textString, sint16 ok);

int		T_RemovePrint(TEXTSTRING *tString);
void 	T_RemoveAllPrints(void);

void	T_DrawText(void);
void	T_DrawThisText(TEXTSTRING *textString);
sint16	T_GetStringLen(char *string);

void	PrintDbug(int x, int y, char *text);
void	RemoveDbugs(void);

uint32	GetTextScaleH(uint32 scaleH);
uint32	GetTextScaleV(uint32 scaleV);

#ifdef __cplusplus
}
#endif

#endif
