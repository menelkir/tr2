/*****************************************************************************
 *
 *			Save and Load Demo Code Data...
 *
 ****************************************************************************/
#include "game.h"

	extern int	DemoPlay;
	

	int		DemoPlay=0;
	uint32	*demoptr;
	int		democount;
//	int		demo_level=LV_FIRSTLEVEL;
	int		demo_loaded = 0;

#define RANDOM_SEED 0xd371f947

#ifdef PSX_VERSION
	extern int psx_demo_mode;
	TEXTSTRING	*demotxt=NULL;
#endif

void LaraCheatGetStuff(void);

/****************************************************************************
 *				Start PlayBack Demo....
 ***************************************************************************/
sint32 DoDemoSequence( int level_number )
{
	static int demo_level = 0;

	if (level_number<0 && !gameflow.num_demos)
	{
#ifdef GAMEDEBUG
		Log(LT_Info,"No Demos!!!");
#endif
		return(EXIT_TO_TITLE);
	}
	if (level_number < 0)
	{
		/* Search for a valid demo level */
		if (demo_level >= gameflow.num_demos)
			demo_level = 0;
		level_number = GF_valid_demos[demo_level];
		demo_level++;
	}
	else
		demo_level = level_number;

	return(GF_DoLevelSequence(level_number, IL_DEMO));
}



sint32 StartDemo(int level_number)
{
	/* Loads the level_number and plays the demo. If level_number is -1 then will play the
		next level in the playback list */
	TEXTSTRING	*txt;
	START_INFO start, *s;
	static int demo_level = 0;
	int return_val=EXIT_TO_TITLE;
	char buf[64];

#ifdef GAMEDEBUG
	Log(LT_Info,"StartDemo %d", level_number);
#endif
	if (level_number<0 && !gameflow.num_demos)
	{
#ifdef GAMEDEBUG
		Log(LT_Info,"No Demos!!!");
#endif
		return(EXIT_TO_TITLE);
	}

/*
#if defined(PSX_VERSION) && defined(PLAY_FMV)	
	if (!demo_level)
	{
		FMV_Play("\\FMV\\LOGO.FMV", 1, 879);
		FMV_Play("\\FMV\\INTRO.FMV", 1, 4370);
	}

#endif
*/
	if (level_number < 0)
	{
		/* Search for a valid demo level */
		if (demo_level >= gameflow.num_demos)
			demo_level = 0;
		level_number = GF_valid_demos[demo_level];
		demo_level++;
	}
	else
		demo_level = level_number;

#ifdef GAMEDEBUG
	Log(LT_Info,"Loading Demo Level %d", level_number);
#endif

	/* Need to avoid messing with player's save game */
	s = &savegame.start[level_number];
	S_MemCpy(&start, s, sizeof(START_INFO));
	s->available = 1;
	s->got_pistols = 1;
	s->pistol_ammo = 1000;
	s->gun_status = LG_ARMLESS;
	s->gun_type = LG_PISTOLS;

	SeedRandomDraw(RANDOM_SEED);
	SeedRandomControl(RANDOM_SEED);

	title_loaded = 0;
	if (!InitialiseLevel(level_number,IL_DEMO))
		return (EXITGAME);

#ifdef GAMEDEBUG
	Log(LT_Info,"LoadDemo Complete");
#endif
	level_complete = 0;
	if (demo_loaded)
	{
		LoadLaraDemoPos();
		LaraCheatGetStuff();
	
		SeedRandomDraw(RANDOM_SEED);
		SeedRandomControl(RANDOM_SEED);
	
#ifdef PC_VERSION
		txt = T_Print( 0, game_setup.dump_height/2-16, 0, GF_PCStrings[PCSTR_DEMOMODE]);
		T_FlashText(txt, 1, 20);
		T_CentreV(txt, 1);
		T_CentreH(txt, 1);
#else
		if ( PadConnected )
		{
			demotxt =  T_Print(0,-16,0, GF_PSXStrings[PSSTR_DEMOTEXT] );
			psx_demo_mode = 1;
		}
		else
		{
			demotxt =  T_Print(0,-16,0, GF_PSXStrings[PSSTR_NOJOY_TEXT] );
			psx_demo_mode = 2;
		}
		T_CentreH(demotxt, 1);
		T_BottomAlign(demotxt, 1);
		T_FlashText(demotxt, 1, 20);

		//if (gameflow.language==3)
		//	txt = T_Print(384-140,30,0, GF_PSXStrings[PSSTR_DEMOMODE]);
		//else
			txt = T_Print(16,26,0, GF_PSXStrings[PSSTR_DEMOMODE]);
//		T_FlashText(txt, 1, 20);
//		T_CentreH(txt, 1);
//		T_BottomAlign(txt, 1);
#endif

		Inventory_DemoMode = 1;
		/* 1 => demo mode; return of 1 means demo played to end, 0 it was interrupted */
#ifdef PSX_VERSION
		fades_enabled = 0;
	  	return_val = GameLoop(1);
		fades_enabled = 1;
#else
	  	return_val = GameLoop(1);
#endif
		Inventory_DemoMode = 0;

		T_RemovePrint(txt);
		txt = NULL;
#ifdef PSX_VERSION
		T_RemovePrint(demotxt);
		demotxt=NULL;
		psx_demo_mode = 0;
#endif
	}
	else
	{
		sprintf(buf,"Level '%s' has no demo data!", GF_levelfilenames[level_number]);
		S_ExitSystem(buf);
	}

	/* Copy back players start stuff */
	S_MemCpy(s, &start, sizeof(START_INFO));

	S_FadeToBlack();

	return (return_val);
}

/******************************************************************************
 *				Get Laras starting Position for this Demo
 *****************************************************************************/
void	LoadLaraDemoPos( void )
{
	sint16		room_number;
	FLOOR_INFO	*floor;
	ITEM_INFO	*item;

	item = lara_item;
	item->pos.x_pos = (sint32)*(demoptr+0);
	item->pos.y_pos = (sint32)*(demoptr+1);
	item->pos.z_pos = (sint32)*(demoptr+2);
	item->pos.x_rot = (sint16)*(demoptr+3);
	item->pos.y_rot = (sint16)*(demoptr+4);
	item->pos.z_rot = (sint16)*(demoptr+5);
	room_number = (sint16)*(demoptr+6);
	if ( item->room_number!=room_number )
		ItemNewRoom( lara.item_number, room_number );
	floor = GetFloor( item->pos.x_pos, item->pos.y_pos, item->pos.z_pos, &room_number);
	lara_item->floor = GetHeight(floor, item->pos.x_pos, item->pos.y_pos, item->pos.z_pos);

	// get Lara's weapon too (and add to inventory if not there by default)
	lara.last_gun_type = (sint16)*(demoptr+7);

	democount += 8;
}


/****************************************************************************
 *			Read Demo Input from Buffer
 ***************************************************************************/
void	GetDemoInput( void )
{
	if ( democount < DEMOCOUNT_MAX )		// Get Recorded Input from
		input = *(demoptr+democount);       //
	else
		input = -1;
	if ( input!=-1 )
		democount++;
}


#ifdef PC_VERSION
#ifdef GAMEDEBUG
/****************************************************************************
 ****************************************************************************
 *			Start Recording A Demo
 ****************************************************************************
 ***************************************************************************/
sint32	StartSaveDemo( int level_number )
{
	SeedRandomDraw(RANDOM_SEED);
	SeedRandomControl(RANDOM_SEED);
	if (!InitialiseLevel(level_number,IL_NORMAL))
		return (EXIT_TO_TITLE);

	democount = 0;
	S_InitSaveDemo();                        	// initialise buffers
//	SaveLaraDemoPos();

	record_demo = -1;
	SeedRandomDraw(RANDOM_SEED);
	SeedRandomControl(RANDOM_SEED);
	GameLoop(0);
	S_SaveDemoData(level_number);
	S_FadeToBlack();

	return (EXIT_TO_TITLE);
}

/******************************************************************************
 *			   Save Laras positon into Demo file
 *****************************************************************************/
void	SaveLaraDemoPos( void )
{
	*(demoptr+0) = (uint32)lara_item->pos.x_pos;
	*(demoptr+1) = (uint32)lara_item->pos.y_pos;
	*(demoptr+2) = (uint32)lara_item->pos.z_pos;
	*(demoptr+3) = (uint32)lara_item->pos.x_rot;
	*(demoptr+4) = (uint32)lara_item->pos.y_rot;
	*(demoptr+5) = (uint32)lara_item->pos.z_rot;
	*(demoptr+6) = (uint32)lara_item->room_number;
	*(demoptr+7) = (uint32)lara.last_gun_type;
	democount += 8;
}

/***************************************************************************
 *				Save demo data...
 **************************************************************************/
void	SaveDemoData( void )
{
	if ( democount<DEMOCOUNT_MAX )
	{
		*(demoptr+democount) = input;
		democount++;
	}
}
#endif
#endif
