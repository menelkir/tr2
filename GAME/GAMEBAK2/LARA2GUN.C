#include "game.h"

#define AIMGUNS_F	0						// 0-4     Pistol animation frames
#define DRAW1_F		5                       // 5-12
#define DRAW2_F		13                      // 13-23
#define RECOIL_F	24                      // 24-32

void set_arm_info(LARA_ARM *arm, int frame)
{
	// 23/9/97: due to superpacking of anim frames, Lara's arms need more info to retrieve correct frame
	int anim_base;

	anim_base = objects[PISTOLS].anim_index;

	if (frame < 5)
		arm->anim_number = anim_base;
	else if (frame < 13)
		arm->anim_number = anim_base+1;
	else if (frame < 24)
		arm->anim_number = anim_base+2;
	else
		arm->anim_number = anim_base+3;

	arm->frame_base = anims[arm->anim_number].frame_ptr;
	arm->frame_number = frame;
}

/*******************************************************************************
 *					Draw default pistols from holsters...
 ******************************************************************************/
void	draw_pistols( int weapon_type )
{
	sint16	ani;

	ani = lara.left_arm.frame_number;
	ani++;
	if ( ani<5 || ani>23 )
		ani = 5;
	else if ( ani==13 )
	{
		draw_pistol_meshes( weapon_type );				// Put Guns In Hands..
		SoundEffect( 6,&lara_item->pos,0 );        // Play DRAW Sample
	}
	else if ( ani==23 )
	{
		ready_pistols( weapon_type );           // Pistols now ready to fire..
		ani = 0;
	}

//	lara.right_arm.frame_number = lara.left_arm.frame_number = ani;
	set_arm_info(&lara.right_arm, ani);
	set_arm_info(&lara.left_arm, ani);
}

/******************************************************************************
 *					Put pistols back in holsters...
 *****************************************************************************/
void	undraw_pistols( int weapon_type )
{
	sint16	anil,anir;


// First We Do Left Arm
	anil = lara.left_arm.frame_number;
	if ( anil>=RECOIL_F ) 									// If Gun Is Recoiling we snap it
	{                                       				// to an Aim position
		anil = DRAW1_F-1;
	}
	else if ( anil>0 && anil<DRAW1_F ) 						// If Gun is Being Aimed at something..
	{
		lara.left_arm.x_rot -= lara.left_arm.x_rot/anil;    // turn arms towards forward position
		lara.left_arm.y_rot -= lara.left_arm.y_rot/anil;
		anil--;
	}
	else if ( anil==0 )                 					// Guns Are Unaimed
	{                                                       // so start Undraw animation
		lara.left_arm.x_rot = lara.left_arm.y_rot = lara.left_arm.z_rot = 0;		// Ensure no targets pointed at..
		anil = RECOIL_F-1;
	}
	else if ( anil>DRAW1_F && anil<RECOIL_F )
	{
		anil--;
		if ( anil==DRAW2_F-1 )
		{
			undraw_pistol_mesh_left( weapon_type );         // Put Gun back On Thigh
			SoundEffect( 7,&lara_item->pos,0 );        // Play HOLSTER Sample
		}
	}
//	lara.left_arm.frame_number = anil;
	set_arm_info(&lara.left_arm, anil);

// Now We Do Right Arm stuff
	anir = lara.right_arm.frame_number;
	if ( anir>=RECOIL_F ) 									// If Gun Is Recoiling we snap it
	{                                       				// to an Aim position
		anir = DRAW1_F-1;
	}
	else if ( anir>0 && anir<DRAW1_F ) 						// If Gun is Being Aimed at something..
	{
		lara.right_arm.x_rot -= lara.right_arm.x_rot/anir;  // turn arms towards forward position
		lara.right_arm.y_rot -= lara.right_arm.y_rot/anir;
		anir--;
	}
	else if ( anir==0 )                 					// Guns Are Unaimed
	{                                                       // so start Undraw animation
		lara.right_arm.x_rot = lara.right_arm.y_rot = lara.right_arm.z_rot = 0;		// Ensure no targets pointed at..
		anir = RECOIL_F-1;
	}
	else if ( anir>DRAW1_F && anir<RECOIL_F )
	{
		anir--;
		if ( anir==DRAW2_F-1 )
		{
			undraw_pistol_mesh_right( weapon_type );		// Put Gun back on thigh
			SoundEffect( 7,&lara_item->pos,0 );        // Play HOLSTER Sample
		}
	}
//	lara.right_arm.frame_number = anir;
	set_arm_info(&lara.right_arm, anir);

	if ( anil==DRAW1_F && anir==DRAW1_F )
	{
		lara.gun_status = LG_ARMLESS;
		lara.right_arm.frame_number = lara.left_arm.frame_number = 0;
		lara.target = NULL;
		lara.left_arm.lock = lara.right_arm.lock = 0;
	}

	if (!(input & IN_LOOK))
	{
		lara.torso_y_rot = lara.head_y_rot = (lara.left_arm.y_rot + lara.right_arm.y_rot)/4;	// Turn head to point at targets
		lara.torso_x_rot = lara.head_x_rot = (lara.left_arm.x_rot + lara.right_arm.x_rot)/4;    // If any exist
	}
}


/*****************************************************************************
 *					Ready the pistols for Use...
 ****************************************************************************/
void	ready_pistols( int weapon_type )
{
	lara.gun_status = LG_READY;				// pistols ready to fire.
	lara.left_arm.x_rot = lara.left_arm.y_rot = lara.left_arm.z_rot = 0;
	lara.right_arm.x_rot = lara.right_arm.y_rot = lara.right_arm.z_rot = 0;
	lara.left_arm.frame_number = lara.right_arm.frame_number = 0;
	lara.target = NULL;
	lara.left_arm.lock = lara.right_arm.lock = 0;
//	lara.torso_x_rot = lara.torso_y_rot = 0;
//	lara.head_x_rot = lara.head_y_rot = 0;
	lara.left_arm.frame_base = lara.right_arm.frame_base = objects[PISTOLS].frame_base;        // use animations in PISTOLS
}


/*******************************************************************************
 *				Switch On Pistol meshes please...
 ******************************************************************************/
void	draw_pistol_meshes( int weapon_type )
{
	int		obj;

	obj = WeaponObject(weapon_type);
	lara.mesh_ptrs[HAND_L]  = meshes[objects[obj].mesh_index + HAND_L];
	lara.mesh_ptrs[HAND_R]  = meshes[objects[obj].mesh_index + HAND_R];
	lara.mesh_ptrs[THIGH_L] = meshes[objects[LARA].mesh_index + THIGH_L];
	lara.mesh_ptrs[THIGH_R] = meshes[objects[LARA].mesh_index + THIGH_R];
}


/******************************************************************************
 *					Switch Off Left Pistol meshes please...
 *****************************************************************************/
void	undraw_pistol_mesh_left( int weapon_type )
{
	int		obj;

	obj = WeaponObject(weapon_type);
	lara.mesh_ptrs[THIGH_L] = meshes[objects[obj].mesh_index + THIGH_L];
	lara.mesh_ptrs[HAND_L]  = meshes[objects[LARA].mesh_index + HAND_L];
}

/*****************************************************************************
 *					Switch Off Right Pistol meshes please...
 ****************************************************************************/
void	undraw_pistol_mesh_right( int weapon_type )
{
	int		obj;

	obj = WeaponObject(weapon_type);
	lara.mesh_ptrs[THIGH_R] = meshes[objects[obj].mesh_index + THIGH_R];
	lara.mesh_ptrs[HAND_R]  = meshes[objects[LARA].mesh_index + HAND_R];
}

/*****************************************************************************
 *						Do ARM targeting stuff
 ****************************************************************************/
void	PistolHandler( int weapon_type )
{
	WEAPON_INFO	*winfo;

	long	x,y,z;


	winfo = &weapons[weapon_type];

	/* Get Target Information */
	if ( input&IN_ACTION )					// If ACTION pressed
		LaraTargetInfo( winfo );            // get info about current Target
	else
		lara.target = NULL;
	if ( lara.target==NULL )				// If No Targets are Locked
		LaraGetNewTarget( winfo );          // attempt to find new target

	/* Aim Individual Weapons */
	AimWeapon( winfo,&lara.left_arm );
	AimWeapon( winfo,&lara.right_arm );

	/* Make Laras Head and Torso face towards target */
	if ( lara.left_arm.lock && !lara.right_arm.lock )
	{
		lara.head_y_rot = lara.torso_y_rot = lara.left_arm.y_rot/2;
		lara.head_x_rot = lara.torso_x_rot = lara.left_arm.x_rot/2;
	}
	else if ( lara.right_arm.lock && !lara.left_arm.lock )
	{
		lara.head_y_rot = lara.torso_y_rot = lara.right_arm.y_rot/2;
		lara.head_x_rot = lara.torso_x_rot = lara.right_arm.x_rot/2;
	}
	else if ( lara.left_arm.lock && lara.right_arm.lock )
	{
		lara.head_y_rot = lara.torso_y_rot = (lara.left_arm.y_rot + lara.right_arm.y_rot)/4;
		lara.head_x_rot = lara.torso_x_rot = (lara.left_arm.x_rot + lara.right_arm.x_rot)/4;
	}

	AnimatePistols( weapon_type );

	// Make flash of light each time Lara fires her guns
	if (lara.left_arm.flash_gun || lara.right_arm.flash_gun)
	{
		AddDynamicLight((x=lara_item->pos.x_pos + (phd_sin(lara_item->pos.y_rot) >> (W2V_SHIFT-10))),
							 (y=lara_item->pos.y_pos - WALL_L/2),
							 (z=lara_item->pos.z_pos + (phd_cos(lara_item->pos.y_rot) >> (W2V_SHIFT-10))),
							 12, 11);
		trigger_gun_flash(x,y,z);
	}
}

/******************************************************************************
 *			Animate the Pistols please
 *****************************************************************************/
void	AnimatePistols( int weapon_type )
{
	sint16		anil, anir, sound_already=0;
	PHD_ANGLE	angles[2];
	WEAPON_INFO	*winfo;
	static int uzi_left=0, uzi_right=0;

	winfo = &weapons[weapon_type];
	anir = lara.right_arm.frame_number;										// Now Handle Animation
	if ( lara.right_arm.lock || (input&IN_ACTION && !lara.target) )
	{
		if ( anir>=0 && anir<DRAW1_F-1 )              						// Gun Is Still Being Aimed
			anir++;
		else if (anir==DRAW1_F-1)
		{
			if (input&IN_ACTION)
			{
				angles[0] = lara.right_arm.y_rot + lara_item->pos.y_rot;
				angles[1] = lara.right_arm.x_rot;
				if (FireWeapon( weapon_type, lara.target, lara_item, angles ))
				{
					lara.right_arm.flash_gun = winfo->flash_time;
					SoundEffect( winfo->sample_num,&lara_item->pos,0 );
					sound_already = 1;
					if (weapon_type == LG_UZIS)
						uzi_right = 1;
#if defined(PC_VERSION) && defined(GAMEDEBUG)
					level.ammo_used += winfo->damage;
#endif
				}
				anir = RECOIL_F;
			}
			else if (uzi_right)
			{
				SoundEffect(winfo->sample_num+1, &lara_item->pos, 0);
				uzi_right = 0;
			}
		}
		else if ( anir>=RECOIL_F ) 								// Gun Is Recoiling..
		{
			if (weapon_type == LG_UZIS)
			{
				SoundEffect( winfo->sample_num,&lara_item->pos,0 );
				uzi_right = 1;
			}
			anir++;
			if ( anir==RECOIL_F+winfo->recoil_frame )
				anir = DRAW1_F-1;
		}
	}
	else       													// Havent GOT a LOCK ON..
	{
		if ( anir>=RECOIL_F ) 									// If Gun is Recoiling Stop it now...
			anir = DRAW1_F-1;
		else if ( anir>0 && anir<=DRAW1_F-1 )
			anir--;												// UnLock ARM

		if (uzi_right)
		{
			SoundEffect(winfo->sample_num+1, &lara_item->pos, 0);
			uzi_right = 0;
		}
	}
//	lara.right_arm.frame_number = anir;
	set_arm_info(&lara.right_arm, anir);

	anil = lara.left_arm.frame_number;
	if ( lara.left_arm.lock || (input&IN_ACTION && !lara.target) )
	{
		if ( anil>=0 && anil<DRAW1_F-1 )              				// Gun Is Still Being Aimed
			anil++;
		else if ( anil==(DRAW1_F-1) )
		{
			if (input&IN_ACTION)
			{
				angles[0] = lara.left_arm.y_rot + lara_item->pos.y_rot;
				angles[1] = lara.left_arm.x_rot;
				if (FireWeapon( weapon_type, lara.target, lara_item, angles ))
				{
					lara.left_arm.flash_gun = winfo->flash_time;
					if (!sound_already)
						SoundEffect( winfo->sample_num,&lara_item->pos,0 );
					if (weapon_type == LG_UZIS)
						uzi_left = 1;
#if defined(PC_VERSION) && defined(GAMEDEBUG)
					level.ammo_used += winfo->damage;
#endif
				}
				anil = RECOIL_F;
			}
			else if (uzi_left)
			{
				SoundEffect(winfo->sample_num+1, &lara_item->pos, 0);
				uzi_left = 0;
			}
		}
		else if ( anil>=RECOIL_F ) 									// Gun Is Recoiling..
		{
			if (weapon_type == LG_UZIS)
			{
				SoundEffect( winfo->sample_num,&lara_item->pos,0 );
				uzi_left = 1;
			}
			anil++;
			if ( anil==RECOIL_F+winfo->recoil_frame )
				anil = DRAW1_F-1;
		}
	}
	else       														// Havent GOT a LOCK ON..
	{
		if ( anil>=RECOIL_F ) 										// If Gun is Recoiling Stop it now...
			anil = DRAW1_F-1;
		else if ( anil>0 && anil<=DRAW1_F-1 )
			anil--;													// UnLock ARM

		if (uzi_left)
		{
			SoundEffect(winfo->sample_num+1, &lara_item->pos, 0);
			uzi_left = 0;
		}
	}
//	lara.left_arm.frame_number = anil;
	set_arm_info(&lara.left_arm, anil);
}

