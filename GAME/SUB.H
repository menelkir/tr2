/****************************************************************************
*
* SUB.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 20/09/98
*   MODIFIED : 20/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for SUB.C
*
*****************************************************************************/

#ifndef _SUB_H
#define _SUB_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "../specific/stypes.h"
#include "../3dsystem/3d_gen.h"
#endif

#include "items.h"
#include "collide.h"

/*---------------------------------------------------------------------------
 *	Type Definitions
\*--------------------------------------------------------------------------*/

typedef struct {
	sint32 Vel;
	sint32 Rot;
	sint32 RotX;
	sint16 FanRot;
	char Flags;
	char WeaponTimer;
}SUBINFO;

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if !defined(SUBINFO_ONLY)

#if defined(PSX_VERSION) && defined(RELOC)

typedef int  (SUBFUNC1)();
typedef void (SUBFUNC2)(sint16, ITEM_INFO *, COLL_INFO *);
typedef void (SUBFUNC3)(ITEM_INFO *);

#define SubInitialise	((VOIDFUNCSINT16 *)VehiclePtr[0])
#define SubControl		((SUBFUNC1 *)VehiclePtr[1])
#define SubCollision	((SUBFUNC2 *)VehiclePtr[2])
#define SubDraw		((SUBFUNC3 *)VehiclePtr[3])
#define SubEffects		((VOIDFUNCSINT16 *)VehiclePtr[4])

#else

extern void SubInitialise(sint16 item_number);
extern int  SubControl();
extern void SubCollision(sint16 item_number, ITEM_INFO *litem, COLL_INFO *coll);
extern void SubDraw(ITEM_INFO *item);
extern void SubEffects(sint16 item_number);

#endif

#endif

#ifdef __cplusplus
}
#endif

#endif
