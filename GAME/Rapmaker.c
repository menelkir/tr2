/********************************************************************
 * Raptor Emitter Controller - TS - 22/9/98
 *******************************************************************/

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "../specific/stypes.h"
#endif

#include "objects.h"
#include "lara.h"
#include "lot.h"

#if defined(PSX_VERSION) && defined(RELOC)

void InitialiseRaptorEmitter(sint16 item_number);
void RaptorEmitterControl(sint16 item_number);

void *func[] __attribute__((section(".header"))) = {
	&InitialiseRaptorEmitter,
	&RaptorEmitterControl,
};

#endif

/*---------------------------------------------------------------------------
 *	Locals
\*--------------------------------------------------------------------------*/

static sint16 RaptorItem[3] = {NO_ITEM, NO_ITEM, NO_ITEM};

#define MAX_RAPTORS 5
#define LARA_TOO_CLOSE SQUARE(WALL_L*4)

/*********************************** FUNCTION CODE *******************************************/
void	RaptorEmitterControl( sint16 item_num )
{
	ITEM_INFO		*item,*raptor,*target;
	sint16		raptor_num, target_number, i;
	sint32		x, z, distance;


	item = &items[item_num];
	if (!(item->active) || item->timer <= 0)
	{
		return;
	}
	else if (RaptorItem[0] == NO_ITEM)
	{

		for (target_number = 0, i = 0; target_number < level_items; target_number++)
		{
			target = &items[target_number];
			if (target->object_number == RAPTOR && target->ai_bits == MODIFY)
			{
				RaptorItem[i] = target_number;
				i++;
			}
			if (i > 2)
				return;
		}

		return;
	}

	else if (items[RaptorItem[0]].data != NULL && items[RaptorItem[1]].data != NULL && items[RaptorItem[2]].data != NULL)
	{
		return;
	}
	else
	{
		z = lara_item->pos.z_pos - item->pos.z_pos;
		x = lara_item->pos.x_pos - item->pos.x_pos;
		distance = (z * z) + (x * x);

		if (distance < LARA_TOO_CLOSE)
			return;
		else if (item->item_flags[0] <= 0)
		{
			item->item_flags[0] = 255;
			item->timer-=30;
		}
		else
		{
			item->item_flags[0]--;
			return;
		}
	}


	for (raptor_num = 0; raptor_num < 3; raptor_num++)
	{
		if (items[RaptorItem[raptor_num]].data == NULL)
			break;
	}


	raptor = &items[RaptorItem[raptor_num]];

//	raptor->room_number = item->room_number;
	raptor->pos = item->pos;
	raptor->anim_number = objects[raptor->object_number].anim_index;
	raptor->frame_number = anims[raptor->anim_number].frame_base;
	raptor->current_anim_state = raptor->goal_anim_state = anims[raptor->anim_number].current_anim_state;
	raptor->required_anim_state = 0;
	raptor->flags &= ~(ONESHOT|KILLED_ITEM|INVISIBLE);
	raptor->data = 0;
  	raptor->status = ACTIVE;
	raptor->mesh_bits = 0xffffffff;
	raptor->hit_points = objects[raptor->object_number].hit_points;
	raptor->collidable = 1;
	if (raptor->active)
			RemoveActiveItem(RaptorItem[raptor_num]);
	AddActiveItem(RaptorItem[raptor_num]);
	ItemNewRoom(RaptorItem[raptor_num], item->room_number);
 	EnableBaddieAI(RaptorItem[raptor_num],1);


}



void InitialiseRaptorEmitter(sint16 item_number)
{
//	InitialiseItem(item_number);
	items[item_number].item_flags[0] = (item_number & 0x3) * 96;
}


