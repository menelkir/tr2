/**************************************************************************************************************/
/*                                                                                                            */
/* Croc.c - London Sewer Crocodile (Crock'O'Shit) control - TS - 13/9/98 based on Shark.c							 				  */
/*                                                                                                            */
/**************************************************************************************************************/

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "../specific/stypes.h"
#endif

#include "objects.h"
#include "lara.h"
#include "laraanim.h"
#include "control.h"
#include "effect2.h"
#include "sphere.h"

/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/

#define CROC_BITE_DAMAGE  100

//---------------------------------------------------------------------------------------------
#define CROC_TOUCH (0x3fc)

BITE_INFO croc_bite = {5, -21, 467, 9};

//---------------------------------------------------------------------------------------------
enum croc_anims {CROC_EMPTY, CROC_SWIM, CROC_ATTACK, CROC_DEATH};

#define CROC_TURN (ONE_DEGREE*3)
#define CROC_FLOAT_SPEED (WALL_L/32)

#define CROC_DIE_ANIM 4

//#define DEBUG_CROC

#ifdef DEBUG_CROC
extern char exit_message[];
#endif


#ifdef DEBUG_CROC
static char *CrocStrings[] = {
	"EMPTY",
	"SWIM",
	"ATTACK",
	"DEATH"
};
#endif

/*********************************** FUNCTION CODE *******************************************/

void CrocControl(sint16 item_number)
{
	ITEM_INFO 	  	*item;
	CREATURE_INFO 	*croc;
	sint16 		  	head, angle;
	AI_INFO 		info;

	if (!CreatureActive(item_number))
		return;

	item = &items[item_number];
	croc = (CREATURE_INFO *)item->data;

	head = angle = 0;

	if (item->hit_points <= 0)
	{
		if (item->current_anim_state != CROC_DEATH)
		{
			item->anim_number = objects[CROCODILE].anim_index + CROC_DIE_ANIM;
			item->frame_number = anims[item->anim_number].frame_base;
			item->current_anim_state = CROC_DEATH;
			item->hit_points = DONT_TARGET;
		}

		// Make dead croc float to surface
		CreatureFloat(item_number);


		return;
	}


	/* Else deal with live croc */
	CreatureAIInfo(item, &info);

	if (info.ahead)
		head = info.angle;

	GetCreatureMood(item, &info, VIOLENT);
	if (!(room[lara_item->room_number].flags & UNDERWATER) && lara.skidoo == NO_ITEM)
		croc->mood = BORED_MOOD;

	CreatureMood(item, &info, VIOLENT);

	CreatureTurn(item, CROC_TURN);

 	switch (item->current_anim_state)
	{
		case CROC_SWIM:
//			if (info.bite && (item->touch_bits & CROC_TOUCH))
			if (info.bite && item->touch_bits)
				item->goal_anim_state = CROC_ATTACK;
			break;

		case CROC_ATTACK:
			/* Clear 'attacked' status whenever anim loops back to start */
			if (item->frame_number == anims[item->anim_number].frame_base)
				item->required_anim_state = 0;

//			if (info.bite && (item->touch_bits & CROC_TOUCH))
			if (info.bite && item->touch_bits)
			{
				if (!item->required_anim_state)
				{
					CreatureEffect(item, &croc_bite, DoBloodSplat);

					lara_item->hit_points -= CROC_BITE_DAMAGE;
					lara_item->hit_status = 1;

					item->required_anim_state = CROC_SWIM;
				}
			}
			else
				item->goal_anim_state = CROC_SWIM;
			break;
	}

#ifdef DEBUG_CROC
	sprintf(exit_message, "Bx:%d, LBx:%d, Z:%d, LZ:%d", item->box_number, lara_item->box_number, info.zone_number, info.enemy_zone);
	PrintDbug(2, 1, exit_message);
	sprintf(exit_message, "Mood: %d, BxH:%d, LBxH:%d", croc->mood, boxes[item->box_number].height, boxes[lara_item->box_number].height);
	PrintDbug(2, 2, exit_message);
	sprintf(exit_message, "%s", CrocStrings[item->current_anim_state]);
	PrintDbug(2, 3, exit_message);
	sprintf(exit_message, "%s", CrocStrings[item->goal_anim_state]);
	PrintDbug(2, 4, exit_message);
	sprintf(exit_message, "LRoom:%d, LRmFlags:%d", lara_item->room_number, room[lara_item->room_number].flags);
	PrintDbug(2, 5, exit_message);
	sprintf(exit_message, "H:%d, LH:%d", item->pos.y_pos, lara_item->pos.y_pos);
	PrintDbug(2, 6, exit_message);
//	sprintf(exit_message, "Anim Number; %d", item->anim_number - objects[TREX].anim_index);
//	PrintDbug(2, 6, exit_message);
//	sprintf(exit_message, "Alert:%d, Goal:%d, Hurt:%d", croc->alerted, croc->reached_goal, croc->hurt_by_lara);
//	PrintDbug(2,6, exit_message);
//	sprintf(exit_message, "Flags: %d", croc->flags);
//	PrintDbug(2,7, exit_message);
	sprintf(exit_message, "Room %d  X:%d  Z:%d", lara_item->room_number, (int)(lara_item->pos.x_pos/1024), (int)(lara_item->pos.z_pos/1024));
	PrintDbug(2, 15, exit_message);
#endif

	CreatureJoint(item, 0, head);

	CreatureUnderwater(item, WALL_L/4);

	/* Actually do animation, allowing for LOT collisions */
	CreatureAnimation(item_number, angle, 0);
}


