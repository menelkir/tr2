/****************************************************************************
*
* RAPMAKER.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 22/09/98
*   MODIFIED : 22/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for RAPMAKER.C
*
*****************************************************************************/

#ifndef _RAPMAKER_H
#define _RAPMAKER_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_RAPMAKER
#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_RAPMAKER)

#define InitialiseRaptorEmitter ((VOIDFUNCSINT16*) Baddie8Ptr[0])
#define RaptorEmitterControl ((VOIDFUNCSINT16*) Baddie8Ptr[1])

#else

void RaptorEmitterControl(sint16 item_number);
void InitialiseRaptorEmitter(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
