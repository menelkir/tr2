/****************************************************************************
*
* INVFUNC.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 03/06/98
*   MODIFIED : 03/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for INVFUNC.C
*
*****************************************************************************/

#ifndef _INVFUNC_H
#define _INVFUNC_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#include "invdata.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

extern sint16 LevelSecrets[];

extern void DrawGameInfo(int timed);
extern void InitColours();
extern int Inv_AddItem(int itemNum);
extern int Inv_RequestItem(int itemNum);
extern void Inv_RemoveAllItems();
extern int Inv_RemoveItem(int itemNum);
extern int Inv_GetCount();
extern int Inv_GetItemOption(int itemNum);
extern void	Inv_GlobeLight(sint16 object_number);

extern void RingIsOpen(RING_INFO *ring);
extern void RingIsNotOpen(RING_INFO *ring);
extern void RingNotActive(INVENTORY_ITEM *inv_item);
extern void RingActive();

#ifdef __cplusplus
}
#endif

#endif
