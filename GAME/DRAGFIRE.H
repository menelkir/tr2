/****************************************************************************
*
* DRAGFIRE.H
*
* PROGRAMMER : Gibby
*    VERSION : 00.00
*    CREATED : 15/09/98
*   MODIFIED : 15/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for DRAGFIRE.C
*
*****************************************************************************/

#ifndef _DRAGFIRE_H
#define _DRAGFIRE_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

//#define NO_RELOC_DRAGFIRE
#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_DRAGFIRE)

#define ControlFlameThrower		((VOIDFUNCSINT16 *)Effect1Ptr[0])

#else

extern void ControlFlameThrower(sint16 fx_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
