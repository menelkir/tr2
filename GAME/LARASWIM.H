/****************************************************************************
*
* LARASWIM.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 03/06/98
*   MODIFIED : 03/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for LARASWIM.C
*
*****************************************************************************/

#ifndef _LARASWIM_H
#define _LARASWIM_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"
#include "collide.h"

/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 *	Type Definitions
\*--------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

extern void LaraUnderWater(ITEM_INFO *item, COLL_INFO *coll);
extern sint32 GetWaterDepth(sint32 x, sint32 y, sint32 z, sint16 room_number);
extern void LaraWaterCurrent(COLL_INFO *coll);

#ifdef __cplusplus
}
#endif

#endif
