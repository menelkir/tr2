/****************************************************************************
*
* KAYAK.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 17/08/98
*   MODIFIED : 17/08/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for KAYAK.C
*
*****************************************************************************/

#ifndef _KAYAK_H
#define _KAYAK_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "..\spec_psx\typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"
#include "collide.h"

/*---------------------------------------------------------------------------
 *	Type Definitions
\*--------------------------------------------------------------------------*/

typedef struct {
	sint32 Vel;
	sint32 Rot;
	sint32 FallSpeedF;
	sint32 FallSpeedL;
	sint32 FallSpeedR;
	sint32 Water;
	PHD_3DPOS OldPos;
	char Turn;
	char Forward;
	char TrueWater;
	char Flags;
}KAYAKINFO;

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if !defined(KAYAK_INFO_ONLY)

#if defined(PSX_VERSION) && defined(RELOC)

typedef void (KAYAKFUNC1)(sint16, ITEM_INFO, COLL_INFO);
typedef int  (KAYAKFUNC2)();
typedef void (KAYAKFUNC3)(ITEM_INFO *);

#define KayakCollision	((KAYAKFUNC1 *)VehiclePtr[0])
#define KayakControl	((KAYAKFUNC2 *)VehiclePtr[1])
#define KayakDraw		((KAYAKFUNC3 *)VehiclePtr[2])
#define KayakInitialise	((VOIDFUNCSINT16 *)VehiclePtr[3])
#define LaraRapidsDrown	((VOIDFUNCVOID *)VehiclePtr[4])

#else

extern void KayakCollision(sint16 item_number, ITEM_INFO *litem, COLL_INFO *coll);
extern int KayakControl();
extern void KayakDraw(ITEM_INFO *);
extern void KayakInitialise(sint16);
extern void LaraRapidsDrown();

#endif

#endif

#ifdef __cplusplus
}
#endif

#endif
