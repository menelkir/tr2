/****************************************************************************
*
* PRISONER.H
*
* PROGRAMMER : Gibby
*    VERSION : 00.00
*    CREATED : 20/08/98
*   MODIFIED : 20/08/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for PRISONER.C
*
*****************************************************************************/

#ifndef _PRISONER_H
#define _PRISONER_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

//#define NO_RELOC_BOB
#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_BOB)

#define InitialisePrisoner ((VOIDFUNCSINT16*) Baddie3Ptr[0])
#define PrisonerControl ((VOIDFUNCSINT16*) Baddie3Ptr[1])

#else

void InitialisePrisoner(sint16 item_number);
void PrisonerControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
