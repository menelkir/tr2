#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "../specific/stypes.h"
#endif

#include "objects.h"
#include "lara.h"
#include "sound.h"
#include "control.h"
#include "sphere.h"
#include "effect2.h"
#include "lot.h"

#if defined(PSX_VERSION) && defined(RELOC)

void InitialiseAutogun(sint16 item_number);
void AutogunControl(sint16 item_number);

void *func[] __attribute__((section(".header"))) = {
	&InitialiseAutogun,
	&AutogunControl,
};

#endif

/*********************************** TYPE DEFINITIONS ****************************************/
//Autogun - coded by TS 20-8-98


#define AUTOGUN_DAMAGE 10
//#define AUTOGUN_DAMAGE 2

enum autogun_anims {AUTOGUN_FIRE, AUTOGUN_STILL};

#define AUTOGUN_STILL_ANIM 1
#define AUTOGUN_TURN (ONE_DEGREE*10)
#define AUTOGUN_RANGE SQUARE(WALL_L*5)

#ifndef AUTOGUN_LEFT_BITE
#define	AUTOGUN_LEFT_BITE 4
#define	AUTOGUN_RIGHT_BITE 5
#endif


static BITE_INFO autogun_left = {110,-30,-530,2};
static BITE_INFO autogun_right = {-110,-30,-530,2};

//#define DEBUG_AUTOGUN

#ifdef DEBUG_AUTOGUN
extern char exit_message[];
#endif


void InitialiseAutogun(sint16 item_number)
{
	ITEM_INFO *item;

	item = &items[item_number];

	/* Start Autogun in pose*/
	item->anim_number = objects[ROBOT_SENTRY_GUN].anim_index + AUTOGUN_STILL_ANIM;
	item->frame_number = anims[item->anim_number].frame_base;
	item->current_anim_state = item->goal_anim_state = AUTOGUN_STILL;
	item->item_flags[0] = 0;	// Clear 'I've shot lara' flag.
}

void AutogunControl(sint16 item_number)
{
	ITEM_INFO *item;
	CREATURE_INFO *autogun;
	sint16 tilt_x, tilt_y, change;
	AI_INFO info;

	item = &items[item_number];
	tilt_x = tilt_y = 0;

	if (item->fired_weapon > 1)	// Dynamic light for firing weapons.
	{
		PHD_VECTOR pos;

		item->fired_weapon--;
		//if (objects[item->object_number].bite_offset == AUTOGUN_RIGHT_BITE)
		if (GetRandomControl()&1)
		{
			objects[item->object_number].bite_offset = AUTOGUN_LEFT_BITE;
			phd_PushMatrix();
			pos.x = autogun_left.x;
			pos.y = autogun_left.y;
			pos.z = autogun_left.z;
			GetJointAbsPosition(item, &pos, autogun_left.mesh_num);
			TriggerDynamic(pos.x, pos.y, pos.z, (item->fired_weapon<<1)+8, 24, 16, 4);
			phd_PopMatrix();
		}
		else
		{
			objects[item->object_number].bite_offset =  AUTOGUN_RIGHT_BITE;
			phd_PushMatrix();
			pos.x = autogun_right.x;
			pos.y = autogun_right.y;
			pos.z = autogun_right.z;
			GetJointAbsPosition(item, &pos, autogun_right.mesh_num);
			TriggerDynamic(pos.x, pos.y, pos.z, (item->fired_weapon<<1)+8, 24, 16, 4);
			phd_PopMatrix();
		}
	}

	if (!CreatureActive(item_number) || item->data == 0)	// Might want to come back to item->data == 0 bit.
		return;

	if (item->hit_status)
		item->really_active=1; // Just to stop cheating bastards like Troy shooting them when they're inactive

	autogun = (CREATURE_INFO *)item->data;

	if (item->hit_points <= 0)
	{
	//	item->mesh_bits = 0xffffffff;
		ExplodingDeath(item_number, 0xffffffff, 0);
		DisableBaddieAI(item_number); //Might need to change if we make it non_lot
		KillItem(item_number);
		item->status = DEACTIVATED;
		item->flags |= ONESHOT;
	}

	if(!items[item_number].really_active)
		return;
	else
	{
//		autogun->enemy = lara_item; //Hack to get the demo to work
		CreatureAIInfo(item, &info);
		tilt_x = -info.x_angle;

//		autogun->maximum_turn = (ONE_DEGREE*10);
//		angle = CreatureTurn(item, autogun->maximum_turn);

		switch (item->current_anim_state)
		{
			case AUTOGUN_FIRE:

				if (Targetable(item, &info))
				{
					if  (item->frame_number == anims[item->anim_number].frame_base)
					{
						item->item_flags[0] = 1;	// Set 'I've shot lara' flag.
						if (objects[item->object_number].bite_offset == AUTOGUN_RIGHT_BITE)
							ShotLara(item, &info, &autogun_left, autogun->joint_rotation[0], AUTOGUN_DAMAGE);
						else
							ShotLara(item, &info, &autogun_right, autogun->joint_rotation[0], AUTOGUN_DAMAGE);
						item->fired_weapon = 10;
						SoundEffect( 44,&item->pos,0 );	// Uzi fire ?
					}
				}
				else
					item->goal_anim_state = AUTOGUN_STILL;
				break;


			case AUTOGUN_STILL:
				if (Targetable(item, &info) && item->item_flags[0] == 0)
					item->goal_anim_state = AUTOGUN_FIRE;
				else if (item->item_flags[0])	// Have I shot Lara and can't see her anymore ?
				{
					if (item->ai_bits == MODIFY)
					{
						item->item_flags[0] = 1;
						item->goal_anim_state = AUTOGUN_FIRE;
					}
					else
					{
						item->item_flags[0]=0;	// Clear 'I've shot lara' flag.
						item->really_active = 0;	// Make them inactive.
					}
				}
				break;
		}



//	sprintf(exit_message, "AI_BITS:%d, active:%d", item->ai_bits, item->really_active);
//		PrintDbug(2, 2, exit_message);

/*

		if (item->current_anim_state == AUTOGUN_STILL)
			PrintDbug(2, 4, "Still");
		else
			PrintDbug(2, 4, "Fire");

		if (item->goal_anim_state == AUTOGUN_STILL)
			PrintDbug(2, 5, "Still");
		else
			PrintDbug(2, 5, "Fire");


		if (autogun->mood == BORED_MOOD)
			PrintDbug(2, 3, "Bored");
		else if (autogun->mood == ESCAPE_MOOD)
			PrintDbug(2, 3, "Escape");
		else if (autogun->mood == ATTACK_MOOD)
			PrintDbug(2, 3, "Attack");
		else if (autogun->mood == STALK_MOOD)
			PrintDbug(2, 3, "Stalk");

*/
/*

		switch (item->current_anim_state)
		{
			case AUTOGUN_WAIT:	// Ready for action - make it targetable
				autogun->flags = 0;
				objects[item->object_number].intelligent = 1;
				if (info.distance > AUTOGUN_FORGET_RADIUS)
					item->goal_anim_state = AUTOGUN_DOWN;
				else if ((lara_item->hit_points > 0) && ((info.ahead && info.distance < AUTOGUN_INNER_RADIUS) || item->hit_status || (lara_item->speed > AUTOGUN_UPSET_SPEED)))
					item->goal_anim_state = AUTOGUN_STRIKE;
				break;

			case AUTOGUN_DOWN:	// Coiled - make it untargetable
				autogun->flags = 0;
				objects[item->object_number].intelligent = 0;

				if (info.distance < AUTOGUN_OUTER_RADIUS && lara_item->hit_points > 0)
				{
					item->goal_anim_state = AUTOGUN_RISE;
					objects[item->object_number].intelligent = 1;
				}
				break;

			case AUTOGUN_STRIKE:
					if (autogun->flags != 1 && (item->touch_bits & AUTOGUN_TOUCH))// &&
//						autogun->flags >= autogun_hitframes[item->current_anim_state][0] &&
//						autogun->flags <= autogun_hitframes[item->current_anim_state][1])
					{
						autogun->flags = 1;
						lara_item->hit_points -= AUTOGUN_DAMAGE;
						lara_item->hit_status = 1;

						SoundEffect(245, &item->pos, 0);
						CreatureEffect(item, &autogun_hit, DoBloodSplat);
					}

				break;

		}
		*/

	}
	//CreatureJoint(item, 0, tilt_y);
	//Can't use CreatureJoint because need to be able to do a full 360 rotation
	/* Limit rate of change */
	change = info.angle - autogun->joint_rotation[0];
	if (change > AUTOGUN_TURN)
		change = AUTOGUN_TURN;
	else if (change < -AUTOGUN_TURN)
		change = -AUTOGUN_TURN;

	autogun->joint_rotation[0] += change;

	//Allow CreatureJoint to deal with this, as there should be a maximum x rotation
	//However, this will give a 'Blind Spot' underneath the gun
	CreatureJoint(item, 1, tilt_x);

	// Actually do animation allowing for collisions
	//CreatureAnimation(item_number, angle, 0);
	AnimateItem(item);

	if (info.angle > 0x4000)
	{
		item->pos.y_rot -=0x8000;
		if (info.angle > 0)
			autogun->joint_rotation[0] -=0x8000;
		else if (info.angle < 0)
			autogun->joint_rotation[0] +=0x8000;
	}
	else if (info.angle < -0x4000)
	{
		item->pos.y_rot +=0x8000;
		if (info.angle > 0)
			autogun->joint_rotation[0] -=0x8000;
		else if (info.angle < 0)
			autogun->joint_rotation[0] +=0x8000;
	}
}
