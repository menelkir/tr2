/****************************************************************************
*
* TARGET.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 30/09/98
*   MODIFIED : 30/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for TARGET.C
*
*****************************************************************************/

#ifndef _TARGET_H
#define _TARGET_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

//#define NO_RELOC_TARGET

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_TARGET)

typedef void (TARGETFUNC1)(void);

#define InitialiseTarget ((VOIDFUNCSINT16*) Baddie2Ptr[0])
#define TargetControl ((VOIDFUNCSINT16*) Baddie2Ptr[1])
#define ResetTargets ((TARGETFUNC1*) Baddie2Ptr[2])

#else

void InitialiseTarget(sint16 item_number);
void TargetControl(sint16 item_number);
void ResetTargets(void);

#endif

#ifdef __cplusplus
}
#endif

#endif
