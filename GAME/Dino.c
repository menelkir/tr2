/*********************************************************************************************/
/*                                                                                           */
/* Dino Control                                                                              */
/*                                                                                           */
/*********************************************************************************************/

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/stypes.h"
extern sint32 input;
#else
#include "../specific/stypes.h"
#endif

/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/

#define DINO_TOUCH_DAMAGE 1
#define DINO_TRAMPLE_DAMAGE 10
#define DINO_BITE_DAMAGE 10000

#define DINO_TOUCH (0x3000)

enum dino_anims {DINO_LINK, DINO_STOP, DINO_WALK, DINO_RUN, DINO_ATTACK1, DINO_DEATH, DINO_ROAR,
	DINO_ATTACK2, DINO_KILL};

#define DINO_ROAR_CHANCE 0x200

#define DINO_RUN_TURN  (ONE_DEGREE*4)
#define DINO_WALK_TURN (ONE_DEGREE*2)

#define DINO_RUN_RANGE SQUARE(WALL_L*5)
#define DINO_ATTACK_RANGE SQUARE(WALL_L*4)
#define DINO_BITE_RANGE SQUARE(1500)

/*********************************** FUNCTION CODE *******************************************/



void DinoControl(sint16 item_number)
{
	ITEM_INFO *item;
	CREATURE_INFO *dino;
	sint16 head, angle;
	AI_INFO info;

	item = &items[item_number];
	if ( item->status==INVISIBLE )
	{
		if ( !EnableBaddieAI(item_number,0) )
			return;
		 item->status = ACTIVE;
	}
	dino = (CREATURE_INFO *)item->data;
	head = angle = 0;

	/* Has dino been killed? */
	if (item->hit_points <= 0)
	{
		if (item->current_anim_state == DINO_STOP)
			item->goal_anim_state = DINO_DEATH;
		else
			item->goal_anim_state = DINO_STOP;
	}
	else
	{
		CreatureAIInfo(item, &info);

		if (info.ahead)
			head = info.angle;

		CreatureMood(item, &info, VIOLENT);

		angle = CreatureTurn(item, dino->maximum_turn);

		/* An enemy touching the dino takes damage just from that */
		if (item->touch_bits)
			lara_item->hit_points -= (item->current_anim_state == DINO_RUN)? DINO_TRAMPLE_DAMAGE : DINO_TOUCH_DAMAGE;

		/* Flag if enemy behind and facing us - stop and turn else they can chase in rings */
		dino->flags = (dino->mood != ESCAPE_MOOD && !info.ahead &&
						 info.enemy_facing > -FRONT_ARC && info.enemy_facing < FRONT_ARC);

		/* Flag if enemy is close ahead - walk to bite range */
		if (!dino->flags && info.distance > DINO_BITE_RANGE && info.distance < DINO_ATTACK_RANGE && info.bite)
			dino->flags = 1;

		/* Decide on dino's next move */
	 	switch (item->current_anim_state)
		{
			case DINO_STOP:
				if (item->required_anim_state)
					item->goal_anim_state = item->required_anim_state;
				else if (info.distance < DINO_BITE_RANGE && info.bite)
					item->goal_anim_state = DINO_ATTACK2;
				else if (dino->mood == BORED_MOOD || dino->flags)
					item->goal_anim_state = DINO_WALK;
				else
					item->goal_anim_state = DINO_RUN;
				break;

			case DINO_WALK:
				dino->maximum_turn = DINO_WALK_TURN;

				if (dino->mood != BORED_MOOD || !dino->flags)
					item->goal_anim_state = DINO_STOP;
				else if (info.ahead && GetRandomControl() < DINO_ROAR_CHANCE)
				{
					item->required_anim_state = DINO_ROAR;
					item->goal_anim_state = DINO_STOP;
				}
				break;

			case DINO_RUN:
				dino->maximum_turn = DINO_RUN_TURN;

				if (info.distance < DINO_RUN_RANGE && info.bite)
					item->goal_anim_state = DINO_STOP;
				else if (dino->flags)
					item->goal_anim_state = DINO_STOP;
				else if (dino->mood != ESCAPE_MOOD && info.ahead && GetRandomControl() < DINO_ROAR_CHANCE)
				{
					item->required_anim_state = DINO_ROAR;
					item->goal_anim_state = DINO_STOP;
				}
				else if (dino->mood == BORED_MOOD)
					item->goal_anim_state = DINO_STOP;
				break;

			case DINO_ATTACK2:
				if (item->touch_bits & DINO_TOUCH)
				{
					lara_item->hit_points -= DINO_BITE_DAMAGE;
					lara_item->hit_status = 1;
					item->goal_anim_state = DINO_KILL;
               		if ( lara_item==lara_item )
                  		LaraDinoDeath(item);
				}

				/* Make dino walk if fail to kill Lara (avoids repeat miss attack) */
				item->required_anim_state = DINO_WALK;
				break;
		}
	}

	/* Split dino's head movement between head and neck */
	CreatureJoint(item, 0, (sint16)(head*2));
	dino->joint_rotation[1] = dino->joint_rotation[0];

	/* Actually do animation, allowing for LOT collisions */
	CreatureAnimation(item_number, angle, 0);

	/* Need to make dino collidable even when he dies */
	item->collidable = 1;
}


/******************************************************************************
 *                              Kick Off Dinosaur Killing Lara Animation...
 *****************************************************************************/
void    LaraDinoDeath( ITEM_INFO *item )
{
        ITEM_INFO       *litem;


        item->goal_anim_state = DINO_KILL;              // Dinosaur is Now Killing Lara Badly
        litem = lara_item;                              // Now Align Lara for Death Animation
		  if (litem->room_number != item->room_number)
				ItemNewRoom(lara.item_number, item->room_number);
        litem->pos.x_pos = item->pos.x_pos;
        litem->pos.y_pos = item->pos.y_pos;
        litem->pos.z_pos = item->pos.z_pos;
        litem->pos.y_rot = item->pos.y_rot;
        litem->pos.x_rot = litem->pos.z_rot = 0;
		litem->gravity_status = 0;

        litem->anim_number = objects[LARA_EXTRA].anim_index + 1;        // Kick Off Dino Death
        litem->frame_number = anims[litem->anim_number].frame_base;     // animation in extras
        litem->current_anim_state = AS_SPECIAL;
        litem->goal_anim_state = AS_SPECIAL;
        LaraSwapMeshExtra();                                    // Use Bloody meshes

        litem->hit_points = -1;
        lara.air = -1;
        lara.gun_status = LG_HANDSBUSY;                         // No Dicking about with Guns you know!!!
        lara.gun_type = LG_UNARMED;

        camera.flags = FOLLOW_CENTRE;
        camera.target_angle = 170*ONE_DEGREE;
        camera.target_elevation = -25*ONE_DEGREE;
}



