/****************************************************************************
*
* FIREHEAD.H
*
* PROGRAMMER : Gibby
*    VERSION : 00.00
*    CREATED : 07/09/98
*   MODIFIED : 07/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for FIREHEAD.C
*
*****************************************************************************/

#ifndef _FIREHEAD_H
#define _FIREHEAD_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_FIREHEAD
#if defined(PSX_VERSION) && defined(RELOC)&& !defined(NO_RELOC_FIREHEAD)


#define ControlFireHead ((VOIDFUNCSINT16*) Effect1Ptr[0])
#define InitialiseFireHead ((VOIDFUNCSINT16*) Effect1Ptr[1])
#define ControlRotateyThing ((VOIDFUNCSINT16*) Effect1Ptr[2])

#else

void ControlFireHead(sint16 item_number);
void InitialiseFireHead(sint16 item_number);
void ControlRotateyThing(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
