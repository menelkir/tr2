/****************************************************************************
*
* BOAT.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 01/06/98
*   MODIFIED : 01/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for BOAT.C
*
*****************************************************************************/

#ifndef _BOAT_H
#define _BOAT_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"
#include "collide.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_BOAT
#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_BOAT)

typedef void (BOATFUNC1)(int);
typedef int  (BOATFUNC2)(void);
typedef void (BOATFUNC3)(sint16, ITEM_INFO *, COLL_INFO *);
typedef void (BOATFUNC4)(ITEM_INFO *);

#define InitialiseBoat	((BOATFUNC1 *)VehiclePtr[0])
#define BoatControl		((BOATFUNC2 *)VehiclePtr[1])
#define BoatCollision	((BOATFUNC3 *)VehiclePtr[2])
#define DrawBoat		((BOATFUNC4 *)VehiclePtr[3])

#else

extern void InitialiseBoat(sint16 item_number);
extern void BoatCollision(sint16 item_number, ITEM_INFO *litem, COLL_INFO *coll);
extern void BoatControl(sint16 item_number);
extern void DrawBoat(ITEM_INFO *item);

#endif

#ifdef __cplusplus
}
#endif

#endif
