/*********************************************************************************************/
/*                                                                                           */
/* Target Trap Control - TS  - 30/9/98                                                     */
/*                                                                                           */
/*********************************************************************************************/

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "../specific/stypes.h"
#endif

#include "objects.h"
#include "items.h"
#include "box.h"
#include "lara.h"
#include "laraanim.h"
#include "control.h"
#include "people.h"
#include "traps.h"
#include "missile.h"
//#include "sphere.h"
#include "effect2.h"
#include "gameflow.h"
#include "title.h"

void InitialiseFusebox(sint16 item_number);
void ControlFusebox(sint16 item_number);

#if defined(PSX_VERSION) && defined(RELOC)

void *func[] __attribute__((section(".header"))) = {
	&InitialiseFusebox,
	&ControlFusebox,
};

#endif

/*---------------------------------------------------------------------------
 *	Constants
\*--------------------------------------------------------------------------*/
/*

#define TARGET_UP_TIME 200
#define TARGET_DROP_SPEED (ONE_DEGREE*2)
enum target_anims{TARGET_RISE, TARGET_HIT1, TARGET_HIT2, TARGET_HIT3};
#define HIT1_HP 15
#define HIT2_HP 10
#define HIT3_HP 5

*/
/*---------------------------------------------------------------------------
 *	Externals
\*--------------------------------------------------------------------------*/
//#define DEBUG_TARGET

#ifdef DEBUG_TARGET
extern char exit_message[];
#endif

#ifdef DEBUG_TARGET
static char *TargetStrings[] = {"RISE", "HIT1", "HIT2", "HIT3"};
#endif

/*---------------------------------------------------------------------------
 *	Public Functions
\*--------------------------------------------------------------------------*/
void InitialiseFusebox(sint16 item_number)
{
}

void ControlFusebox(sint16 item_number)
{
	ITEM_INFO *item, *target_item;
	FLOOR_INFO 	*floor;
	sint32 x, z, height;
	sint16 room_number, i;
	item = &items[item_number];

//	if (item->status == INVISIBLE && item->hit_points != DONT_TARGET)
//		item->status = NOT_ACTIVE;

	if (item->hit_points <= 0 && item->anim_number == objects[item->object_number].anim_index)
	{
		item->anim_number = objects[item->object_number].anim_index + 1;
		item->frame_number = anims[item->anim_number].frame_base;
		x = (item->pos.x_pos + (420 * phd_sin(item->pos.y_rot + 0x8000))) >> W2V_SHIFT;
		z = (item->pos.z_pos + (420 * phd_cos(item->pos.y_rot + 0x8000))) >> W2V_SHIFT;
		TriggerExplosionSparks(x, item->pos.y_pos - (STEP_L*3), z,2,0,0,item->room_number);
		TriggerExplosionSmoke(x, item->pos.y_pos - (STEP_L*3), z,0);
		if (CurrentLevel == LV_OFFICE)
		{
			room_number = item->room_number;
			floor = GetFloor(item->pos.x_pos, item->pos.y_pos, item->pos.z_pos, &room_number);
			height = GetHeight(floor, item->pos.x_pos, item->pos.y_pos, item->pos.z_pos);
  			TestTriggers(trigger_index, 1);
			for (i=0, target_item=&items[0]; i<level_items; i++, target_item++)
			{
				if (target_item->object_number == LON_BOSS)
				{
					target_item->item_flags[2] = 1;
//					target_item->hit_points = 0; //Just for testing...
					break;
				}
			}
		}
	}

	AnimateItem(item);

}
