/****************************************************************************
*
* CROC.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 30/09/98
*   MODIFIED : 30/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for CROC.C
*
*****************************************************************************/

#ifndef _CROC_H
#define _CROC_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_CROC
#if defined(PSX_VERSION) && defined(RELOC) && !defined (NO_RELOC_CROC)

#define CrocControl ((VOIDFUNCSINT16 *)Baddie5Ptr)
#else
extern void CrocControl(sint16 item_number);
#endif

#ifdef __cplusplus
}
#endif

#endif
