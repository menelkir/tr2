/****************************************************************************
*
* TRIBEAXE.H
*
* PROGRAMMER : Gibby
*    VERSION : 00.00
*    CREATED : 01/06/98
*   MODIFIED : 01/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for TRIBEAXE.C
*
*****************************************************************************/

#ifndef _TRIBEAXE_H
#define _TRIBEAXE_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC)

#define TribeAxeControl	((VOIDFUNCSINT16 *)Baddie3Ptr)

#else

extern void TribeAxeControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
