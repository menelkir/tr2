/****************************************************************************
*
* RAPTOR.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 02/06/98
*   MODIFIED : 02/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for RAPTOR.C
*
*****************************************************************************/

#ifndef _RAPTOR_H
#define _RAPTOR_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_RAPTOR
#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_RAPTOR)


#define RaptorControl ((VOIDFUNCSINT16 *)Baddie3Ptr)

#else

extern void RaptorControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif

