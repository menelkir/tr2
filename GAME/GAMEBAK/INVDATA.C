/*********************************************************************************************/
/*                                                                                           */
/* Inventory Data                                                                            */
/*                                                                                           */
/*********************************************************************************************/
   
#include "game.h"
#include "invdata.h"			  

/************************** GLOBALS *********************************/

sint32	inv_nframes=2;

SG_COL	inv_colours[C_NUMBER_COLOURS];

/****************************** Inventory Item Sprites ***************************************/
/* GOURAUDS */
														  
/*
SG_COL	req_bgnd_moreup[]={S_GOURAUD_COL(  0,  0,  0,  0), 	// Background Gouraud
						   S_GOURAUD_COL(  0,  0,  0,  0),
						   S_GOURAUD_COL(128, 64, 64,128),
						   S_GOURAUD_COL(  0,  0,  0,  0),

						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(128, 64, 64,128),

						   S_GOURAUD_COL(128, 64,  0,128),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(128, 64,  0,128),
						   S_GOURAUD_COL(255,255,255,255),

						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(128, 64,  0,128),
						   S_GOURAUD_COL(255,255,255,255),
						   S_GOURAUD_COL(128, 64,  0,128)};

SG_COL req_bgnd_moredown[]={S_GOURAUD_COL(128, 64, 0,128), 	// Background Gouraud
						   S_GOURAUD_COL(255,255,255,255),
						   S_GOURAUD_COL(128, 64,  0,128),
						   S_GOURAUD_COL(  0,  0,  0, 0),

						   S_GOURAUD_COL(255,255,255,255),
						   S_GOURAUD_COL(128, 64,  0,128),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(128, 64,  0,128),

						   S_GOURAUD_COL(128, 64, 64,128),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),

						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(128, 64, 64,128),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0)};
*/



SG_COL	req_bgnd_gour1[]={S_GOURAUD_COL(   0,  32,  0, 32), 	// Background Gouraud
						   S_GOURAUD_COL(  0,  32,  0, 32),
						   S_GOURAUD_COL(  0,  96,  0, 96),
						   S_GOURAUD_COL(  0,  32,  0, 32),

						   S_GOURAUD_COL(  0,  32,  0, 32),
						   S_GOURAUD_COL(  0,  32,  0, 32),
						   S_GOURAUD_COL(  0,  32,  0, 32),
						   S_GOURAUD_COL(  0,  96,  0, 96),

						   S_GOURAUD_COL(  0,  96,  0, 96),
						   S_GOURAUD_COL(  0,  32,  0, 32),
						   S_GOURAUD_COL(  0,  32,  0, 32),
						   S_GOURAUD_COL(  0,  32,  0, 32),

						   S_GOURAUD_COL(  0,  32,  0, 32),
						   S_GOURAUD_COL(  0,  96,  0, 96),
						   S_GOURAUD_COL(  0,  32,  0, 32),
						   S_GOURAUD_COL(  0,  32,  0, 32)};



SG_COL	req_bgnd_gour2[]={S_GOURAUD_COL(  96, 96, 96,  96), 	// Outline Gouraud
						   S_GOURAUD_COL(128,128,128, 128),
						   S_GOURAUD_COL( 32, 32, 32,  32),
					   	   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL( 32, 32, 32,  32),
						   S_GOURAUD_COL( 64, 64, 64,  64),
						   S_GOURAUD_COL( 64, 64, 64,  64),
						   S_GOURAUD_COL( 96, 96, 96,  96)};

SG_COL	req_main_gour1[]={S_GOURAUD_COL(   0,  0,  0,   0), 	// Background Gouraud
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL( 16,128, 56, 128),
						   S_GOURAUD_COL(  0,  0,  0,   0),

						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL( 16,128, 56, 128),

						   S_GOURAUD_COL( 16,128, 56, 128),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),

						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL( 16,128, 56, 128),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0)};



SG_COL	req_main_gour2[]={S_GOURAUD_COL(   0,  0,  0, 0), 	// Outline Gouraud
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0)};


SG_COL	req_sel_gour1[]= {S_GOURAUD_COL(   0,  0,  0,   0), 	// Background Gouraud
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL( 56,240,128, 192),
						   S_GOURAUD_COL(  0,  0,  0,   0),

						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL( 56,240,128, 192),

						   S_GOURAUD_COL( 56,240,128, 192),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0),

						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL( 56,240,128, 192),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(  0,  0,  0,   0)};



SG_COL	req_sel_gour2[]={S_GOURAUD_COL(    0,  0,  0,   0), 	// Outline Gouraud
						   S_GOURAUD_COL(255,255,255, 255),
						   S_GOURAUD_COL(  0,  0,  0,   0),
					   	   S_GOURAUD_COL( 56,240,128, 192),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL(255,255,255, 255),
						   S_GOURAUD_COL(  0,  0,  0,   0),
						   S_GOURAUD_COL( 56,240,128, 192),
						   S_GOURAUD_COL(  0,  0,  0,   0)};


SG_COL	req_unsel_gour1[]={S_GOURAUD_COL(  0,  0,  0, 0), 	// Background Gouraud
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL( 28,120, 64, 96),
						   S_GOURAUD_COL(  0,  0,  0, 0),

						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL( 28,120, 64, 96),

						   S_GOURAUD_COL( 28,120, 64, 96),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0),

						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL( 28,120, 64, 96),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL(  0,  0,  0, 0)};



SG_COL	req_unsel_gour2[]={S_GOURAUD_COL(  0,  0,  0, 0), 	// Outline Gouraud
						   S_GOURAUD_COL( 14, 60, 32, 60),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL( 14, 60, 32, 60),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL( 14, 60, 32, 60),
						   S_GOURAUD_COL(  0,  0,  0, 0),
						   S_GOURAUD_COL( 14, 60, 32, 60),
						   S_GOURAUD_COL(  0,  0,  0, 0)};





SG_COL	is_gour1[] = {S_GOURAUD_COL(255,255,255, 255), 	// Top & Left Bevel Edge (HIGHLIGHTED)
					  S_GOURAUD_COL(160,160,160, 160)};
SG_COL	is_gour2[] = {S_GOURAUD_COL(96,96,96, 96),		// Bottom & Right Bevel Edge (HIGHLIGHTED)
					  S_GOURAUD_COL(32,32,32, 32)};
SG_COL	is_gour3[] = {S_GOURAUD_COL(160,160,160, 160), 	// Gauge Surround		(HIGHLIGHTED)
					  S_GOURAUD_COL(128,128,128, 128),
					  S_GOURAUD_COL(96,96,96, 96),
					  S_GOURAUD_COL(128,128,128, 128)};

SG_COL	is_gour4[] = {S_GOURAUD_COL(0,0,0, 0), 		// Moving Gauge (BLACK>RED)
					  S_GOURAUD_COL(255,0,0, 255),
					  S_GOURAUD_COL(255,0,0, 255),
					  S_GOURAUD_COL(0,0,0, 0)};
SG_COL	is_gour5[] = {S_GOURAUD_COL(0,0,0, 0), 		// Moving Gauge (BLACK>ORANGE)
					  S_GOURAUD_COL(255,128,0, 255),
					  S_GOURAUD_COL(255,128,0, 255),
					  S_GOURAUD_COL(0,0,0, 0)};
SG_COL	is_gour6[] = {S_GOURAUD_COL(0,0,0, 0), 		// Moving Gauge (BLACK>BLUE)
					  S_GOURAUD_COL(0,128,255, 255),
					  S_GOURAUD_COL(0,128,255, 255),
					  S_GOURAUD_COL(0,0,0, 0)};

SG_COL	is_gour7[] = {S_GOURAUD_COL(0,0,0, 0), 		// Moving Gauge (BLACK>MAGENTA)
					  S_GOURAUD_COL(255,0,255, 255),
					  S_GOURAUD_COL(255,0,255, 255),
					  S_GOURAUD_COL(0,0,0, 0)};

SG_COL	is_gour8[] = {S_GOURAUD_COL(223,223,223, 223), 	// Top & Left Bevel Edge (DULL)
					  S_GOURAUD_COL(128,128,128, 128)};
SG_COL	is_gour9[] = {S_GOURAUD_COL(64,64,64, 64),		// Bottom & Right Bevel Edge (DULL)
					  S_GOURAUD_COL(0,0,0, 0)};
SG_COL	is_gour10[]= {S_GOURAUD_COL(128,128,128, 128), 	// Gauge Surround		(DULL)
					  S_GOURAUD_COL(96,96,96, 96),
					  S_GOURAUD_COL(64,64,64, 64),
					  S_GOURAUD_COL(96,96,96, 96)};
SG_COL	is_gour11[]= {S_GOURAUD_COL(0,0,0, 0), 		// Moving Gauge (DK ORANGE)
					  S_GOURAUD_COL(128,64,0, 128),
					  S_GOURAUD_COL(128,64,0, 128),
					  S_GOURAUD_COL(0,0,0, 0)};
SG_COL	is_gour12[]= {S_GOURAUD_COL(0,0,0, 0), 		// Moving Gauge (DK TURQUOISE)
					  S_GOURAUD_COL(0,64,128, 128),
					  S_GOURAUD_COL(0,64,128, 128),
					  S_GOURAUD_COL(0,0,0, 0)};

/* DISPLAYED CONTROLS ************************************************************************/
/*GAMMA*/

/*
INVENTORY_SPRITE is_gamma_level[] ={{SHAPE_LINE,  -66,-80, 32, 132, 0, is_gour1, C_GREY},
 									{SHAPE_LINE,  -66,-65, 32, 132, 0, is_gour2, C_GREY},
 									{SHAPE_LINE,  -66,-80, 32,   0,16, is_gour1, C_GREY},
 									{SHAPE_LINE,   66,-80, 32,   0,16, is_gour2, C_GREY},
 									{SHAPE_BOX,	  -65,-79, 32, 130,14, is_gour3, C_GREY},
 									{SHAPE_BOX,	  -64,-78, 32, 129,12, is_gour3, C_GREY},
 									{SHAPE_FBOX,  -63,-77, 32, 126,10, is_gour4, C_RED},
 									{SHAPE_FBOX,  -63,-77, 33, 127,10, 0, C_BLACK},
 									{SHAPE_LINE,    1,-77, 31,   0,10, 0, C_WHITE},
 									{0}};
INVENTORY_SPRITE *is_gamma_list[] = { is_gamma_level,
								  NULL
									};
*/
/*DETAIL*/
INVENTORY_SPRITE is_detail_level[]={{SHAPE_LINE,  -66,-72, 32, 132, 0, is_gour1, C_GREY},
									{SHAPE_LINE,  -66,-56, 32, 132, 0, is_gour2, C_GREY},
									{SHAPE_LINE,  -66,-72, 32,   0,16, is_gour1, C_GREY},
									{SHAPE_LINE,   66,-72, 32,   0,16, is_gour2, C_GREY},
									{SHAPE_BOX,	  -65,-71, 32, 130,14, is_gour3, C_GREY},
									{SHAPE_BOX,	  -64,-70, 32, 128,12, is_gour3, C_GREY},
									{SHAPE_FBOX,  -63,-69, 32, 126,10, is_gour7, C_MAGENTA},
									{SHAPE_FBOX,  -63,-69, 33, 126,10, 0, C_BLACK},
									{SHAPE_LINE,    1,-69, 31,   0,10, 0, C_WHITE},
									{0}};
INVENTORY_SPRITE *is_detail_list[] ={ is_detail_level,
									  NULL
									};
/* SOUND */
#ifdef PC_VERSION
INVENTORY_SPRITE is_sound_muslevel[] ={{SHAPE_LINE,  -66,-80, 32, 132, 0, is_gour1, C_GREY},
									   {SHAPE_LINE,  -66,-65, 32, 132, 0, is_gour2, C_GREY},
									   {SHAPE_LINE,  -66,-80, 32,   0,16, is_gour1, C_GREY},
									   {SHAPE_LINE,   66,-80, 32,   0,16, is_gour2, C_GREY},
									   {SHAPE_BOX,	 -65,-79, 32, 131,14, is_gour3, C_GREY},
									   {SHAPE_BOX,	 -64,-78, 32, 129,12, is_gour3, C_GREY},
									   {SHAPE_FBOX,  -63,-77, 32, 126,10, is_gour5, C_RED},
									   {SHAPE_FBOX,  -63,-77, 33, 127,10, 0, C_BLACK},
									   {0}};
INVENTORY_SPRITE is_sound_sfxlevel[] ={{SHAPE_LINE,  -66,-56, 32, 132, 0, is_gour1, C_GREY},
									   {SHAPE_LINE,  -66,-41, 32, 132, 0, is_gour2, C_GREY},
									   {SHAPE_LINE,  -66,-56, 32,   0,16, is_gour1, C_GREY},
									   {SHAPE_LINE,   66,-56, 32,   0,16, is_gour2, C_GREY},
									   {SHAPE_BOX,	 -65,-55, 32, 131,14, is_gour3, C_GREY},
									   {SHAPE_BOX,	 -64,-54, 32, 129,12, is_gour3, C_GREY},
									   {SHAPE_FBOX,  -63,-53, 32, 126,10, is_gour6, C_BLUE},
									   {SHAPE_FBOX,  -63,-53, 33, 127,10, 0, C_BLACK},
									   {0}};
INVENTORY_SPRITE is_sound_muslevellow[]={{SHAPE_LINE,  -66,-80, 32, 132, 0, is_gour8,  C_GREY},
									   	 {SHAPE_LINE,  -66,-65, 32, 132, 0, is_gour9,  C_GREY},
									   	 {SHAPE_LINE,  -66,-80, 32,   0,16, is_gour8,  C_GREY},
				  					   	 {SHAPE_LINE,   66,-80, 32,   0,16, is_gour9,  C_GREY},
								    	 {SHAPE_BOX,   -65,-79, 32, 131,14, is_gour10, C_GREY},
									   	 {SHAPE_BOX,   -64,-78, 32, 129,12, is_gour10, C_GREY},
									   	 {SHAPE_FBOX,  -63,-77, 32, 126,10, is_gour11,  C_RED},
									   	 {SHAPE_FBOX,  -63,-77, 33, 127,10, 0,  C_BLACK},
//#ifdef PSX_VERSION
//										{SHAPE_SPRITE, -105, -66, 32,  0x10000, 0x10000, 0, 108},
//										{SHAPE_SPRITE, 80, -66, 32,  0x10000, 0x10000, 0, 109},
//#endif
									   	 {0}};
INVENTORY_SPRITE is_sound_sfxlevellow[]={{SHAPE_LINE,  -66,-56, 32, 132, 0, is_gour8,  C_GREY},
									   	 {SHAPE_LINE,  -66,-41, 32, 132, 0, is_gour9,  C_GREY},
									   	 {SHAPE_LINE,  -66,-56, 32,   0,16, is_gour8,  C_GREY},
									   	 {SHAPE_LINE,   66,-56, 32,   0,16, is_gour9,  C_GREY},
									   	 {SHAPE_BOX,   -65,-55, 32, 131,14, is_gour10, C_GREY},
									   	 {SHAPE_BOX,   -64,-54, 32, 129,12, is_gour10, C_GREY},
									   	 {SHAPE_FBOX,  -63,-53, 32, 126,10, is_gour12, C_BLUE},
									   	 {SHAPE_FBOX,  -63,-53, 33, 127,10, 0,  C_BLACK},
//#ifdef PSX_VERSION
//									   {SHAPE_SPRITE, -105, -66, 32,  0x10000, 0x10000, 0, 108},
//									   {SHAPE_SPRITE, 80, -66, 32,  0x10000, 0x10000, 0, 109},
//#endif
									   	 {0}};

#endif
//INVENTORY_SPRITE is_sound_names[]  ={{SHAPE_SPRITE, -74, -72, 32,  0x80000, 0x80000, 0, 101},
//									 {SHAPE_SPRITE, -74, -48, 32,  0x80000, 0x80000, 0, 102},
#ifdef PC_VERSION
INVENTORY_SPRITE is_sound_names[]  ={{SHAPE_SPRITE, -75, -63, 32,  0x80000, 0x80000, 0, 101},
									 {SHAPE_SPRITE, -75, -40, 32,  0x80000, 0x80000, 0, 102},
									 {0}};
#else
INVENTORY_SPRITE is_sound_names[]  ={{SHAPE_SPRITE, -84, -63, 32,  0x10000, 0x10000, 0, 101},
									 {SHAPE_SPRITE, -84, -40, 32,  0x10000, 0x10000, 0, 102},
									 {NULL}};
#endif
#ifdef PC_VERSION
INVENTORY_SPRITE *is_sound_list[] = { is_sound_names,
									  is_sound_muslevel,
									  is_sound_sfxlevel,
									  NULL
										};
#endif

												 																				  

/****************************** Inventory Contents ******************************************/
//																											   CP			 C	C 			   CD			  C	 MESH DRAW  CURRENT	   			Pointer
//													 							 F	C 	   A A A			   uT			 u	u			   ui			  u	   			MESH DRAW  			to
//													 							 R	u  	   n n n			   r-			 r	r  Distance	   rs			  ra Set         		   			list of
//													 							 A	r G	O  i i i			   rX			 X	Y  item 	   rt Angle		  rn bits to     		   			sprites
//													 							 M	r o	p  D S C  Pre-		   eR			 R	R  entends	   ea of item	  eg determine   		   Pos		attached
//																				 E	n a	e  i p n  Translation  no  Object	 o	o  when		   nn when		  nl which mesh  		   of item	to
//								  					object_number				 S	t l	n  r d t  X Rotation   tt  X Rot'n   t	t  selected    tc selected	  te to draw     		   ring		this item
/* Items always in Main List */

INVENTORY_ITEM	icompass_option= {NULL/*compass_name*/,    	MAP_OPTION, 		  1,0,0, 0,1,1,0, MAP_PT_XROT, 0, MAP_X_ROT, 0,0, MAP_Y_ROT, 0, MAP_YTRANS, 0, MAP_ZTRANS, 0, MAP_MESH,  MAP_MESH,  MAP_POS, NULL };
INVENTORY_ITEM	igun_option = 	 {NULL/*pistols_name*/,		GUN_OPTION, 		 12,0,0,11,1,1,0, GUNS_PT_XROT,0, GUNS_X_ROT,0,0, GUNS_Y_ROT,0, GUNS_YTRANS,0, GUNS_ZTRANS,0, GUNS_MESH, GUNS_MESH, GUN_POS, NULL }; 

/* Items sometimes in Main List */
INVENTORY_ITEM	iflare_option =  {NULL/*flare_name*/,		FLAREBOX_OPTION, 	 31,0,0,30,1,1,0, FLAR_PT_XROT,0, FLAR_X_ROT,0,0, FLAR_Y_ROT,0, FLAR_YTRANS,0, FLAR_ZTRANS,0, FLAR_MESH, FLAR_MESH, FLR_POS, NULL };
INVENTORY_ITEM	ishotgun_option ={NULL/*shotgun_name*/,		SHOTGUN_OPTION, 	 13,0,0,12,1,1,0, SGUN_PT_XROT,0, SGUN_X_ROT,0,0, SGUN_Y_ROT,0, SGUN_YTRANS,0, SGUN_ZTRANS,0, SGUN_MESH, SGUN_MESH, SGN_POS, NULL };
INVENTORY_ITEM	imagnum_option = {NULL/*magnum_name*/,		MAGNUM_OPTION, 	 	 12,0,0,11,1,1,0, MAGN_PT_XROT,0, MAGN_X_ROT,0,0, MAGN_Y_ROT,0, MAGN_YTRANS,0, MAGN_ZTRANS,0, MAGN_MESH, MAGN_MESH, MAG_POS, NULL };
INVENTORY_ITEM	iuzi_option = 	 {NULL/*uzi_name*/,   		UZI_OPTION, 	  	 13,0,0,12,1,1,0, UZI_PT_XROT, 0, UZI_X_ROT, 0,0,  UZI_Y_ROT, 0, UZI_YTRANS, 0, UZI_ZTRANS, 0, UZI_MESH,  UZI_MESH,  UZI_POS, NULL };
INVENTORY_ITEM	iharpoon_option = {NULL/*harpoon_name*/,	HARPOON_OPTION, 	 12,0,0,11,1,1,0, HARPOON_PT_XROT,0,HARPOON_X_ROT,0,0, HARPOON_Y_ROT, 0, HARPOON_YTRANS, 0, HARPOON_ZTRANS, 0, HARPOON_MESH,  HARPOON_MESH,  HARPOON_POS, NULL };
INVENTORY_ITEM	im16_option		= {NULL/*m16_name*/,		M16_OPTION, 	  	 12,0,0,11,1,1,0, M16_PT_XROT, 0, M16_X_ROT, 0,0, M16_Y_ROT, 0, M16_YTRANS, 0, M16_ZTRANS, 0, M16_MESH,  M16_MESH,  M16_POS, NULL };
INVENTORY_ITEM	irocket_option = {NULL/*rocket_name*/,		ROCKET_OPTION, 	  	 12,0,0,11,1,1,0, ROCKET_PT_XROT,0,ROCKET_X_ROT,0,0, ROCKET_Y_ROT, 0, ROCKET_YTRANS, 0, ROCKET_ZTRANS, 0, ROCKET_MESH,  ROCKET_MESH,  ROCKET_POS, NULL };

INVENTORY_ITEM	igunammo_option ={NULL/*gun_ammo_name*/, 	GUN_AMMO_OPTION, 	  1,0,0, 0,1,1,0, GAMO_PT_XROT,0, GAMO_X_ROT,0,0, GAMO_Y_ROT,0, GAMO_YTRANS,0, GAMO_ZTRANS,0, GAMO_MESH, GAMO_MESH, GUN_POS, NULL };
INVENTORY_ITEM	isgunammo_option={NULL/*sg_ammo_name*/, 	SG_AMMO_OPTION, 	  1,0,0, 0,1,1,0, SGAM_PT_XROT,0, SGAM_X_ROT,0,0, SGAM_Y_ROT,0, SGAM_YTRANS,0, SGAM_ZTRANS,0, SGAM_MESH, SGAM_MESH, SGN_POS, NULL };
INVENTORY_ITEM	imagammo_option= {NULL/*mag_ammo_name*/, 	MAG_AMMO_OPTION, 	  1,0,0, 0,1,1,0, MGAM_PT_XROT,0, MGAM_X_ROT,0,0, MGAM_Y_ROT,0, MGAM_YTRANS,0, MGAM_ZTRANS,0, MGAM_MESH, MGAM_MESH, MAG_POS, NULL };
INVENTORY_ITEM	iuziammo_option= {NULL/*uzi_ammo_name*/,   	UZI_AMMO_OPTION, 	  1,0,0, 0,1,1,0, UZAM_PT_XROT,0, UZAM_X_ROT,0,0, UZAM_Y_ROT,0, UZAM_YTRANS,0, UZAM_ZTRANS,0, UZAM_MESH, UZAM_MESH, UZI_POS, NULL };
INVENTORY_ITEM	iharpoonammo_option= {NULL/*harpoon_a_nme*/,HARPOON_AMMO_OPTION,  1,0,0, 0,1,1,0, HAM_PT_XROT, 0, HAM_X_ROT, 0,0, HAM_Y_ROT,0, HAM_YTRANS,0, HAM_ZTRANS,0, HAM_MESH, HAM_MESH, HARPOON_POS, NULL };
INVENTORY_ITEM	im16ammo_option= {NULL/*m16_ammo_name*/,	M16_AMMO_OPTION, 	  1,0,0, 0,1,1,0, M16AM_PT_XROT,0,M16AM_X_ROT,0,0, M16AM_Y_ROT,0, M16AM_YTRANS,0, M16AM_ZTRANS,0, M16AM_MESH, M16AM_MESH, M16_POS, NULL };
INVENTORY_ITEM	irocketammo_option= {NULL/*rocket_am_name*/,ROCKET_AMMO_OPTION,   1,0,0, 0,1,1,0, RAM_PT_XROT, 0, RAM_X_ROT, 0,0, RAM_Y_ROT,0, RAM_YTRANS,0, RAM_ZTRANS,0, RAM_MESH, RAM_MESH, ROCKET_POS, NULL };

INVENTORY_ITEM	imedi_option = 	 {NULL/*medi_name*/,		MEDI_OPTION, 	 	 26,0,0,25,1,1,0, MEDI_PT_XROT,0, MEDI_X_ROT,0,0, MEDI_Y_ROT,0, MEDI_YTRANS,0, MEDI_ZTRANS,0, MEDI_MESH, MEDI_MESH, MED_POS, NULL };
INVENTORY_ITEM	ibigmedi_option ={NULL/*bigmedi_name*/,		BIGMEDI_OPTION, 	 20,0,0,19,1,1,0, BMED_PT_XROT,0, BMED_X_ROT,0,0, BMED_Y_ROT,0, BMED_YTRANS,0, BMED_ZTRANS,0, BMED_MESH, BMED_MESH, BGM_POS, NULL };
INVENTORY_ITEM	ipickup1_option ={NULL/*pickup_name*/,		PICKUP_OPTION1, 	  1,0,0, 0,1,1,0, PCK1_PT_XROT,0, PCK1_X_ROT,0,0, PCK1_Y_ROT,0, PCK1_YTRANS,0, PCK1_ZTRANS,0, PCK1_MESH, PCK1_MESH, PK1_POS, NULL };
INVENTORY_ITEM	ipickup2_option ={NULL/*pickup_name*/,		PICKUP_OPTION2, 	  1,0,0, 0,1,1,0, PCK2_PT_XROT,0, PCK2_X_ROT,0,0, PCK2_Y_ROT,0, PCK2_YTRANS,0, PCK2_ZTRANS,0, PCK2_MESH, PCK2_MESH, PK2_POS, NULL };
INVENTORY_ITEM	ipuzzle1_option ={NULL/*puzzle_name*/,		PUZZLE_OPTION1, 	  1,0,0, 0,1,1,0, PUZ1_PT_XROT,0, PUZ1_X_ROT,0,0, PUZ1_Y_ROT,0, PUZ1_YTRANS,0, PUZ1_ZTRANS,0, PUZ1_MESH, PUZ1_MESH, PZ1_POS, NULL };
INVENTORY_ITEM	ipuzzle2_option ={NULL/*puzzle_name*/,		PUZZLE_OPTION2, 	  1,0,0, 0,1,1,0, PUZ2_PT_XROT,0, PUZ2_X_ROT,0,0, PUZ2_Y_ROT,0, PUZ2_YTRANS,0, PUZ2_ZTRANS,0, PUZ2_MESH, PUZ2_MESH, PZ2_POS, NULL };
INVENTORY_ITEM	ipuzzle3_option ={NULL/*puzzle_name*/,		PUZZLE_OPTION3, 	  1,0,0, 0,1,1,0, PUZ3_PT_XROT,0, PUZ3_X_ROT,0,0, PUZ3_Y_ROT,0, PUZ3_YTRANS,0, PUZ3_ZTRANS,0, PUZ3_MESH, PUZ3_MESH, PZ3_POS, NULL };
INVENTORY_ITEM	ipuzzle4_option ={NULL/*puzzle_name*/,		PUZZLE_OPTION4, 	  1,0,0, 0,1,1,0, PUZ4_PT_XROT,0, PUZ4_X_ROT,0,0, PUZ4_Y_ROT,0, PUZ4_YTRANS,0, PUZ4_ZTRANS,0, PUZ4_MESH, PUZ4_MESH, PZ4_POS, NULL };
INVENTORY_ITEM	ikey1_option =   {NULL/*key_name*/,			KEY_OPTION1, 	      1,0,0, 0,1,1,0, KEY1_PT_XROT,0, KEY1_X_ROT,0,0, KEY1_Y_ROT,0, KEY1_YTRANS,0, KEY1_ZTRANS,0, KEY1_MESH, KEY1_MESH, KY1_POS, NULL };
INVENTORY_ITEM	ikey2_option =   {NULL/*key_name*/,			KEY_OPTION2, 	      1,0,0, 0,1,1,0, KEY2_PT_XROT,0, KEY2_X_ROT,0,0, KEY2_Y_ROT,0, KEY2_YTRANS,0, KEY2_ZTRANS,0, KEY2_MESH, KEY2_MESH, KY2_POS, NULL };
INVENTORY_ITEM	ikey3_option =   {NULL/*key_name*/,			KEY_OPTION3, 	      1,0,0, 0,1,1,0, KEY3_PT_XROT,0, KEY3_X_ROT,0,0, KEY3_Y_ROT,0, KEY3_YTRANS,0, KEY3_ZTRANS,0, KEY3_MESH, KEY3_MESH, KY3_POS, NULL };
INVENTORY_ITEM	ikey4_option =   {NULL/*key_name*/,			KEY_OPTION4, 	      1,0,0, 0,1,1,0, KEY4_PT_XROT,0, KEY4_X_ROT,0,0, KEY4_Y_ROT,0, KEY4_YTRANS,0, KEY4_ZTRANS,0, KEY4_MESH, KEY4_MESH, KY4_POS, NULL };
/* Items vary between versions in Options List */
INVENTORY_ITEM	ipassport_option={NULL/*game_name*/,		PASSPORT_CLOSED, 	 30,0,0,14,1,1,0, PASS_PT_XROT,0, PASS_X_ROT,0,0, PASS_Y_ROT,0, PASS_YTRANS,0, PASS_ZTRANS,0, PASS_MESH, PASS_MESH, 		 0, NULL };
INVENTORY_ITEM	idetail_option = {NULL/*detail_name*/,		DETAIL_OPTION, 	      1,0,0, 0,1,1,0, DETL_PT_XROT,0, DETL_X_ROT,0,0, DETL_Y_ROT,0, DETL_YTRANS,0, DETL_ZTRANS,0, DETL_MESH, DETL_MESH, 		 1, NULL };
INVENTORY_ITEM	isound_option =  {NULL/*sound_name*/,		SOUND_OPTION, 	      1,0,0, 0,1,1,0, SND_PT_XROT, 0, SND_X_ROT, 0,0, SND_Y_ROT, 0, SND_YTRANS, 0, SND_ZTRANS, 0, SND_MESH, 	SND_MESH, 		 2, NULL };
INVENTORY_ITEM	icontrol_option ={NULL/*control_name*/, 	CONTROL_OPTION, 	  1,0,0, 0,1,1,0, CTRL_PT_XROT,0, CTRL_X_ROT,5632,5632, CTRL_Y_ROT,0, CTRL_YTRANS,0, CTRL_ZTRANS,0, CTRL_MESH, CTRL_MESH, 		 3, NULL };
//INVENTORY_ITEM	igamma_option =  {NULL/*gamma_name*/,		GAMMA_OPTION, 	      1,0,0, 0,1,1,0, GAMM_PT_XROT,0, GAMM_X_ROT,0, GAMM_Y_ROT,0, GAMM_YTRANS,0, GAMM_ZTRANS,0, GAMM_MESH, GAMM_MESH, 		 4, NULL };
INVENTORY_ITEM	iphoto_option =  {NULL/*gym_name*/,			PHOTO_OPTION, 	      1,0,0, 0,1,1,0, GYM_PT_XROT, 0, GYM_X_ROT, 0,0, GYM_Y_ROT, 0, GYM_YTRANS, 0, GYM_ZTRANS, 0, GYM_MESH,  GYM_MESH, 		 5, NULL };


/* List of objects in MAIN inventory ring */
sint16	inv_main_objects=8;
sint16	inv_main_current=0;
sint16	inv_main_select=-1;
sint16	inv_main_qtys[23] = {1,1,1,1,1,1,1,1,1};
INVENTORY_ITEM	*inv_main_list[23] = {
				&icompass_option,
				&iflare_option,
				&igun_option,
				&ishotgun_option,
				&imagnum_option,
				&iuzi_option,
				&im16_option,
				&irocket_option,
				&iharpoon_option,
				&ibigmedi_option,
				&imedi_option,
				};
/* List of objects in KEYS inventory ring */
sint16	inv_keys_objects=0;
sint16	inv_keys_current=0;
sint16	inv_keys_select=-1;
sint16	inv_keys_qtys[23] = {1,1,1,1,1,1,1,1,1,1,1};
INVENTORY_ITEM	*inv_keys_list[23] = {
	&ipuzzle1_option,
	&ipuzzle2_option,
	&ipuzzle3_option,
	&ipuzzle4_option,
	&ikey1_option,
	&ikey2_option,
	&ikey3_option,
	&ikey4_option,
	&ipickup1_option,
	&ipickup2_option
};

#ifdef	PC_VERSION
#ifdef DEMO
sint16 inv_option_objects=1;
sint16 inv_option_current=0;
INVENTORY_ITEM *inv_option_list[] = {&ipassport_option};
#else
/* List of objects in OPTIONS inventory ring */
 #ifdef NO_GYM
sint16	inv_option_objects=3;
 #else
sint16	inv_option_objects=4;
 #endif
sint16	inv_option_current=0;
INVENTORY_ITEM	*inv_option_list[] = {
				&ipassport_option,
				&icontrol_option,
				&isound_option,
				&iphoto_option
				};
#endif
#endif

#ifdef	PSX_VERSION
/* List of objects in OPTIONS inventory ring */
sint16	inv_option_objects=4;
sint16	inv_option_current=4;
INVENTORY_ITEM	*inv_option_list[5] = {
				&ipassport_option,
				&isound_option,
				&idetail_option,
				&icontrol_option,
				&iphoto_option,
				};
#endif

TEXTSTRING *Inv_itemText[IT_NUMBEROF]={NULL};
TEXTSTRING *Inv_levelText=NULL;
TEXTSTRING *Inv_ringText=NULL;
TEXTSTRING *Inv_tagText=NULL;
TEXTSTRING *Inv_upArrow1=NULL;
TEXTSTRING *Inv_upArrow2=NULL;
TEXTSTRING *Inv_downArrow1=NULL;
TEXTSTRING *Inv_downArrow2=NULL;

int	OpenInvOnGym = 1;

//	REQUESTERS -------------------------------------------------

uint32	RequesterFlags1[MAXIMUM_LEVELS];
uint32	RequesterFlags2[MAXIMUM_LEVELS];
#ifdef PC_VERSION
uint32	SaveGameReqFlags1[MAXIMUM_LEVELS];
uint32	SaveGameReqFlags2[MAXIMUM_LEVELS];
#endif


char Save_Game_Strings[MAXIMUM_LEVELS*LEVLEN];	    
char Save_Game_Strings2[MAXIMUM_LEVELS*LEVLEN];	    		    

REQUEST_INFO Load_Game_Requester={
	0,						// noselector
	1,						// items
	0, 5, 0,0,				// selected, vis_lines, line_offset, old
	296, TEXT_HEIGHT+7,		// pixwidth, line_height
	0,-32,0,				// xpos, ypos, zpos
	LEVLEN,				// length of each item text
	Save_Game_Strings,	// item texts
	Save_Game_Strings2,	// item texts 2
};

#ifdef PC_VERSION
char Valid_Level_Strings[MAXIMUM_LEVELS][LEVLEN];
char Valid_Level_Strings2[MAXIMUM_LEVELS][LEVLEN];
#endif

REQUEST_INFO Level_Select_Requester={
	0,						// noselector
	1,						// items
	0, 5, 0,0,				// selected, vis_lines, line_offset, old
	272, TEXT_HEIGHT+7,		// pixwidth, line_height
	0,-32,0,				// xpos, ypos, zpos
	LEVLEN, 				// length of each item text
#ifdef PC_VERSION
	Valid_Level_Strings[0],	// item texts
	Valid_Level_Strings2[0],	// item texts 2
#else
	Save_Game_Strings,	// item texts
	Save_Game_Strings2,	// item texts 2
#endif
};

sint32	inputDB=0;
sint32	oldInputDB=0;
