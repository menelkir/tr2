/****************************************************************************
*
* PEOPLE.H
*
* PROGRAMMER : Chris
*    VERSION : 00.00
*    CREATED : 04/06/98
*   MODIFIED : 04/06/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for PEOPLE.C
*
*****************************************************************************/

#ifndef _PEOPLE_H
#define _PEOPLE_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#include "../spec_psx/3d_gen.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"
#include "effects.h"
#include "box.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

extern int Targetable(ITEM_INFO *item, AI_INFO *info);
extern sint16 GunShot(sint32 x, sint32 y, sint32 z, sint16 speed, PHD_ANGLE yrot, sint16 room_number);
extern sint16 GunHit(sint32 x, sint32 y, sint32 z, sint16 speed, PHD_ANGLE yrot, sint16 room_number);
extern sint16 GunMiss(sint32 x, sint32 y, sint32 z, sint16 speed, PHD_ANGLE yrot, sint16 room_number);
extern int ShotLara(ITEM_INFO *item, AI_INFO *info, BITE_INFO *gun, sint16 extra_rotation, int damage);
extern void WinstonControl(sint16 item_number);
extern int TargetVisible(ITEM_INFO *item, AI_INFO *info);

#ifdef __cplusplus
}
#endif

#endif
