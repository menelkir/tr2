/****************************************************************************
*
* FUSEBOX.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 30/09/98
*   MODIFIED : 30/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for FUSEBOX.C
*
*****************************************************************************/

#ifndef _FUSEBOX_H
#define _FUSEBOX_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

//#include "items.h"

//#define NO_RELOC_FUSEBOX

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_FUSEBOX)

#define InitialiseFusebox ((VOIDFUNCSINT16*) Effect3Ptr[0])
#define ControlFusebox ((VOIDFUNCSINT16*) Effect3Ptr[1])

#else

void InitialiseFusebox(sint16 item_number);
void ControlFusebox(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
