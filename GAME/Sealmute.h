/****************************************************************************
*
* SEALMUTE.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 13/09/98
*   MODIFIED : 13/09/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for SEALMUTE.C
*
*****************************************************************************/

#ifndef _SEALMUTE_H
#define _SEALMUTE_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/
//#define NO_RELOC_SEALMUTE

#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_SEALMUTE)


#define SealmuteControl	((VOIDFUNCSINT16 *)Baddie1Ptr)

#else

extern void SealmuteControl(sint16 item_number);

#endif

#ifdef __cplusplus
}
#endif

#endif
