/****************************************************************************
*
* WINSTON.H
*
* PROGRAMMER : Tom
*    VERSION : 00.00
*    CREATED : 26/08/98
*   MODIFIED : 26/08/98
*       TABS : 5
*
* DESCRIPTION
*	Header file for WINSTON.C
*
*****************************************************************************/

#ifndef _WINSTON_H
#define _WINSTON_H

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------
 *	Import Headers
\*--------------------------------------------------------------------------*/

#ifdef PSX_VERSION
#include "../spec_psx/typedefs.h"
#else
#include "..\specific\stypes.h"
#endif

#include "items.h"

//#define NO_RELOC_WINSTON

/*---------------------------------------------------------------------------
 *	Prototypes
\*--------------------------------------------------------------------------*/

#if defined(PSX_VERSION) && defined(RELOC) && !defined(NO_RELOC_WINSTON)

//typedef void (WINSTONFUNC1)(ITEM_INFO *);

//#define InitialiseWinston ((VOIDFUNCSINT16*) Baddie1Ptr[0])
#define WinstonControl ((VOIDFUNCSINT16*) Baddie1Ptr[0])
#define OldWinstonControl ((VOIDFUNCSINT16*) Baddie1Ptr[1])
//#define DrawWinston ((WINSTONFUNC1*) Baddie1Ptr[2])

#else

//void InitialiseWinston(sint16 item_number);
void WinstonControl(sint16 item_number);
void OldWinstonControl(sint16 item_number);
//void DrawWinston(ITEM_INFO *item);

#endif

#ifdef __cplusplus
}
#endif

#endif
