#include <stdio.h>
#include <string.h>
#include "gf_data.h"
#include "\projects\tomb21\spec_psx\typedefs.h"
#ifndef GF_DATA_DEF
#define GF_DATA_DEF

#define NTSC
#define LANGUAGE 0

#define USE_SCRIPT
#define DEF_SCRIPT

#define MAX_SEQ_SIZE 10000

#define MAX_PICS	50
#define MAX_TITLE	50
#define MAX_FMV		50
#define MAX_LEVELS	50
#define MAX_CUTS	50
#define MAX_DEMOS	50



enum title_types {
	GFT_TITLEFILE=0
};


enum game_levels {
        LV_GYM=0,
        LV_FIRSTLEVEL,
};

enum types_of_levels {
		LVT_PLAYABLE,
		LVT_ROLLINGDEMO,
		LVT_CUTSCENE,
		LVT_FMV
};

enum add_inv_types {
	INV_PISTOLS,
	INV_SHOTGUN,
	INV_AUTOPISTOLS,
	INV_UZIS,
	INV_HARPOON,
	INV_M16,
	INV_ROCKETLAUNCHER,
	INV_GRENADELAUNCHER,

	INV_PISTOL_AMMO,
	INV_SHOTGUN_AMMO,
	INV_AUTOPISTOLS_AMMO,
	INV_UZI_AMMO,
	INV_HARPOON_AMMO,
	INV_M16_AMMO,
	INV_ROCKETLAUNCHER_AMMO,
	INV_GRENADELAUNCHER_AMMO,
	INV_FLARES,

	INV_MEDI,
	INV_BIGMEDI,

	INV_PICKUP1,
	INV_PICKUP2,
	INV_PUZZLE1,
	INV_PUZZLE2,
	INV_PUZZLE3,
	INV_PUZZLE4,
	INV_KEY1,
	INV_KEY2,
	INV_KEY3,
	INV_KEY4,
	INV_NUMBEROF
};

enum gf_event_types {
	GFE_PICTURE=0,
	GFE_LIST_START,
	GFE_LIST_END,
	GFE_PLAYFMV,

	GFE_STARTLEVEL,
	GFE_CUTSCENE,
	GFE_LEVCOMPLETE,

	GFE_DEMOPLAY,

	GFE_JUMPTO_SEQ,
	GFE_END_SEQ,

	GFE_SETTRACK,
	GFE_SUNSET,
	GFE_LOADINGPIC,
	GFE_DEADLY_WATER,
	GFE_REMOVE_WEAPONS,
	GFE_GAMECOMPLETE,
	GFE_CUTANGLE,
	GFE_NOFLOOR,
	GFE_ADD2INV,
	GFE_STARTANIM,
	GFE_NUMSECRETS,
	GFE_KILL2COMPLETE,
	GFE_REMOVE_AMMO
};


enum game_string_ids {
	GT_MAIN_HEADING=0,
	GT_OPTION_HEADING,
	GT_KEYS_HEADING,
	GT_GAMEOVER_HEADING,
	// Passport Text Section
	GT_LOADGAME,
	GT_SAVEGAME,
	GT_STARTGAME,
	GT_RESTARTLEVEL,
	GT_EXIT2TITLE,
	GT_EXITDEMO,
	GT_EXITGAME,
	GT_SELECTLEVEL,
	GT_SAVEPOSITION,
	// Detail text
	GT_DETAIL,
	GT_HIGH_DETAIL,
	GT_MEDIUM_DETAIL,
	GT_LOW_DETAIL,
	// Control strings
	GT_WALK,
	GT_ROLL,
	GT_RUN,
	GT_LEFT,
	GT_RIGHT,
	GT_BACK,
	GT_STEPLEFT1,
	GT_STEPLEFT2,
	GT_STEPRIGHT1,
	GT_STEPRIGHT2,
	GT_LOOK,
	GT_JUMP,
	GT_ACTION,
	GT_DRAWWEAPON1,
	GT_DRAWWEAPON2,
	GT_INVENTORY,
	GT_USEFLARE,
	GT_STEPSHIFT,
	// Inventory Item Strings
	GT_STOPWATCH,
	GT_PISTOLS,
	GT_SHOTGUN,
	GT_AUTOPISTOLS,
	GT_UZIS,
	GT_HARPOON,
	GT_M16,
	GT_ROCKETLAUNCHER,
	GT_GRENADELAUNCHER,
	GT_FLARE,

	GT_PISTOLCLIPS,
	GT_SHOTGUNSHELLS,
	GT_AUTOPISTOLCLIPS,
	GT_UZICLIPS,
	GT_HARPOONBOLTS,
	GT_M16CLIPS,
	GT_ROCKETS,
	GT_GRENADES,

	GT_SMALLMEDI,
	GT_LARGEMEDI,
	GT_PICKUP,
	GT_PUZZLE,
	GT_KEY,
	GT_GAME,

	GT_GYM,
	GT_LOADING,

	// Stats text
	GT_STAT_TIME,
	GT_STAT_SECRETS,
	GT_STAT_LOCATION,
	GT_STAT_KILLS,
	GT_STAT_AMMO,
	GT_STAT_RATIO,
	GT_STAT_SAVES,
	GT_STAT_DISTANCE,
	GT_STAT_HEALTH,

	GT_SECURITY_TAG,

	GT_NONE,
	GT_FINISH,
	GT_BESTTIMES,
	GT_NOTIMES,
	GT_NOTAVAILABLE,
	GT_CURRENTPOS,
	GT_GAMESTATS,
	GT_OF,
	GT_SPARE9,
	GT_SPARE10,
	GT_SPARE11,
	GT_SPARE12,
	GT_SPARE13,
	GT_SPARE14,
	GT_SPARE15,
	GT_SPARE16,
	GT_SPARE17,
	GT_SPARE18,
	GT_SPARE19,
	GT_SPARE20,
	GT_NUM_GAMESTRINGS
};

// PC Specific
enum pc_string_ids {
	PCSTR_DETAILLEVEL=0,
	PCSTR_DEMOMODE,
	PCSTR_SOUND,
	PCSTR_CONTROLS,
	PCSTR_GAMMA,
	PCSTR_SETVOLUME,
	PCSTR_USERKEYS,
// Savegame Strings
	PCSTR_SAVEMESS1,
	PCSTR_SAVEMESS2,
	PCSTR_SAVEMESS3,
	PCSTR_SAVEMESS4,
	PCSTR_SAVEMESS5,
	PCSTR_SAVEMESS6,
	PCSTR_SAVEMESS7,
	PCSTR_SAVEMESS8,
	PCSTR_SAVESLOT,
	PCSTR_OFF,
	PCSTR_ON,
	PCSTR_SETUPSOUND,
	PCSTR_DEFAULTKEYS,
	PCSTR_DOZY_STRING,
	PCSTR_SPARE1,
	PCSTR_SPARE2,
	PCSTR_SPARE3,
	PCSTR_SPARE4,
	PCSTR_SPARE5,
	PCSTR_SPARE6,
	PCSTR_SPARE7,
	PCSTR_SPARE8,
	PCSTR_SPARE9,
	PCSTR_SPARE10,
	PCSTR_SPARE11,
	PCSTR_SPARE12,
	PCSTR_SPARE13,
	PCSTR_SPARE14,
	PCSTR_SPARE15,
	PCSTR_SPARE16,
	PCSTR_SPARE17,
	PCSTR_SPARE18,
	PCSTR_SPARE19,
	PCSTR_SPARE20,
	PCSTR_NUM_STRINGS
};

// PSX Specific
enum psx_string_ids {
	PSSTR_DETAILLEVEL=0,
	PSSTR_DEMOMODE,
	PSSTR_SOUND,
	PSSTR_CONTROLS,
	PSSTR_GAMMA,
	PSSTR_SETVOLUME,
	PSSTR_USERKEYS,
	PSSTR_SAVEMESS1,// Savegame Strings
	PSSTR_SAVEMESS2,
	PSSTR_SAVEMESS3,
	PSSTR_SAVEMESS4,
	PSSTR_SAVEMESS5,
	PSSTR_SAVEMESS6,
	PSSTR_SAVEMESS7,
	PSSTR_SAVEMESS8,
	PSSTR_SAVESLOT,
	PSSTR_DEMOTEXT,
	PSSTR_MOVESTR0,
	PSSTR_MOVESTR1,
	PSSTR_SELECT_TEXT,
	PSSTR_DESELECT_TEXT,
	PSSTR_CONTINUE_TEXT,
	PSSTR_PAUSED_TEXT,
	PSSTR_NOJOY_TEXT,
	PSSTR_GAPSTRING0,
	PSSTR_GAPSTRING1,
	PSSTR_GAPSTRING2,
	PSSTR_GAPSTRING3,
	PSSTR_GAPSTRING4,
	PSSTR_GAPSTRING5,
	PSSTR_GAPSTRING6,
	PSSTR_GAPSTRING7,
	PSSTR_GAPSTRING8,
	PSSTR_GAPSTRING9,
	PSSTR_GAPSTRING10,
	PSSTR_GAPSTRING11,
	PSSTR_GAPSTRING12,
	PSSTR_GAPSTRING13,
	PSSTR_GAPSTRING14,
	PSSTR_GAPSTRING15,
	PSSTR_GAPSTRING16,
	PSSTR_GAPSTRING17,
	PSSTR_GAPSTRING18,
	PSSTR_GAPSTRING19,
	PSSTR_GAPSTRING20,
	PSSTR_GAPSTRING21,
	PSSTR_GAPSTRING22,
	PSSTR_GAPSTRING23,
	PSSTR_GAPSTRING24,
	PSSTR_GAPSTRING25,
	PSSTR_GAPSTRING26,
	PSSTR_GAPSTRING27,
	PSSTR_GAPSTRING28,
	PSSTR_GAPSTRING29,
	PSSTR_GAPSTRING30,
	PSSTR_GAPSTRING31,
	PSSTR_GAPSTRING32,
	PSSTR_GAPSTRING33,
	PSSTR_GAPSTRING34,
	PSSTR_GAPSTRING35,
	PSSTR_GAPSTRING36,
	PSSTR_GAPSTRING37,
	PSSTR_FILENAMEPAL,
	PSSTR_FILENAMENTSC,
	PSSTR_LOADOK,
	PSSTR_LOADFAIL,
	PSSTR_SAVEOK,
	PSSTR_SAVEFAIL,
	PSSTR_SPARE9,
	PSSTR_SPARE10,
	PSSTR_SPARE11,
	PSSTR_SPARE12,
	PSSTR_SPARE13,
	PSSTR_SPARE14,
	PSSTR_SPARE15,
	PSSTR_SPARE16,
	PSSTR_SPARE17,
	PSSTR_SPARE18,
	PSSTR_SPARE19,
	PSSTR_SPARE20,
	PSSTR_NUM_STRINGS
};



typedef struct gameflow_info {
	// 16 x sint32  (16)
	sint32	firstOption;			// First option to do (usually EXIT_TO_TITLE)
	sint32	title_replace;			// Replace the title with STARTGAME|LEVEL1 / STARTDEMO|LEVEL1 / ETC (unused if title_enabled==1)
	sint32	ondeath_demo_mode;		// What to do on death in demo mode EXIT_TO_TITLE / STARTDEMO
	sint32	ondeath_ingame;			// What to do on death in a level EXIT_TO_TITLE
	sint32	noinput_time;			// Used on title page (and in game/demos if noinput_timeout==1)
	sint32	on_demo_interrupt;
	sint32	on_demo_end;
	sint32	unused1[9];

	// 24 x sint16	(12)
	sint16	num_levels;				// Game levels total inc GYM
	sint16	num_picfiles;			// Pics
	sint16	num_titlefiles;			// Title files!
	sint16	num_fmvfiles;
	sint16	num_cutfiles;
	sint16	num_demos;
	sint16	title_track;
	sint16	singlelevel;			// Single playable level number for demos
	uint16	unused2[16];

	// 4 x uint16 for bits	(2)
	uint16	demoversion:1;
	uint16	title_disabled:1;			// Disable title page (used for demos)
	uint16	cheatmodecheck_disabled:1;	// Retail version of cheat
	uint16	noinput_timeout:1;			// Timeout if no input!
	uint16	loadsave_disabled:1;
	uint16	screensizing_disabled:1;
	uint16	lockout_optionring:1;			// Lock out main ring
	uint16	dozy_cheat_enabled:1;		// Allow simple cheat input
	uint16	cyphered_strings:1;
	uint16	gym_enabled:1;
	uint16	play_any_level:1;		// Requester on middle pages of passport
	uint16	cheat_enable:1;			// 'C' key fly, get weapons cheat
	uint16	securitytag:1;
	uint16	unused3[3];

	// 8 x char	(2)
	uint8	cypher_code;
	uint8	language;
	uint8	secret_track;
	char	pads[5];
}GAMEFLOW_INFO;

#ifdef PSX_VERSION
typedef struct {
		        int startframe;						// fmv start frame
				int endframe;						// fmv end frame
} FMV_INFO;
#endif

/************ Externs ******************/
extern GAMEFLOW_INFO gameflow;
extern char game_description[];
extern sint16 *GF_LevelSequences[];
extern int	num_strings;
extern char allstrings[][64];
extern char tempstrings[][64];

/* Variables */
extern int GF_num_levels;
extern int GF_num_picfiles;
extern int GF_num_titlefiles;
extern int GF_num_fmvfiles;
extern int GF_num_cutfiles;
extern int GF_title_track;
extern int GF_sunset_enable;

extern int GF_title_enabled;
extern int GF_title_replace;
extern int GF_firstOption ;
extern int GF_single_playable;
extern int GF_cheatmodecheck;
extern int GF_noinput_timeout;
extern int GF_noinput_time;
extern int GF_ondeath_demo_mode;
extern int GF_ondeath_ingame;
extern int GF_enable_loadsave_keys;
extern int GF_screen_sizing_keys;
extern int GF_enable_inv_ringchange;
extern int GF_allow_dozy_cheat;

extern sint16 *GF_frontendSequence;
/* Arrays */
extern sint16 *GF_level_sequence_list[];
extern sint16 *GF_demo_sequence_list[];
extern char *GF_picfilenames[];
extern char *GF_titlefilenames[];
extern char *GF_fmvfilenames[];
extern char	*GF_levelfilenames[];
extern char	*GF_cutscenefilenames[];
extern char	*GF_demofilenames[];
extern char	GF_secret_totals[];

/* Game Strings & Inventory Items */
extern char *LevelName_Strings[];
extern char *game_strings[];
extern char *pc_strings[];
extern char *psx_strings[];
extern char *puzzle1_strings[];
extern char *puzzle2_strings[];
extern char *puzzle3_strings[];
extern char *puzzle4_strings[];
extern char *secret1_strings[];
extern char *secret2_strings[];
extern char *secret3_strings[];
extern char *secret4_strings[];
extern char *special1_strings[];
extern char *special2_strings[];
extern char *pickup1_strings[];
extern char *pickup2_strings[];
extern char *key1_strings[];
extern char *key2_strings[];
extern char *key3_strings[];
extern char *key4_strings[];


extern sint16 GF_valid_demos[];
char	*GF_loadpicfilenames[];

#ifdef PC_VERSION
extern sint16 level_music[];
#endif
#ifdef PSX_VERSION
FMV_INFO	GF_fmv_data[];
#endif

extern char game_name[];
extern char filenames[][256];

extern char stringin_fname[];
extern char stringout_fname[];
/********** Prototypes *****************/
int		GF_LoadScriptFile(char *fname);

#define GF_SEQRET_NONCOMMAND -1
#define	GF_SEQRET_QUIT		 -2
#define GF_SEQRET_LOAD		 -3

sint32	GF_DoFrontEndSequence( void );
sint32	GF_DoLevelSequence( int level_number, int type );

int	ReadScript(char *fname, FILE *log);
void	build_ext( char *fname, char *ext );

/********** Sequences *****************/
extern sint16 GF_frontend[];




#endif
