#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "gameflow.h"

/*   M a c r o s   */
#define IF_CMD(s) if (strcmp(cmd,s)==0)
#define ELIF_CMD(s) else if (strcmp(cmd,s)==0)
#define IF_OPTION(s) if (strncmp(string,s,strlen(s))==0)
#define ELIF_OPTION(s) else if (strncmp(string,s,strlen(s))==0)
#define IF_STRING(s) if (strcmp(string,s)==0)
#define ELIF_STRING(s) else if (strcmp(string,s)==0)

void conkanji(char **sptr, char **dptr);
void get_description(char *buf);
int parse_line(char *line, char **cmd, char **string, FILE *logfile);
int get_sequence(FILE *file, sint16 *seq_buf, FILE *logfile, int level, int type);
void get_titlefiles(FILE *file, FILE *logfile);
char *add_string(char *string);
char *add_fname(char *fname);
int ReadGameStrings(FILE *logfile, char *fname);
int get_options(FILE *file, FILE *logfile);
int GetOption(char *string);

char buf[300],line[300];
char outname[256]={"data\\out.dat"};
int	number_fmv = 0;
int number_fnames = 0;
int number_levels = 1;
int	num_strings=0;
int	number_cuts=0;

int number_demos=0;
int	number_title=0;
int	number_pics=0;

int	num_game_strings=0;
int	num_pc_strings=0;
int	num_psx_strings=0;

int	ReadScript(char *fname, FILE *logfile)
{
	FILE *file;
	char *cmd, *string;
	int i;


	if (!(file=fopen(fname,"r")))
	{
		printf("\nCould not open script file '%s'\n\n",fname);
		return(0);
	}
	printf("Creating script...\n\n");
	fprintf(logfile,"Script;-\n");

	while(fgets(buf,256,file))
	{
		if (parse_line(buf, &cmd, &string, logfile))
		{
			IF_CMD("DESCRIPTION")
			{
				printf("'%s'\n",string);
				fprintf(logfile,"DESCRIPTION\n");
				strcpy(game_description,string);
			}
			ELIF_CMD("OPTIONS")
			{
				printf("OPTIONS\n\n");
				fprintf(logfile,"	OPTIONS\n");
				get_options(file,logfile);
			}
			ELIF_CMD("TITLE")
			{
				printf("TITLE FILES\n\n");
				fprintf(logfile,"	TITLE_FILES\n");
				get_titlefiles(file, logfile);
			}
			ELIF_CMD("FRONTEND")
			{
				printf("FRONTEND SEQUENCE\n\n");
				fprintf(logfile,"	FRONTEND SEQUENCE\n");
				get_sequence(file,GF_frontend, logfile, 0, 2);
			}
			ELIF_CMD("GYM")
			{
				printf("GYM: %s\n", string);
				fprintf(logfile,"	GYM\n");
				
				LevelName_Strings[0] = add_string(string);
				GF_level_sequence_list[0] = (sint16*)malloc(MAX_SEQ_SIZE*sizeof(sint16));
				get_sequence(file,GF_level_sequence_list[0], logfile, 0, 1);
				gameflow.gym_enabled = 1;
				printf("\n");
			}
			ELIF_CMD("LEVEL")
			{
				printf("LEVEL %d: %s\n", number_levels,string);
				fprintf(logfile,"	LEVEL\n");
				
				LevelName_Strings[number_levels] = add_string(string);
				GF_level_sequence_list[number_levels] = (sint16*)malloc(MAX_SEQ_SIZE*sizeof(sint16));
				get_sequence(file,GF_level_sequence_list[number_levels], logfile, number_levels, 0);
				number_levels++;
				printf("\n");
			}
			ELIF_CMD("DEMOLEVEL")
			{
				printf("DEMOLEVEL %d: %s\n", number_demos,string);
				fprintf(logfile,"	LEVEL\n");
				
				LevelName_Strings[number_levels] = add_string(string);
				GF_level_sequence_list[number_levels] = (sint16*)malloc(MAX_SEQ_SIZE*sizeof(sint16));
				get_sequence(file,GF_level_sequence_list[number_levels], logfile, number_levels, 0);
				GF_valid_demos[number_demos] = number_levels;
				number_levels++;
				number_demos++;
				printf("\n");
			}
			ELIF_CMD("GAMESTRINGS")
			{
				printf("Reading GameStrings...\n");
				ReadGameStrings(logfile,string);
			}
			else
			{
				printf("ERROR: Unknown command '%s %s'\n", cmd,string);
				fprintf(logfile,"ERROR: Unknown command '%s %s'\n", cmd,string);
			}
//			printf("CMD: '%s'\nSTR: '%s'\n",cmd,string);
		}
	}
	if (num_game_strings!=GT_NUM_GAMESTRINGS)
	{
		printf("ERROR: Incorrect number of game strings.\n");
		printf("       %d strings, there should be %d\n\n",num_game_strings, GT_NUM_GAMESTRINGS);
		for (i=0;i<num_game_strings;i++)
			printf("%d:	'%s'\n",i,game_strings[i]);
	    fprintf(logfile,"ERROR: Incorrect number of game strings.\n");
		fprintf(logfile,"       %d strings, there should be %d\n\n",num_game_strings, GT_NUM_GAMESTRINGS);
		for (i=0;i<num_game_strings;i++)
			fprintf(logfile,"%d:	'%s'\n",i,game_strings[i]);
		return(0);
	}
	if (num_pc_strings!=PCSTR_NUM_STRINGS)
	{
		printf("ERROR: Incorrect number of pc strings.\n");
		printf("       %d strings, there should be %d\n\n",num_pc_strings, PCSTR_NUM_STRINGS);
		for (i=0;i<num_pc_strings;i++)
			printf("%d:	'%s'\n",i,pc_strings[i]);
		fprintf(logfile,"ERROR: Incorrect number of pc strings.\n");
		fprintf(logfile,"       %d strings, there should be %d\n\n",num_pc_strings, PCSTR_NUM_STRINGS);
		for (i=0;i<num_pc_strings;i++)
			fprintf(logfile,"%d:	'%s'\n",i,pc_strings[i]);
		return(0);
	}
	if (num_psx_strings!=PSSTR_NUM_STRINGS)
	{
		printf("ERROR: Incorrect number of psx strings.\n");
		printf("       %d strings, there should be %d\n\n",num_psx_strings, PSSTR_NUM_STRINGS);
		for (i=0;i<num_psx_strings;i++)
			printf("%d:	'%s'\n",i,psx_strings[i]);
		fprintf(logfile,"ERROR: Incorrect number of psx strings.\n");
		fprintf(logfile,"       %d strings, there should be %d\n\n",num_psx_strings, PSSTR_NUM_STRINGS);
		for (i=0;i<num_psx_strings;i++)
			fprintf(logfile,"%d:	'%s'\n",i,psx_strings[i]);
		return(0);
	}

	gameflow.num_levels = number_levels;
	gameflow.num_fmvfiles = number_fmv;
	gameflow.num_cutfiles = number_cuts;
	gameflow.num_picfiles = number_pics;
	gameflow.num_titlefiles = number_title;
	gameflow.num_demos = number_demos;

	printf("Script read successfully\n\n");
	fprintf(logfile,"Script read successfully\n\n");
//	printf("\nScript '%s' created.\n\n",outname);
	fclose(file);

	return(1);
}

void convert_kanji(char **string)
{
	char buf[256];
	char dbuf[256];
	char chr[8];
	char *ptr,*dptr;

	// Copy contents of string to new buffer
	strcpy(buf,*string);
	// First search for kanji start code '@'
	ptr = buf;
	dptr = dbuf;

	while(*ptr)
	{
		if (*ptr=='@')
			conkanji(&ptr,&dptr);
		else
			*dptr++ = *ptr++;
	}
	*dptr = 0;

	strcpy(*string,dbuf);
	return;
}

void conkanji(char **sptr, char **dptr)
{
	int c,n=0,pos=0;
	char *src, *str;
	uint16	*ptr;
	char buf[128];
	char chr[8];
	sint16 kanji[64];

	str = src = *sptr;

	pos++;

	printf("Found kanji string - '%s'\n", str);

	// kanji string is in format '!0,1,2,3,4'
	// convert to words
	while(str[pos]!='@') // convert until we find a '@' code which signals the end of a kanji sequence
	{
		c=0;
		while(str[pos] && str[pos]!=',' && str[pos]!='@')
		{
			chr[c++] = str[pos++];
		}
		if (str[pos]==',')
			pos++;
		chr[c]=0;
//		printf("'%s'\n", chr);
		kanji[n++] = atoi(chr);
	}
	if (str[pos]=='@')
		pos++;
	*sptr = &str[pos];

	str = *dptr;			// set 'str' to point to dest string
	*str++ = '@';			// make '@' signal start of kanji in game code
	ptr =(uint16*) str;		// word ptr to after '@'
	printf("Done = ");
	for (c=0;c<n;c++)
	{
		*ptr++ = ((kanji[c]<<4)+1)|0x8000;	// store kanji codes (0-311)as words! ( <<1+1|0x8000 prevents any 0byte codes )
		printf("%d ", kanji[c]);
	}
	*ptr++ = 0xffff;		// 0xffff signals end of string
	*dptr = (char*) ptr;
}


int parse_line(char *buf, char **cmd, char **string, FILE *logfile)
{
	int	len,line_len, i, j, ls, q, com, hascmd;

	// first remove remarks from line
	len = strlen(buf);
	j = q = com = 0;
	ls = -1;
	for (i=0;i<len;i++)
	{
		if (buf[i]=='/' && buf[i+1]=='*')
		{
			i++;
			com++;
			continue;
		}
		if (buf[i]=='*' && buf[i+1]=='/')
		{
			if (!com)
			{
				printf("\nERROR: '*/' without '/*'\n\n");
				fprintf(logfile,"\nERROR: '*/' without '/*'\n\n");
				return(0);
			}
			com--;
			i++;
			continue;
		}

		if ((buf[i]=='/' && buf[i+1]=='/') || buf[i]=='*' || buf[i]=='\n')
		{
			if (ls>=0)
				line[ls]=0;
			else
				line[j]=0;
			break;
		}
		
		if (!com)
		{
			if (buf[i]==' ' || buf[i]==9)
			{
				if (ls<0)
					ls = j;
			}
			else ls = -1;

			line[j] = buf[i];
			j++;
		}
	}

	// Now seperate into COMMAND: and STRING
	line_len = strlen(line);
	hascmd=0;
	for (len=0;len<256;len++)
	{
		if ( line[len]==':' && line[len+1]!=':')
		{
			hascmd=1;
			break;
		}
		else if (line[len]==0)
			break;
		else if (line[len]==':' && line[len+1]==':')
		{
			for( i=len;i<256;i++ )
			{
				line[i] = line[i+1];
				if (line[i]==0)
					break;
			}
		}
	}

   	if (len)
   	{
   		j=0;
   		while(line[j]==9 || line[j]==' ') // remove any TABs and SPACES before COMMAND or STRING
   			j++;

		*cmd = &line[j];					// Set cmd ptr to COMMAND

		if (!hascmd)
		{
			string = cmd;					// if no COMMAND then set string ptr to COMMAND
	 		convert_kanji(string);
			return(1);						// And return
		}
   		for (i=j;i<len;i++)					// Convert COMMAND to upper case
   			line[i] = toupper(line[i]);
   		line[len] = 0;

   		i=len+1;
   		while(line[i]==9 || line[i]==' ')	// Remove any TABs and SPACES between COMMAND and STRING
   			i++;
		if (line[i]=='"' || line[i]==39)	// String can have ' or " around them so remove them
		{
			i++;
			len = strlen(&line[i]);
			if (line[i+len-1]=='"' || line[i+len-1]==39)
				line[i+len-1] = 0;
		}

		*string = &line[i];					// Set string ptr to string

	 	convert_kanji(string);

		if (*cmd==0)
			return(0);						// Return 0 if no command
		return(1);
   	}
	return(0);
}

int	GetLanguage(char *string)
{
	int i;

	for (i=0;i<strlen(string);i++)
		string[i] = toupper(string[i]);

	if (strcmp("ENGLISH",string)==0)
		return(0);
	if (strcmp("FRENCH",string)==0)
		return(1);
	if (strcmp("GERMAN",string)==0)
		return(2);
	if (strcmp("AMERICAN",string)==0)
		return(3);
	if (strcmp("JAPANESE",string)==0)
		return(4);
	
}

int get_options(FILE *file, FILE *logfile)
{
	char *cmd, *string;

	while(fgets(buf,256,file))
	{
		if (parse_line(buf, &cmd, &string,logfile))
		{
			IF_CMD("LANGUAGE")
				gameflow.language = GetLanguage(string);
			ELIF_CMD("SECRET_TRACK")
				gameflow.secret_track = atoi(string);
			ELIF_CMD("FIRSTOPTION")
				gameflow.firstOption = GetOption(string);
			ELIF_CMD("TITLE_REPLACE")
				gameflow.title_replace = GetOption(string);
			ELIF_CMD("ONDEATH_DEMO_MODE")
				gameflow.ondeath_demo_mode = GetOption(string);
			ELIF_CMD("ONDEATH_INGAME")
				gameflow.ondeath_ingame = GetOption(string);
			ELIF_CMD("NOINPUT_TIME")
				gameflow.noinput_time = atoi(string);
			ELIF_CMD("ON_DEMO_INTERRUPT")
				gameflow.on_demo_interrupt = GetOption(string);
			ELIF_CMD("ON_DEMO_END")
				gameflow.on_demo_end = GetOption(string);
			ELIF_CMD("SINGLELEVEL")
				gameflow.singlelevel = atoi(string);
			ELIF_CMD("DEMOVERSION")
				gameflow.demoversion = 1;
			ELIF_CMD("TITLE_DISABLED")
				gameflow.title_disabled = 1;
			ELIF_CMD("CHEATMODECHECK_DISABLED")
				gameflow.cheatmodecheck_disabled = 1;
			ELIF_CMD("NOINPUT_TIMEOUT")
				gameflow.noinput_timeout = 1;
			ELIF_CMD("LOADSAVE_DISABLED")
				gameflow.loadsave_disabled = 1;
			ELIF_CMD("SCREENSIZING_DISABLED")
				gameflow.screensizing_disabled = 1;
			ELIF_CMD("LOCKOUT_OPTIONRING")
				gameflow.lockout_optionring = 1;
			ELIF_CMD("DOZY_CHEAT_ENABLED")
				gameflow.dozy_cheat_enabled = 1;
			ELIF_CMD("CYPHER_CODE")
			{
				gameflow.cyphered_strings = 1;
				gameflow.cypher_code = atoi(string);
			}
			ELIF_CMD("SELECT_ANY_LEVEL")
			{
				gameflow.play_any_level = 1;
			}
			ELIF_CMD("ENABLE_CHEAT_CODE")
			{
				gameflow.cheat_enable = 1;
			}
			ELIF_CMD("END")
				return(1);
			else
			{
				printf("ERROR: Unknown option '%s'\n",cmd);
				fprintf(logfile,"ERROR: Unknown option '%s'\n",cmd);
			}
		}
	}
	return(1);
}

int GetOption(char *string)
{
	IF_OPTION("EXIT_TO_TITLE")
		return(EXIT_TO_TITLE);
	ELIF_OPTION("LEVEL")
		return(STARTGAME|atoi(string+5));
	ELIF_OPTION("DEMO")
		return(STARTDEMO|atoi(string+4));
	ELIF_OPTION("SEQUENCE")
		return(STARTGAME|atoi(string+8));
	ELIF_OPTION("EXITGAME")
		return(EXITGAME);
}

int	GetInvItem(char *string)
{
	int	i;

	for (i=0;i<INV_NUMBEROF;i++)
	{
		IF_STRING(inv_types[i])
			return(i);
	}
	return(-1);
}



int get_sequence(FILE *file, sint16 *seq_buf, FILE *logfile, int level, int type)
{
	char *cmd, *string, newstring[64];
	int	num, haslevel=0;

	while(fgets(buf,256,file))
	{
		if (parse_line(buf, &cmd, &string,logfile))
		{
			IF_CMD("FMV")
			{
#ifdef PC_VERSION
				build_ext(string,"RPL");
#else
				build_ext(string,"FMV");
				sprintf(newstring,"\\%s", string);
				string = newstring;
#endif
				for (num=0;num<strlen(string);num++)
					string[num] = toupper(string[num]);
				printf("FMV: '%s'\n",string);
				seq_buf[0] = GFE_PLAYFMV;
				seq_buf[1] = number_fmv;
				GF_fmvfilenames[number_fmv] = add_fname(string);
				number_fmv++;
				seq_buf+=2;
			}
			ELIF_CMD("PCFMV")
			{
#ifdef PC_VERSION
				for (num=0;num<strlen(string);num++)
					string[num] = toupper(string[num]);
				printf("FMV: '%s'\n",string);
				seq_buf[0] = GFE_PLAYFMV;
				seq_buf[1] = number_fmv;
				GF_fmvfilenames[number_fmv] = add_fname(string);
				number_fmv++;
				seq_buf+=2;
#endif
			}
			ELIF_CMD("PSXFMV")
			{
#ifdef PSX_VERSION
				sprintf(newstring,"\\%s", string);
				string = newstring;
				for (num=0;num<strlen(string);num++)
					string[num] = toupper(string[num]);
				printf("FMV: '%s'\n",string);
				seq_buf[0] = GFE_PLAYFMV;
				seq_buf[1] = number_fmv;
				GF_fmvfilenames[number_fmv] = add_fname(string);
				number_fmv++;
				seq_buf+=2;
#endif
			}
			ELIF_CMD("FMV_START")
			{
#ifdef PSX_VERSION
				num = atoi(string);
				printf("FMV_START: %d\n",num);
				GF_fmv_data[number_fmv].startframe = num;
#endif
			}
			ELIF_CMD("FMV_END")
			{
#ifdef PSX_VERSION
				num = atoi(string);
				printf("FMV_END: %d\n",num);
				GF_fmv_data[number_fmv].endframe = num;
#endif
			}
			ELIF_CMD("GAME")
			{
#ifdef PC_VERSION
				build_ext(string,"TR2");
#else
				build_ext(string,"PSX");
#endif
				printf("GAME: '%s'\n",string);
				seq_buf[0] = GFE_STARTLEVEL;
				seq_buf[1] = level;
				GF_levelfilenames[level] = add_fname(string);
				seq_buf+=2;
				haslevel=1;
			}
			ELIF_CMD("DEMO")
			{
				printf("DEMO: '%s'\n",string);
				seq_buf[0] = GFE_DEMOPLAY;
				seq_buf[1] = level;
				GF_levelfilenames[level] = add_fname(string);
				seq_buf+=2;
				haslevel=1;
			}
			ELIF_CMD("PCDEMO")
			{
#ifdef PC_VERSION
				printf("PCDEMO: '%s'\n",string);
				seq_buf[0] = GFE_DEMOPLAY;
				seq_buf[1] = level;
				GF_levelfilenames[level] = add_fname(string);
				seq_buf+=2;
				haslevel=1;
#endif
			}
			ELIF_CMD("PSXDEMO")
			{
#ifdef PSX_VERSION
				printf("PSXDEMO: '%s'\n",string);
				seq_buf[0] = GFE_DEMOPLAY;
				seq_buf[1] = level;
				GF_levelfilenames[level] = add_fname(string);
				seq_buf+=2;
				haslevel=1;
#endif
			}
			ELIF_CMD("CUTANGLE")
			{
				num = atoi(string);
				seq_buf[0] = GFE_CUTANGLE;
				seq_buf[1] = num;
				seq_buf+=2;
			}
			ELIF_CMD("CUT")
			{
#ifdef PC_VERSION
				build_ext(string,"TR2");
#else
				build_ext(string,"PSX");
#endif
				printf("CUT: '%s'\n",string);
				seq_buf[0] = GFE_CUTSCENE;
				seq_buf[1] = number_cuts;
				GF_cutscenefilenames[number_cuts] = add_fname(string);
				number_cuts++;
				seq_buf+=2;
			}
			ELIF_CMD("LOAD_PIC")
			{
#ifdef PSX_VERSION
				printf("LOAD PIC: '%s'\n",string);
				seq_buf[0] = GFE_LOADINGPIC;
				seq_buf[1] = number_pics;
				GF_picfilenames[number_pics] = add_fname(string);
				number_pics++;
				seq_buf+=2;
#endif
			}
			ELIF_CMD("PICTURE")
			{
				printf("PICTURE: '%s'\n",string);
				seq_buf[0] = GFE_PICTURE;
				seq_buf[1] = number_pics;
				GF_picfilenames[number_pics] = add_fname(string);
				number_pics++;
				seq_buf+=2;
			}
			ELIF_CMD("COMPLETE")
			{
				printf("COMPLETE\n");
				seq_buf[0] = GFE_LEVCOMPLETE;
				seq_buf++;
			}
			ELIF_CMD("GAMECOMPLETE")
			{
				printf("GAMECOMPLETE\n");
				seq_buf[0] = GFE_GAMECOMPLETE;
				seq_buf++;
			}
			ELIF_CMD("TRACK")
			{
				num = atoi(string);
				printf("TRACK:	%d\n",num);
				seq_buf[0] = GFE_SETTRACK;
				seq_buf[1] = num;
				seq_buf+=2;
			}
			ELIF_CMD("PCTRACK")
			{
#ifdef PC_VERSION
				num = atoi(string);
				printf("TRACK:	%d\n",num);
				seq_buf[0] = GFE_SETTRACK;
				seq_buf[1] = num;
				seq_buf+=2;
#endif
			}
			ELIF_CMD("PSXTRACK")
			{
#ifdef PSX_VERSION
				num = atoi(string);
				printf("TRACK:	%d\n",num);
				seq_buf[0] = GFE_SETTRACK;
				seq_buf[1] = num;
				seq_buf+=2;
#endif
			}
			ELIF_CMD("SUNSET")
			{
				printf("SUNSET ENABLED\n");
				seq_buf[0] = GFE_SUNSET;
				seq_buf++;
			}
			ELIF_CMD("NOFLOOR")
			{
				printf("NO FLOOR ENABLED\n");
				seq_buf[0] = GFE_NOFLOOR;
				seq_buf[1] = atoi(string);
				seq_buf+=2;
			}
			ELIF_CMD("STARTANIM")
			{
				printf("LARA START ANIM:	%d\n");
				seq_buf[0] = GFE_STARTANIM;
				seq_buf[1] = atoi(string);
				seq_buf+=2;
			}
			ELIF_CMD("SECRETS")
			{
				printf("LEVEL SECRETS:	%d\n");
				seq_buf[0] = GFE_NUMSECRETS;
				seq_buf[1] = atoi(string);
				seq_buf+=2;
			}
			ELIF_CMD("DEADLY_WATER")
			{
				printf("DEADLY WATER\n");
				seq_buf[0] = GFE_DEADLY_WATER;
				seq_buf++;
			}
			ELIF_CMD("REMOVE_WEAPONS")
			{
				printf("REMOVE WEAPONS\n");
				seq_buf[0] = GFE_REMOVE_WEAPONS;
				seq_buf++;
			}
			ELIF_CMD("REMOVE_AMMO")
			{
				printf("REMOVE AMMO\n");
				seq_buf[0] = GFE_REMOVE_AMMO;
				seq_buf++;
			}
			ELIF_CMD("KILLTOCOMPLETE")
			{
				printf("KILL TO COMPLETE\n");
				seq_buf[0] = GFE_KILL2COMPLETE;
				seq_buf++;
			}
			ELIF_CMD("BONUS")
			{
				num = GetInvItem(string);
				if (num<0)
				{
					printf("*********************************************\n");
					printf("ERROR: Could not match '%s' with inv items\n",string);
					printf("*********************************************\n");
				}
				else
				{
					printf("SECRET BONUS: %s\n", string);
					seq_buf[0] = GFE_ADD2INV;
					seq_buf[1] = num;
					seq_buf+=2;
				}
			}
			ELIF_CMD("STARTINV")
			{
				num = GetInvItem(string);
				if (num<0)
				{
					printf("*********************************************\n");
					printf("ERROR: Could not match '%s' with inv items\n",string);
					printf("*********************************************\n");
				}
				else
				{
					printf("START INVENTORY: %s\n", string);
					seq_buf[0] = GFE_ADD2INV;
					seq_buf[1] = num+1000;
					seq_buf+=2;
				}
			}
			ELIF_CMD("PUZZLE1")
			{
				if (type!=2)
					puzzle1_strings[level] = add_string(string);
			}
			ELIF_CMD("PUZZLE2")
			{
				if (type!=2)
					puzzle2_strings[level] = add_string(string);
			}
			ELIF_CMD("PUZZLE3")
			{
				if (type!=2)
					puzzle3_strings[level] = add_string(string);
			}
			ELIF_CMD("PUZZLE4")
			{
				if (type!=2)
					puzzle4_strings[level] = add_string(string);
			}
			ELIF_CMD("SECRET1")
			{
				if (type!=2)
					secret1_strings[level] = add_string(string);
			}
			ELIF_CMD("SECRET2")
			{
				if (type!=2)
					secret2_strings[level] = add_string(string);
			}
			ELIF_CMD("SECRET3")
			{
				if (type!=2)
					secret3_strings[level] = add_string(string);
			}
			ELIF_CMD("SECRET4")
			{
				if (type!=2)
					secret4_strings[level] = add_string(string);
			}
			ELIF_CMD("SPECIAL1")
			{
				if (type!=2)
					special1_strings[level] = add_string(string);
			}
			ELIF_CMD("SPECIAL2")
			{
				if (type!=2)
					special2_strings[level] = add_string(string);
			}
			ELIF_CMD("PICKUP1")
			{
				if (type!=2)
					pickup1_strings[level] = add_string(string);
			}
			ELIF_CMD("PICKUP2")
			{
				if (type!=2)
					pickup2_strings[level] = add_string(string);
			}
			ELIF_CMD("KEY1")
			{
				if (type!=2)
					key1_strings[level] = add_string(string);
			}
			ELIF_CMD("KEY2")
			{
				if (type!=2)
					key2_strings[level] = add_string(string);
			}
			ELIF_CMD("KEY3")
			{
				if (type!=2)
					key3_strings[level] = add_string(string);
			}
			ELIF_CMD("KEY4")
			{
				if (type!=2)
					key4_strings[level] = add_string(string);
			}
			ELIF_CMD("END")
			{
				seq_buf[0] = GFE_END_SEQ;
//				if (type==0)
//					number_levels++;
				return(1);
			}
			else
			{
				printf("ERROR: Unkown command '%s %s'\n",cmd,string);
				fprintf(logfile,"ERROR: Unkown command '%s %s'\n",cmd,string);
			}
		}
	}
	printf("WARNING: EOF before end of sequence\n\n");
	fprintf(logfile,"WARNING: EOF before end of sequence\n\n");
	return(0);
}
void get_titlefiles(FILE *file, FILE *logfile)
{
	int	num;
	char *cmd, *string;

	while(fgets(buf,256,file))
	{
		if (parse_line(buf, &cmd, &string, logfile))
		{
			IF_CMD("GAME")
			{
#ifdef PC_VERSION
				build_ext(string,"TR2");
#else
				build_ext(string,"PSX");
#endif
				GF_titlefilenames[number_title] = add_fname(string);
				number_title++;
			}
			ELIF_CMD("PCFILE")
			{
#ifdef PC_VERSION
				GF_titlefilenames[number_title] = add_fname(string);
				number_title++;
#endif
			}
			ELIF_CMD("PSXFILE")
			{
#ifdef PSX_VERSION
				GF_titlefilenames[number_title] = add_fname(string);
				number_title++;
#endif
			}
			ELIF_CMD("TRACK")
			{
				num = atoi(string);
				printf("Title Track: %d\n",num);
				gameflow.title_track = num;
			}
			ELIF_CMD("PCTRACK")
			{
#ifdef PC_VERSION
				num = atoi(string);
				printf("Title Track: %d\n",num);
				gameflow.title_track = num;
#endif
			}
			ELIF_CMD("PSXTRACK")
			{
#ifdef PSX_VERSION
				num = atoi(string);
				printf("Title Track: %d\n",num);
				gameflow.title_track = num;
#endif
			}
			ELIF_CMD("END")
				return;
			else
			{
				printf("ERROR: Unknown command '%s %s'\n",cmd,string);
				fprintf(logfile,"ERROR: Unknown command '%s %s'\n",cmd,string);
			}
		}
	}
	printf("WARNING: EOF before end of title file list\n\n");
	fprintf(logfile,"WARNING: EOF before end of title file list\n\n");
}

int ReadGameStrings(FILE *logfile, char *fname)
{
	FILE *strfile;
	int j;
	int set_if=0;
	char *cmd, *string;

	if (!(strfile=fopen(fname,"r")))
	{
		printf("ERROR: Could not open string file '%s'\n\n", stringin_fname);
		return(0);
	}

	while(fgets(buf,256,strfile))
	{
		if (parse_line(buf, &cmd, &string, logfile))
		{
			IF_CMD("GAME_STRINGS")
			{
				j=0;
				while( fgets(buf,256,strfile) )
				{
					if (parse_line(buf, &cmd, &string, logfile))
					{
						IF_CMD("END")
							break;
						else
						{
						 	game_strings[j] = add_string(cmd);
						 	j++;
						}
					}
				}
				num_game_strings = j;
			}
			ELIF_CMD("PC_STRINGS")
			{
				j=0;
				while( fgets(buf,256,strfile) )
				{
					if (parse_line(buf, &cmd, &string, logfile))
					{
						IF_CMD("END")
							break;
						else
						{
						 	pc_strings[j] = add_string(cmd);
						 	j++;
						}
					}
				}
				num_pc_strings = j;
			}
			ELIF_CMD("PSX_STRINGS")
			{
				j=0;
				while( fgets(buf,256,strfile) )
				{
					if (parse_line(buf, &cmd, &string, logfile))
					{
						IF_CMD("END")
							break;
						else
						{
						 	psx_strings[j] = add_string(cmd);
						 	j++;
						}
					}
				}
				num_psx_strings = j;
			}
			ELIF_CMD("END")
			{
				fclose(strfile);
				return(1);
			}
		}
	}
	return(0);
}
char *add_string(char *string)
{
	strcpy(allstrings[num_strings],string);
	num_strings++;
	return(allstrings[num_strings-1]);
}

char *add_fname(char *fname)
{
	strcpy(filenames[number_fnames],fname);
	number_fnames++;
	return(filenames[number_fnames-1]);
}

