/*

  Options & validity tests
  Demo Sequences
  No Level Complete option
  Exit_to_title in sequences
  Cheat codes

*/



enum title_options {
		STARTGAME 		= 0,
		STARTSAVEDGAME 	= 1<<8,
		STARTCINE 		= 2<<8,
		STARTFMV 		= 3<<8,
		STARTDEMO 		= 4<<8,
		EXIT_TO_TITLE   = 5<<8,
		LEVELCOMPLETE	= 6<<8,
		EXITGAME 		= 7<<8,
		EXIT_TO_OPTION	= 8<<8,
		TITLE_DESELECT	= 9<<8
};



enum gf_picfile_defs{
	PIC_EIDOS,
	NUM_PICS
};
enum gf_titfile_defs{
	TITLE_WAD=0,
	NUM_TITLE
};
enum gf_fmvfile_defs{
	FMV_CORE=0,
	FMV_ESCAPE,
	FMV_ASSAULT,
	FMV_WALLOLD,
	FMV_WALLPRESENT,
	FMV_RIG,
	FMV_MINISUB,
	FMV_SEAPLANE,
	FMV_JEEP,
	FMV_ENDSEQ,
	NUM_FMV
};
enum gf_levfile_defs{
			ASSAULT=0,
			TEST,
			THEWALL,
			THEBOAT,
			VENICE,
			OPERA,
			RIG,
			UNWATER,
			KEEL,
			LIVING,
			DECK,
			SKIDOO_L,
			MONASTRY,
			CATACOMB,
			ICECAVE,
			EMPRTOMB,
			FLOATING,
			HOUSE,
			NUM_LEVELS
};

enum gf_cutfile_defs{
		CUT_CULTCAMP_1=0,
		CUT_CARGOHOLD_2,
		CUT_RIG_3,
		CUT_RITUAL_4,
		NUM_CUTS
};

enum gf_demfile_defs{
		DEM_1=0,
		NUM_DEMOS
};


extern	char	*title_option_strings[];
extern	char	*onoff_strings[];
extern	char	*enadis_strings[];
extern	char	*inv_types[];


/***************************************************************
		O U T P U T   F O R M A T
****************************************************************

uint32			1x					Version
GAMEFLOW_INFO	1x					GameFlow structure
(STRINGS)		NUM_LEVELS			Level Names
(STRINGS)		NUM_PICS			Picture Filenames





***************************************************************/
