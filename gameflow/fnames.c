#include "gameflow.h"

char filenames[256][256];

#ifdef PC_VERSION

/****************************************************************************************************/
/*		P C   V E R S I O N																		*/
/****************************************************************************************************/

char *GF_picfilenames[MAX_PICS]={   
	{"data\\eidospc"},
};

char *GF_titlefilenames[MAX_TITLE]={
		"data\\titleh.pcx",
		"data\\title.tr2",
};
char *GF_fmvfilenames[MAX_FMV]={
		"fmv\\corelogo.rpl",
		"fmv\\escape.rpl",
		"fmv\\home.rpl",
		"fmv\\wallold.rpl",
		"fmv\\wallpres.rpl",
		"fmv\\rig.rpl",
		"fmv\\minisub.rpl",
		"fmv\\seaplane.rpl",
		"fmv\\jeep.rpl",
		"fmv\\fendseq.rpl",
};
char	*GF_levelfilenames[MAX_LEVELS] = {
			"data\\assault.tr2",
			"data\\test.tr2",
			"data\\wall.tr2",
			"data\\boat.tr2",
			"data\\venice.tr2",
			"data\\opera.tr2",
			"data\\rig.tr2",
			"data\\unwater.tr2",
			"data\\keel.tr2",
			"data\\living.tr2",
			"data\\dock.tr2",
			"data\\skidoo.tr2",
			"data\\monastry.tr2",
			"data\\catacomb.tr2",
			"data\\icecave.tr2",
			"data\\emprtomb.tr2",
			"data\\floating.tr2",
			"data\\house.tr2",
};

char	*GF_cutscenefilenames[MAX_CUTS] = {
		"data\\cut1.cut",
		"data\\cut2.cut",
		"data\\cut3.cut",
		"data\\cut4.cut"
};

char	*GF_demofilenames[MAX_DEMOS] = {
		"data\\venice.dem"
};


#else

/****************************************************************************************************/
/*		P S X   V E R S I O N																		*/
/****************************************************************************************************/

char *GF_picfilenames[MAX_PICS]={   
	{"data\\eidospc.tga"},
};

char *GF_titlefilenames[MAX_TITLE]={
		"data\\titleh.pcx",
		"data\\title.psx",
};
char *GF_fmvfilenames[MAX_FMV]={
		"fmv\\logo.fmv",
		"fmv\\escape.fmv",
		"fmv\\home.fmv",
		"fmv\\wallold.fmv",
		"fmv\\wallpres.fmv",
		"fmv\\rig.fmv",
		"fmv\\minisub.fmv",
		"fmv\\seaplane.fmv",
		"fmv\\jeep.fmv",
		"fmv\\fendseq.fmv"
};
char	*GF_levelfilenames[MAX_LEVELS] = {
			"data\\assault.psx",
			"data\\test.psx",
			"data\\wall.psx",
			"data\\boat.psx",
			"data\\venice.psx",
			"data\\opera.psx",
			"data\\rig.psx",
			"data\\unwater.psx",
			"data\\keel.psx",
			"data\\living.psx",
			"data\\deck.psx",
			"data\\skidoo.psx",
			"data\\monastry.psx",
			"data\\catacomb.psx",
			"data\\icecave.psx",
			"data\\emprtomb.psx",
			"data\\floating.psx",
			"data\\house.psx"
};

char	*GF_cutscenefilenames[MAX_CUTS] = {
		"data\\cut1.psx",
		"data\\cut2.psx",
		"data\\cut3.psx",
		"data\\cut4.psx"
};

char	*GF_demofilenames[MAX_DEMOS] = {
		"data\\venice.psx"
};


#endif