#include "gameflow.h"

char allstrings[1000][64];
char tempstrings[1000][64];

#ifdef PC_VERSION
char game_description[256]={"Tomb Raider II PC Internal Development Version (c) Core Design Ltd 1997"};
#else
char game_description[256]={"Tomb Raider II PSX Internal Development Version (c) Core Design Ltd 1997"};
#endif


char *LevelName_Strings[MAX_LEVELS]={"Not Set"};
/* =
{
	"Assault Course",
	"Test Level",
	"The Wall",
	"The Boat",
	"Venice",
	"Opera",
	"Rig",
	"Underwater",
	"Keel",
	"Living Area",
	"Deck",
	"Skidoo",
	"Monastery",
	"Catacomb",
	"Ice Cave",
	"Emporer's Tomb",
	"Floating Islands",
	"House"
};
*/
char *pc_strings[1000];
char *psx_strings[1000];
char *game_strings[1000];
/*
	// Inventory Ring headings
	"INVENTORY",		// char inventory_main_heading[]
	"OPTION",			// char inventory_option_heading[]
	"ITEMS",			// char inventory_keys_heading[]
	"GAME OVER",		// char inventory_gameover_heading[]
//  Passport Text Section
	"Load Game",		// char passport_load_game[]
	"Save Game",		// char passport_save_game[]
	"New Game",			// char passport_start_game[]
	"Restart Level",	// char passport_restart_game[]
	"Exit to Title",	// char passport_exit2title[]
	"Exit Demo",		// char passport_exit2title_demo[]
	"Exit Game",		// char passport_exit_game[]

	"Select Level",		// char load_game_heading[] & select_level_heading[]
	"Save Position",	// char save_game_heading[]
//  Detail text 
	"Select Detail",	//char str_detail[] = {
	"High",				//char str_high[] =	{
	"Medium",			//char str_medium[] = {
	"Low",				//char str_low[] =	{
// Control strings
	"Walk",					//char str_slow[] 
	"Roll",					//char str_roll[]
	"Run",					//char str_run[]
	"Left",					//char str_left[] 
	"Right",				//char str_right[]
	"Back",					//char str_back[]
	"Step Left",			//char str_stepl[]
	"Step Right",			//char str_stepr[]
	"Look",					//char str_look[]
	"Jump",					//char str_jump[]
	"Action",				//char str_action[]
	"Draw Weapon",			//char str_draw[]
	"Inventory",			//char str_inv[]
	"Flare",				//char str_flare[]
// Inventory Item Strings
	"Stop Watch",          //char compass_name[]
	"Pistols",             //char pistols_name[]
	"Shotgun",             //char shotgun_name[]
	"Automatic Pistols",   //char magnum_name[]
	"Uzis",                //char uzi_name[]
	"Harpoon",             //char harpoon_name[]
	"M16",                 //char m16_name[]
	"Rocket Launcher",     //char rocket_name[]
	"Flare",               //char flare_name[]
	                        
	"Pistol Clips",        //char gun_ammo_name[]
	"Shotgun Shells",      //char sg_ammo_name[]
	"Automatic Pistol Clips",//char mag_ammo_name[]
	"Uzi Clips",           //char uzi_ammo_name[]
	"Harpoons",				//char harpoon_ammo_name[]
	"M16 Clips",			//char m16_ammo_name[]=	{"M16 Clips"};
	"Rockets",				//char rocket_ammo_name[]={"Rockets"};
							
	"Small Medi Pack",		//char medi_name[]
	"Large Medi Pack",		//char bigmedi_name[]
	"Lead Bar",				//char leadbar_name[]
	"Pickup",				//char pickup_name[]
	"Scion",				//char scion_name[]
	"Puzzle",				//char puzzle_name[]
	"Key",					//char key_name[]
	"Game",					//char game_name[]

	"Lara's Home",				// gym_name[]
	"LOADING",					// loading_string[]

// Stats text
	"TIME TAKEN",	//char str_timetaken[]
	"SECRETS",		//char str_secrets[]
	"PICKUPS",		//char str_pickups[]
	"KILLS",		//char str_kills[]
	"of",			//char str_of[]
#ifdef PC_VERSION
//	PC General Text
 	"Detail Levels",			// detail_name[]
	"Demo Mode",				// demomode_text[]
	"Sound",					// sound_name[]
	"Controls",					// control_name[]
	"Gamma",					// gamma_name[]
	"Set Volumes",				// char str_volume[]
	"User Keys",				// char str_heading[] = {	"User Keys"};
// SAVE GAME TEXT 
	"The file could not be saved!",	//char sav_mess1[]
	"Try Again?",					//char sav_mess2[]
	"YES",							//char sav_mess3[]
	"NO",							//char sav_mess4[]
	"Save Complete!",				//char sav_mess5[]
	"No save games!",				//char sav_mess6[]
	"None valid",					//char sav_mess7[]
	"Save Game?",					//char sav_mess8[]
	"- EMPTY SLOT -",				//char sav_slot[] 

											    
	// MACHINE SPECIFIC TEXT
	"OFF",						// char sa_off[] =			"OFF";
	"ON",						// char sa_on[] =			"ON";
	"Setup Sound Card",			// char sa_setup[] =		"Setup Sound Card";
	"Default Keys",				// char str_default[] = {	"Default Keys"};
#else
//	PSX General Text
	"Screen Adjust",				// detail_name[]
	"DEMO MODE",					// psx_demomode_text[]
	"Sound",						// sound_name[]
	"Controls",						// control_name[]
	"Gamma",						// gamma_name[]
	"Set Volumes",					// char str_volume[]
	"User Keys",				// char str_heading[] = {	"User Keys"};
// SAVE GAME TEXT 
	"The file could not be saved!",	//	char sav_mess1[]
	"Try Again?",					//	char sav_mess2[]
	"YES",							//	char sav_mess3[]
	"NO",							//	char sav_mess4[]
	"Save Complete!",				//	char sav_mess5[]
	"No save games!",				//	char sav_mess6[]
	"None valid",					//	char sav_mess7[]
	"Save Game?",					//	char sav_mess8[]
	"- EMPTY SLOT -",				//	char sav_slot[] 
// MACHINE SPECIFIC TEXT
	"Press any button to exit",		// psx_demo_text[] 
	"Use directional buttons",		// *move_strings[]
	"to adjust screen position",	// *move_strings[]
	"> Select",						// psx_select_text[]
	"\\ Go Back",					// psx_deselect_text[]
	"> Continue",					// psx_continue_text[]
	"Paused",						// psx_paused_text[]
	"No Controller",				// psx_nojoy_text[]
// Old GapString[] Text
	"Save game to",
	"Overwrite game on",
	"Load game from",
	"the memory card in",
	"memory card slot 1",
	"Are you sure ?",
	"Yes",
	"No",
	"is full",
	"There are no games on",
	"There is a problem with",
	"There is no memory card in",
	"is unformatted",
	"Would you like to",
	"format it ?",
	"Saving game to",
	"Loading game from",
	"Formatting",
	"Overwrite game",
	"Save game",
	"Load game",
	"Format memory card",
	"Exit",
	"Continue",
	"You will not be able",
	"to save any games unless",
	"you insert a formatted",
	"memory card with at least",
	"one free block",
	"The memory card in",
	"Exit without saving",
	"Exit without loading",
	"Start game",
//char GacSaveGameFileName_pal[]=
	"bu00:BESLES-00024TOMBRAID",		// English
//char GacSaveGameFileName_ntsc[]=
	"bu00:BASLUS-00152TOMBRAID",		// Yankers
#endif
};
*/



// Inventory item names
char *puzzle1_strings[MAX_LEVELS];
char *puzzle2_strings[MAX_LEVELS];
char *puzzle3_strings[MAX_LEVELS];
char *puzzle4_strings[MAX_LEVELS];
char *secret1_strings[MAX_LEVELS];
char *secret2_strings[MAX_LEVELS];
char *secret3_strings[MAX_LEVELS];
char *secret4_strings[MAX_LEVELS];
char *special1_strings[MAX_LEVELS];
char *special2_strings[MAX_LEVELS];
char *pickup1_strings[MAX_LEVELS];
char *pickup2_strings[MAX_LEVELS];
char *key1_strings[MAX_LEVELS];
char *key2_strings[MAX_LEVELS];
char *key3_strings[MAX_LEVELS];
char *key4_strings[MAX_LEVELS];

