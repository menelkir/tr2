# Microsoft Developer Studio Project File - Name="gameflow" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=gameflow - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "gameflow.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "gameflow.mak" CFG="gameflow - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "gameflow - Win32 Release" (based on\
 "Win32 (x86) Console Application")
!MESSAGE "gameflow - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "gameflow - Win32 PSX Version" (based on\
 "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""$/gameflow", YJAAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "PC_VERSION" /FR /YX /FD /c
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "PC_VERSION" /FR /YX /FD /c
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "gameflow"
# PROP BASE Intermediate_Dir "gameflow"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "psx"
# PROP Intermediate_Dir "psx"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "PC_VERSION" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "PSX_VERSION" /FR /YX /FD /c
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /subsystem:console /machine:I386 /out:"psx/psxgame.exe"

!ENDIF 

# Begin Target

# Name "gameflow - Win32 Release"
# Name "gameflow - Win32 Debug"
# Name "gameflow - Win32 PSX Version"
# Begin Group "scripts"

# PROP Default_Filter ""
# Begin Group "Demos"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\scripts\sonyshel.txt
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\tokyo.txt
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\USoffice.txt
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\USstrings.txt
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=.\scripts\Frfinal.txt

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\Frstr.txt

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\gerfinal.txt

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\gerstr.txt

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\itafinal.txt

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\itastr.txt

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\JapFinal.txt
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\jstrings.txt
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\PCfinal.txt

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\strings.txt

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# End Group
# Begin Group "output"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\scripts\frfinal.log

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\gerfinal.log

!IF  "$(CFG)" == "gameflow - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "gameflow - Win32 Debug"

!ELSEIF  "$(CFG)" == "gameflow - Win32 PSX Version"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scripts\japfinal.log
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\japfinal.str
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\pcfinal.log
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\pcfinal.str
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\tokyo.log
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\scripts\tokyo.str
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=.\fnames.c
# End Source File
# Begin Source File

SOURCE=.\Gameflow.h
# End Source File
# Begin Source File

SOURCE=.\Gf_data.c
# End Source File
# Begin Source File

SOURCE=.\gf_data.h
# End Source File
# Begin Source File

SOURCE=.\main.c
# End Source File
# Begin Source File

SOURCE=.\script.c
# End Source File
# Begin Source File

SOURCE=.\sequence.c
# End Source File
# Begin Source File

SOURCE=.\strings.c
# End Source File
# End Target
# End Project
