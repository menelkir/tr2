Description:	Tomb Raider II Script. Mag Preview (c) Core Design Ltd 1997
// Option Types
//	EXIT_TO_TITLE
//	LEVEL%d
//	DEMO%d
//	SEQUENCE%d
//	EXITGAME

Options:								// Defaults
Language:				ENGLISH			// ENGLISH
Secret_Track:			3				// 47
//todo enable_indoor_reverb:					// NO


cypher_code:			166				// 0xa6

firstOption:			LEVEL 1			// EXIT_TO_TITLE
title_replace:			EXITGAME			// NONE (-1)
ondeath_demo_mode:	LEV			// EXIT_TO_TITLE
ondeath_ingame:		LEVEL 1			// EXIT_TO_TITLE
//noinput_time:	 		150				// 900
//on_demo_interrupt:	LEVEL 1			// EXIT_TO_TITLE
//on_demo_end:			DEMO1			// EXIT_TO_TITLE
singlelevel:			1				// NONE (-1)
demoversion:			  			
title_disabled:
cheatmodecheck_disabled:
//noinput_timeout:
//loadsave_disabled:
//screensizing_disabled:
//lockout_optionring:
//dozy_cheat_enabled:

//select_any_level:
//todo enable_cheat_key:
//todo use_security_tag:

end:

Frontend:
//pcpicture:	pix\demopic1.pcx
end:

Level:		The Great Wall
track:		2
game:		data\wall.tr2
complete:
gamecomplete:
Key1:		Guardhouse Key
Key2:		Rusty Key
end:

GameStrings:	strings.txt
