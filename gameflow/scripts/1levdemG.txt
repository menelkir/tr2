Description:	Tomb Raider II Script. Demo 1.0 (c) Core Design Ltd 1997
// Option Types
//	EXIT_TO_TITLE
//	LEVEL%d
//	DEMO%d
//	SEQUENCE%d
//	EXITGAME


//------------------------------------------------------------------------
//		O P T I O N S
//------------------------------------------------------------------------

Options:								// Defaults
Language:				GERMAN			// ENGLISH
Secret_Track:			0				// 47
//enable_indoor_reverb:					// NO


cypher_code:			166

//firstOption:			LEVEL 1			// EXIT_TO_TITLE
//title_replace:		LEVEL 1			// NONE (-1)
ondeath_ingame:			EXIT_TO_TITLE
//noinput_time:	 		150				// 900
//on_demo_interrupt:	LEVEL 1			// EXIT_TO_TITLE
//on_demo_end:			DEMO1			// EXIT_TO_TITLE
singlelevel:			1				// NONE (-1)
demoversion:			  			
//title_disabled:
cheatmodecheck_disabled:
//noinput_timeout:
//loadsave_disabled:
//screensizing_disabled:
//lockout_optionring:
//dozy_cheat_enabled:

//select_any_level:
//enable_cheat_key:
//use_security_tag:

end:


//------------------------------------------------------------------------
//		T I T L E   S E T U P
//------------------------------------------------------------------------

Title:
Game:		data\title.tr2		// First file in 'Title' must always be title.tr2
PSXfile:	pixG\title.raw
PSXfile:	pixG\legal.raw
PSXfile:	pixUS\titleUS.raw
PSXfile:	pixUS\legalUS.raw
PSXfile:	pixJAP\titleJAP.raw
PSXfile:	pixJAP\legalJAP.raw
Track:		0
end:

//------------------------------------------------------------------------
//		F R O N T E N D
//------------------------------------------------------------------------

Frontend:

//fmv_start:	1
//fmv_end:	880
//fmv:		fmv\logo.rpl									//Logo

//fmv_start:	1
//fmv_end:	4370
//fmv:		fmv\ancient.rpl									//Intro

end:



//------------------------------------------------------------------------
//WALL		T H E   G R E A T   W A L L
//------------------------------------------------------------------------

Level:		Venedig

track:		0						//Ambient (Wind)
Load_Pic:	pixG\venice.raw
game:		data\boat.tr2					


complete:

// Object Names
Key1:		Bootshaus-Schl~ussel
Key2:		St~ahlerner Schl~ussel
Key3:		Eiserner Schl~ussel
end:

GameStrings:	gerstr.txt



