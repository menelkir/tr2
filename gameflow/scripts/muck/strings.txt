// PC & PSX VERSION OF GENERAL GAME STRINGS

GAME_STRINGS:

INVENTORY
OPTION
ITEMS
GAME OVER

Load Game
Save Game
New Game
Restart Level
Exit to Title
Exit Demo
Exit Game

Select Level
Save Position

Select Detail
High
Medium
Low

Walk
Roll
Run
Left
Right
Back
Step Left
?
Step Right
?
Look
Jump
Action
Draw Weapon
?
Inventory
Flare
Step

Statistics
Pistols
Shotgun
Automatic Pistols
Uzis
Harpoon Gun
M16
Grenade Launcher
Flare

Pistol Clips
Shotgun Shells
Automatic Pistol Clips
Uzi Clips
Harpoons
M16 Clips
Grenades

Small Medi Pack
Large Medi Pack
Pickup
Puzzle
Key
Game

Lara's Home
LOADING


Time Taken
Secrets Found
Location
Kills
Ammo Used
Hits
Saves Performed
Distance Travelled
Health Packs Used

Release Version 1.0

None
Finish
BEST TIMES
No Times Set
N/A
Current Position
Final Statistics
of
Story so far...
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare

END:



PC_STRINGS:

Detail Levels
Demo Mode
Sound
Controls
Gamma
Set Volumes
User Keys

The file could not be saved!
Try Again?
YES
NO
Save Complete!
No save games!
None valid
Save Game?
- EMPTY SLOT -



OFF
ON
Setup Sound Card
Default Keys
DOZY

spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare

END:


PSX_STRINGS:

Screen Adjust
DEMO MODE
Sound
Controls
Gamma
Set Volumes
Control Method
The file could not be saved!
Try Again?
YES
NO
Save Complete!
No save games!
None valid
Save Game?
- EMPTY SLOT -
Press any button to quit
Use directional buttons
to adjust screen position
> Select
; Go Back
> Continue
Paused
Controller Removed
Save game to
Overwrite game on
Load game from
the Memory card in
Memory card slot 1
Are you sure ?
Yes
No
is full
There are no
Tomb Raider II games on
There is a problem with
There is no Memory card in
is unformatted
Would you like to
format it ?
Saving game to
Loading game from
Formatting
Overwrite game
Save game
Load game
Format Memory card
Exit
Continue
You will not be able
to save any games unless
you insert a formatted
Memory card with at least
one free block
The Memory card in
Exit without saving
Exit without loading
Start game
UNFORMATTED MEMORY CARD
Insert a formatted
or press > to continue
without saving.

// PAL CODES: ENGLISH= SLES-00718, FRENCH= SLES-0719, GERMAN= SLES-0720 
bu00::BESLES-00718TOMB2   
// NTSC CODES: US= UNKNOWN, JAPAN= UNKNOWN
bu00::BASLUS-00437TOMB2   

spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare
spare

END:

