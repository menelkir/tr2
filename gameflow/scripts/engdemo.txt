Description:	Tomb Raider II Script. Demo 1.0 (c) Core Design Ltd 1997
// Option Types
//	EXIT_TO_TITLE
//	LEVEL%d
//	DEMO%d
//	SEQUENCE%d
//	EXITGAME


//------------------------------------------------------------------------
//		O P T I O N S
//------------------------------------------------------------------------

Options:								// Defaults
Language:				ENGLISH			// ENGLISH
Secret_Track:			0				// 47
//enable_indoor_reverb:					// NO


cypher_code:			166

//firstOption:			LEVEL 1			// EXIT_TO_TITLE
//title_replace:		LEVEL 1			// NONE (-1)
ondeath_ingame:			EXIT_TO_TITLE
//noinput_time:	 		150				// 900
//on_demo_interrupt:	LEVEL 1			// EXIT_TO_TITLE
//on_demo_end:			DEMO1			// EXIT_TO_TITLE
singlelevel:			1				// NONE (-1)
demoversion:			  			
//title_disabled:
cheatmodecheck_disabled:
noinput_timeout:
loadsave_disabled:
//screensizing_disabled:
//lockout_optionring:
//dozy_cheat_enabled:

//select_any_level:
//enable_cheat_key:
//use_security_tag:

end:


//------------------------------------------------------------------------
//		T I T L E   S E T U P
//------------------------------------------------------------------------

Title:
Game:		data\title.tr2		// First file in 'Title' must always be title.tr2
PSXfile:	pixUK\title.raw
PSXfile:	pixUK\legal.raw
PSXfile:	pixUS\titleUS.raw
PSXfile:	pixUS\legalUS.raw
PSXfile:	pixJAP\titleJAP.raw
PSXfile:	pixJAP\legalJAP.raw
Track:		0
end:

//------------------------------------------------------------------------
//		F R O N T E N D
//------------------------------------------------------------------------

Frontend:

//fmv_start:	1
//fmv_end:	864
//fmv:		fmv\logo.rpl									//Logo

//fmv_start:	1
//fmv_end:	4370
//fmv:		fmv\ancient.rpl									//Intro

end:


//------------------------------------------------------------------------
//BOAT		V E N I C E		-	PLAYABLE
//------------------------------------------------------------------------

Level:		Venice

track:		0						//Ambient (Wind)
Load_Pic:	pix\venice.raw
game:		data\playable.tr2					

complete:

// Object Names
Key1:		Boathouse Key    
Key2:		Steel Key
Key3:		Iron Key
end:

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//		D E M O S
//------------------------------------------------------------------------
//------------------------------------------------------------------------

DemoLevel:	Venice

track:		0
Load_Pic:	pix\venice.raw
PCdemo:		data\boat.tr2
PSXdemo:	data\boat.psx

Key1:		Boathouse Key    
Key2:		Steel Key
Key3:		Iron Key
end:


DemoLevel:	Wreck of the Maria Doria

track:		0						//Ambient (Wind?)
Load_Pic:	pix\titan.raw
PCdemo:		data\keel.tr2
PSXdemo:	data\keel.psx

Puzzle1:	Circuit Breaker
Key1:		Rest Room Key
Key2:		Rusty Key
Key3:		Cabin Key
end:




DemoLevel:	Tibetan Foothills

track:		0
Load_Pic:	pix\tibet.raw
StartInv:	PUZZLE4
PCdemo:		data\skidoo.tr2
PSXdemo:	data\skidoo.psx

Puzzle4:	The Seraph
Key1:		Drawbridge Key
Key2:		Hut Key
end:



GameStrings:	strings.txt



