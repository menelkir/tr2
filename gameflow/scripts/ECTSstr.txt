// PC & PSX VERSION OF GENERAL GAME STRINGS

GAME_STRINGS:

INVENTORY
OPTION
ITEMS
GAME OVER

Load Game
Save Game
New Game
Restart Level
Exit to Title
Exit Demo
Exit Game

Select Level
Save Position

Select Detail
High
Medium
Low

Walk
Roll
Run
Left
Right
Back
Step Left
Step Right
Look
Jump
Action
Draw Weapon
Inventory
Flare

Stop Watch
Pistols
Shotgun
Automatic Pistols
Uzis
Harpoon
M16
Grenade Launcher
Flare

Pistol Clips
Shotgun Shells
Automatic Pistol Clips
Uzi Clips
Harpoons
M16 Clips
Grenades

Small Medi Pack
Large Medi Pack
Pickup
Puzzle
Key
Game

Lara's Home
LOADING


TIME TAKEN
SECRETS
PICKUPS
KILLS
of

ECTS Demo 4th Sept '97

END:



PC_STRINGS:

Detail Levels
Demo Mode
Sound
Controls
Gamma
Set Volumes
User Keys

The file could not be saved!
Try Again?
YES
NO
Save Complete!
No save games!
None valid
Save Game?
- EMPTY SLOT -



OFF
ON
Setup Sound Card
Default Keys
DOZY

END:


PSX_STRINGS:

Screen Adjust
DEMO MODE
Sound
Controls
Gamma
Set Volumes
User Keys

The file could not be saved!
Try Again?
YES
NO
Save Complete!
No save games!
None valid
Save Game?
- EMPTY SLOT -

Press any button to exit
Use directional buttons
to adjust screen position
> Select
\ Go Back
> Continue
Paused
No Controller

Save game to
Overwrite game on
Load game from
the memory card in
memory card slot 1
Are you sure ?
Yes
No
is full
There are no games on
There is a problem with
There is no memory card in
is unformatted
Would you like to
format it ?
Saving game to
Loading game from
Formatting
Overwrite game
Save game
Load game
Format memory card
Exit
Continue
You will not be able
to save any games unless
you insert a formatted
memory card with at least
one free block
The memory card in
Exit without saving
Exit without loading
Start game
// PAL CODES: ENGLISH= SLES-00718, FRENCH= SLES-0719, GERMAN= SLES-0720 
bu00::BESLES-00718TOMBRAID
// NTSC CODES: US= UNKNOWN, JAPAN= UNKNOWN
bu00::BASLUS-00152TOMBRAID

END:

