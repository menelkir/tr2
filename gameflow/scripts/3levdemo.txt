Description:	Tomb Raider II Script. 3 Level Demo Version 1.0 (c) Core Design Ltd 1997
// Option Types
//	EXIT_TO_TITLE
//	LEVEL%d
//	DEMO%d
//	SEQUENCE%d
//	EXITGAME


//------------------------------------------------------------------------
//		O P T I O N S
//------------------------------------------------------------------------

Options:								// Defaults
Language:				ENGLISH			// ENGLISH
Secret_Track:			47				// 47
//enable_indoor_reverb:					// NO


cypher_code:			166

firstOption:			DEMO			// EXIT_TO_TITLE
title_replace:			DEMO			// NONE (-1)
ondeath_demo_mode:		DEMO			// EXIT_TO_TITLE
ondeath_ingame:			LEVEL 1			// EXIT_TO_TITLE
noinput_timeout:
//noinput_time:	 		150				// 900
on_demo_interrupt:		LEVEL 1			// EXIT_TO_TITLE
on_demo_end:			DEMO			// EXIT_TO_TITLE
singlelevel:			1				// NONE (-1)
demoversion:			  			
title_disabled:
cheatmodecheck_disabled:
loadsave_disabled:
screensizing_disabled:
lockout_optionring:
//dozy_cheat_enabled:

//select_any_level:
//enable_cheat_key:
//use_security_tag:

end:


//------------------------------------------------------------------------
//		T I T L E   S E T U P
//------------------------------------------------------------------------

Title:
Game:		data\title.tr2		// First file in 'Title' must always be title.tr2
PSXfile:	pixUK\title.raw
PSXfile:	pixUK\legal.raw
PSXfile:	pixUS\titleUS.raw
PSXfile:	pixUS\legalUS.raw
PSXfile:	pixJAP\titleJAP.raw
PSXfile:	pixJAP\legalJAP.raw
Track:		64
end:

//------------------------------------------------------------------------
//		F R O N T E N D
//------------------------------------------------------------------------

Frontend:
end:

//------------------------------------------------------------------------
//WALL		T H E   G R E A T   W A L L
//------------------------------------------------------------------------

Level:		Venice

track:		0						//Ambient (Wind)
Load_Pic:	demo\title.raw
StartInv:	SHOTGUN
StartInv:	MEDI
StartInv:	BIGMEDI
game:		demo\playable.tr2					

gamecomplete:

// Object Names
Key1:		Boathouse Key    
Key2:		Steel Key
Key3:		Iron Key
end:


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//		D E M O S
//------------------------------------------------------------------------
//------------------------------------------------------------------------

DemoLevel:	Venice

track:		0
Load_Pic:	demo\title.raw
PCdemo:		demo\boat.tr2
PSXdemo:	demo\boat.psx

Key1:		Boathouse Key    
Key2:		Steel Key
Key3:		Iron Key
end:


DemoLevel:	Wreck of the Maria Doria

track:		32						//Ambient (Wind?)
Load_Pic:	demo\title.raw
PCdemo:		demo\keel.tr2
PSXdemo:	demo\keel.psx

Puzzle1:	Circuit Breaker
Key1:		Rest Room Key
Key2:		Rusty Key
Key3:		Cabin Key
end:




DemoLevel:	Tibetan Foothills

track:		33
Load_Pic:	demo\title.raw
StartInv:	PUZZLE4
PCdemo:		demo\skidoo.tr2
PSXdemo:	demo\skidoo.psx

Puzzle4:	The Seraph
Key1:		Drawbridge Key
Key2:		Hut Key
end:



GameStrings:	strings.txt



